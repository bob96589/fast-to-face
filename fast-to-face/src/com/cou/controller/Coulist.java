package com.cou.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.acc.model.AccService;
import com.acc.model.AccVO;
import com.buy.model.BuyVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.pro.model.ProService;
import com.pro.model.ProVO;

public class Coulist extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
		
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		if ("getCouList".equals(action)) {
				
			
			String pronoStr = req.getParameter("prono").trim();
			ProService proSvc = new ProService();
			CouService couSvc = new CouService();
			
			List<CouVO> couVOList = null;			
			if("all".equals(pronoStr)){			
				Set<CouVO> couVOSet = couSvc.getCousByCoustate(3);
				couVOList = new ArrayList<CouVO>(couVOSet);
				req.setAttribute("couVOList", couVOList);				
			}else if(pronoStr.startsWith("20")){
				Integer prono = new Integer(pronoStr);				
				Set<CouVO> couVOSet = proSvc.getCousByProno(prono);
				couVOList = new ArrayList<CouVO>(couVOSet);
				req.setAttribute("couVOList", couVOList);
			}else{
				 couVOList = couSvc.getIntroKeyword(pronoStr);
				 req.setAttribute("couVOList", couVOList);
			}
			
			
			//has the course bought
			HttpSession session = req.getSession();
			MemVO memVO = (MemVO)session.getAttribute("memVO");
			if(memVO != null){
				MemService memSvc = new MemService();
				Set<BuyVO> buyVOSet = memSvc.getBuysByMemno(memVO.getMemno());
				
				List<Boolean> boughtYetList = new ArrayList<Boolean>();
				for(CouVO couVO : couVOList){
					Boolean boughtYet = false;
					for(BuyVO buyVO : buyVOSet){
						if(couVO.getCouno().equals(buyVO.getCouno()))
							boughtYet = true;
					}
					boughtYetList.add(boughtYet);
				}
				req.setAttribute("boughtYetList", boughtYetList);
			}
			
			
			
			//appscore
			List<Double> averageAppscoreList = new ArrayList<Double>();
			for(CouVO couVO : couVOList){
				Double averageAppscore = couSvc.getAverageAppscoreByCouno(couVO.getCouno());
				averageAppscoreList.add(averageAppscore);
			}
			
			//course count
			List<Integer> couCountList = couSvc.getCouCountListGroupByProno();
			
			Integer couCountSum = 0;
			for(Integer couCount : couCountList){
				couCountSum += couCount;				
			}
			
			List<ProVO> proVOList = proSvc.getAll();
			req.setAttribute("proVOList", proVOList);			
			req.setAttribute("prono", pronoStr);			
			req.setAttribute("averageAppscoreList", averageAppscoreList);			
			req.setAttribute("couCountList", couCountList);			
			req.setAttribute("couCountSum", couCountSum);			
			
			String url = "/front/course_list/course_list.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);
			successView.forward(req, res);
			
		}		
		
		if ("getMyCouList".equals(action) || "getMyBadge".equals(action)) {
			
			HttpSession session= req.getSession();
			MemVO memVO = (MemVO)session.getAttribute("memVO");
			
			MemService memSvc = new MemService();
			Set<BuyVO> buyVOSet = memSvc.getBuysByMemno(memVO.getMemno());
			
			//remove double purchased course
			List<CouVO> couVOList = getSinglePurchasedCou(buyVOSet);			
			List<Integer> completePercentageList = getCompletePercentageList(memVO, couVOList);		
			
			req.setAttribute("couVOList", couVOList);
			req.setAttribute("completePercentageList", completePercentageList);
			
			String url = null;
			if("getMyCouList".equals(action))
				url = "/front/my_course/myCourse.jsp";
			else
				url = "/front/my_badge/myBadge.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);
			successView.forward(req, res);		
		}
		
		//hsu
		if("Search_Keyword".equals(action)){  
	    	   
 	       req.setAttribute("temp", "Search_Keyword");
 	       List<String> errorMsgs = new LinkedList<String>();				 
 	       req.setAttribute("errorMsgs", errorMsgs);
 		/***************************1.接收請求參數****************************************/
 				String couintro = req.getParameter("couintro").trim();
			    if (couintro == null || (couintro.trim()).length() == 0){
			    	errorMsgs.add("請輸入您的關鍵字");	
				}
			    
			    CouService couSvc2 = new CouService();
			    List<CouVO> couVOList = couSvc2.getIntroKeyword(couintro);
				MemService memSvc = new MemService();
				List<MemVO> memVOlist = memSvc.getAll();
				
				List<Integer> couCountList = couSvc2.getCouCountListGroupByProno();
				
				Integer couCountSum = 0;
				for(Integer couCount : couCountList){
					couCountSum += couCount;				
				}
 			    				  
 	   /***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("couCountSum", couCountSum); 
 				req.setAttribute("memVOlist", memVOlist); 
 				req.setAttribute("couVOList", couVOList);
 				String url = "/front/course_list/course_list.jsp";
 				RequestDispatcher successView = req.getRequestDispatcher(url);
 				successView.forward(req, res);
	
 		}

			
	}
	
	private List<CouVO> getSinglePurchasedCou(Set<BuyVO> buyVOSet){		
		CouService couSvc = new CouService();		
		List<CouVO> couVOList = new ArrayList<CouVO>();
		for(BuyVO buyVO : buyVOSet){				
			Integer couno = buyVO.getCouno();
			boolean isExist = false;
			for(CouVO couVO : couVOList){
				if(couno.equals(couVO.getCouno()))
					isExist = true;
			}				
			if(!isExist){
				CouVO couVO = couSvc.getOneCou(couno);
				couVOList.add(couVO);					
			}								
		}		
		return couVOList;		
	}
	
	private List<Integer> getCompletePercentageList(MemVO memVO, List<CouVO> couVOList){
		CouService couSvc = new CouService();
		List<Integer> completePercentageList = new ArrayList<Integer>();
		for(CouVO couVO : couVOList){
			List<Integer> isfinishList = couSvc.getIsfinishByMemnoCouno(memVO.getMemno(), couVO.getCouno());
			Integer completed = 0;
			
			for(Integer isfinish : isfinishList){
				completed += isfinish;					
			}
			Integer completePercentage = completed * 100 / isfinishList.size();
			completePercentageList.add(completePercentage);				
		}
		return completePercentageList;
	}
	
}
