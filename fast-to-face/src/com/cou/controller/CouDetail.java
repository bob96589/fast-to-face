package com.cou.controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.acc.model.AccService;
import com.acc.model.AccVO;
import com.app.model.AppService;
import com.app.model.AppVO;
import com.buy.model.BuyService;
import com.buy.model.BuyVO;
import com.content.model.ConVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.fin.model.FinService;
import com.fin.model.FinVO;
import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.sto.model.StoVO;
import com.unit.model.UnitService;
import com.unit.model.UnitVO;

public class CouDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		if ("getCouDetail".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			HttpSession session = req.getSession();

			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				
				Integer couno = new Integer(req.getParameter("couno"));					
				MemVO memVO = (MemVO)session.getAttribute("memVO");				
				String identity = getUserIdentity(memVO, couno);		
				
				/***************************2.開始查詢資料*****************************************/
				
				CouService couSvc = new CouService();
				List<Object> wholeCourseList = couSvc.getWholeCouByCounoWithoutAss(couno);					
				Double averageAppscore = couSvc.getAverageAppscoreByCouno(couno);
				Integer appraiseCount = couSvc.getAppraiseCountByCouno(couno);
				
				AccService accSvc = new AccService();
				AccVO accVO = accSvc.getOneAcc(((CouVO)wholeCourseList.get(0)).getAccno());
				
				
				if(memVO != null){
					Set<FinVO> finVOSet = couSvc.getFinVOSetByMemnoCouno(memVO.getMemno(), couno);
					req.setAttribute("finVOSet", finVOSet); 					
				}
				
				/***************************3.查詢完成,準備轉交(Send the Success view)*************/
				
				req.setAttribute("identity", identity); 				
				req.setAttribute("averageAppscore", averageAppscore); 				
				req.setAttribute("appraiseCount", appraiseCount); 				
				req.setAttribute("accVO", accVO);
				
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 				
				
				String url = "/front/course_detail/couDetail.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/unit/select_page.jsp");
				failureView.forward(req, res);
			}
		}	
	}
	
	
	private String getUserIdentity(MemVO memVO, Integer couno){		
		if (memVO != null) {
			//member or buy member
			MemService memSvc = new MemService();
			Set<BuyVO> buyVOSet = memSvc.getBuysByMemno(memVO.getMemno());
			for (BuyVO buyVO : buyVOSet) {
				if (buyVO.getCouno().equals(couno)) {
					//member or give appraise member
					AppService appSvc = new AppService();
					AppVO appVO = appSvc.getOneApp(memVO.getMemno(), couno);
					if(appVO != null)
						return "appraised";
					return "purchased";
				}
			}		
		}	
		return "unpurchased";		
	}

}
