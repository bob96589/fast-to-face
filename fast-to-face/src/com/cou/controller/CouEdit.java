package com.cou.controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.acc.model.AccService;
import com.acc.model.AccVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.pro.model.ProService;
import com.pro.model.ProVO;

public class CouEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);		
	}

	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		if ("getCouListByAccno".equals(action)) {
			
			
			HttpSession session = req.getSession();
			AccVO accVO = (AccVO)session.getAttribute("accVO");
			
			AccService accSvc = new AccService();
			Set<CouVO> couVOSet = accSvc.getCousByAccno(accVO.getAccno());

			ProService proSvc = new ProService();
			List<ProVO> proList = proSvc.getAll();
			
			req.setAttribute("couVOSet", couVOSet);
			req.setAttribute("proList", proList);
			
			String url = "/back/course_editing/listCouByAccno.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);
			successView.forward(req, res);
			
		}
		
		
		if ("getWholeCouForCourseEditing".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				
				Integer couno = new Integer(req.getParameter("couno"));				
				
				//跳頁準備
				CouService couSvc = new CouService();								
				
				Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
				req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
				req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
				req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 				
				
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3)); 				
				
				String url = "/back/course_editing/editCou.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/unit/select_page.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("updateCoustateToOne".equals(action)) { 
			
			/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
			
			Integer couno = new Integer(req.getParameter("couno"));			
			
			/***************************2.開始修改資料*****************************************/
			
			CouService couSvc = new CouService();
			couSvc.updateCoustateToOne(couno);
			
			/***************************3.修改完成,準備轉交(Send the Success view)*************/			
							
			String url = "/couEdit/cou.do?action=getCouListByAccno";
			RequestDispatcher successView = req.getRequestDispatcher(url); 
			successView.forward(req, res);
			
			/***************************其他可能的錯誤處理*************************************/
			
		}
		
		
		
		
		
		
		
		
		
	}

}
