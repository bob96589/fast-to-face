package com.cou.controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.acc.model.AccService;
import com.acc.model.AccVO;
import com.app.model.AppService;
import com.app.model.AppVO;
import com.buy.model.BuyService;
import com.buy.model.BuyVO;
import com.content.model.ConVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.fin.model.FinService;
import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.unit.model.UnitService;
import com.unit.model.UnitVO;

public class CouBuying extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		
		
		
		if ("getOneForBuying".equals(action)) { 
			
			
			//reset memVO in session
			HttpSession session = req.getSession();			
			MemVO memVObj = (MemVO)session.getAttribute("memVO");			
			MemService memSvc = new MemService();
			MemVO memVO = memSvc.getOneMem(memVObj.getMemno());
			session.setAttribute("memVO", memVO);
			
		
			try {
				/***************************1.接收請求參數****************************************/
				
				Integer couno = new Integer(req.getParameter("couno"));			
				
				/***************************2.開始查詢資料****************************************/
				//CouVO
				CouService couSvc = new CouService();
				CouVO couVO = couSvc.getOneCou(couno);				
				
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("couVO", couVO);						
				String url = "/front/course_detail/couBuying.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

				/***************************其他可能的錯誤處理************************************/
			} catch (Exception e) {				
				RequestDispatcher failureView = req.getRequestDispatcher("/front/course_detail/couDetail.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("buying".equals(action)) { 		
			
			String requestURL = req.getParameter("requestURL");	
			
			HttpSession session = req.getSession();			
			MemVO memVO = (MemVO)session.getAttribute("memVO");			
			
			try {
				/***************************1.接收請求參數****************************************/
				Integer couno = new Integer(req.getParameter("couno"));				
				
				/***************************2.開始查詢資料****************************************/
				//CouVO
				CouService couSvc = new CouService();
				CouVO couVO = couSvc.getOneCou(couno);
				
				//pay
				MemService memSvc = new MemService();
				memSvc.updateMempointByMemno(memVO.getMempoint() - couVO.getCoudiscount() ,memVO.getMemno());					
				//add buy record
				BuyService buySvc = new BuyService();
				buySvc.addBuy(couVO.getCoudiscount(), memVO.getMemno(), couno);					
				//add finish record
				FinService finSvc = new FinService();
				Set<UnitVO> unitVOSet = couSvc.getUnitsByCouno(couno);
				for(UnitVO unitVO : unitVOSet){
					UnitService unitSvc = new UnitService();
					Set<ConVO> conVOSet = unitSvc.getConsByUnitno(unitVO.getUnitno());
					for(ConVO conVO : conVOSet){
						finSvc.addFin(memVO.getMemno(), conVO.getConno());
					}					
				}				
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				
				//reset memVO in session
				memVO = memSvc.getOneMem(memVO.getMemno());
				session.setAttribute("memVO", memVO);
				
				req.setAttribute("couVO", couVO);						
				String url = "/coulist/cou.do?action=getMyCouList";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				

				/***************************其他可能的錯誤處理************************************/
			} catch (Exception e) {
				RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
				failureView.forward(req, res);
			}
		}
		
		
		if ("insert".equals(action)) { 			
			
			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
				
				//couno
				Integer	couno = new Integer(req.getParameter("couno").trim());
				
				//memno
				Integer	memno = new Integer(req.getParameter("memno").trim());
				
				//appscore
				Double	appscore = new Double(req.getParameter("appscore").trim());				
				System.out.println("couno"+couno);
				System.out.println("memno"+memno);			
							
				/***************************2.開始新增資料***************************************/
				
				AppService appSvc = new AppService();
				AppVO appVO = appSvc.addApp(memno, couno, appscore);				
				
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				
				
				String url = "/couDetail/cou.do?" + req.getParameter("reURL");
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				
			} catch (Exception e) {				
				String url = "/couDetail/cou.do?" + req.getParameter("reURL");
				RequestDispatcher failureView = req.getRequestDispatcher(url);
				failureView.forward(req, res);
			}		

		}
				
	}
	
	
	private String getUserIdentity(MemVO memVO, Integer couno){		
		if (memVO != null) {
			//member or buy member
			MemService memSvc = new MemService();
			Set<BuyVO> buyVOSet = memSvc.getBuysByMemno(memVO.getMemno());
			for (BuyVO buyVO : buyVOSet) {
				if (buyVO.getCouno().equals(couno)) {
					//member or give appraise member
					AppService appSvc = new AppService();
					AppVO appVO = appSvc.getOneApp(memVO.getMemno(), couno);
					if(appVO != null)
						return "appraised";
					return "purchased";
				}
			}		
		}	
		return "unpurchased";		
	}

}
