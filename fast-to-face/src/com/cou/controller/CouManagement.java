package com.cou.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.acc.model.AccService;
import com.acc.model.AccVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.oreilly.servlet.MultipartRequest;
import com.pro.model.ProService;
import com.pro.model.ProVO;

public class CouManagement extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		doPost(req, res);
		
		
		
		
	}

	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		
		
		String contentType = req.getContentType();
		String action = null;
		MultipartRequest multi = null;
		if(contentType!=null && contentType.startsWith("multipart/form-data")){
			multi = new MultipartRequest(req, getServletContext().getRealPath("/shared/temp"), 5*1024*1024, "UTF-8");			
			action = multi.getParameter("action");				
		}else{
			req.setCharacterEncoding("UTF-8");
			action = req.getParameter("action");			
		}
		
		
		if ("getCouList".equals(action)) { 
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");					
			String whichPage = req.getParameter("whichPage");					
			
			try {				
				
				CouService couSvc = new CouService();
				List<CouVO> couVOlist = couSvc.getAll();
				
				ProService proSvc = new ProService();
				List<ProVO> proVOList = proSvc.getAll();
				
				AccService accSvc = new AccService();
				List<AccVO> accVOList = accSvc.getAll();			
								
				req.setAttribute("couVOlist", couVOlist);						
				req.setAttribute("proVOList", proVOList);						
				req.setAttribute("accVOList", accVOList);						
				req.setAttribute("requestURL", requestURL);						
				req.setAttribute("whichPage", whichPage);						
				String url = "/back/course_management/listAllCou.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				

				/***************************其他可能的錯誤處理************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料取出時失敗:"+e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
				failureView.forward(req, res);
			}
		}
		
		
		if ("insert".equals(action)) { 				
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
				
				//couname
				String couname = multi.getParameter("couname");
				if (couname == null || (couname.trim()).length() == 0) {
					errorMsgs.add("請輸入課程名稱");
				}					
				
				//couintro
				String couintro = multi.getParameter("couintro");
				if (couintro == null || (couintro.trim()).length() == 0) {
					errorMsgs.add("請輸入課程簡介");
				}
									
				//coupic
				File coupic = multi.getFile("coupic");
				byte[] picBuffer = null;
				if(coupic != null){						
					InputStream in = new FileInputStream(coupic);
					picBuffer = new byte[in.available()];
					in.read(picBuffer);						
					coupic.delete();
					in.close();
				}else{					
					errorMsgs.add("請選擇圖檔");						
				}
				
				//coubadge
				File coubadge = multi.getFile("coubadge");
				byte[] badgeBuffer = null;
				if(coubadge != null){						
					InputStream in = new FileInputStream(coubadge);
					badgeBuffer = new byte[in.available()];
					in.read(badgeBuffer);
					coubadge.delete();
					in.close();
				}else{					
					errorMsgs.add("請選擇圖檔");						
				}
				
				
				//couprice					
				Integer couprice = null;
				try {
					couprice = new Integer(multi.getParameter("couprice").trim());
				} catch (NumberFormatException e) {
					couprice = 0;
					errorMsgs.add("購買點數請填數字");
				}
				
				//coudiscount					
				Integer coudiscount = null;
				try {
					coudiscount = new Integer(multi.getParameter("coudiscount").trim());
				} catch (NumberFormatException e) {
					coudiscount = 0;
					errorMsgs.add("折扣點數請填數字");
				}
				
				//accno
				Integer accno = new Integer(multi.getParameter("accno").trim());
				
				//prono
				Integer prono = null;
				if("".equals(multi.getParameter("prono"))){//選擇未分類
					prono = null;						
				}else{						
					prono = new Integer(multi.getParameter("prono").trim());
				}
				
				
													
				CouVO couVO = new CouVO();
				couVO.setCouname(couname);
				couVO.setCouintro(couintro);
				couVO.setCoupic(picBuffer);
				couVO.setCoubadge(badgeBuffer);
				couVO.setCouprice(couprice);
				couVO.setCoudiscount(coudiscount);		
				couVO.setAccno(accno);		
				couVO.setProno(prono);		

				if (!errorMsgs.isEmpty()) {
					req.setAttribute("couVO", couVO); 
					RequestDispatcher failureView = req.getRequestDispatcher("/back/course_management/addCou.jsp");
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				CouService couSvc = new CouService();
				couVO = couSvc.addCou(couname, couintro, picBuffer, badgeBuffer, couprice, coudiscount, accno, prono);
				
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				
				
				List<CouVO> couVOlist = couSvc.getAll();
				
				ProService proSvc = new ProService();
				List<ProVO> proVOList = proSvc.getAll();
				
				AccService accSvc = new AccService();
				List<AccVO> accVOList = accSvc.getAll();			
								
				req.setAttribute("couVOlist", couVOlist);						
				req.setAttribute("proVOList", proVOList);						
				req.setAttribute("accVOList", accVOList);				
				req.setAttribute("couVO", couVO);				
				
				String url = "/back/course_management/listAllCou.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
				successView.forward(req, res);				
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/back/course_management/addCou.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		
		if ("getOne_For_Update".equals(action)) { 
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");					
			String whichPage = req.getParameter("whichPage");					
			
			try {
				/***************************1.接收請求參數****************************************/
				Integer couno = new Integer(req.getParameter("couno"));
				
				/***************************2.開始查詢資料****************************************/
				CouService couSvc = new CouService();
				CouVO couVO = couSvc.getOneCou(couno);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("couVO", couVO);						
				req.setAttribute("requestURL", requestURL);						
				req.setAttribute("whichPage", whichPage);						
				String url = "/back/course_management/update_cou_input.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				

				/***************************其他可能的錯誤處理************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料取出時失敗:"+e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
				failureView.forward(req, res);
			}
		}
		
		
		if ("update".equals(action)) { 
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = multi.getParameter("requestURL");
			String whichPage = multi.getParameter("whichPage");
		
//			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				
				//couno
				Integer couno = new Integer(multi.getParameter("couno").trim());
				
				//couname
				String couname = multi.getParameter("couname");
				if (couname == null || (couname.trim()).length() == 0) {
					errorMsgs.add("請輸入課程名稱");
				}					
				
				//couintro
				String couintro = multi.getParameter("couintro");
				if (couintro == null || (couintro.trim()).length() == 0) {
					errorMsgs.add("請輸入課程簡介");
				}
									
				//coupic
				File coupic = multi.getFile("coupic");
				byte[] picBuffer = null;
				if(coupic != null){					
					InputStream in = new FileInputStream(coupic);
					picBuffer = new byte[in.available()];
					in.read(picBuffer);						
					coupic.delete();
					in.close();
				}else{					
					//read propic from DB
					CouService couSvc = new CouService();
					CouVO couVO = couSvc.getOneCou(couno);
					picBuffer = couVO.getCoupic();							
				}
				
				//coubadge
				File coubadge = multi.getFile("coubadge");
				byte[] badgeBuffer = null;
				if(coubadge != null){					
					InputStream in = new FileInputStream(coubadge);
					badgeBuffer = new byte[in.available()];
					in.read(badgeBuffer);
					coubadge.delete();
					in.close();
				}else{					
					//read propic from DB
					CouService couSvc = new CouService();
					CouVO couVO = couSvc.getOneCou(couno);
					badgeBuffer = couVO.getCoubadge();						
				}
				
				
				//couprice					
				Integer couprice = null;
				try {
					couprice = new Integer(multi.getParameter("couprice").trim());
				} catch (NumberFormatException e) {
					couprice = 0;
					errorMsgs.add("購買點數請填數字");
				}
				
				//coudiscount					
				Integer coudiscount = null;
				try {
					coudiscount = new Integer(multi.getParameter("coudiscount").trim());
				} catch (NumberFormatException e) {
					coudiscount = 0;
					errorMsgs.add("折扣點數請填數字");
				}
				
				//accno
				Integer accno = new Integer(multi.getParameter("accno").trim());
				
				//coustate
				Integer coustate = new Integer(multi.getParameter("coustate").trim());
				
				//prono
				Integer prono = null;
				if("".equals(multi.getParameter("prono"))){//選擇未分類
					prono = null;						
				}else{						
					prono = new Integer(multi.getParameter("prono").trim());
				}
				
				

				CouVO couVO = new CouVO();
				couVO.setCouno(couno);
				couVO.setCouname(couname);
				couVO.setCouintro(couintro);
				couVO.setCoupic(picBuffer);
				couVO.setCoubadge(badgeBuffer);
				couVO.setCouprice(couprice);
				couVO.setCoudiscount(coudiscount);		
				couVO.setAccno(accno);		
				couVO.setCoustate(coustate);		
				couVO.setProno(prono);		

				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("couVO", couVO);
					req.setAttribute("requestURL", requestURL);
					req.setAttribute("whichPage", whichPage);
					RequestDispatcher failureView = req.getRequestDispatcher("/back/course_management/update_cou_input.jsp");
					failureView.forward(req, res);
					return; //程式中斷
				}
				
				/***************************2.開始修改資料*****************************************/
				CouService couSvc = new CouService();
				couVO = couSvc.updateCou(couno, couname, couintro, picBuffer, badgeBuffer, couprice, coudiscount, accno, coustate, prono);
				
				/***************************3.修改完成,準備轉交(Send the Success view)*************/			
				
				List<CouVO> couVOlist = couSvc.getAll();
				
				ProService proSvc = new ProService();
				List<ProVO> proVOList = proSvc.getAll();
				
				AccService accSvc = new AccService();
				List<AccVO> accVOList = accSvc.getAll();			
								
				req.setAttribute("couVOlist", couVOlist);						
				req.setAttribute("proVOList", proVOList);						
				req.setAttribute("accVOList", accVOList);				
				
				String url = "/back/course_management/listAllCou.jsp" + "?whichPage=" + whichPage;		
				
				
				req.setAttribute("couVO", couVO);
				
				RequestDispatcher successView = req.getRequestDispatcher(url); 
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
//			} catch (Exception e) {
//				errorMsgs.add("修改資料失敗:"+e.getMessage());
//				RequestDispatcher failureView = req.getRequestDispatcher("/back/course_management/update_cou_input.jsp");
//				failureView.forward(req, res);
//			}
		}
		
		
		
		if ("delete".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");
			
			try {
				/***************************1.接收請求參數***************************************/
				Integer couno = new Integer(req.getParameter("couno"));
				
				/***************************2.開始刪除資料***************************************/
				CouService couSvc = new CouService();
				couSvc.deleteCou(couno);
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/
				
				List<CouVO> couVOlist = couSvc.getAll();
				
				ProService proSvc = new ProService();
				List<ProVO> proVOList = proSvc.getAll();
				
				AccService accSvc = new AccService();
				List<AccVO> accVOList = accSvc.getAll();			
								
				req.setAttribute("couVOlist", couVOlist);						
				req.setAttribute("proVOList", proVOList);						
				req.setAttribute("accVOList", accVOList);				
				
				
				
				
				
				
				String url = requestURL;
				RequestDispatcher successView = req.getRequestDispatcher(url); // 刪除成功後,轉交回送出刪除的來源網頁
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("刪除資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
				failureView.forward(req, res);
			}
		}
		
		
		if ("getWholeCouForCourseManagement".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				
				Integer couno = new Integer(req.getParameter("couno"));				
				
				//跳頁準備
				CouService couSvc = new CouService();								
				
				Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
				req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
				req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
				req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 				
				
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3)); 				
				
				String url = "/back/course_management/couDetail.jsp";					
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/unit/select_page.jsp");
				failureView.forward(req, res);
			}
		}		
		
		
		
	}

}
