package com.cou.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.acc.model.AccService;
import com.acc.model.AccVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.pro.model.ProService;
import com.pro.model.ProVO;

public class HotCouManagement extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
	
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		if ("getCouList".equals(action)) {		
					
			CouService couSvc = new CouService(); 
			Set<CouVO> couVOSet = couSvc.getCousByCoustate(3);
		 	
			ProService proSvc = new ProService();
			List<ProVO> proVOList = proSvc.getAll();
			
			AccService accSvc = new AccService();
			List<AccVO> accVOList = accSvc.getAll();
		
			req.setAttribute("couVOSet", couVOSet);			
			req.setAttribute("proVOList", proVOList);			
			req.setAttribute("accVOList", accVOList);			
			
			String url = "/back/hot_course_management/listAllOnlineCou.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);
			successView.forward(req, res);
			
		}
		
		
		
		if ("update_hot".equals(action)) { 
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");			
		
			try {
				
				CouService couSvc = new CouService();
				Enumeration en = req.getParameterNames();
				while(en.hasMoreElements()){
					String name = (String)en.nextElement();					
					if (name.startsWith("10")) {//排除非學程資料的參數ex:action, requestURL, whichPage
						String value = req.getParameter(name);//資料一定正確，不做錯誤處理
						couSvc.updateHot(Integer.parseInt(name), Integer.parseInt(value));//修改資料
					}	
				}
				
				
				Set<CouVO> couVOSet = couSvc.getCousByCoustate(3);
			 	
				ProService proSvc = new ProService();
				List<ProVO> proVOList = proSvc.getAll();
				
				AccService accSvc = new AccService();
				List<AccVO> accVOList = accSvc.getAll();
			
				req.setAttribute("couVOSet", couVOSet);			
				req.setAttribute("proVOList", proVOList);			
				req.setAttribute("accVOList", accVOList);	
				
								
				String url = requestURL;
				RequestDispatcher successView = req.getRequestDispatcher(url); 
				successView.forward(req, res);

			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
				failureView.forward(req, res);
			}
		}
	
	}

}
