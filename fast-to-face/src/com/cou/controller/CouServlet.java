package com.cou.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.acc.model.AccService;
import com.acc.model.AccVO;
import com.app.model.AppService;
import com.app.model.AppVO;
import com.buy.model.BuyService;
import com.buy.model.BuyVO;
import com.content.model.ConVO;
import com.cou.model.*;
import com.fin.model.FinService;
import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.oreilly.servlet.MultipartRequest;
import com.pro.model.*;
import com.sto.model.StoService;
import com.sto.model.StoVO;
import com.unit.model.UnitService;
import com.unit.model.UnitVO;

public class CouServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");		
		
		if ("getCoupic".equals(action)) { 			
			
				/***************************1.钡Μ叫―把计****************************************/
				Integer couno = new Integer(req.getParameter("couno"));
				
				/***************************2.秨﹍琩高戈****************************************/
				CouService couSvc = new CouService();
				CouVO couVO = couSvc.getOneCou(couno);				
				byte[] coupic = couVO.getCoupic();				
				res.setContentType("image/png");
				ServletOutputStream out = res.getOutputStream();
				out.write(coupic);
				out.flush();
		}
		if ("getCoubadge".equals(action)) { 			
			
			/***************************1.钡Μ叫―把计****************************************/
			Integer couno = new Integer(req.getParameter("couno"));
			
			/***************************2.秨﹍琩高戈****************************************/
			CouService couSvc = new CouService();
			CouVO couVO = couSvc.getOneCou(couno);				
			byte[] coubadge = couVO.getCoubadge();				
			res.setContentType("image/png");
			ServletOutputStream out = res.getOutputStream();
			out.write(coubadge);
			out.flush();
		}
		
		if ("getAccpic".equals(action)) { 			
			
			/***************************1.钡Μ叫―把计****************************************/
			Integer accno = new Integer(req.getParameter("accno"));
			
			/***************************2.秨﹍琩高戈****************************************/
			AccService accSvc = new AccService();
			AccVO accVO = accSvc.getOneAcc(accno);				
			byte[] accpic = accVO.getAccpic();				
			res.setContentType("image/*");
			ServletOutputStream out = res.getOutputStream();
			out.write(accpic);
			out.flush();
		}	
	}
}
