package com.cou.model;

import java.sql.Timestamp;

public class CouVO  implements java.io.Serializable {
	private Integer couno;
	private String couname;
	private String couintro;
	private byte[] coupic;
	private byte[] coubadge;
	private Integer couprice;
	private Integer coudiscount;
	private Integer couishot;
	private Integer accno;
	private Integer coustate;
	private Integer prono;
	private Timestamp couupdate;
	
	public Integer getCouno() {
		return couno;
	}
	public void setCouno(Integer couno) {
		this.couno = couno;
	}
	public String getCouname() {
		return couname;
	}
	public void setCouname(String couname) {
		this.couname = couname;
	}
	public String getCouintro() {
		return couintro;
	}
	public void setCouintro(String couintro) {
		this.couintro = couintro;
	}
	public byte[] getCoupic() {
		return coupic;
	}
	public void setCoupic(byte[] coupic) {
		this.coupic = coupic;
	}
	public byte[] getCoubadge() {
		return coubadge;
	}
	public void setCoubadge(byte[] coubadge) {
		this.coubadge = coubadge;
	}
	public Integer getCouprice() {
		return couprice;
	}
	public void setCouprice(Integer couprice) {
		this.couprice = couprice;
	}
	public Integer getCoudiscount() {
		return coudiscount;
	}
	public void setCoudiscount(Integer coudiscount) {
		this.coudiscount = coudiscount;
	}
	public Integer getCouishot() {
		return couishot;
	}
	public void setCouishot(Integer couishot) {
		this.couishot = couishot;
	}
	public Integer getAccno() {
		return accno;
	}
	public void setAccno(Integer accno) {
		this.accno = accno;
	}
	public Integer getCoustate() {
		return coustate;
	}
	public void setCoustate(Integer coustate) {
		this.coustate = coustate;
	}
	public Integer getProno() {
		return prono;
	}
	public void setProno(Integer prono) {
		this.prono = prono;
	}
	public Timestamp getCouupdate() {
		return couupdate;
	}
	public void setCouupdate(Timestamp couupdate) {
		this.couupdate = couupdate;
	}
	
	
	
	
	
	
	
	

}
