package com.cou.model;

import java.util.List;
import java.util.Set;

import com.fin.model.FinVO;
import com.unit.model.UnitVO;

public interface CouDAO_interface {
	public Integer insert(CouVO couVO);
    public void update(CouVO couVO);
    public void delete(Integer couno);
    public CouVO findByPrimaryKey(Integer couno);
    public List<CouVO> getAll();
    
    public void updateHot(Integer couno, Integer hot);
    public void updateCoustateToOne(Integer couno);
    public Set<CouVO> getCousByCoustate(Integer coustate);
    public Set<UnitVO> getUnitsByCouno(Integer couno);
    public Integer getMaxUnitorderByCouno(Integer couno);
    public Integer getUnitnoByCounoUnitorder(Integer couno, Integer unitorder);
    public Set<CouVO> getHotCous();
    public Double getAverageAppscoreByCouno(Integer couno);
    public Integer getAppraiseCountByCouno(Integer couno);
    public List<Integer> getCouCountListGroupByProno();
    public List<Integer> getIsfinishByMemnoCouno(Integer memno, Integer couno);
    public Set<FinVO> getFinVOSetByMemnoCouno(Integer memno, Integer couno);
    public List<CouVO> getIntroKeyword(String couintro);
    public List<CouVO> getAllByCouno();

}
