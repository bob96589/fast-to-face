package com.cou.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.fin.model.FinVO;
import com.unit.model.UnitVO;

public class CouDAO implements CouDAO_interface {
	
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/TestDB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private static final String INSERT_STMT = 
			"INSERT INTO course (couno, couname, couintro, coupic, coubadge, couprice, coudiscount, couishot, accno, coustate, prono, couupdate)"
			+ "VALUES (cou_seq.nextval, ?, ?, ?, ?, ?, ?, 0, ?, 0, ?, sysdate)";
	private static final String GET_ALL_STMT = 
			"SELECT * FROM course where coustate != -1 order by couupdate desc, couno";
	private static final String GET_ONE_STMT = 
			"SELECT * FROM course where couno = ?";
	private static final String DELETE = 
			"UPDATE course set coustate=-1 where couno = ?";
	private static final String UPDATE = 
			"UPDATE course set couname=?,couintro=?,coupic=?,coubadge=?,couprice=?,coudiscount=?,accno=?,coustate=?,prono=? where couno = ?";
	
	private static final String UPDATE_HOT = 
			"UPDATE course set couishot=? where couno = ?";	
	private static final String UPDATE_COUSTATE_TO_ONE = 
			"UPDATE course set coustate=1 where couno = ?";	
	private static final String GET_UNITS_BY_COUNO = 
			"SELECT * FROM unit where couno = ? and unitorder != -1 order by unitorder";
	private static final String GET_COUS_BY_COUSTATE = 
			"select * from course where coustate = ?";
	private static final String GET_MAX_UNITORDER_BY_COUNO = 
			"select max(unitorder) from unit where couno = ?";
	private static final String GET_UNITNO_BY_COUNO_UNITORDER = 
			"select unitno from unit where couno = ? and unitorder = ?";
	private static final String GET_HOTCOUS = 
			"select * from course where couishot = 1 and coustate = 3";
	private static final String GET_AVERAGE_APPSCORE_BY_COUNO = 
			"select round(avg(appscore),2) from appraise where couno = ?";
	private static final String GET_APPRAISE_COUNT_BY_COUNO = 
			"select count(*) from appraise where couno = ?";
	private static final String GET_COU_COUNT_LIST_GROUP_BY_PRONO = 
			"select count(*) from course  where coustate = 3 group by prono order by prono desc";
	private static final String GET_ISFINISH_BY_MEMNO_COUNO = 
			"select isfinish from finishrecord where memno = ? and conno in "
			+ "( select conno from content where unitno in "
			+ "( select unitno from unit where couno = ? ) )";
	private static final String GET_FINS_BY_MEMNO_COUNO = 
			"select * from finishrecord where memno = ? and conno in "
			+ "( select conno from content where unitno in "
			+ "( select unitno from unit where couno = ? ) )";
	private static final String GET_COUINTRO_KEYWORD = 
			 "select * from course where upper(couintro) like ('%'||upper(?)||'%') and coustate != -1  order by couno";
	private static final String GET_ALL_STMT_BYCOUNO = 
			"select * from course where coustate != -1 order by couno";
	
	@Override
	public Integer insert(CouVO couVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		Integer keyno = null;

		try {

			con = ds.getConnection();
			String[] cols = {"couno"};
			pstmt = con.prepareStatement(INSERT_STMT, cols);					
			
			pstmt.setString(1, couVO.getCouname());
			pstmt.setString(2, couVO.getCouintro());
			pstmt.setBytes(3, couVO.getCoupic());
			pstmt.setBytes(4, couVO.getCoubadge());
			pstmt.setInt(5, couVO.getCouprice());			
			pstmt.setInt(6, couVO.getCoudiscount());			
			pstmt.setInt(7, couVO.getAccno());			
			if(couVO.getProno()==null)
				pstmt.setString(8, null);
			else
				pstmt.setInt(8, couVO.getProno());			

			pstmt.executeUpdate();
			
			ResultSet rsKeys =pstmt.getGeneratedKeys();
			if(rsKeys.next()){
				keyno = rsKeys.getInt(1);				
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return keyno;

	}

	@Override
	public void update(CouVO couVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);			
			
			pstmt.setString(1, couVO.getCouname());
			pstmt.setString(2, couVO.getCouintro());
			pstmt.setBytes(3, couVO.getCoupic());
			pstmt.setBytes(4, couVO.getCoubadge());
			pstmt.setInt(5, couVO.getCouprice());			
			pstmt.setInt(6, couVO.getCoudiscount());			
			pstmt.setInt(7, couVO.getAccno());			
			pstmt.setInt(8, couVO.getCoustate());
			if(couVO.getProno()==null)
				pstmt.setString(9, null);
			else
				pstmt.setInt(9, couVO.getProno());	
			pstmt.setInt(10, couVO.getCouno());		

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer couno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, couno);
			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public CouVO findByPrimaryKey(Integer couno) {
		CouVO couVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, couno);

			rs = pstmt.executeQuery();
			
			while (rs.next()) {						
				couVO = new CouVO();
				couVO.setCouno(rs.getInt("couno"));
				couVO.setCouname(rs.getString("couname"));
				couVO.setCouintro(rs.getString("couintro"));
				couVO.setCoupic(rs.getBytes("coupic"));
				couVO.setCoubadge(rs.getBytes("coubadge"));
				couVO.setCouprice(rs.getInt("couprice"));
				couVO.setCoudiscount(rs.getInt("coudiscount"));
				couVO.setCouishot(rs.getInt("couishot"));
				couVO.setAccno(rs.getInt("accno"));
				couVO.setCoustate(rs.getInt("coustate"));
				couVO.setProno(rs.getInt("prono"));
				couVO.setCouupdate(rs.getTimestamp("couupdate"));
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return couVO;
	}

	@Override
	public List<CouVO> getAll() {
		List<CouVO> list = new ArrayList<CouVO>();
		CouVO couVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				couVO = new CouVO();
				couVO.setCouno(rs.getInt("couno"));
				couVO.setCouname(rs.getString("couname"));
				couVO.setCouintro(rs.getString("couintro"));
				couVO.setCoupic(rs.getBytes("coupic"));
				couVO.setCoubadge(rs.getBytes("coubadge"));
				couVO.setCouprice(rs.getInt("couprice"));
				couVO.setCoudiscount(rs.getInt("coudiscount"));
				couVO.setCouishot(rs.getInt("couishot"));
				couVO.setAccno(rs.getInt("accno"));
				couVO.setCoustate(rs.getInt("coustate"));
				couVO.setProno(rs.getInt("prono"));
				couVO.setCouupdate(rs.getTimestamp("couupdate"));
				list.add(couVO);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	
	
	
	//更新熱門課程狀態
	@Override
	public void updateHot(Integer couno, Integer hot) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_HOT);
			
			pstmt.setInt(1, hot);	
			pstmt.setInt(2, couno);			
			
			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	
	
	@Override
	public void updateCoustateToOne(Integer couno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_COUSTATE_TO_ONE);
			
			pstmt.setInt(1, couno);			
			
			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	
	//這門課有哪些單元
	@Override
	public Set<UnitVO> getUnitsByCouno(Integer couno) {
		Set<UnitVO> set = new LinkedHashSet<UnitVO>();
		UnitVO unitVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_UNITS_BY_COUNO);

			pstmt.setInt(1, couno);

			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				unitVO = new UnitVO();
				unitVO.setUnitno(rs.getInt("unitno"));
				unitVO.setCouno(rs.getInt("couno"));
				unitVO.setUnitname(rs.getString("unitname"));
				unitVO.setUnitorder(rs.getInt("unitorder"));
				set.add(unitVO); // Store the row in the list
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}
	
	//依課程狀態找課程(0:未審核 1:待審核 2:已審核 3:上線中)
	@Override
	public Set<CouVO> getCousByCoustate(Integer coustate) {
		Set<CouVO> set = new LinkedHashSet<CouVO>();
		CouVO couVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_COUS_BY_COUSTATE);
			pstmt.setInt(1, coustate);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				couVO = new CouVO();
				couVO.setCouno(rs.getInt("couno"));
				couVO.setCouname(rs.getString("couname"));
				couVO.setCouintro(rs.getString("couintro"));
				couVO.setCoupic(rs.getBytes("coupic"));
				couVO.setCoubadge(rs.getBytes("coubadge"));
				couVO.setCouprice(rs.getInt("couprice"));
				couVO.setCoudiscount(rs.getInt("coudiscount"));
				couVO.setCouishot(rs.getInt("couishot"));
				couVO.setAccno(rs.getInt("accno"));
				couVO.setCoustate(rs.getInt("coustate"));
				couVO.setProno(rs.getInt("prono"));
				couVO.setCouupdate(rs.getTimestamp("couupdate"));
				set.add(couVO);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}
	
	@Override
	public Integer getMaxUnitorderByCouno(Integer couno) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer unitorder = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_MAX_UNITORDER_BY_COUNO);
			
			pstmt.setInt(1, couno);	
			
			rs = pstmt.executeQuery();
			rs.next();
			unitorder = rs.getInt(1);

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return unitorder;
	}
	
	
	
	@Override
	public Integer getUnitnoByCounoUnitorder(Integer couno, Integer unitorder) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer unitno = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_UNITNO_BY_COUNO_UNITORDER);
			
			pstmt.setInt(1, couno);	
			pstmt.setInt(2, unitorder);	
			
			rs = pstmt.executeQuery();
			rs.next();
			unitno = rs.getInt(1);

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return unitno;
	}
	
	
	
	
	@Override
	public Set<CouVO> getHotCous() {
		Set<CouVO> set = new LinkedHashSet<CouVO>();
		CouVO couVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_HOTCOUS);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				couVO = new CouVO();
				couVO.setCouno(rs.getInt("couno"));
				couVO.setCouname(rs.getString("couname"));
				couVO.setCouintro(rs.getString("couintro"));
				couVO.setCoupic(rs.getBytes("coupic"));
				couVO.setCoubadge(rs.getBytes("coubadge"));
				couVO.setCouprice(rs.getInt("couprice"));
				couVO.setCoudiscount(rs.getInt("coudiscount"));
				couVO.setCouishot(rs.getInt("couishot"));
				couVO.setAccno(rs.getInt("accno"));
				couVO.setCoustate(rs.getInt("coustate"));
				couVO.setProno(rs.getInt("prono"));
				couVO.setCouupdate(rs.getTimestamp("couupdate"));
				set.add(couVO);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}
	
	
	@Override
	public Double getAverageAppscoreByCouno(Integer couno) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Double averageAppscore = null;
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_AVERAGE_APPSCORE_BY_COUNO);
			
			pstmt.setInt(1, couno);	
			
			rs = pstmt.executeQuery();
			rs.next();
			averageAppscore = rs.getDouble(1);

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return averageAppscore;
	}
	
	
	@Override
	public Integer getAppraiseCountByCouno(Integer couno) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer averageCount = null;
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_APPRAISE_COUNT_BY_COUNO);
			
			pstmt.setInt(1, couno);	
			
			rs = pstmt.executeQuery();
			rs.next();
			averageCount = rs.getInt(1);

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return averageCount;
	}
	
	
	
	@Override
	public List<Integer> getCouCountListGroupByProno() {
		List<Integer> couCountList = new ArrayList<Integer>();
		Integer couCount = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_COU_COUNT_LIST_GROUP_BY_PRONO);
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				couCount = rs.getInt(1);
				couCountList.add(couCount);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return couCountList;
	}
	
	
	@Override
	public List<Integer> getIsfinishByMemnoCouno(Integer memno, Integer couno) {
	
		List<Integer> isfinishList = new ArrayList<Integer>();
		Integer isFinish = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ISFINISH_BY_MEMNO_COUNO);
			pstmt.setInt(1, memno);
			pstmt.setInt(2, couno);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				isFinish = rs.getInt(1);
				isfinishList.add(isFinish);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return isfinishList;	
		
	}
	
	
	@Override
	public Set<FinVO> getFinVOSetByMemnoCouno(Integer memno, Integer couno) {
		Set<FinVO> finVOSet = new LinkedHashSet<FinVO>();
		FinVO finVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_FINS_BY_MEMNO_COUNO);
			pstmt.setInt(1, memno);
			pstmt.setInt(2, couno);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {	
				finVO = new FinVO();
				finVO.setConno(rs.getInt("conno"));
				finVO.setMemno(rs.getInt("memno"));
				finVO.setIsfinish(rs.getInt("isfinish"));
				finVOSet.add(finVO);				
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return finVOSet;
	}

	@Override
	public List<CouVO> getIntroKeyword(String couintro) {    
		List<CouVO> list = new ArrayList<CouVO>();
		CouVO couVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			
			con = ds.getConnection(); 
			pstmt = con.prepareStatement(GET_COUINTRO_KEYWORD);

			 
			pstmt.setString(1,couintro);

			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				couVO = new CouVO();
				couVO.setCouno(rs.getInt("couno"));
				couVO.setCouname(rs.getString("couname"));
				couVO.setCouintro(rs.getString("couintro"));
				couVO.setCoupic(rs.getBytes("coupic"));
				couVO.setCoubadge(rs.getBytes("coubadge"));
				couVO.setCouprice(rs.getInt("couprice"));
				couVO.setCoudiscount(rs.getInt("coudiscount"));
				couVO.setCouishot(rs.getInt("couishot"));
				couVO.setAccno(rs.getInt("accno"));
				couVO.setCoustate(rs.getInt("coustate"));
				couVO.setProno(rs.getInt("prono"));
				couVO.setCouupdate(rs.getTimestamp("couupdate"));
				list.add(couVO);
			}
		 
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;			 
	}
	@Override
	public List<CouVO> getAllByCouno() {
		List<CouVO> list = new ArrayList<CouVO>();
		CouVO couVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT_BYCOUNO);
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				couVO = new CouVO();
				couVO.setCouno(rs.getInt("couno"));
				couVO.setCouname(rs.getString("couname"));
				couVO.setCouintro(rs.getString("couintro"));
				couVO.setCoupic(rs.getBytes("coupic"));
				couVO.setCoubadge(rs.getBytes("coubadge"));
				couVO.setCouprice(rs.getInt("couprice"));
				couVO.setCoudiscount(rs.getInt("coudiscount"));
				couVO.setCouishot(rs.getInt("couishot"));
				couVO.setAccno(rs.getInt("accno"));
				couVO.setCoustate(rs.getInt("coustate"));
				couVO.setProno(rs.getInt("prono"));
				couVO.setCouupdate(rs.getTimestamp("couupdate"));
				list.add(couVO);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

}
