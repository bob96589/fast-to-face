package com.cou.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ass.model.AssVO;
import com.content.model.ConDAO;
import com.content.model.ConService;
import com.content.model.ConVO;
import com.fin.model.FinVO;
import com.unit.model.UnitDAO;
import com.unit.model.UnitService;
import com.unit.model.UnitVO;

public class CouService {
	private CouDAO_interface dao;
	//
	

	public CouService() {
		dao = new CouDAO();
	}

	public CouVO addCou(String couname, String couintro, byte[] coupic, byte[] coubadge, Integer couprice,
			Integer coudiscount, Integer accno, Integer prono) {

		CouVO couVO = new CouVO();

		couVO.setCouname(couname);
		couVO.setCouintro(couintro);
		couVO.setCoupic(coupic);
		couVO.setCoubadge(coubadge);
		couVO.setCouprice(couprice);
		couVO.setCoudiscount(coudiscount);		
		couVO.setAccno(accno);		
		couVO.setProno(prono);		
		
		Integer couno = dao.insert(couVO);
		couVO.setCouno(couno);
		
		return couVO;
	}

	public CouVO updateCou(Integer couno, String couname, String couintro, byte[] coupic, byte[] coubadge,
			Integer couprice, Integer coudiscount, Integer accno, Integer coustate,
			Integer prono) {

		CouVO couVO = new CouVO();

		couVO.setCouno(couno);
		couVO.setCouname(couname);
		couVO.setCouintro(couintro);
		couVO.setCoupic(coupic);
		couVO.setCoubadge(coubadge);
		couVO.setCouprice(couprice);
		couVO.setCoudiscount(coudiscount);
		couVO.setAccno(accno);
		couVO.setCoustate(coustate);
		couVO.setProno(prono);
		dao.update(couVO);
		
		return couVO;
	}

	public void deleteCou(Integer couno) {
		dao.delete(couno);
	}

	public CouVO getOneCou(Integer couno) {
		return dao.findByPrimaryKey(couno);
	}

	public List<CouVO> getAll() {
		return dao.getAll();
	}

	public void updateHot(Integer couno, Integer hot){
		dao.updateHot(couno, hot);
	}
	
	public void updateCoustateToOne(Integer couno){
		dao.updateCoustateToOne(couno);
	}
	
	public Set<UnitVO> getUnitsByCouno(Integer couno){
		return dao.getUnitsByCouno(couno);
	}
	
	public Set<CouVO> getCousByCoustate(Integer coustate){
		return dao.getCousByCoustate(coustate);		
	}
	
	public Integer getMaxUnitorderByCouno(Integer couno){
		return dao.getMaxUnitorderByCouno(couno);
	}
	
	public Integer getUnitnoByCounoUnitorder(Integer couno, Integer unitorder){
		return dao.getUnitnoByCounoUnitorder(couno, unitorder);
	}
	
	public Set<CouVO> getHotCous(){
		return dao.getHotCous();
	}
	
	public Double getAverageAppscoreByCouno(Integer couno){
		return dao.getAverageAppscoreByCouno(couno);
	}
	
	public Integer getAppraiseCountByCouno(Integer couno){
		return dao.getAppraiseCountByCouno(couno);
	}
	
	public List<Integer> getCouCountListGroupByProno(){
		return dao.getCouCountListGroupByProno();
	}
	
	public List<Integer> getIsfinishByMemnoCouno(Integer memno, Integer couno){
		return dao.getIsfinishByMemnoCouno(memno, couno);		
	}
	
	public Set<FinVO> getFinVOSetByMemnoCouno(Integer memno, Integer couno){
		return dao.getFinVOSetByMemnoCouno(memno, couno);	
	}
	
	
	
	
	
	
 	public Map<String, Integer> getCouDetailCountByCouno(Integer couno){
		
		
		Map<String, Integer> CouDetailCountMap = new HashMap<String, Integer>();
		
		UnitDAO unitDao = new UnitDAO();
		ConDAO conDao = new ConDAO();		
		Set<UnitVO> unitVOSet = dao.getUnitsByCouno(couno);
		
		Integer unitCount = unitVOSet.size();
		Integer videoCount = 0;
		Integer assCount = 0;		
		
		for(UnitVO unitVO : unitVOSet){			
			Set<ConVO> conVOSet = unitDao.getConsByUnitno(unitVO.getUnitno());					
			for(ConVO conVO : conVOSet){				
				if("�v��".equals(conVO.getContype()))	
					videoCount++;	
				if("���q".equals(conVO.getContype()))	
					assCount++;	
			}			
		}
		
		CouDetailCountMap.put("unitCount", unitCount);
		CouDetailCountMap.put("videoCount", videoCount);
		CouDetailCountMap.put("assCount", assCount);
		
		return CouDetailCountMap;	
	}
		
	public List<Object> getWholeCouByCouno(Integer couno){
		
		UnitDAO unitDao = new UnitDAO();
		ConDAO conDao = new ConDAO();
		
		//1
		CouVO couVO = dao.findByPrimaryKey(couno);
		//2
		Set<UnitVO> unitVOSet = dao.getUnitsByCouno(couno);
		//3
		List<Set<ConVO>> conVOSetList = new ArrayList<Set<ConVO>>();
		//4
		List<List<Set<AssVO>>> assVOSetListList = new ArrayList<List<Set<AssVO>>>();
		
		for(UnitVO unitVO : unitVOSet){			
			Set<ConVO> conVOSet = unitDao.getConsByUnitno(unitVO.getUnitno());					
			conVOSetList.add(conVOSet);
			
			List<Set<AssVO>> assVOSetList = new ArrayList<Set<AssVO>>();
			for(ConVO conVO : conVOSet){				
				Set<AssVO> assVOSet = conDao.getAsssByConno(conVO.getConno());
				assVOSetList.add(assVOSet);						
			}
			assVOSetListList.add(assVOSetList);					
		}
		List<Object> WholeCourseList = new ArrayList<Object>();
		WholeCourseList.add(couVO);
		WholeCourseList.add(unitVOSet);
		WholeCourseList.add(conVOSetList);
		WholeCourseList.add(assVOSetListList);	
		
		return WholeCourseList;
	}
	
	public List<Object> getWholeCouByCounoWithoutAss(Integer couno){
		
		UnitDAO unitDao = new UnitDAO();
		
		//1
		CouVO couVO = dao.findByPrimaryKey(couno);
		//2
		Set<UnitVO> unitVOSet = dao.getUnitsByCouno(couno);
		//3
		List<Set<ConVO>> conVOSetList = new ArrayList<Set<ConVO>>();
		
		for(UnitVO unitVO : unitVOSet){			
			Set<ConVO> conVOSet = unitDao.getConsByUnitno(unitVO.getUnitno());					
			conVOSetList.add(conVOSet);		
		}
		
		List<Object> WholeCourseWithoutAssList = new ArrayList<Object>();
		WholeCourseWithoutAssList.add(couVO);
		WholeCourseWithoutAssList.add(unitVOSet);
		WholeCourseWithoutAssList.add(conVOSetList);
		
		return WholeCourseWithoutAssList;
	}
	//hsu
		public List<CouVO> getAllByCouno() {
			return dao.getAllByCouno();
		}
	//hsu
		public List<CouVO> getIntroKeyword(String couintro) {
			return dao.getIntroKeyword(couintro);
		}


}
