package com.ass.model;

import java.util.List;

public interface AssDAO_interface {
	public void insert(AssVO assVO);
    public void update(AssVO assVO);
    public void delete(Integer assno);
    public AssVO findByPrimaryKey(Integer assno);
    public List<AssVO> getAll();

}
