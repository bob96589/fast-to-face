package com.ass.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AssJDBCDAO implements AssDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "user1";
	String passwd = "u111";

	private static final String INSERT_STMT = 
			"INSERT INTO assessment (assno, assques, assans, assansa, assansb, assansc, assansd, conno)"
			+ "VALUES (ass_seq.nextval, ?, ?, ?, ?, ?, ?, ?)";
	private static final String GET_ALL_STMT = 
			"SELECT * FROM assessment order by assno";
	private static final String GET_ONE_STMT = 
			"SELECT * FROM assessment where assno = ?";
	private static final String DELETE = 
			"DELETE FROM assessment where assno = ?";
	private static final String UPDATE = 
			"UPDATE assessment set assques=?,assans=?,assansa=?,assansb=?,assansc=?,assansd=?,conno=? where assno = ?";
	
	
	@Override
	public void insert(AssVO assVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, assVO.getAssques());
			pstmt.setString(2, assVO.getAssans());
			pstmt.setString(3, assVO.getAssansa());
			pstmt.setString(4, assVO.getAssansb());
			pstmt.setString(5, assVO.getAssansc());
			pstmt.setString(6, assVO.getAssansd());
			pstmt.setInt(7, assVO.getConno());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(AssVO assVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, assVO.getAssques());
			pstmt.setString(2, assVO.getAssans());
			pstmt.setString(3, assVO.getAssansa());
			pstmt.setString(4, assVO.getAssansb());
			pstmt.setString(5, assVO.getAssansc());
			pstmt.setString(6, assVO.getAssansd());
			pstmt.setInt(7, assVO.getConno());
			pstmt.setInt(8, assVO.getAssno());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer assno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, assno);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public AssVO findByPrimaryKey(Integer assno) {
		AssVO assVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setInt(1, assno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				assVO = new AssVO();
				assVO.setAssno(rs.getInt("assno"));
				assVO.setAssques(rs.getString("assques"));
				assVO.setAssans(rs.getString("assans"));
				assVO.setAssansa(rs.getString("assansa"));
				assVO.setAssansb(rs.getString("assansb"));
				assVO.setAssansc(rs.getString("assansc"));
				assVO.setAssansd(rs.getString("assansd"));
				assVO.setConno(rs.getInt("conno"));
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return assVO;
	}

	@Override
	public List<AssVO> getAll() {
		List<AssVO> list = new ArrayList<AssVO>();
		AssVO assVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				assVO = new AssVO();
				assVO.setAssno(rs.getInt("assno"));
				assVO.setAssques(rs.getString("assques"));
				assVO.setAssans(rs.getString("assans"));
				assVO.setAssansa(rs.getString("assansa"));
				assVO.setAssansb(rs.getString("assansb"));
				assVO.setAssansc(rs.getString("assansc"));
				assVO.setAssansd(rs.getString("assansd"));
				assVO.setConno(rs.getInt("conno"));
				list.add(assVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	public static void main(String[] args) {

		AssJDBCDAO dao = new AssJDBCDAO();

		//insert
		AssVO vo1 = new AssVO();

		vo1.setAssques("���D");
		vo1.setAssans("a");
		vo1.setAssansa("a");
		vo1.setAssansb("b");
		vo1.setAssansc("c");
		vo1.setAssansd("d");
		vo1.setConno(40015);
		dao.insert(vo1);

		//update
		AssVO vo2 = new AssVO();
		
		vo2.setAssno(50038);
		vo2.setAssques("���D");
		vo2.setAssans("a");
		vo2.setAssansa("a");
		vo2.setAssansb("a");
		vo2.setAssansc("a");
		vo2.setAssansd("a");
		vo2.setConno(40018);
		dao.update(vo2);

		//delete
		dao.delete(50037);

		//get one
		AssVO vo = dao.findByPrimaryKey(50001);
		System.out.println(vo.getAssno());
		System.out.println(vo.getAssques());
		System.out.println(vo.getAssans());
		System.out.println(vo.getAssansa());
		System.out.println(vo.getAssansb());
		System.out.println(vo.getAssansc());
		System.out.println(vo.getAssansd());
		System.out.println(vo.getConno());
		System.out.println("---------------------");

		//get all
		List<AssVO> list = dao.getAll();
		for (AssVO p : list) {
			System.out.println(p.getAssno());
			System.out.println(p.getAssques());
			System.out.println(p.getAssans());
			System.out.println(p.getAssansa());
			System.out.println(p.getAssansb());
			System.out.println(p.getAssansc());
			System.out.println(p.getAssansd());
			System.out.println(p.getConno());
			System.out.println("---------------------");
		}

	}

}
