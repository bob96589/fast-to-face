package com.ass.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class AssDAO implements AssDAO_interface {
	
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/TestDB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
			"INSERT INTO assessment (assno, assques, assans, assansa, assansb, assansc, assansd, conno)"
			+ "VALUES (ass_seq.nextval, ?, ?, ?, ?, ?, ?, ?)";
	private static final String GET_ALL_STMT = 
			"SELECT * FROM assessment order by assno";
	private static final String GET_ONE_STMT = 
			"SELECT * FROM assessment where assno = ?";
	private static final String DELETE = 
			"DELETE FROM assessment where assno = ?";
	private static final String UPDATE = 
			"UPDATE assessment set assques=?,assans=?,assansa=?,assansb=?,assansc=?,assansd=?,conno=? where assno = ?";
	
	
	@Override
	public void insert(AssVO assVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, assVO.getAssques());
			pstmt.setString(2, assVO.getAssans());
			pstmt.setString(3, assVO.getAssansa());
			pstmt.setString(4, assVO.getAssansb());
			pstmt.setString(5, assVO.getAssansc());
			pstmt.setString(6, assVO.getAssansd());
			pstmt.setInt(7, assVO.getConno());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(AssVO assVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, assVO.getAssques());
			pstmt.setString(2, assVO.getAssans());
			pstmt.setString(3, assVO.getAssansa());
			pstmt.setString(4, assVO.getAssansb());
			pstmt.setString(5, assVO.getAssansc());
			pstmt.setString(6, assVO.getAssansd());
			pstmt.setInt(7, assVO.getConno());
			pstmt.setInt(8, assVO.getAssno());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer assno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, assno);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public AssVO findByPrimaryKey(Integer assno) {
		AssVO assVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setInt(1, assno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				assVO = new AssVO();
				assVO.setAssno(rs.getInt("assno"));
				assVO.setAssques(rs.getString("assques"));
				assVO.setAssans(rs.getString("assans"));
				assVO.setAssansa(rs.getString("assansa"));
				assVO.setAssansb(rs.getString("assansb"));
				assVO.setAssansc(rs.getString("assansc"));
				assVO.setAssansd(rs.getString("assansd"));
				assVO.setConno(rs.getInt("conno"));
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return assVO;
	}

	@Override
	public List<AssVO> getAll() {
		List<AssVO> list = new ArrayList<AssVO>();
		AssVO assVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				assVO = new AssVO();
				assVO.setAssno(rs.getInt("assno"));
				assVO.setAssques(rs.getString("assques"));
				assVO.setAssans(rs.getString("assans"));
				assVO.setAssansa(rs.getString("assansa"));
				assVO.setAssansb(rs.getString("assansb"));
				assVO.setAssansc(rs.getString("assansc"));
				assVO.setAssansd(rs.getString("assansd"));
				assVO.setConno(rs.getInt("conno"));
				list.add(assVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
}
