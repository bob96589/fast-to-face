package com.ass.model;

public class AssVO  implements java.io.Serializable {
	private Integer assno;
	private String assques;
	private String assans;
	private String assansa;
	private String assansb;
	private String assansc;
	private String assansd;
	private Integer conno;
	public Integer getAssno() {
		return assno;
	}
	public void setAssno(Integer assno) {
		this.assno = assno;
	}
	public String getAssques() {
		return assques;
	}
	public void setAssques(String assques) {
		this.assques = assques;
	}
	public String getAssans() {
		return assans;
	}
	public void setAssans(String assans) {
		this.assans = assans;
	}
	public String getAssansa() {
		return assansa;
	}
	public void setAssansa(String assansa) {
		this.assansa = assansa;
	}
	public String getAssansb() {
		return assansb;
	}
	public void setAssansb(String assansb) {
		this.assansb = assansb;
	}
	public String getAssansc() {
		return assansc;
	}
	public void setAssansc(String assansc) {
		this.assansc = assansc;
	}
	public String getAssansd() {
		return assansd;
	}
	public void setAssansd(String assansd) {
		this.assansd = assansd;
	}
	public Integer getConno() {
		return conno;
	}
	public void setConno(Integer conno) {
		this.conno = conno;
	}
	
	

	
	

}
