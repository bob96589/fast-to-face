package com.ass.model;

import java.util.List;



public class AssService {
	
	private AssDAO_interface dao;

	public AssService() {
		dao = new AssDAO();
	}
	
	public AssVO addAss(AssVO assVO) {		
		
		dao.insert(assVO);
		return assVO;
	}
	

	public AssVO addAss(String assques, String assans, String assansa, String assansb,
			String assansc, String assansd, Integer conno) {

		AssVO assVO = new AssVO();

		assVO.setAssques(assques);
		assVO.setAssans(assans);
		assVO.setAssansa(assansa);
		assVO.setAssansb(assansb);
		assVO.setAssansc(assansc);
		assVO.setAssansd(assansd);
		assVO.setConno(conno);
		dao.insert(assVO);

		return assVO;
	}
	
	
	public AssVO updateAss(AssVO assVO) {		
		
		dao.update(assVO);
		return assVO;
	}
	
	
	public AssVO updateAss(Integer assno, String assques, String assans, String assansa, String assansb,
			String assansc, String assansd, Integer conno) {

		AssVO assVO = new AssVO();

		assVO.setAssno(assno);
		assVO.setAssques(assques);
		assVO.setAssans(assans);
		assVO.setAssansa(assansa);
		assVO.setAssansb(assansb);
		assVO.setAssansc(assansc);
		assVO.setAssansd(assansd);
		assVO.setConno(conno);
		dao.update(assVO);

		return assVO;
	}

	public void deleteAss(Integer assno) {
		dao.delete(assno);
	}

	public AssVO getOneAss(Integer assno) {
		return dao.findByPrimaryKey(assno);
	}

	public List<AssVO> getAll() {
		return dao.getAll();
	}

}
