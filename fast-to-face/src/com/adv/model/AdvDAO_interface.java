package com.adv.model;

import java.util.*;

public interface AdvDAO_interface {
	public void insert(AdvVO advVO);
	public void update(AdvVO advVO);
	public void delete(Integer advno);
	public AdvVO findByPrimaryKey(Integer advno);
	public AdvVO findCou(Integer couno);
	public List<AdvVO> getAll();
}
