package com.adv.model;

import java.util.List;

public class AdvService {
	
	private AdvDAO_interface dao;

	public AdvService() {
		dao = new AdvHibernateDAO();
	}

	public AdvVO addAdv(byte[] advpic,Integer advisplay,String advformat,String advdescription,Integer couno) {

		AdvVO advVO = new AdvVO();

		advVO.setAdvpic(advpic);
		advVO.setAdvisplay(advisplay);
		advVO.setAdvformat(advformat);
		advVO.setAdvdescription(advdescription);
		advVO.setCouno(couno);
		dao.insert(advVO);

		return advVO;
	}

	public AdvVO updateAdv(Integer advno,byte[] advpic,Integer advisplay,String advformat,String advdescription,Integer couno) {

		AdvVO advVO = new AdvVO();

		advVO.setAdvno(advno);
		advVO.setAdvpic(advpic);
		advVO.setAdvisplay(advisplay);
		advVO.setAdvformat(advformat);
		advVO.setAdvdescription(advdescription);
		advVO.setCouno(couno);
		dao.update(advVO);

		return advVO;
	}

	public void deleteAdv(Integer advno) {
		dao.delete(advno);
	}

	public AdvVO getOneAdv(Integer advno) {
		return dao.findByPrimaryKey(advno);
	}
	
	public AdvVO getOneCou(Integer couno) {
		return dao.findCou(couno);
	}
	
	public List<AdvVO> getAll() {
		return dao.getAll();
	}
}
