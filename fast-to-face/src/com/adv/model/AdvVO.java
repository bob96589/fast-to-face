package com.adv.model;

import java.sql.*;

public class AdvVO implements java.io.Serializable{
	private Integer advno;
	private byte[] advpic;
	private Integer advisplay;
	private String advformat;
	private String advdescription;
	private Integer couno;
	
	public Integer getAdvno() {
		return advno;
	}
	public void setAdvno(Integer advno) {
		this.advno = advno;
	}
	public byte[] getAdvpic() {
		return advpic;
	}
	public void setAdvpic(byte[] advpic) {
		this.advpic = advpic;
	}
	public Integer getAdvisplay() {
		return advisplay;
	}
	public void setAdvisplay(Integer advisplay) {
		this.advisplay = advisplay;
	}
	public String getAdvformat() {
		return advformat;
	}
	public void setAdvformat(String advformat) {
		this.advformat = advformat;
	}
	public String getAdvdescription() {
		return advdescription;
	}
	public void setAdvdescription(String advdescription) {
		this.advdescription = advdescription;
	}
	public Integer getCouno() {
		return couno;
	}
	public void setCouno(Integer couno) {
		this.couno = couno;
	}


}
