package com.adv.controller;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.acc.model.AccService;
import com.acc.model.AccVO;
import com.adv.model.*;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.oreilly.servlet.MultipartRequest;

public class AdvServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String action = req.getParameter("action");
		MultipartRequest multi = null;
		if(req.getContentType() != null && req.getContentType().startsWith("multipart")){
			multi = new MultipartRequest(req, getServletContext().getRealPath("/back/temp"), 5 * 1024 * 1024, "UTF-8");
			action = multi.getParameter("action");
		}

		if ("getOne_For_Display".equals(action)) { 
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			try {
				/*************************** 1.接收請求參數 - 輸入格式的錯誤處理 **********************/

				Integer advno =  new Integer(req.getParameter("advno"));
				
				/*************************** 2.開始查詢資料 *****************************************/
				AdvService advSvc = new AdvService();
				AdvVO advVO = advSvc.getOneAdv(advno);
		
				/*************************** 3.查詢完成,準備轉交(Send the Success view) *************/
				req.setAttribute("advVO", advVO);
				String url = "/back/adv/NewFile.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				/*************************** 其他可能的錯誤處理 *************************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/back/adv/NewFile.jsp");
				failureView.forward(req, res);
			}
		}

		if ("update".equals(action)) {
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			String requestURL = req.getParameter("requestURL");
			String whichPage = req.getParameter("whichPage");
			
			try {
				/*************************** 1.接收請求參數 - 輸入格式的錯誤處理 **********************/
				Integer advno = new Integer(multi.getParameter("advno").trim());
				Integer advisplay = new Integer(multi.getParameter("advisplay").trim());
				String advformat = multi.getParameter("advformat").trim();
				if (advformat == null || (advformat.trim()).length() == 0) {
					errorMsgs.add("廣告名稱:不能為空值");
				}
				String advdescription = multi.getParameter("advdescription").trim();
				if (advdescription == null || (advdescription.trim()).length() == 0) {
					errorMsgs.add("廣告描述:不能為空值");
				}
				Integer couno = new Integer(multi.getParameter("couno").trim());
	
				File advpic = multi.getFile("advpic");
				byte[] advpicBuffer = null;
				if(advpic != null){
					InputStream in = new FileInputStream(advpic);
					advpicBuffer = new byte[in.available()];
					in.read(advpicBuffer);		
					advpic.delete();
					in.close();			
				}else{					
					AdvService advSvc = new AdvService();
					AdvVO advVO = advSvc.getOneAdv(advno);
					advpicBuffer = advVO.getAdvpic();					
				}
				AdvVO advVO = new AdvVO();
				advVO.setAdvno(advno);
				advVO.setAdvpic(advpicBuffer);
				advVO.setAdvisplay(advisplay);
				advVO.setAdvformat(advformat);
				advVO.setAdvdescription(advdescription);
				advVO.setCouno(couno);
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("advVO", advVO);
					RequestDispatcher failureView = req.getRequestDispatcher("/back/adv/listAllAdv.jsp");
					failureView.forward(req, res);
					return;
				}
				
				/*************************** 2.開始修改資料 *****************************************/
				AdvService advSvc = new AdvService();
				advVO = advSvc.updateAdv(advno, advpicBuffer, advisplay, advformat,advdescription,couno);
				/*************************** 3.修改完成,準備轉交(Send the Success view) *************/
				req.setAttribute("advVO", advVO); 
				req.setAttribute("requestURL", requestURL);						
				req.setAttribute("whichPage", whichPage);	
				String url = "/back/adv/listAllAdv.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); 
				successView.forward(req, res);

				/*************************** 其他可能的錯誤處理 *************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back/adv/listAllAdv.jsp");
				failureView.forward(req, res);
			}
		}

		if ("insert".equals(action)) {
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			try {
				/*********************** 1.接收請求參數 - 輸入格式的錯誤處理 *************************/
				Integer advisplay = new Integer(multi.getParameter("advisplay").trim());
				String advformat = multi.getParameter("advformat").trim();
				if (advformat == null || (advformat.trim()).length() == 0) {
					errorMsgs.add("廣告名稱:不能為空值");
				}
				String advdescription = multi.getParameter("advdescription").trim();
				if (advdescription == null || (advdescription.trim()).length() == 0) {
					errorMsgs.add("廣告描述:不能為空值");
				}
				Integer couno = new Integer(multi.getParameter("couno").trim());
				File advpic = multi.getFile("advpic");
				byte[] advpicBuffer = null;
				if(advpic != null){
					InputStream in = new FileInputStream(advpic);
					advpicBuffer = new byte[in.available()];
					in.read(advpicBuffer);		
					advpic.delete();
					in.close();						
				}else{					
					errorMsgs.add("請選擇上傳圖片");						
				}	
				AdvVO advVO = new AdvVO();
				advVO.setAdvpic(advpicBuffer);
				advVO.setAdvisplay(advisplay);
				advVO.setAdvformat(advformat);
				advVO.setAdvdescription(advdescription);
				advVO.setCouno(couno);
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("advVO", advVO);
					RequestDispatcher failureView = req.getRequestDispatcher("/back/adv/listAllAdv.jsp");
					failureView.forward(req, res);
					return;
				}
				/*************************** 2.開始新增資料 ***************************************/
				AdvService advSvc = new AdvService();
				advVO = advSvc.addAdv(advpicBuffer, advisplay, advformat, advdescription, couno);
				/*************************** 3.新增完成,準備轉交(Send the Success view) ***********/
				String url = "/back/adv/listAllAdv.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				/*************************** 其他可能的錯誤處理 **********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/back/adv/listAllAdv.jsp");
				failureView.forward(req, res);
			}
		}

		if ("delete".equals(action)) {
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			try {
				/*************************** 1.接收請求參數 ***************************************/
				Integer advno = new Integer(multi.getParameter("advno"));
				/*************************** 2.開始刪除資料 ***************************************/
				AdvService advSvc = new AdvService();
				advSvc.deleteAdv(advno);
				/*************************** 3.刪除完成,準備轉交(Send the Success view) ***********/
				String url = "/back/adv/listAllAdv.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				/*************************** 其他可能的錯誤處理 **********************************/
			} catch (Exception e) {
				errorMsgs.add("刪除資料失敗:" + e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/back/adv/listAllAdv.jsp");
				failureView.forward(req, res);
			}
		}

		if("getAdvpic".equals(action)){
			/*************************** 1.接收請求參數 - 輸入格式的錯誤處理 **********************/
			Integer advno = new Integer(req.getParameter("advno"));	
			/*************************** 2.開始查詢資料 *****************************************/
			AdvService advSvc = new AdvService();
			AdvVO advVO = advSvc.getOneAdv(advno);				
			byte[] advpic = advVO.getAdvpic();				
			res.setContentType("image/png");
			ServletOutputStream out = res.getOutputStream();
			out.write(advpic);
			out.flush();
		}
		
		if("getCouPath".equals(action)){
			/*************************** 1.接收請求參數 - 輸入格式的錯誤處理 **********************/
			Integer couno = new Integer(req.getParameter("couno"));
			/*************************** 2.開始查詢資料 *****************************************/
			AdvService advSvc = new AdvService();
			AdvVO advVO = advSvc.getOneAdv(couno);
			CouService couSvc = new CouService();
			List<Object> wholeCourseList = couSvc.getWholeCouByCounoWithoutAss(couno);					
			Double averageAppscore = couSvc.getAverageAppscoreByCouno(couno);
			Integer appraiseCount = couSvc.getAppraiseCountByCouno(couno);
			AccService accSvc = new AccService();
			AccVO accVO = accSvc.getOneAcc(((CouVO)wholeCourseList.get(0)).getAccno());		
			/***************************3.查詢完成,準備轉交(Send the Success view)*************/	
			req.setAttribute("averageAppscore", averageAppscore); 				
			req.setAttribute("appraiseCount", appraiseCount); 				
			req.setAttribute("accVO", accVO);			
			req.setAttribute("couVO", wholeCourseList.get(0)); 
			req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
			req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
			req.setAttribute("advVO",advVO);
			String url="/front/course_detail/couDetail.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);
			successView.forward(req, res);
		}
	}
}