package com.adv.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.adv.model.AdvService;
import com.adv.model.AdvVO;


	public class searchOne extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		/*************************** 1.接收請求參數 - 輸入格式的錯誤處理 **********************/
		Integer couno = new Integer(req.getParameter("couno"));
		/*************************** 2.開始查詢資料 *****************************************/
		AdvService advSvc = new AdvService();	
		AdvVO advVO = advSvc.getOneCou(couno);
		/*************************** 3.查詢完成,準備轉交(Send the Success view) *************/
		req.setAttribute("advVO", advVO);
		String url = "/back/adv/NewFile.jsp";
		RequestDispatcher successView = req.getRequestDispatcher(url);
		successView.forward(req, res);
			
		}

	}

