package com.rep.model;

import java.util.*;
import java.sql.*;

public class RepService {
      private RepDAO_interface dao ;
      public RepService() {
  		dao = new RepDAO();
  	}

  	public RepVO insertrep(Integer artno , Integer memno , String repcon  
  			  ) {

  		RepVO repVO = new RepVO();
  		repVO.setArtno(artno);
  		repVO.setMemno(memno);
  		repVO.setRepcon(repcon);  		   		 
  		dao.insertrep(repVO);
  		return repVO;
  	}

  	public RepVO addEmp(Integer repno,Integer artno , Integer memno , String repcon ,
  			Timestamp reptime ) {

  		RepVO repVO = new RepVO();
  		repVO.setArtno(repno);
  		repVO.setArtno(artno);
  		repVO.setMemno(memno);
  		repVO.setRepcon(repcon);
  		repVO.setReptime(reptime);  
  		dao.update(repVO);
  		return repVO;
  	}

  	public void deleteArt(Integer artno) {
  		dao.delete(artno);
  	}

  	public RepVO getOneEmp(Integer repno) {
  		return dao.findByPrimaryKey(repno);
  	}

  	public List<RepVO> getAll() {
  		return dao.getAll();
  	}
		
}
