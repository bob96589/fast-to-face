package com.rep.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RepJDBCDAO implements RepDAO_interface{

	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "user1";
	String passwd = "u111";
	
	private static final String INSERT_STMT =  //輸入資料
	"INSERT INTO Report ( repno,artno,memno,repcon,reptime) VALUES (REP_SEQ.NEXTVAL,?,?,?,sysdate)";
	private static final String GET_ALL_STMT = //查詢該表格全部欄位
	"SELECT repno,artno,memno,repcon,reptime FROM Report order by repno desc";
	private static final String GET_ONE_STMT = //根據該欄位查詢資料
	"SELECT * FROM Report where repno = ?";
	private static final String DELETE =       //根據該欄位刪除資料
	"DELETE FROM Report where artno = ?";
	private static final String UPDATE =       //更新該表格所有欄位
	"UPDATE Report set  artno=?, memno=?, repcon=?, reptime=? where repno = ?";

	
	
	@Override
	public void insertrep(RepVO repVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);
		 
			pstmt.setInt(1, repVO.getArtno());
			pstmt.setInt(2, repVO.getMemno());
			pstmt.setString(3, repVO.getRepcon());
			
			 
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	@Override
	public void update(RepVO repVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			//更新資料須按照SQL指令欄位順序排列 			
			pstmt.setInt(1, repVO.getArtno());
			pstmt.setInt(2, repVO.getMemno());
			pstmt.setString(3, repVO.getRepcon());
			pstmt.setTimestamp(4, repVO.getReptime());
			pstmt.setInt(5, repVO.getRepno());
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	@Override
	public void delete(Integer artno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, artno);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	@Override
	public RepVO findByPrimaryKey(Integer repno) {
		RepVO repVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, repno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// repVO 也稱為 Domain objects
				repVO = new RepVO();
				repVO.setRepno(rs.getInt("repno"));
				repVO.setArtno(rs.getInt("artno"));
				repVO.setMemno(rs.getInt("memno")); 
				repVO.setRepcon(rs.getString("repcon"));
				repVO.setReptime(rs.getTimestamp("reptime"));												 				 
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return repVO;	
	}
	
	@Override
	public List<RepVO> getAll() {
		List<RepVO> list = new ArrayList<RepVO>();
		RepVO repVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				repVO = new RepVO();
				repVO.setRepno(rs.getInt("repno"));
				repVO.setArtno(rs.getInt("artno"));
				repVO.setMemno(rs.getInt("memno")); 
				repVO.setRepcon(rs.getString("repcon"));
				repVO.setReptime(rs.getTimestamp("reptime"));
				list.add(repVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
		
	}
	public static void main(String[] args) {

		RepJDBCDAO dao = new RepJDBCDAO();

//		// 新增
//		RepVO repVO1 = new RepVO();		 
//		repVO1.setArtno(170002);
//		repVO1.setMemno(90005);
//		repVO1.setRepcon("字太醜");
//		repVO1.setReptime(java.sql.Timestamp.valueOf("2014-10-31 15:00:00"));		 
//		dao.insert(repVO1);
		 
	    
//		// 修改
//		RepVO repVO2 = new RepVO();	
//		repVO2.setRepno(180005);
//		repVO2.setArtno(170005);
//		repVO2.setMemno(90005);
//		repVO2.setRepcon("字太小");
//		repVO2.setReptime(java.sql.Timestamp.valueOf("2014-10-31 10:00:00"));		
//		dao.update(repVO2);
		
//		// 刪除
//		dao.delete(180005);

		// 查詢
//		RepVO repVO3 = dao.findByPrimaryKey(180001);
//		System.out.print(repVO3.getRepno() + ",");
//		System.out.print(repVO3.getArtno() + ",");
//		System.out.print(repVO3.getMemno() + ",");
//		System.out.print(repVO3.getRepcon() + ",");
//		System.out.print(repVO3.getReptime() + ",");	 
//		System.out.println("---------------------");

		// 查詢
//		List<RepVO> list = dao.getAll();
//		for (RepVO repVO4 : list) {
//			System.out.print(repVO4.getRepno() + ",");
//			System.out.print(repVO4.getArtno() + ",");
//			System.out.print(repVO4.getMemno() + ",");
//			System.out.print(repVO4.getRepcon() + ",");
//			System.out.print(repVO4.getReptime() + ",");
//			System.out.println();
//		} 
		 
//		RepVO repVO1 = new RepVO();		 
//		repVO1.setArtno(170002);
//		repVO1.setMemno(90005);
//		repVO1.setRepcon("字太醜");
//		//repVO1.setReptime(java.sql.Timestamp.valueOf("2014-10-31 15:00:00"));		 
//		dao.insertrep(repVO1);
//		
//	
		
		
		
	   }
	
	
	
	
	
	
}
