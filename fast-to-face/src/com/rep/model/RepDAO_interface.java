package com.rep.model;

import java.util.List;

public interface RepDAO_interface {
	public void insertrep(RepVO repVO);
	public void update(RepVO repVO);
	public void delete(Integer artno);
	public RepVO findByPrimaryKey(Integer repno);
	public List<RepVO> getAll();

}
