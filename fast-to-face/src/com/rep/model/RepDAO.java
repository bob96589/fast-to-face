package com.rep.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class RepDAO implements RepDAO_interface{
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/TestDB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	private static final String INSERT_STMT =  //輸入資料
	"INSERT INTO Report ( repno,artno,memno,repcon,reptime) VALUES (REP_SEQ.NEXTVAL,?,?,?,sysdate)";
	private static final String GET_ALL_STMT = //查詢該表格全部欄位
	"SELECT * FROM Report  where artno  in (SELECT artno from  Article where artState != 0) order by repno desc";
//	"SELECT repno,artno,memno,repcon,reptime FROM Report order by repno desc";
	private static final String GET_ONE_STMT = //根據該欄位查詢資料
	"SELECT * FROM Report where repno = ?";
	private static final String DELETE =       //復原檢舉後的文章,並刪除檢舉表中記錄
	"DELETE FROM Report where artno =?";
	private static final String UPDATE =       //更新該表格所有欄位
	"UPDATE Report set  artno=?, memno=?, repcon=?, reptime=? where repno = ?";
	
	@Override
	public void insertrep(RepVO repVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);
		 
			pstmt.setInt(1, repVO.getArtno());
			pstmt.setInt(2, repVO.getMemno());
			pstmt.setString(3, repVO.getRepcon());
			 
			 
			pstmt.executeUpdate();			
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	@Override
	public void update(RepVO repVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			//更新資料須按照SQL指令欄位順序排列 			
			pstmt.setInt(1, repVO.getArtno());
			pstmt.setInt(2, repVO.getMemno());
			pstmt.setString(3, repVO.getRepcon());
			pstmt.setTimestamp(4, repVO.getReptime());
			pstmt.setInt(5, repVO.getRepno());
			pstmt.executeUpdate();
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	@Override
	public void delete(Integer artno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, artno);

			pstmt.executeUpdate();
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	@Override
	public RepVO findByPrimaryKey(Integer repno) {
		RepVO repVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);
			pstmt.setInt(1, repno);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// repVO 也稱為 Domain objects
				repVO = new RepVO();
				repVO.setRepno(rs.getInt("repno"));
				repVO.setArtno(rs.getInt("artno"));
				repVO.setMemno(rs.getInt("memno")); 
				repVO.setRepcon(rs.getString("repcon"));
				repVO.setReptime(rs.getTimestamp("reptime"));												 				 
			}
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return repVO;	
	}
	
	@Override
	public List<RepVO> getAll() {
		List<RepVO> list = new ArrayList<RepVO>();
		RepVO repVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				repVO = new RepVO();
				repVO.setRepno(rs.getInt("repno"));
				repVO.setArtno(rs.getInt("artno"));
				repVO.setMemno(rs.getInt("memno")); 
				repVO.setRepcon(rs.getString("repcon"));
				repVO.setReptime(rs.getTimestamp("reptime"));
				list.add(repVO); // Store the row in the list
			}
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
		
	}
}
