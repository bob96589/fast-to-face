package com.rep.model;

import java.sql.Timestamp;

public class RepVO implements java.io.Serializable{
	
	private Integer    repno ;
	private Integer    artno ;
	private Integer    memno ;	 
	private String     repcon ;
	private Timestamp  reptime ;
	
	public Integer getRepno() {
		return repno;
	}
	public void setRepno(Integer repno) {
		this.repno = repno;
	}
	public Integer getArtno() {
		return artno;
	}
	public void setArtno(Integer artno) {
		this.artno = artno;
	}
	public Integer getMemno() {
		return memno;
	}
	public void setMemno(Integer memno) {
		this.memno = memno;
	}
	public String getRepcon() {
		return repcon;
	}
	public void setRepcon(String repcon) {
		this.repcon = repcon;
	}
	public Timestamp getReptime() {
		return reptime;
	}
	public void setReptime(Timestamp reptime) {
		this.reptime = reptime;
	}
	 
    
}
