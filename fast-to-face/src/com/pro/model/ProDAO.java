package com.pro.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.cou.model.CouVO;

public class ProDAO implements ProDAO_interface {
	
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/TestDB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private static final String INSERT_STMT = 
			"INSERT INTO program (prono, proname, prointro, propic, proprice, prodiscount, proishot, prostate, proupdate)"
			+ "VALUES (pro_seq.nextval, ?, ?, ?, ?, ?, 0, 0, sysdate)";
	private static final String GET_ALL_STMT = 
			"SELECT * FROM program where prostate != -1 order by proupdate desc, prono";
	private static final String GET_ONE_STMT = 
			"SELECT * FROM program where prono = ?";
	private static final String DELETE = 
			"UPDATE program set prostate = -1 where prono = ?";
	private static final String UPDATE = 
			"UPDATE program set proname=?,prointro=?,propic=?,proprice=?,prodiscount=?,prostate=? where prono = ?";
	
	private static final String UPDATE_HOT = 
			"UPDATE program set proishot=? where prono = ?";
	private static final String GET_COUS_BY_PRONO =
			"select * from course where prono = ? and coustate = 3";
	private static final String GET_PROS_BY_PROSTATE = 
			"select * from program where prostate = ?";
	private static final String GET_HOTPROS = 
			"select * from program where proishot = 1 and prostate = 1";
	
	
	@Override
	public Integer insert(ProVO proVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		Integer keyno = null;

		try {

			con = ds.getConnection();
			String[] cols = {"prono"};
			pstmt = con.prepareStatement(INSERT_STMT,cols);
						
			pstmt.setString(1, proVO.getProname());
			pstmt.setString(2, proVO.getProintro());
			pstmt.setBytes(3, proVO.getPropic());
			pstmt.setInt(4, proVO.getProprice());
			pstmt.setInt(5, proVO.getProdiscount());			

			pstmt.executeUpdate();
			
			ResultSet rsKeys =pstmt.getGeneratedKeys();
			if(rsKeys.next()){
				keyno = rsKeys.getInt(1);				
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		}  finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return keyno;
	}
	
	@Override
	public void update(ProVO proVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, proVO.getProname());
			pstmt.setString(2, proVO.getProintro());
			pstmt.setBytes(3, proVO.getPropic());
			pstmt.setInt(4, proVO.getProprice());
			pstmt.setInt(5, proVO.getProdiscount());			
			pstmt.setInt(6, proVO.getProstate());			
			pstmt.setInt(7, proVO.getProno());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	
	@Override
	public void delete(Integer prono) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, prono);
			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	
	@Override
	public ProVO findByPrimaryKey(Integer prono) {
		ProVO proVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, prono);

			rs = pstmt.executeQuery();
			
			while (rs.next()) {						
				proVO = new ProVO();
				proVO.setProno(rs.getInt("prono"));
				proVO.setProname(rs.getString("proname"));
				proVO.setProintro(rs.getString("prointro"));
				proVO.setPropic(rs.getBytes("propic"));
				proVO.setProprice(rs.getInt("proprice"));
				proVO.setProdiscount(rs.getInt("prodiscount"));
				proVO.setProishot(rs.getInt("proishot"));
				proVO.setProstate(rs.getInt("prostate"));
				proVO.setProupdate(rs.getTimestamp("proupdate"));
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return proVO;
	}
	
	@Override
	public List<ProVO> getAll() {
		List<ProVO> list = new ArrayList<ProVO>();
		ProVO proVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				proVO = new ProVO();
				proVO.setProno(rs.getInt("prono"));
				proVO.setProname(rs.getString("proname"));
				proVO.setProintro(rs.getString("prointro"));
				proVO.setPropic(rs.getBytes("propic"));
				proVO.setProprice(rs.getInt("proprice"));
				proVO.setProdiscount(rs.getInt("prodiscount"));
				proVO.setProishot(rs.getInt("proishot"));
				proVO.setProstate(rs.getInt("prostate"));
				proVO.setProupdate(rs.getTimestamp("proupdate"));				
				list.add(proVO);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
		
	
	
	@Override
	public void updateHot(Integer prono, Integer hot) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_HOT);
			
			pstmt.setInt(1, hot);	
			pstmt.setInt(2, prono);			
			
			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	
	@Override
	public Set<CouVO> getCousByProno(Integer prono) {
		Set<CouVO> set = new LinkedHashSet<CouVO>();
		CouVO couVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;		
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_COUS_BY_PRONO);
			pstmt.setInt(1, prono);
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				couVO = new CouVO();
				couVO.setCouno(rs.getInt("couno"));
				couVO.setCouname(rs.getString("couname"));
				couVO.setCouintro(rs.getString("couintro"));
				couVO.setCoupic(rs.getBytes("coupic"));
				couVO.setCoubadge(rs.getBytes("coubadge"));
				couVO.setCouprice(rs.getInt("couprice"));
				couVO.setCoudiscount(rs.getInt("coudiscount"));
				couVO.setCouishot(rs.getInt("couishot"));
				couVO.setAccno(rs.getInt("accno"));
				couVO.setCoustate(rs.getInt("coustate"));
				couVO.setProno(rs.getInt("prono"));
				couVO.setCouupdate(rs.getTimestamp("couupdate"));
				set.add(couVO);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}

	@Override
	public Set<ProVO> getProsByProstate(Integer prostate) {
		Set<ProVO> set = new LinkedHashSet<ProVO>();
		ProVO proVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_PROS_BY_PROSTATE);
			pstmt.setInt(1, prostate);
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				proVO = new ProVO();
				proVO.setProno(rs.getInt("prono"));
				proVO.setProname(rs.getString("proname"));
				proVO.setProintro(rs.getString("prointro"));
				proVO.setPropic(rs.getBytes("propic"));
				proVO.setProprice(rs.getInt("proprice"));
				proVO.setProdiscount(rs.getInt("prodiscount"));
				proVO.setProishot(rs.getInt("proishot"));
				proVO.setProstate(rs.getInt("prostate"));
				proVO.setProupdate(rs.getTimestamp("proupdate"));				
				set.add(proVO);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}
	
	
	
	@Override
	public Set<ProVO> getHotPros() {
		Set<ProVO> set = new LinkedHashSet<ProVO>();
		ProVO proVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_HOTPROS);
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				proVO = new ProVO();
				proVO.setProno(rs.getInt("prono"));
				proVO.setProname(rs.getString("proname"));
				proVO.setProintro(rs.getString("prointro"));
				proVO.setPropic(rs.getBytes("propic"));
				proVO.setProprice(rs.getInt("proprice"));
				proVO.setProdiscount(rs.getInt("prodiscount"));
				proVO.setProishot(rs.getInt("proishot"));
				proVO.setProstate(rs.getInt("prostate"));
				proVO.setProupdate(rs.getTimestamp("proupdate"));				
				set.add(proVO);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}
	
	
}
