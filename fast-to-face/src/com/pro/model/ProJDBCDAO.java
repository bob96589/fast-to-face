package com.pro.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.cou.model.CouVO;

public class ProJDBCDAO implements ProDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:xe";
	String userid = "user1";
	String passwd = "u111";
	
	private static final String INSERT_STMT = 
			"INSERT INTO program (prono, proname, prointro, propic, proprice, prodiscount, proishot, prostate, proupdate)"
			+ "VALUES (pro_seq.nextval, ?, ?, ?, ?, ?, 0, 0, sysdate)";
	private static final String GET_ALL_STMT = 
			"SELECT * FROM program where prostate != -1 order by proupdate desc, prono";
	private static final String GET_ONE_STMT = 
			"SELECT * FROM program where prono = ?";
	private static final String DELETE = 
			"UPDATE program set prostate = -1 where prono = ?";
	private static final String UPDATE = 
			"UPDATE program set proname=?,prointro=?,propic=?,proprice=?,prodiscount=?,prostate=? where prono = ?";
	
	private static final String UPDATE_HOT = 
			"UPDATE program set proishot=? where prono = ?";
	private static final String GET_COUS_BY_PRONO =
			"select * from course where prono = ? and coustate = 3";
	private static final String GET_PROS_BY_PROSTATE = 
			"select * from program where prostate = ?";
	private static final String GET_HOTPROS = 
			"select * from program where proishot = 1 and prostate = 1";
	
	
	@Override
	public Integer insert(ProVO proVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		Integer keyno = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			String[] cols = {"prono"};
			pstmt = con.prepareStatement(INSERT_STMT,cols);
						
			pstmt.setString(1, proVO.getProname());
			pstmt.setString(2, proVO.getProintro());
			pstmt.setBytes(3, proVO.getPropic());
			pstmt.setInt(4, proVO.getProprice());
			pstmt.setInt(5, proVO.getProdiscount());			

			pstmt.executeUpdate();
			
			ResultSet rsKeys =pstmt.getGeneratedKeys();
			if(rsKeys.next()){
				keyno = rsKeys.getInt(1);				
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		}  finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return keyno;
	}
	
	@Override
	public void update(ProVO proVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, proVO.getProname());
			pstmt.setString(2, proVO.getProintro());
			pstmt.setBytes(3, proVO.getPropic());
			pstmt.setInt(4, proVO.getProprice());
			pstmt.setInt(5, proVO.getProdiscount());			
			pstmt.setInt(6, proVO.getProstate());			
			pstmt.setInt(7, proVO.getProno());

			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	
	@Override
	public void delete(Integer prono) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, prono);
			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	
	@Override
	public ProVO findByPrimaryKey(Integer prono) {
		ProVO proVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, prono);

			rs = pstmt.executeQuery();
			
			while (rs.next()) {						
				proVO = new ProVO();
				proVO.setProno(rs.getInt("prono"));
				proVO.setProname(rs.getString("proname"));
				proVO.setProintro(rs.getString("prointro"));
				proVO.setPropic(rs.getBytes("propic"));
				proVO.setProprice(rs.getInt("proprice"));
				proVO.setProdiscount(rs.getInt("prodiscount"));
				proVO.setProishot(rs.getInt("proishot"));
				proVO.setProstate(rs.getInt("prostate"));
				proVO.setProupdate(rs.getTimestamp("proupdate"));
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return proVO;
	}
	
	@Override
	public List<ProVO> getAll() {
		List<ProVO> list = new ArrayList<ProVO>();
		ProVO proVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				proVO = new ProVO();
				proVO.setProno(rs.getInt("prono"));
				proVO.setProname(rs.getString("proname"));
				proVO.setProintro(rs.getString("prointro"));
				proVO.setPropic(rs.getBytes("propic"));
				proVO.setProprice(rs.getInt("proprice"));
				proVO.setProdiscount(rs.getInt("prodiscount"));
				proVO.setProishot(rs.getInt("proishot"));
				proVO.setProstate(rs.getInt("prostate"));
				proVO.setProupdate(rs.getTimestamp("proupdate"));				
				list.add(proVO);
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public void updateHot(Integer prono, Integer hot) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE_HOT);
			
			pstmt.setInt(1, hot);	
			pstmt.setInt(2, prono);			
			
			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	
	@Override
	public Set<CouVO> getCousByProno(Integer prono) {
		Set<CouVO> set = new LinkedHashSet<CouVO>();
		CouVO couVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;		
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_COUS_BY_PRONO);
			pstmt.setInt(1, prono);
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				couVO = new CouVO();
				couVO.setCouno(rs.getInt("couno"));
				couVO.setCouname(rs.getString("couname"));
				couVO.setCouintro(rs.getString("couintro"));
				couVO.setCoupic(rs.getBytes("coupic"));
				couVO.setCoubadge(rs.getBytes("coubadge"));
				couVO.setCouprice(rs.getInt("couprice"));
				couVO.setCoudiscount(rs.getInt("coudiscount"));
				couVO.setCouishot(rs.getInt("couishot"));
				couVO.setAccno(rs.getInt("accno"));
				couVO.setCoustate(rs.getInt("coustate"));
				couVO.setProno(rs.getInt("prono"));
				couVO.setCouupdate(rs.getTimestamp("couupdate"));
				set.add(couVO);
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}

	@Override
	public Set<ProVO> getProsByProstate(Integer prostate) {
		Set<ProVO> set = new LinkedHashSet<ProVO>();
		ProVO proVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_PROS_BY_PROSTATE);
			pstmt.setInt(1, prostate);
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				proVO = new ProVO();
				proVO.setProno(rs.getInt("prono"));
				proVO.setProname(rs.getString("proname"));
				proVO.setProintro(rs.getString("prointro"));
				proVO.setPropic(rs.getBytes("propic"));
				proVO.setProprice(rs.getInt("proprice"));
				proVO.setProdiscount(rs.getInt("prodiscount"));
				proVO.setProishot(rs.getInt("proishot"));
				proVO.setProstate(rs.getInt("prostate"));
				proVO.setProupdate(rs.getTimestamp("proupdate"));				
				set.add(proVO);
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}
	
	@Override
	public Set<ProVO> getHotPros() {
		Set<ProVO> set = new LinkedHashSet<ProVO>();
		ProVO proVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_HOTPROS);
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				proVO = new ProVO();
				proVO.setProno(rs.getInt("prono"));
				proVO.setProname(rs.getString("proname"));
				proVO.setProintro(rs.getString("prointro"));
				proVO.setPropic(rs.getBytes("propic"));
				proVO.setProprice(rs.getInt("proprice"));
				proVO.setProdiscount(rs.getInt("prodiscount"));
				proVO.setProishot(rs.getInt("proishot"));
				proVO.setProstate(rs.getInt("prostate"));
				proVO.setProupdate(rs.getTimestamp("proupdate"));				
				set.add(proVO);
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}
	
	
	
	
	public static void main(String[] args) {
		
		ProJDBCDAO dao = new ProJDBCDAO();
		
//		try {
//			
//		
//			ProVO vo1 = new ProVO();
//			
//			FileInputStream in = new FileInputStream(new File("C:/pic/20002.png"));
//			byte[] buffer = new byte[in.available()];
//			in.read(buffer);
//			
//			vo1.setProname("�d�ç�2");
//			vo1.setProintro("MANAGER2");			
//			vo1.setPropic(buffer);
//			vo1.setProprice(123);
//			vo1.setProdiscount(345);		
//			dao.insert(vo1);
//			in.close();
//
//		
//			ProVO vo2 = new ProVO();
//			
//			FileInputStream in2 = new FileInputStream(new File("C:/pic/222.jpg"));
//			byte[] buffer2 = new byte[in2.available()];
//			in2.read(buffer2);
//			
//			vo2.setProno(20006);
//			vo2.setProname("�d�ç�2");
//			vo2.setProintro("MANAGER2");
//			vo2.setPropic(buffer2);
//			vo2.setProprice(222);
//			vo2.setProdiscount(222);		
//			vo2.setProstate(1);			
//			dao.update(vo2);
//			in2.close();
//			
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}			
//
//			
//		
//		
//			
//		dao.delete(20004);
//			
//	
//		ProVO vo = dao.findByPrimaryKey(20001);
//		System.out.println(vo.getProno());
//		System.out.println(vo.getProname());
//		System.out.println(vo.getProintro());
//		System.out.println(vo.getProprice());
//		System.out.println(vo.getProdiscount());
//		System.out.println(vo.getProishot());
//		System.out.println(vo.getProstate());
//		System.out.println(vo.getProupdate());
//		System.out.println("---------------------");
//		
//	
//		List<ProVO> list = dao.getAll();
//		for (ProVO p : list) {
//			System.out.println(p.getProno());
//			System.out.println(p.getProname());
//			System.out.println(p.getProintro());
//			System.out.println(p.getProprice());
//			System.out.println(p.getProdiscount());
//			System.out.println(p.getProishot());
//			System.out.println(p.getProstate());
//			System.out.println(p.getProupdate());
//			System.out.println("---------------------");
//		}
		
		
//		dao.updateHot(20006, 1);
		
//		List<CouVO> couList = dao.getCousByProno(20003);
//		for (CouVO p : couList) {
//			System.out.println(p.getCouno());
//			System.out.println(p.getCouname());
//			System.out.println(p.getCouintro());
//			System.out.println(p.getCouprice());
//			System.out.println(p.getCoudiscount());
//			System.out.println(p.getCouishot());
//			System.out.println(p.getAccno());
//			System.out.println(p.getCoustate());
//			System.out.println(p.getProno());
//			System.out.println(p.getCouupdate());
//			System.out.println(p.getCouorder());
//			System.out.println("---------------------");
//		}
		
//		Set<ProVO> proSet = dao.getProsByProstate(1);
//		for (ProVO p : proSet) {
//			System.out.println(p.getProno());
//			System.out.println(p.getProname());
//			System.out.println(p.getProintro());
//			System.out.println(p.getProprice());
//			System.out.println(p.getProdiscount());
//			System.out.println(p.getProishot());
//			System.out.println(p.getProstate());
//			System.out.println(p.getProupdate());
//			System.out.println("---------------------");
//		}
		
		
		
		Set<ProVO> set = dao.getHotPros();
		for (ProVO p : set) {
			System.out.println(p.getProno());
			System.out.println(p.getProname());
			System.out.println(p.getProintro());
			System.out.println(p.getProprice());
			System.out.println(p.getProdiscount());
			System.out.println(p.getProishot());
			System.out.println(p.getProstate());
			System.out.println(p.getProupdate());
			System.out.println("---------------------");
		}
		
	}



	
	
}
