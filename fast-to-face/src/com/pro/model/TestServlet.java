package com.pro.model;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		ProService ser = new ProService();
		ProVO vo = ser.getOnePro(20001);
		res.setContentType("text/plain");
	    PrintWriter out = res.getWriter();
	    
	    out.println("");
	    System.out.println(vo.getProno());
		System.out.println(vo.getProname());
		System.out.println(vo.getProintro());
		System.out.println(vo.getProprice());
		System.out.println(vo.getProdiscount());
		System.out.println(vo.getProishot());
		System.out.println(vo.getProstate());
		System.out.println(vo.getProupdate());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
