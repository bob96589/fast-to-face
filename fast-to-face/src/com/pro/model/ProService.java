package com.pro.model;

import java.util.List;
import java.util.Set;

import com.cou.model.CouVO;

public class ProService {
	private ProDAO_interface dao;

	public ProService() {
		dao = new ProDAO();
	}

	public ProVO addPro(String proname, String prointro, byte[] propic, Integer proprice, Integer prodiscount) {

		ProVO proVO = new ProVO();

		proVO.setProname(proname);
		proVO.setProintro(prointro);			
		proVO.setPropic(propic);
		proVO.setProprice(proprice);
		proVO.setProdiscount(prodiscount);
		
		Integer prono = dao.insert(proVO);
		proVO.setProno(prono);

		return proVO;
	}

	public ProVO updatePro(Integer prono, String proname, String prointro, byte[] propic,
			Integer proprice, Integer prodiscount, Integer prostate) {

		ProVO proVO = new ProVO();

		proVO.setProno(prono);
		proVO.setProname(proname);
		proVO.setProintro(prointro);			
		proVO.setPropic(propic);
		proVO.setProprice(proprice);
		proVO.setProdiscount(prodiscount);
		proVO.setProstate(prostate);
		dao.update(proVO);

		return proVO;
	}

	public void deletePro(Integer prono) {
		dao.delete(prono);
	}

	public ProVO getOnePro(Integer prono) {
		return dao.findByPrimaryKey(prono);
	}

	public List<ProVO> getAll() {
		return dao.getAll();
	}
	

	
	public void updateHot(Integer prono, Integer hot) {
		dao.updateHot(prono, hot);
	}
	
	public Set<CouVO> getCousByProno(Integer prono){
		return dao.getCousByProno(prono);		
	}
	
	public Set<ProVO> getProsByProstate(Integer prostate){
		return dao.getProsByProstate(prostate);		
	}
	
	public Set<ProVO> getHotPros(){
		return dao.getHotPros();	
	}
	
}
