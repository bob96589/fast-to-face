package com.pro.model;

import java.util.List;
import java.util.Set;

import com.cou.model.CouVO;

public interface ProDAO_interface {
	public Integer insert(ProVO proVO);
    public void update(ProVO proVO);
    public void delete(Integer prono);
    public ProVO findByPrimaryKey(Integer prono);
    public List<ProVO> getAll();
    
    public void updateHot(Integer prono, Integer hot);
    public Set<CouVO> getCousByProno(Integer prono);
    public Set<ProVO> getProsByProstate(Integer prostate);
    public Set<ProVO> getHotPros();

}
