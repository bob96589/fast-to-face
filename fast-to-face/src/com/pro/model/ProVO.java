package com.pro.model;

import java.sql.Timestamp;

public class ProVO  implements java.io.Serializable {
	private Integer prono;
	private String proname;
	private String prointro;
	private byte[] propic;
	private Integer proprice;
	private Integer prodiscount;
	private Integer proishot;
	private Integer prostate;
	private Timestamp proupdate;
	public Integer getProno() {
		return prono;
	}
	public void setProno(Integer prono) {
		this.prono = prono;
	}
	public String getProname() {
		return proname;
	}
	public void setProname(String proname) {
		this.proname = proname;
	}
	public String getProintro() {
		return prointro;
	}
	public void setProintro(String prointro) {
		this.prointro = prointro;
	}
	public byte[] getPropic() {
		return propic;
	}
	public void setPropic(byte[] propic) {
		this.propic = propic;
	}
	public Integer getProprice() {
		return proprice;
	}
	public void setProprice(Integer proprice) {
		this.proprice = proprice;
	}
	public Integer getProdiscount() {
		return prodiscount;
	}
	public void setProdiscount(Integer prodiscount) {
		this.prodiscount = prodiscount;
	}
	public Integer getProishot() {
		return proishot;
	}
	public void setProishot(Integer proishot) {
		this.proishot = proishot;
	}
	public Integer getProstate() {
		return prostate;
	}
	public void setProstate(Integer prostate) {
		this.prostate = prostate;
	}
	public Timestamp getProupdate() {
		return proupdate;
	}
	public void setProupdate(Timestamp proupdate) {
		this.proupdate = proupdate;
	}
	
	

}
