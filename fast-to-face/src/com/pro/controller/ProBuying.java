package com.pro.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.buy.model.BuyService;
import com.buy.model.BuyVO;
import com.content.model.ConVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.fin.model.FinService;
import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.pro.model.ProService;
import com.pro.model.ProVO;
import com.unit.model.UnitService;
import com.unit.model.UnitVO;

public class ProBuying extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);		
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
	
		if ("getOneForBuying".equals(action)) { 
			
			
			//reset memVO in session
			HttpSession session = req.getSession();			
			MemVO memVObj = (MemVO)session.getAttribute("memVO");
			MemService memSvc = new MemService();
			MemVO memVO = memSvc.getOneMem(memVObj.getMemno());
			session.setAttribute("memVO", memVO);
			
			try {
				/***************************1.接收請求參數****************************************/
				Integer prono = new Integer(req.getParameter("prono"));				
				
				/***************************2.開始查詢資料****************************************/
				//CouVO
				ProService proSvc = new ProService();
				ProVO proVO = proSvc.getOnePro(prono);
				Set<CouVO> couVOSet = proSvc.getCousByProno(prono);				
				
				//return the course already bought
				Set<CouVO> boughtCouVO = boughtYet(memVO,couVOSet);				
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("proVO", proVO);						
				req.setAttribute("couVOSet", couVOSet);						
				req.setAttribute("boughtCouVO", boughtCouVO);						
				String url = "/front/program_detail/proBuying.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				

				/***************************其他可能的錯誤處理************************************/
			} catch (Exception e) {				
				RequestDispatcher failureView = req.getRequestDispatcher("/front/program_detail/proDetail.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("buying".equals(action)) { 		
			
			String requestURL = req.getParameter("requestURL");			
			HttpSession session = req.getSession();	
			
			MemVO memVO = (MemVO)session.getAttribute("memVO");
			
			try {
				/***************************1.接收請求參數****************************************/
				Integer prono = new Integer(req.getParameter("prono"));				
				
				/***************************2.開始查詢資料****************************************/
				
				ProService proSvc = new ProService();
				ProVO proVO = proSvc.getOnePro(prono);
				Set<CouVO> couVOSet = proSvc.getCousByProno(prono);
				
				CouService couSvc = new CouService();
				
				//pay
				MemService memSvc = new MemService();
				memSvc.updateMempointByMemno(memVO.getMempoint() - proVO.getProdiscount() ,memVO.getMemno());
				
				int i = 1;
				for(CouVO couVO : couVOSet){
					//add buy record
					BuyService buySvc = new BuyService();
					if(i == 1)
						buySvc.addBuy(proVO.getProdiscount(), memVO.getMemno(), couVO.getCouno());
					else
						buySvc.addBuy(0, memVO.getMemno(), couVO.getCouno());							
					//add finish record
					FinService finSvc = new FinService();
					Set<UnitVO> unitVOSet = couSvc.getUnitsByCouno(couVO.getCouno());
					for(UnitVO unitVO : unitVOSet){
						UnitService unitSvc = new UnitService();
						Set<ConVO> conVOSet = unitSvc.getConsByUnitno(unitVO.getUnitno());
						for(ConVO conVO : conVOSet){
							try {
								finSvc.addFin(memVO.getMemno(), conVO.getConno());
							} catch (Exception e) {}
						}					
					}
					i++;
				}	
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				
				//reset memVO in session
				memVO = memSvc.getOneMem(memVO.getMemno());
				session.setAttribute("memVO", memVO);
				
				String url = "/coulist/cou.do?action=getMyCouList";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理************************************/
			} catch (Exception e) {
				RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
				failureView.forward(req, res);
			}
		}
		
	}
	
	private Set<CouVO> boughtYet(MemVO memVO, Set<CouVO> couVOSet){
		Set<CouVO> boughtCouVO = new LinkedHashSet<>();		
		//guest or member				
		if (memVO != null) {
			MemService memSvc = new MemService();
			Set<BuyVO> buyVOSet = memSvc.getBuysByMemno(memVO.getMemno());					
			//member or buy member					
			for(CouVO couVO : couVOSet){
				boolean bought = false;
				for (BuyVO buyVO : buyVOSet) {
					if (buyVO.getCouno().equals(couVO.getCouno())) bought = true;							
				}
				if(bought) 
					boughtCouVO.add(couVO);
			}										
		}
		return boughtCouVO;	
		
	}
  

}
