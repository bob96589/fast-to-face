package com.pro.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.acc.model.AccService;
import com.acc.model.AccVO;
import com.buy.model.BuyService;
import com.buy.model.BuyVO;
import com.content.model.ConVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.fin.model.FinService;
import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.oreilly.servlet.MultipartRequest;
import com.pro.model.*;
import com.sto.model.StoVO;
import com.unit.model.UnitService;
import com.unit.model.UnitVO;


public class ProServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		if ("getPropic".equals(action)) { 
			

				/***************************1.接收請求參數****************************************/
				Integer prono = new Integer(req.getParameter("prono"));
				
				/***************************2.開始查詢資料****************************************/
				ProService proSvc = new ProService();
				ProVO proVO = proSvc.getOnePro(prono);
				
				byte[] propic = proVO.getPropic();
				
				res.setContentType("image/png");
				ServletOutputStream out = res.getOutputStream();
				out.write(propic);
				out.flush();								

		}		
	}
}
