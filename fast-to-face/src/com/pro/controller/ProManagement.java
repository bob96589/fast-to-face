package com.pro.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.acc.model.AccService;
import com.acc.model.AccVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.oreilly.servlet.MultipartRequest;
import com.pro.model.ProService;
import com.pro.model.ProVO;

public class ProManagement extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		doPost(req, res);	
		
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		String contentType = req.getContentType();
		String action = null;
		MultipartRequest multi = null;
		if(contentType!=null && contentType.startsWith("multipart/form-data")){
			multi = new MultipartRequest(req, getServletContext().getRealPath("/shared/temp"), 5*1024*1024, "UTF-8");			
			action = multi.getParameter("action");				
		}else{
			req.setCharacterEncoding("UTF-8");
			action = req.getParameter("action");			
		}
		
		
		
		if ("getProList".equals(action)) { 
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");					
			String whichPage = req.getParameter("whichPage");					
			
			try {
				
				ProService proSvc = new ProService();
				List<ProVO> proVOList = proSvc.getAll();
											
				req.setAttribute("proVOList", proVOList);						
				req.setAttribute("requestURL", requestURL);						
				req.setAttribute("whichPage", whichPage);						
				String url = "/back/program_management/listAllPro.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);			

				/***************************其他可能的錯誤處理************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料取出時失敗:"+e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
				failureView.forward(req, res);
			}
		}
		
		
		if ("insert".equals(action)) { 				
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
				
				//proname
				String proname = multi.getParameter("proname");
				if (proname == null || (proname.trim()).length() == 0) {
					errorMsgs.add("請輸入學程名稱");
				}					
				
				//prointro
				String prointro = multi.getParameter("prointro");
				if (prointro == null || (prointro.trim()).length() == 0) {
					errorMsgs.add("請輸入學程簡介");
				}
									
				//propic
				File currentFile = multi.getFile("propic");
				byte[] buffer = null;
				if(currentFile != null){						
					InputStream in = new FileInputStream(currentFile);
					buffer = new byte[in.available()];
					in.read(buffer);					
					currentFile.delete();
					in.close();
				}else{					
					errorMsgs.add("請選擇檔案");						
				}						
				
				//proprice					
				Integer proprice = null;
				try {
					proprice = new Integer(multi.getParameter("proprice").trim());
				} catch (NumberFormatException e) {
					proprice = 0;
					errorMsgs.add("原價請填數字");
				}
				
				//prodiscount					
				Integer prodiscount = null;
				try {
					prodiscount = new Integer(multi.getParameter("prodiscount").trim());
				} catch (NumberFormatException e) {
					prodiscount = 0;
					errorMsgs.add("特價請填數字");
				}										

				ProVO proVO = new ProVO();
				proVO.setProname(proname);
				proVO.setProintro(prointro);
				proVO.setPropic(buffer);
				proVO.setProprice(proprice);
				proVO.setProdiscount(prodiscount);					

				if (!errorMsgs.isEmpty()) {
					req.setAttribute("proVO", proVO); 
					RequestDispatcher failureView = req.getRequestDispatcher("/back/program_management/addPro.jsp");
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				ProService proSvc = new ProService();
				proVO = proSvc.addPro(proname, prointro, buffer, proprice, prodiscount);
				
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				
				List<ProVO> proVOList = proSvc.getAll();
											
				req.setAttribute("proVOList", proVOList);						
				req.setAttribute("proVO", proVO);						
				
				String url = "/back/program_management/listAllPro.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
				successView.forward(req, res);				
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/back/program_management/addPro.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		
		
		if ("getOne_For_Update".equals(action)) { 
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");
			String whichPage = req.getParameter("whichPage");
						
			try {
				/***************************1.接收請求參數****************************************/
				Integer prono = new Integer(req.getParameter("prono"));
				
				/***************************2.開始查詢資料****************************************/
				ProService proSvc = new ProService();
				ProVO proVO = proSvc.getOnePro(prono);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("proVO", proVO);
				req.setAttribute("requestURL", requestURL);
				req.setAttribute("whichPage", whichPage);
				String url = "/back/program_management/update_pro_input.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交update_emp_input.jsp
				successView.forward(req, res);
				

				/***************************其他可能的錯誤處理************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料取出時失敗:"+e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
				failureView.forward(req, res);
			}
		}
		
		
		if ("update".equals(action)) { 
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = multi.getParameter("requestURL");
			String whichPage = multi.getParameter("whichPage");
		
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				//prono
				Integer prono = new Integer(multi.getParameter("prono").trim());
				
				//proname
				String proname = multi.getParameter("proname");
				if (proname == null || (proname.trim()).length() == 0) {
					errorMsgs.add("請輸入學程名稱");
				}
				
				//prointro
				String prointro = multi.getParameter("prointro");
				if (prointro == null || (prointro.trim()).length() == 0) {
					errorMsgs.add("請輸入學程簡介");
				}
									
				//propic
				
				File currentFile = multi.getFile("propic");
				byte[] buffer = null;
				if(currentFile != null){					
					InputStream in = new FileInputStream(currentFile);
					buffer = new byte[in.available()];
					in.read(buffer);					
					currentFile.delete();				
				}else{
					//read propic from DB
					ProService proSvc = new ProService();
					ProVO proVO = proSvc.getOnePro(prono);
					buffer = proVO.getPropic();				
				}						
				
				//proprice					
				Integer proprice = null;
				try {
					proprice = new Integer(multi.getParameter("proprice").trim());
				} catch (NumberFormatException e) {
					proprice = 0;
					errorMsgs.add("原價請填數字");
				}
				
				//prodiscount					
				Integer prodiscount = null;
				try {
					prodiscount = new Integer(multi.getParameter("prodiscount").trim());
				} catch (NumberFormatException e) {
					prodiscount = 0;
					errorMsgs.add("特價請填數字");
				}

				Integer prostate = new Integer(multi.getParameter("prostate").trim());

				ProVO proVO = new ProVO();
				
				proVO.setProno(prono);
				proVO.setProname(proname);
				proVO.setProintro(prointro);
				proVO.setPropic(buffer);
				proVO.setProprice(proprice);
				proVO.setProdiscount(prodiscount);
				proVO.setProstate(prostate);
				
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("proVO", proVO);
					req.setAttribute("requestURL", requestURL);
					req.setAttribute("whichPage", whichPage);
					RequestDispatcher failureView = req.getRequestDispatcher("/back/program_management/update_pro_input.jsp");
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始修改資料*****************************************/
				ProService proSvc = new ProService();
				proVO = proSvc.updatePro(prono, proname, prointro, buffer, proprice, prodiscount, prostate);
				
				/***************************3.修改完成,準備轉交(Send the Success view)*************/			
				
				List<ProVO> proVOList = proSvc.getAll();
											
				req.setAttribute("proVOList", proVOList);			
				req.setAttribute("proVO", proVO);
				
				String url = requestURL + "?whichPage=" + whichPage;
				RequestDispatcher successView = req.getRequestDispatcher(url); 
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/back/program_management/update_pro_input.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("delete".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");

			try {
				/***************************1.接收請求參數***************************************/
				Integer prono = new Integer(req.getParameter("prono"));
				
				/***************************2.開始刪除資料***************************************/
				ProService proSvc = new ProService();
				proSvc.deletePro(prono);
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/
				
				List<ProVO> proVOList = proSvc.getAll();
											
				req.setAttribute("proVOList", proVOList);						
				
				String url = requestURL;
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("刪除資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
				failureView.forward(req, res);
			}
		}
		
		
		
	}

}
