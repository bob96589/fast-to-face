package com.pro.controller;

import java.io.IOException;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pro.model.ProService;
import com.pro.model.ProVO;

public class HotProManagement extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);	
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		
		if ("getProList".equals(action)) {		
			
			ProService proSvc = new ProService();
			Set<ProVO> proVOSet = proSvc.getProsByProstate(1);
		
			req.setAttribute("proVOSet", proVOSet);			
			
			String url = "/back/hot_program_management/listAllOnlinePro.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);
			successView.forward(req, res);
			
		}
		
		
		if ("update_hot".equals(action)) { 
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");			
		
			try {
				
				ProService proSvc = new ProService();
				Enumeration en = req.getParameterNames();
				while(en.hasMoreElements()){
					String name = (String)en.nextElement();					
					if (name.startsWith("20")) {//排除非學程資料的參數ex:action, requestURL, whichPage
						String value = req.getParameter(name);//資料一定正確，不做錯誤處理
						proSvc.updateHot(Integer.parseInt(name), Integer.parseInt(value));//修改資料
					}	
				}
				
				Set<ProVO> proVOSet = proSvc.getProsByProstate(1);
			
				req.setAttribute("proVOSet", proVOSet);				
								
				String url = requestURL;
				RequestDispatcher successView = req.getRequestDispatcher(url); 
				successView.forward(req, res);

			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
				failureView.forward(req, res);
			}
		}
		
		
	}

}
