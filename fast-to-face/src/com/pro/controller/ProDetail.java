package com.pro.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.buy.model.BuyService;
import com.buy.model.BuyVO;
import com.content.model.ConVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.fin.model.FinService;
import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.pro.model.ProService;
import com.pro.model.ProVO;
import com.sto.model.StoVO;
import com.unit.model.UnitService;
import com.unit.model.UnitVO;

public class ProDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);		
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		if ("getProDetail".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			
			

			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				
				Integer prono = new Integer(req.getParameter("prono"));	
				
				/***************************2.開始查詢資料*****************************************/
				
				ProService proSvc = new ProService();
				ProVO proVO = proSvc.getOnePro(prono);
				Set<CouVO> couVOSet = proSvc.getCousByProno(prono);	
				
				CouService couSvc = new CouService();
				List<Double> averageAppscoreList = new ArrayList<Double>();
				for(CouVO couVO : couVOSet){
					Double averageAppscore = couSvc.getAverageAppscoreByCouno(couVO.getCouno());
					averageAppscoreList.add(averageAppscore);
				}
				
				HttpSession session = req.getSession();
				MemVO memVO = (MemVO)session.getAttribute("memVO");
				
				//return the course already bought
				Set<CouVO> boughtCouVO = boughtYet(memVO,couVOSet);
				
				if(couVOSet.size()==boughtCouVO.size()){
					req.setAttribute("alreadyBought", true);
				}
				/***************************3.查詢完成,準備轉交(Send the Success view)*************/
				
				req.setAttribute("proVO", proVO); 
				req.setAttribute("couVOSet", couVOSet);
				req.setAttribute("averageAppscoreList", averageAppscoreList);
				
				String url = "/front/program_detail/proDetail.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/unit/select_page.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		
		
	}
	
	private Set<CouVO> boughtYet(MemVO memVO, Set<CouVO> couVOSet){
		Set<CouVO> boughtCouVO = new LinkedHashSet<>();		
		//guest or member				
		if (memVO != null) {
			MemService memSvc = new MemService();
			Set<BuyVO> buyVOSet = memSvc.getBuysByMemno(memVO.getMemno());					
			//member or buy member					
			for(CouVO couVO : couVOSet){
				boolean bought = false;
				for (BuyVO buyVO : buyVOSet) {
					if (buyVO.getCouno().equals(couVO.getCouno())) bought = true;							
				}
				if(bought) 
					boughtCouVO.add(couVO);
			}										
		}
		return boughtCouVO;	
		
	}

}
