package com.back.controller;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.naming.java.javaURLContextFactory;

import java.sql.Timestamp;

import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.rep.model.RepService;
import com.top.model.*;
import com.art.model.ArtService;
import com.art.model.ArtVO;
import com.fro.model.*;

public class BackServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		//更新主題類別
	       if ("update".equals(action)) { // 來自back-upforum.jsp的請求
				
				List<String> errorMsgs = new LinkedList<String>();
				// Store this set in the request scope, in case we need to
				// send the ErrorPage view.
				req.setAttribute("errorMsgs", errorMsgs);
			
				try {
					/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/

					 Integer forno = new Integer(req.getParameter("forno"));	
				
					 String	fortype = req.getParameter("fortype").trim();
					    if (fortype == null || (fortype.trim()).length() == 0){
					    	errorMsgs.add("請輸入主題名稱");	
						}	 		    
				    ForVO forVO = new ForVO();				   
				    forVO.setFortype(fortype); 
				    forVO.setForno(forno); 
					// Send the use back to the form, if there were errors
					if (!errorMsgs.isEmpty()) {
						req.setAttribute("forVO", forVO); // 含有輸入格式錯誤的forVO物件,也存入req
						RequestDispatcher failureView = req
								.getRequestDispatcher("/back/back-mainx/back-mainx.jsp");
						failureView.forward(req, res);
						return; //程式中斷
					}
					
					/***************************2.開始修改資料*****************************************/
				        	ForService forSvc = new ForService();
					        forVO = forSvc.update( forno,fortype);
	
					/***************************3.修改完成,準備轉交(Send the Success view)*************/
//					req.setAttribute("forVO", forVO); // 資料庫update成功後,正確的的forVO物件,存入req
					String url = "/back/back-mainx/back-mainx.jsp";
					RequestDispatcher successView = req.getRequestDispatcher(url); // 修改成功後,轉交/back/back-mainx/back-mainx.jsp
					successView.forward(req, res);

					/***************************其他可能的錯誤處理*************************************/
				} catch (Exception e) {
					errorMsgs.add("修改資料失敗:"+e.getMessage());
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back/back-mainx/back-mainx.jsp");
					failureView.forward(req, res);
				}
				
			}
	       
	       //查詢討論區主題表
			if("showtop".equals(action)){ 				
								
		/***************************1.接收請求參數****************************************/
		try {					 			 
				
				Integer	forno = new Integer(req.getParameter("forno"));
													 					
		/***************************2.開始查詢資料****************************************/				   
					
				ForService forSvc = new ForService(); 			
				List<TopVO>  list = forSvc.getShowtop(forno);
				
				ForVO forVO = forSvc.getOneFor(forno);
				
				MemService memSvc = new MemService();
				List<MemVO> memVOlist = memSvc.getAll();
															
	   /***************************3.查詢完成,準備轉交(Send the Success view)************/				 
				req.setAttribute("memVOlist",memVOlist);
				req.setAttribute("forVO",forVO);
				req.setAttribute("list", list);				
				String url = "/back/back-mainx/back-mainx.jsp"; // 資料庫取出的VO物件,存入req
				RequestDispatcher successView = req.getRequestDispatcher(url);   // 成功轉交 jsp
				successView.forward(req, res);
				 		
			} catch (Exception e) {
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back/back-mainx/back-mainx.jsp");
				failureView.forward(req, res);
			}
		
	   }
		     //修改主題類別名稱-點按後彈出欲修改的類別視窗
			if ("getOneFor".equals(action)) { // 來自back-mainx.jsp的請求

				List<String> errorMsgs = new LinkedList<String>();
				// Store this set in the request scope, in case we need to
				// send the ErrorPage view.
				req.setAttribute("errorMsgs", errorMsgs);
				
 				try {
					/***************************1.接收請求參數****************************************/
					Integer forno = new Integer(req.getParameter("forno"));
	
					/***************************2.開始查詢資料****************************************/
				         	ForService forSvc = new ForService();
				         	ForVO forVO = forSvc.getOneFor(forno);
									
					/***************************3.查詢完成,準備轉交(Send the Success view)************/
					req.setAttribute("forVO", forVO);         // 資料庫取出的forVO物件,存入req
					String url = "/back/back-mainx/back-mainx.jsp";
					RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 /back/back-mainx/back-mainx.jsp
					successView.forward(req, res);

					/***************************其他可能的錯誤處理**********************************/
				} catch (Exception e) {
					errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back/back-mainx/back-mainx.jsp");
					failureView.forward(req, res);
				}
			}
			 //主題文章-點按主題名稱查詢到該主題下之文章
			 //管理主題列表-點按主題名稱查詢到該主題下之文章
			if("showart".equals(action)){                 
				
				
		/***************************1.接收請求參數****************************************/
				
				Integer topno = new Integer(req.getParameter("topno"));
				 

		/***************************2.開始查詢資料****************************************/
				
				TopService topSvc = new TopService();
				TopVO topVO = topSvc.getOnetop(topno);				
				List<ArtVO> artVOlist = topSvc.getShowfor(topno);	//查詢XX主題文章下的所有文章
				
				MemService memSvc = new MemService();
				List<MemVO> memVOlist = memSvc.getAll();
				
				ForService forSvc = new ForService(); 
				 
	   /***************************3.查詢完成,準備轉交(Send the Success view)************/
				 
				req.setAttribute("topVO", topVO);
				req.setAttribute("artVOlist", artVOlist);
				req.setAttribute("memVOlist", memVOlist);
				String url = "/back/back-mainx/back-article.jsp"; 			 
				RequestDispatcher successView = req.getRequestDispatcher(url);  
				successView.forward(req, res);
	 		
			}
			
			//查詢XX主題文章下的所有文章--article-page2
			if("getShowfor".equals(action)){                 
								
		/***************************1.接收請求參數****************************************/
				Integer	topno = new Integer(req.getParameter("topno"));
				
		/***************************2.開始查詢資料****************************************/

				TopService topSvc = new TopService(); 			
				List <ArtVO>  list = topSvc.getShowfor(topno);
								
	   /***************************3.查詢完成,準備轉交(Send the Success view)************/

				req.setAttribute("list", list);
				String url = "/back/back-mainx/back-article.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);  
				successView.forward(req, res);
	 		
			}
			 //管理主題列表-文章列表-移除文章 
			//管理主題類別-主題列表-文章列表-移除文章
			if ("deleteart".equals(action)) {  

				List<String> errorMsgs = new LinkedList<String>();
				// Store this set in the request scope, in case we need to
				// send the ErrorPage view.
				req.setAttribute("errorMsgs", errorMsgs);
		
				try {
					/***************************1.接收請求參數***************************************/
					Integer  artno = new Integer(req.getParameter("artno"));
					Integer topno = new Integer(req.getParameter("topno"));
				     
					/***************************2.開始刪除資料***************************************/
					ArtService artSvc = new ArtService();
					ArtVO artVO = artSvc.getOneArt(artno);
					TopService topSvc = new TopService();
					TopVO topVO = topSvc.getOnetop(topno);					
					List<ArtVO> artVOlist = topSvc.getShowfor(topno);
				
					
					
					if(artVOlist.size()==0 || artVO.getArtno().equals(artVOlist.get(0).getArtno())){       // 如果主題下的文章數=0則將主題一併刪除
						topSvc.delete(topno);
						artSvc.delete(artno);
					 
					}	
									
					else {
						artSvc.delete(artno);
						 
					}
										
					/***************************3.刪除完成,準備轉交(Send the Success view)***********/												 
					
					MemService memSvc = new MemService();
					List<MemVO> memVOlist = memSvc.getAll();
					 
					
					
					req.setAttribute("memVOlist", memVOlist);
					
					
					if(artVO.getArtno().equals(artVOlist.get(0).getArtno())){ //如果刪除的編號與主題發文者編號相同
						System.out.print(artVOlist.size()); 
					  	List<TopVO> list = topSvc.getAllTime();			  
					  	req.setAttribute("list", list);					  
						String url = "/back/back-mainx/back-articleall.jsp" ;
						RequestDispatcher successView = req.getRequestDispatcher(url); 
						successView.forward(req, res);
						
					}
					else {						  
						List<ArtVO> artVOlist2 = topSvc.getShowfor(topno);
						req.setAttribute("artVOlist", artVOlist2);
						req.setAttribute("topVO", topVO);
						String url = "/back/back-mainx/back-article.jsp" ;
						RequestDispatcher successView = req.getRequestDispatcher(url);
						successView.forward(req, res);
					}
					 					
					/***************************其他可能的錯誤處理**********************************/
										
				} catch (Exception e) {
					errorMsgs.add("刪除資料失敗:"+e.getMessage());
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back/back-mainx/back-mainx.jsp");
					failureView.forward(req, res);
				}
	
			}
			//管理主題類別-主題列表-移除主題
			if ("deletetop".equals(action)) {  

				List<String> errorMsgs = new LinkedList<String>();
				// Store this set in the request scope, in case we need to
				// send the ErrorPage view.
				req.setAttribute("errorMsgs", errorMsgs);
		
				try {
					/***************************1.接收請求參數***************************************/
					 
					Integer topno = new Integer(req.getParameter("topno"));
					Integer forno = new Integer(req.getParameter("forno"));
					/***************************2.開始刪除資料***************************************/
					TopService topSvc = new TopService();	
					  topSvc.delete(topno);
															
					/***************************3.刪除完成,準備轉交(Send the Success view)***********/								
					    ForService forSvc = new ForService(); 			
						List<TopVO>  list = forSvc.getShowtop(forno);						
						ForVO forVO = forSvc.getOneFor(forno);
						
						MemService memSvc = new MemService();
						List<MemVO> memVOlist = memSvc.getAll();
										
				     	req.setAttribute("memVOlist",memVOlist);
				    	req.setAttribute("forVO",forVO);
					    req.setAttribute("list", list);	
										 
						String url = "/back/back-mainx/back-mainx.jsp";
						RequestDispatcher successView = req.getRequestDispatcher(url); 
						successView.forward(req, res);
				 					 					
					/***************************其他可能的錯誤處理**********************************/
										
				} catch (Exception e) {
					errorMsgs.add("刪除資料失敗:"+e.getMessage());
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back/back-mainx/back-mainx.jsp");
					failureView.forward(req, res);
				}
	
			}
			
			 //查詢討論區XX主題下的文章
			if("showart2".equals(action)){                 
								
		/***************************1.接收請求參數****************************************/				
				Integer topno = new Integer(req.getParameter("topno"));
				
		/***************************2.開始查詢資料****************************************/
				
				TopService topSvc = new TopService();
				TopVO topVO = topSvc.getOnetop(topno);
				List<ArtVO> artVOlist = topSvc.getShowfor(topno);			
				
				MemService memSvc = new MemService();
				List<MemVO> memVOlist = memSvc.getAll();
			
	   /***************************3.查詢完成,準備轉交(Send the Success view)************/

				req.setAttribute("topVO", topVO);
				req.setAttribute("artVOlist", artVOlist);
				req.setAttribute("memVOlist", memVOlist);
				
				String url = "/back/back-mainx/back-article2.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);  
				successView.forward(req, res);	 		
			}
			
			
			 //查詢檢舉清單中的文章編號
	    if("getOneArtByRep".equals(action))  {                 								
		/***************************1.接收請求參數****************************************/
				
				Integer artno = new Integer(req.getParameter("artno"));

		/***************************2.開始查詢資料****************************************/
				
				ArtService artSvc = new ArtService();
				ArtVO artVO = artSvc.getOneArtByRep(artno);				
			
	   /***************************3.查詢完成,準備轉交(Send the Success view)************/
				
				req.setAttribute("artVO", artVO);
				String url = "/back/back-mainx/back-report.jsp"; 			 
				RequestDispatcher successView = req.getRequestDispatcher(url);  
				successView.forward(req, res);
	 		
			}
	    
		 //移除檢舉清單中的文章
	  if("deleteRepord".equals(action))  { 
	    	List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
		    	
	    try {	
	    	/***************************1.接收請求參數***************************************/
			Integer  artno = new Integer(req.getParameter("artno"));
			Integer topno = new Integer(req.getParameter("topno")); 
			/***************************2.開始刪除資料***************************************/		
			TopService topSvc = new TopService();
			 			
			List<ArtVO> artVOlist = topSvc.getShowfor(topno);
			ArtService artSvc = new ArtService();
			ArtVO artVO = artSvc.getOneArt(artno); 				
//			System.out.print(artVO.getArtno());
//			System.out.print("----------------------");
//			System.out.print(artVOlist.get(0).getArtno()  );
//			
//			System.out.print("----------------------");
//			
			if(artVOlist.size()==0 || artVO.getArtno().equals(artVOlist.get(0).getArtno()) ){ // 如果主題下的文章數=0則將主題一併刪除
				
				 topSvc.delete(topno);
				 artSvc.delete(artno);
			}		
		    else {
		    	
		    	 artSvc.delete(artno);	    	
			}			     			
			/***************************3.刪除完成,準備轉交(Send the Success view)***********/								

			String url = "/back/back-mainx/back-report.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);// 刪除成功後,轉交回送出刪除的來源網頁
			successView.forward(req, res);
	 		
	   } catch (Exception e) {
			errorMsgs.add("資料已刪除");
			RequestDispatcher failureView = req
					.getRequestDispatcher("/back/back-mainx/back-report.jsp");
			failureView.forward(req, res);
		 }
			
			
   }
	
	  //復原檢舉清單中的文章
	    if("updaterep".equals(action))  {   
	    	
	     
	    	/***************************1.接收請求參數***************************************/
			Integer  artno = new Integer(req.getParameter("artno"));
			Integer  topno = new Integer(req.getParameter("topno"));
			 
			/***************************2.開始復原資料***************************************/
			
			TopService topSvc = new TopService();
			topSvc.updateByRep(topno);
			ArtService artSvc = new ArtService();
			artSvc.updaterep(artno);
			RepService repSvc = new RepService();
			repSvc.deleteArt(artno); 
			
			/***************************3.復原完成,準備轉交(Send the Success view)***********/								
			 
			String url = "/back/back-mainx/back-report.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);// 刪除成功後,轉交回送出刪除的來源網頁
			successView.forward(req, res);
	 		
			}
	
	   
   }

}