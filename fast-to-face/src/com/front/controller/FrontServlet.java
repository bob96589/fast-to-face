package com.front.controller;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.naming.java.javaURLContextFactory;

import java.sql.Timestamp;

import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.rep.model.RepService;
import com.rep.model.RepVO;
import com.top.model.*;
import com.art.model.ArtService;
import com.art.model.ArtVO;
import com.fro.model.*;

public class FrontServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		//根據該欄位查詢一筆資料
		if ("getOne_Top_Display".equals(action)) { // 來自select_top.jsp的請求			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String str = req.getParameter("topno");
				if (str == null || (str.trim()).length() == 0) {
					errorMsgs.add("請輸入主題編號");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back/Topic/select_top.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				Integer topno = null;
				try {
					topno = new Integer(str);
				} catch (Exception e) {
					errorMsgs.add("主題編號格式不正確");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back/Topic/select_top.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************2.開始查詢資料*****************************************/
				TopService topSvc = new TopService();
				List<TopVO> topVO = topSvc.getAll();
				if (topVO == null) {
					errorMsgs.add("查無資料");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back/Topic/select_top.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************3.查詢完成,準備轉交(Send the Success view)*************/
				
				req.setAttribute("topVO", topVO); // 資料庫取出的topVO物件,存入req
				String url = "/back/Topic/listOneTop.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back/Topic/select_top.jsp");
				failureView.forward(req, res);
			}
		}
		//輸入一筆資料//文章回文
       if ("insertart".equals(action)) { // 來自art-ckeditor.jsp的請求  
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/			 
				Integer memno2 = null;
				try {
					memno2 = new Integer(req.getParameter("memno2").trim());
				} catch (NumberFormatException e) {
					memno2 = 0;
					errorMsgs.add("請重新登入");
					 
				}					
				String	artcon = req.getParameter("artcon").trim();
			    if (artcon == null || (artcon.trim()).length() <=30){
			    	errorMsgs.add("內容請大於15個字");	
				}
			     			    
			    Integer topno = new Integer(req.getParameter("topno"));
			    
											    								 															    			    			   					    
			    ArtVO artVO = new ArtVO();			    
			    artVO.setMemno(memno2);
			    artVO.setArtcon(artcon);
			    artVO.setTopno(topno);
			    			    
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					
					//準備資料
					TopService topSvc = new TopService();
					TopVO topVO = topSvc.getOnetop(topno);
					List<ArtVO> artVOlist = topSvc.getShowfor(topno);			
					
					MemService memSvc = new MemService();
					List<MemVO> memVOlist = memSvc.getAll();				

					req.setAttribute("topVO", topVO);
					req.setAttribute("artVOlist", artVOlist);
					req.setAttribute("memVOlist", memVOlist);					
					
					
					req.setAttribute("artVO", artVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front/front-mainx/front-article.jsp");
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				ArtService artSvc = new ArtService();
				artVO = artSvc.insertart( memno2, artcon, topno);
								
				//準備資料
				TopService topSvc = new TopService();
				TopVO topVO = topSvc.getOnetop(topno);
				List<ArtVO> artVOlist = topSvc.getShowfor(topno);			
				
				MemService memSvc = new MemService();
				List<MemVO> memVOlist = memSvc.getAll();				

				req.setAttribute("topVO", topVO);
				req.setAttribute("artVOlist", artVOlist);
				req.setAttribute("memVOlist", memVOlist);
				
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				String url = "/front/front-mainx/front-article.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
				successView.forward(req, res);				
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front/front-mainx/front-article.jsp");
				failureView.forward(req, res);
			}
		}
             
       if ("inserttop".equals(action)) { // 來自addEmp.jsp的請求  
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/	 				 					
				
				Integer mem = null;
				try {
					mem = new Integer(req.getParameter("memno").trim());
				} catch (NumberFormatException e) {
					mem = 0;
					errorMsgs.add("請重新登入");
				}	
				
			    String	artcon = req.getParameter("artcon").trim();
			    if (artcon == null || (artcon.trim()).length() == 0){
			    	errorMsgs.add("請勿空白");	
				}	
			    Integer top = null;
				try {
					top = new Integer(req.getParameter("topno").trim());
				} catch (NumberFormatException e) {
					top = 0;
					errorMsgs.add("請重新輸入");						
				}
												
			    ArtVO artVO = new ArtVO();
			    artVO.setMemno(mem);  		 
		  		artVO.setArtcon(artcon);
		  		artVO.setTopno(top);
		  		 	
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("artVO", artVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
					.getRequestDispatcher("/front/front-mainx/front-topic.jsp");
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				ArtService artSvc = new ArtService();
				artVO = artSvc.inserttop( mem, artcon, top );
				
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				String url = "/back/Topic/listAllTop.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
				successView.forward(req, res);				
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front/front-mainx/front-article.jsp");
				failureView.forward(req, res);
			}
		}
	
       
       //更新該表格所有欄位
       if ("update".equals(action)) { // 來自update_emp_input.jsp的請求
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
		
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/

				Integer top = null;
				try {
					top = new Integer(req.getParameter("topno").trim());
				} catch (NumberFormatException e) {
					top = 0;
					errorMsgs.add("主題編號請填數字如:160001.");
				}				
				Integer forn = null;
				try {
					forn = new Integer(req.getParameter("forno").trim());
				} catch (NumberFormatException e) {
					forn = 0;
					errorMsgs.add("類別編號請填數字如:150001.");
				}																
				Integer mem = null;
				try {
					mem = new Integer(req.getParameter("memno").trim());
				} catch (NumberFormatException e) {
					mem = 0;
					errorMsgs.add("會員編號請填數字如:90001.");
				}								
		 
			    String	name = req.getParameter("topname").trim();
			    if (name == null || (name.trim()).length() == 0){
			    	errorMsgs.add("請輸入主題名稱");	
				}	
			    
			   // Timestamp deptno = java.sql.Timestamp.valueOf("2014-02-02 11:11:11");
			    
			    TopVO topVO = new TopVO();
			    topVO.setTopno(top);;
			    topVO.setForno(forn);
			    topVO.setMemno(mem);
			    topVO.setTopname(name);
			    //topVO.setToptime(deptno);
				

				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("topVO", topVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back/Topic/update_top_input.jsp");
					failureView.forward(req, res);
					return; //程式中斷
				}
				
				/***************************2.開始修改資料*****************************************/
				TopService topSvc = new TopService();
				topVO = topSvc.update(top, forn, mem, name);
				
				/***************************3.修改完成,準備轉交(Send the Success view)*************/
				req.setAttribute("topVO", topVO); // 資料庫update成功後,正確的的empVO物件,存入req
				String url = "/back/Topic/listOneTop.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 修改成功後,轉交listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back/Topic/update_top_input.jsp");
				failureView.forward(req, res);
			}
			
		}
        //根據該欄位查詢一筆資料
       if ("getOnetop".equals(action)) { // 來自listAllEmp.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				/***************************1.接收請求參數****************************************/
				Integer topno = new Integer(req.getParameter("topno"));
				
				/***************************2.開始查詢資料****************************************/
				TopService topSvc = new TopService();
				TopVO topVO = topSvc.getOnetop(topno);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("topVO", topVO);         // 資料庫取出的empVO物件,存入req
				String url = "/back/Topic/update_top_input.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back/Topic/listAllTop.jsp");
				failureView.forward(req, res);
			}
		}
       
       //根據該欄位刪除一筆資料
		if ("delete".equals(action)) { // 來自listAllEmp.jsp

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
	
			try {
				/***************************1.接收請求參數***************************************/
				Integer  topno = new Integer(req.getParameter("topno"));
				
				/***************************2.開始刪除資料***************************************/
		       /******************************************************************************************/
		        TopService topSvc = new TopService();
				topSvc.delete(topno);
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/								
				String url = "/back/Topic/listAllTop.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 刪除成功後,轉交回送出刪除的來源網頁
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("刪除資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back/Topic/listAllTop.jsp");
				failureView.forward(req, res);
			}
		}
				
		 //查詢討論區主題表
		if("showtop".equals(action)){      
			
			req.setAttribute("temp", "showtop");           
						
	/***************************1.接收請求參數****************************************/
			
			Integer forno = new Integer(req.getParameter("forno"));
			
	/***************************2.開始查詢資料****************************************/

			ForService forSvc = new ForService(); 			
			ForVO forVO = forSvc.getOneFor(forno);
			List<TopVO>  list = forSvc.getShowtop(forno);
			List<ForVO>  forlist=forSvc.getAll();
			
			MemService memSvc = new MemService();
			List<MemVO> memVOlist = memSvc.getAll();
						
   /***************************3.查詢完成,準備轉交(Send the Success view)************/
			req.setAttribute("forlist", forlist);
			req.setAttribute("forVO", forVO);
			req.setAttribute("list", list);
			req.setAttribute("memVOlist", memVOlist);
			
			String url = "/front/front-mainx/front-topic.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);  
			successView.forward(req, res);
 		
		}
				
		 //查詢討論區XX主題下的文章
		if("showart".equals(action)){                 
						
	/***************************1.接收請求參數****************************************/
			
			Integer topno = new Integer(req.getParameter("topno"));
			
	/***************************2.開始查詢資料****************************************/
			
			TopService topSvc = new TopService();
			TopVO topVO = topSvc.getOnetop(topno);
			List<ArtVO> artVOlist = topSvc.getShowfor(topno);			
			
			MemService memSvc = new MemService();
			List<MemVO> memVOlist = memSvc.getAll();
			
			 
   /***************************3.查詢完成,準備轉交(Send the Success view)************/
			 
			req.setAttribute("topVO", topVO);
			req.setAttribute("artVOlist", artVOlist);
			req.setAttribute("memVOlist", memVOlist);
			
			String url = "/front/front-mainx/front-article.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);  
			successView.forward(req, res);
 		
		}
		
		//查詢討論區文章
		if("getShowfor".equals(action)){                 
						
	/***************************1.接收請求參數****************************************/
			Integer	topno = new Integer(req.getParameter("topno"));
			
	/***************************2.開始查詢資料****************************************/

			TopService topSvc = new TopService(); 			
			List <ArtVO>  list = topSvc.getShowfor(topno);
						
   /***************************3.查詢完成,準備轉交(Send the Success view)************/

			req.setAttribute("list", list);
			String url = "/front/front-mainx/front-article.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);  
			successView.forward(req, res);		
		}
				
		 if("getShowArtTime".equals(action)){        //查最新發表時間          
				
		/***************************1.接收請求參數****************************************/
				Integer	topno = new Integer(req.getParameter("topno"));
				
		/***************************2.開始查詢資料****************************************/

				TopService topSvc = new TopService();	 			
				ArtVO  artVO = topSvc.getShowArtTime(topno);
				
	   /***************************3.查詢完成,準備轉交(Send the Success view)************/

				req.setAttribute("artVO", artVO);
				String url = "/front/front-mainx/front-topic.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);  
				successView.forward(req, res);	 		
			}
		 
		  // 發表主題文章
		 if ("insertArt".equals(action)) { 
			 
			 req.setAttribute("temp", "showtop");   
				List<String> errorMsgs = new LinkedList<String>();
				req.setAttribute("errorMsgs", errorMsgs);

 			try {
			/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/	 				 										
					//拿到top資料新曾主題，並作錯誤處理
 				   		
					Integer forno = new Integer(req.getParameter("forno"));
					Integer memno = new Integer(req.getParameter("memno"));
					 				 
					
					String	topname = req.getParameter("topname").trim();
				    if (topname == null || (topname.trim()).length() == 0){
				    	errorMsgs.add("請重新輸入主題名稱");	
				    }
				    if (topname.trim().length() > 60){
				    	errorMsgs.add("主題名稱不可超過30個字");	
				    }
				    
				    String	artcon = req.getParameter("artcon").trim();
					if (artcon == null || (artcon.trim()).length() <= 30){
					     errorMsgs.add("內容請大於15個字");					    	
					}	
				     					 									     									
				    TopVO topVO = new TopVO();
				    topVO.setForno(forno);  		 
				    topVO.setMemno(memno);
				    topVO.setTopname(topname);
				    				
				    if (!errorMsgs.isEmpty()) {
						req.setAttribute("topVO", topVO); // 含有輸入格式錯誤的topVO物件,也存入req
						RequestDispatcher failureView = req
						.getRequestDispatcher("/front/front-mainx/top-ckeditor.jsp");
						failureView.forward(req, res);
						return;
					}										    
			 
				    /***************************2.開始新增資料***************************************/
					
				    TopService topSvc = new TopService();
					topVO = topSvc.inserttop( forno, memno, topname );
																		
										 				
					//拿到ART資料新增文章，並作錯誤處理
				    Integer topno = topSvc.getMaxTopno();				    					 							    								 															    			    			   					    
				    ArtVO artVO = new ArtVO();			    
				    artVO.setMemno(memno);
				    artVO.setArtcon(artcon);
				    artVO.setTopno(topno);				
										
					/***************************2.開始新增資料***************************************/
				    
					ArtService artSvc = new ArtService();
					artVO = artSvc.insertart(memno, artcon, topno);										 										
					
					
					/***************************3.新增完成,準備轉交(Send the Success view)***********/

					ForService forSvc = new ForService(); 			
					ForVO forVO = forSvc.getOneFor(forno);
					List<TopVO>  list = forSvc.getShowtop(forno);
					
					MemService memSvc = new MemService();
					List<MemVO> memVOlist = memSvc.getAll(); 					
					 
					req.setAttribute("forVO", forVO);
					req.setAttribute("list", list);
					req.setAttribute("memVOlist", memVOlist);
					String url = "/front/front-mainx/front-topic.jsp";
					RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交/front/front-mainx/front-topic.jsp
					successView.forward(req, res);					
				}																						
					/***************************其他可能的錯誤處理**********************************/
					catch (Exception e) {
						errorMsgs.add(e.getMessage());
						RequestDispatcher failureView = 
						req.getRequestDispatcher("/front/front-mainx/top-ckeditor.jsp");
						failureView.forward(req, res);
					}					
				}
		 
		    //檢舉文章
	       if ("insertrep".equals(action)) {  
				
				List<String> errorMsgs = new LinkedList<String>();
				// Store this set in the request scope, in case we need to
				// send the ErrorPage view.
				req.setAttribute("errorMsgs", errorMsgs);

				try {
					/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/	 				 					
										 
					Integer	artno = new Integer(req.getParameter("artno")) ;					 
					Integer	memno = new Integer(req.getParameter("memno") );
					Integer	memno2 = new Integer(req.getParameter("memno2") );
					Integer	topno = new Integer(req.getParameter("topno")) ;
					 
				    String	repcon = req.getParameter("repcon").trim();
				    if (repcon == null || (repcon.trim()).length() == 0){
				    	errorMsgs.add("請勿空白");	
					}	
					
				    RepVO repVO = new RepVO();
				    repVO.setArtno(artno);  		 
				    repVO.setMemno(memno2);
				    repVO.setRepcon(repcon);
			  		
				    
				    ArtService artSvc = new ArtService();					
					artSvc.repdelete(artno);

					// Send the use back to the form, if there were errors
					if (!errorMsgs.isEmpty()) {									
						
						req.setAttribute("repVO", repVO); // 含有輸入格式錯誤的repVO物件,也存入req
						RequestDispatcher failureView = req
						.getRequestDispatcher("/front/front-mainx/front-article.jsp");
						failureView.forward(req, res);
						return;
					}
					
					/***************************2.開始新增資料***************************************/
					
					
					RepService repSvc = new RepService();
					repVO = repSvc.insertrep( artno, memno2, repcon );
					
					/***************************3.新增完成,準備轉交(Send the Success view)***********/
					
					
					MemService memSvc = new MemService();
					List<MemVO> memVOlist = memSvc.getAll();
					
					
					TopService topSvc = new TopService();
					TopVO topVO = topSvc.getOnetop(topno);
					List<ArtVO> artVOlist = topSvc.getShowfor(topno); 
					
					
					req.setAttribute("memVOlist", memVOlist);  
					req.setAttribute("topVO", topVO);
					req.setAttribute("artVOlist", artVOlist);
					
					String url = "/front/front-mainx/front-article.jsp";
					RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交/front/front-mainx/front-article.jsp
					successView.forward(req, res);				
					
					/***************************其他可能的錯誤處理**********************************/
				} catch (Exception e) {
					errorMsgs.add(e.getMessage());
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front/front-mainx/front-article.jsp");
					failureView.forward(req, res);
				}
			}
		
	       //查詢會員討論區發文紀錄
	       if("getAllmember".equals(action)){
	    	   			
	    	   			HttpSession session = req.getSession();
	    	   			MemVO memVO = (MemVO)session.getAttribute("memVO");
								
	    		/***************************1.接收請求參數****************************************/	    				
	    				Integer memno = memVO.getMemno();
	    				
	    		/***************************2.開始查詢資料****************************************/
	    				MemService memSvc = new MemService();
						List<MemVO> memVOlist = memSvc.getAll(); 
	    				TopService topSvc = new TopService(); 			
	    				List <TopVO>  list = topSvc.getAllmember(memno);
	    				  
	    	   /***************************3.查詢完成,準備轉交(Send the Success view)************/
	    				
	    				req.setAttribute("memVOlist", memVOlist); 
	    				req.setAttribute("list", list);
	    				String url = "/front/front-mainx/front-member.jsp";
	    				RequestDispatcher successView = req.getRequestDispatcher(url);  
	    				successView.forward(req, res);
	    	 		
	    			}
	       
	       //搜尋主題文章
	       if("SelectTop".equals(action)){  
	    	   
	    	       req.setAttribute("temp", "SelectTop");
	    	       List<String> errorMsgs = new LinkedList<String>();				 
				      req.setAttribute("errorMsgs", errorMsgs);
				      
				      Integer forno = new Integer(req.getParameter("forno"));
	    		/***************************1.接收請求參數****************************************/
	    			if(forno == 1) {
	    				
	    				String	topname = req.getParameter("topname").trim();
					    if (topname == null || (topname.trim()).length() == 0){
					    	errorMsgs.add("請勿空白");	
						}	
					    ForService forSvc = new ForService();
					    List<ForVO>  forlist=forSvc.getAll();					    			
						ForVO forVO = forSvc.getOneFor(forno);
						 
					    TopService topSvc = new TopService(); 			
	    				List <TopVO>  list = topSvc.getOneName(topname);
						
						MemService memSvc = new MemService();
						List<MemVO> memVOlist = memSvc.getAll(); 
						
						
						req.setAttribute("forVO", forVO); 
						req.setAttribute("forlist", forlist); 
	    				req.setAttribute("memVOlist", memVOlist); 
	    				req.setAttribute("list", list);
	    			}
	    			
	    		else {
	    			
	    			String	topname = req.getParameter("topname").trim();
				    if (topname == null || (topname.trim()).length() == 0){
				    	errorMsgs.add("請勿空白");	
					}	
				    ForService forSvc = new ForService();
				    List<ForVO>  forlist=forSvc.getAll();
				    ForVO forVO = forSvc.getOneFor(forno);
					 
				    TopService topSvc = new TopService(); 			
    				List <TopVO>  list = topSvc.getOneForno(topname,forno);
					
					MemService memSvc = new MemService();
					List<MemVO> memVOlist = memSvc.getAll();
					
					req.setAttribute("forVO", forVO);
					req.setAttribute("forlist", forlist); 
    				req.setAttribute("memVOlist", memVOlist); 
    				req.setAttribute("list", list);
				    
				}			    				  
	    	   /***************************3.查詢完成,準備轉交(Send the Success view)************/
	    			    
	    				String url = "/front/front-mainx/front-topic.jsp";
	    				RequestDispatcher successView = req.getRequestDispatcher(url);  
	    				successView.forward(req, res);
	    	 		
	    			}
	       
	     
		 
		 }
}

				
						
	