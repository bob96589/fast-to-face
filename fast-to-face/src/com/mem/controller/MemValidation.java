package com.mem.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.buy.model.BuyService;
import com.buy.model.BuyVO;
import com.cou.model.CouService;
import com.mem.model.MemService;
import com.mem.model.MemVO;

public class MemValidation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		if ("updateMemstate".equals(action)) {
			
			String memrandom = req.getParameter("memrandom");
			Integer memno = new Integer(req.getParameter("memno"));
			
			MemService memSvc = new MemService();
			MemVO memVO = memSvc.getOneMem(memno);
			
			if(!memVO.getMemrandom().equals(memrandom)){
				req.setAttribute("errorMsgs", "���ҽX���~");
				RequestDispatcher failureView = req.getRequestDispatcher("/front/member_register/registerSuccess.jsp");
				failureView.forward(req, res);
				return;
			}else{
				memSvc.updateMemstatusByMemno(1, memno);
				
				//reset memVO in session
				HttpSession session = req.getSession();			
				MemVO memVObj = (MemVO)session.getAttribute("memVO");			
				memVO = memSvc.getOneMem(memVObj.getMemno());
				session.setAttribute("memVO", memVO);
				
				RequestDispatcher successView = req.getRequestDispatcher("/front/member_register/validationSuccess.jsp");
				successView.forward(req, res);
				
			}		
		}
		
		
	}
}
