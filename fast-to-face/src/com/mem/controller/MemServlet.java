package com.mem.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.oreilly.servlet.MultipartRequest;

public class MemServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		String contentType = req.getContentType();
		String action = null;
		MultipartRequest multi = null;

		if (contentType != null && contentType.startsWith("multipart/form-data")) {
			multi = new MultipartRequest(req, getServletContext().getRealPath("/shared/temp"), 5 * 1024 * 1024, "UTF-8");
			action = multi.getParameter("action");
		} else {
			req.setCharacterEncoding("UTF-8");
			action = req.getParameter("action");
		}

		if ("getAll".equals(action)) {
			/*************************** 開始查詢資料 ****************************************/
			MemService memSvc = new MemService();
			List<MemVO> list = memSvc.getAll();

			/*************************** 查詢完成,準備轉交(Send the Success view) *************/
			HttpSession session = req.getSession();
			session.setAttribute("list", list); // 資料庫取出的list物件,存入session
			// Send the Success view
			String url = "/front/mem/listAllMem.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交listAllMem.jsp
			successView.forward(req, res);
			return;
		}

		if ("getMem_For_One".equals(action)) {
			/*************************** 1.接收請求參數 ***************************/
			Integer memno = new Integer(req.getParameter("memno"));
			/*************************** 開始查詢資料 ****************************************/
			MemService memSvc = new MemService();
			MemVO memVO = memSvc.getOneMem(memno);
			/*************************** 查詢完成,準備轉交(Send the Success view) *************/
			HttpSession session = req.getSession();
			session.setAttribute("memVO", memVO);
			String url = "/front/member_profile/member_basic.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交
																			// listOneEmp.jsp
			successView.forward(req, res);
			return;
		}

		// 顯示所有會員資料
		if ("getOne_For_Update".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			String requestURL = req.getParameter("requestURL"); // 送出修改的來源網頁路徑:
																// /Front/mem/listAllMem.jsp
			/*
			 * controller 3步驟 1.接收請求參數 - 輸入格式的錯誤處理 2.永續層存取 - 資料庫 新增、修改、刪除、查詢
			 * 3.資料庫完成動作,準備轉交資料給view
			 */

			try {
				// 1.接收請求參數- 輸入格式錯誤處理
				Integer memno = new Integer(req.getParameter("memno"));

				// 2.開始查詢資料
				MemService memSvc = new MemService();
				MemVO memVO = memSvc.getOneMem(memno);

				// 3.完成查詢,準備轉交給view
				req.setAttribute("memVO", memVO); // 資料庫取出的memVO物件,存入req
				String url = "/front/mem/update_mem.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交
																				// Update_mem.jsp
				successView.forward(req, res);

			} catch (Exception e) { // 其他可能的錯誤處理
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front/mem/listAllMem.jsp");
				failureView.forward(req, res);
			}
		}

		// 修改會員資料
		if ("update".equals(action)) {
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			String requestURL = multi.getParameter("requestURL");
			

//			try {
				
				// memno
				Integer memno = new Integer(multi.getParameter("memno").trim());

				// mempsw
				String mempsw = multi.getParameter("mempsw").trim();
				if (mempsw == null || (mempsw.trim()).length() == 0) {
					errorMsgs.add("請輸入密碼");
				}
				
				// mempsw2
				String mempsw2 = multi.getParameter("mempsw2").trim();
				if (mempsw2 == null || (mempsw2.trim()).length() == 0) {
					errorMsgs.add("請輸入確認密碼");
				}

				// memname
				String memname = multi.getParameter("memname").trim();
				
				// mememail
				String mememail = multi.getParameter("mememail").trim();
				if (mememail == null || (mememail.trim()).length() == 0) {
					errorMsgs.add("請輸入電子信箱");
				}else{
					
					if(!isValidEmail(mememail)){
						errorMsgs.add("Email格式不正確");
					}else{
						//確認信箱是否已被使用
						MemService memSvc = new MemService();
						List<MemVO> memVOList = memSvc.getAll();
						for(MemVO memVO : memVOList){
							if(mememail.equals(memVO.getMememail()) && !memno.equals(memVO.getMemno())){
								errorMsgs.add("信箱已使用");
								break;
							}				
						}
					}
				}
				
				// memgender
				Integer memgender = new Integer(multi.getParameter("memgender"));				

				// mempic				
				File mempic = multi.getFile("mempic");
				byte[] buffer = null;
				if (mempic != null) {					
					InputStream in = new FileInputStream(mempic);
					buffer = new byte[in.available()];
					in.read(buffer);
					mempic.delete();
				} else {
					// read mempic from DB
					MemService memSvc = new MemService();
					MemVO memVO = memSvc.getOneMem(memno);
					buffer = memVO.getMempic();
				}

				MemVO memVO = new MemVO();
				memVO.setMemno(memno);
				memVO.setMemname(memname);
				memVO.setMemgender(memgender);
				memVO.setMememail(mememail);
				memVO.setMempic(buffer);

				if (!errorMsgs.isEmpty()) {
					req.setAttribute("memVO", memVO); // 含有輸入格式錯誤的empVO物件,也存入req
					req.setAttribute("requestURL", requestURL);
					RequestDispatcher failureView = req.getRequestDispatcher("/front/member_profile/member_update.jsp");
					failureView.forward(req, res);
					return; // 程式中斷
				}
				
				//確認兩次密碼是否相同
				if(!mempsw2.equals(mempsw)){
					errorMsgs.add("密碼與確認密碼不同");					
				}				
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("memVO", memVO); 
					RequestDispatcher failureView = req.getRequestDispatcher("/front/member_profile/member_update.jsp");
					failureView.forward(req, res);
					return;
				}

				
				MemService memSvc = new MemService();
				memSvc.updateMem(memno, mempsw, memname, memgender,mememail, buffer);
				
				//reset memVO in session
				HttpSession session = req.getSession();			
				MemVO memVObj = (MemVO)session.getAttribute("memVO");			
				memVO = memSvc.getOneMem(memVObj.getMemno());
				session.setAttribute("memVO", memVO);			
				
				String url = "/front/member_profile/member_basic.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

//			} catch (Exception e) {
//				errorMsgs.add("修改資料失敗:" + e.getMessage());
//				RequestDispatcher failureView = req
//						.getRequestDispatcher("/front/mem/update_mem.jsp");
//				failureView.forward(req, res);
//			}

		}

		// 會員註冊
		if ("insert".equals(action)) { 

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

//			try {

				/*********************** 1.接收請求參數 - 輸入格式的錯誤處理 *************************/
				// memaccount
				String memaccount = req.getParameter("memaccount").trim();
				if (memaccount == null || (memaccount.trim()).length() == 0) {
					errorMsgs.add("請輸入帳號");
				}else{
					//確認帳號是否已被使用
					MemService memSvc = new MemService();
					List<MemVO> memVOList = memSvc.getAll();
					for(MemVO memVO : memVOList){
						if(memaccount.equals(memVO.getMemaccount()))
							errorMsgs.add("帳號已使用");					
					}
				}

				// mempsw
				String mempsw = req.getParameter("mempsw").trim();
				if (mempsw == null || (mempsw.trim()).length() == 0) {
					errorMsgs.add("請輸入密碼");

				}

				String pswchecked = req.getParameter("pswchecked").trim();
				if (pswchecked == null || (pswchecked.trim()).length() == 0) {
					errorMsgs.add("請輸入確認密碼");
				}		

				// mememail
				String mememail = req.getParameter("mememail").trim();
				if (mememail == null || (mememail.trim()).length() == 0) {
					errorMsgs.add("請輸入電子信箱");
				}else{
					if(!isValidEmail(mememail)){
						errorMsgs.add("Email格式不正確");
					}else{
						//確認信箱是否已被使用
						MemService memSvc = new MemService();
						List<MemVO> memVOList = memSvc.getAll();
						for(MemVO memVO : memVOList){
							if(mememail.equals(memVO.getMememail())){
								errorMsgs.add("信箱已使用");
								break;
							}
						}
					}
				}
				
				//mempic
				FileInputStream in = new FileInputStream(new File(getServletContext().getRealPath("/shared/image/noPhoto.jpg")));
				byte[] buffer = new byte[in.available()];
				in.read(buffer);
				
				//memrandom
				StringBuffer memrandomBuffer = new StringBuffer();
				for(int i = 0; i < 6; i++){
					int randomNum = (int)(Math.random()*10);
					memrandomBuffer.append(String.valueOf(randomNum));
				}
				String memrandom = memrandomBuffer.toString();
				

				MemVO memVO = new MemVO();
				memVO.setMemaccount(memaccount);
				memVO.setMempsw(mempsw);
				memVO.setMememail(mememail);		

				if (!errorMsgs.isEmpty()) {
					req.setAttribute("memVO", memVO); 
					RequestDispatcher failureView = req.getRequestDispatcher("/front/member_register/member_register.jsp");
					failureView.forward(req, res);
					return;
				}
				
				//確認兩次密碼是否相同
				if(!pswchecked.equals(mempsw)){
					errorMsgs.add("密碼與確認密碼不同");					
				}				
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("memVO", memVO); 
					RequestDispatcher failureView = req.getRequestDispatcher("/front/member_register/member_register.jsp");
					failureView.forward(req, res);
					return;
				}

				/*************************** 2.開始新增資料 ***************************************/

				MemService memSvc = new MemService();				
				memVO = memSvc.addMem(memaccount, mempsw, mememail, buffer, memrandom);

				/*************************** 3.新增完成,準備轉交(Send the Success view) ***********/
				
				memVO = memSvc.getMemPsw(memaccount, mempsw);
				HttpSession session = req.getSession();
				session.setAttribute("memVO", memVO);
				req.removeAttribute("errorMsgs");
				
				RequestDispatcher email = req.getRequestDispatcher("/front/member_register/JavaMailProccess.jsp");
				res.setCharacterEncoding("Big5");
				email.include(req, res);
				
				String url = "/front/member_register/registerSuccess.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
				successView.forward(req, res);

				/*************************** 其他可能的錯誤處理 **********************************/
//			} catch (Exception e) {
//				errorMsgs.add(e.getMessage());
//				RequestDispatcher failureView = req
//						.getRequestDispatcher("/front/member_register/member_register.jsp");
//
//				failureView.forward(req, res);
//			}
		}
		
		
		

		if ("login_front".equals(action)) {
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			/*********************** 1.接收請求參數 - 輸入格式的錯誤處理 *************************/
			
			String memaccount = req.getParameter("account");
			if (memaccount == null || (memaccount.trim()).length() == 0) {
				errorMsgs.add("請輸入帳號");
			}

			String mempsw = req.getParameter("password");
			if (mempsw == null || (mempsw.trim()).length() == 0) {
				errorMsgs.add("請輸入密碼");
			}

			MemVO memVO = new MemVO();
			memVO.setMemaccount(memaccount);
			memVO.setMempsw(mempsw);

			if (!errorMsgs.isEmpty()) {
				req.setAttribute("MemVO", memVO);
				RequestDispatcher failureView = req.getRequestDispatcher("/front/frontlogin/frontlogin.jsp");
				failureView.forward(req, res);
				return; 
			}

			/*************************** 開始查詢資料 ****************************************/
			
			MemService memService = new MemService();
			memVO = memService.getMemPsw(memaccount, mempsw);
			
			/*************************** 3.查詢完成,準備轉交(Send the Success view) ***********/
			if (memVO == null) {
				errorMsgs.add("登入失敗，帳號密碼有誤");
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("memVO", memVO);
					RequestDispatcher failureView = req.getRequestDispatcher("/front/frontlogin/frontlogin.jsp");
					failureView.forward(req, res);
					return; 
				}
			} else {
				if(memVO.getMemstate().equals(3)){
					errorMsgs.add("此帳號已被停權");
					req.setAttribute("memVO", memVO);
					RequestDispatcher failureView = req.getRequestDispatcher("/front/frontlogin/frontlogin.jsp");
					failureView.forward(req, res);
					return; 
				}
				
				HttpSession session = req.getSession();
				session.setAttribute("memVO", memVO);
				try { 
					String location = (String) session.getAttribute("location");
					if (location != null) {
						session.removeAttribute("location");
						res.sendRedirect(location);
						return;
					}
				} catch (Exception ignored) {}
				
				String url = "/front/frontlogin/loginSuccess.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); 
				successView.forward(req, res);
			}
		}
		
		
		if("logout_front".equals(action)){
			HttpSession session = req.getSession();
			session.removeAttribute("memVO");
			
			String url = "/front/frontlogin/logoutSuccess.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url); 
			successView.forward(req, res);
		}

		if ("getMempic".equals(action)) {

			/*********************** 1.接收請求參數 - 輸入格式的錯誤處理 *************************/
			Integer memno = new Integer(req.getParameter("memno"));
			/*************************** 開始查詢資料 ****************************************/
			MemService memSvc = new MemService();
			MemVO memVO = memSvc.getOneMem(memno);

			byte[] mempic = memVO.getMempic();

			res.setContentType("image/png");
			ServletOutputStream out = res.getOutputStream();
			out.write(mempic);
			out.flush();
			/*************************** 3.新增完成,準備轉交(Send the Success view) ***********/

			/*************************** 其他可能的錯誤處理 **********************************/

		}
		
		
		
		
		
		
		

	}
	
	public static boolean isValidEmail(String email) {
		if (email == null) {
			return false;
		}
		String emailPattern = "^([\\w]+)(([-\\.][\\w]+)?)*@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		return email.matches(emailPattern);
	}
	
}
