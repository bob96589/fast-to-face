package com.mem.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mem.model.MemService;
import com.mem.model.MemVO;

public class MemManagement extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		if("memState".equals(action)){
			
			Integer memno = new Integer(req.getParameter("memno"));
			Integer memstate = new Integer(req.getParameter("memstate"));
			String memreason = req.getParameter("memreason");
			
			MemService memSvc = new MemService();
			MemVO memVO = memSvc.updateMemstate(memstate, memreason, memno);
			
			req.setAttribute("memVO", memVO);
			String url = "/back/mem/memState.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);
			successView.forward(req, res);
		}
	}

}
