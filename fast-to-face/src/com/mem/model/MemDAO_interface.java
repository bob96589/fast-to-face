package com.mem.model;

import java.util.*;

import com.buy.model.BuyVO;
import com.sto.model.StoVO;

public interface MemDAO_interface {
	public void insert(MemVO memVO);
	public void update(MemVO memVO);
	public void delete(Integer memno);
	public MemVO findByPrimaryKey(Integer memno);	
	public List<MemVO> getAll();
	
	// 自建方法呼叫資料庫 account ,password 比對使用者輸入是否正確
	public MemVO findByMemPsw(String memaccount, String mempsw);
	// 恭豪
	public Set<StoVO> getStosByMemno(Integer memno);
	// 恭豪
	public Set<BuyVO> getBuysByMemno(Integer memno);
	
	//bob
	public void updateMempointByMemno(Integer mempoint, Integer memno);
	//bob
	public void updateMemstatusByMemno(Integer memstatus, Integer memno);
	// 恭豪
	public void updateMemstate(MemVO memVO);
	
}
