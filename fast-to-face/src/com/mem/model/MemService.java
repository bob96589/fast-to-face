package com.mem.model;

import java.util.List;
import java.util.Set;

import com.buy.model.BuyVO;
import com.sto.model.StoVO;

public class MemService {

	private MemDAO_interface dao;

	public MemService() {
		dao = new MemDAO();
	}
	
	//註冊
	public MemVO addMem(String memaccount, String mempsw, String mememail, byte[] mempic, String memrandom){
		
		MemVO memVO = new MemVO();

		memVO.setMemaccount(memaccount);
		memVO.setMempsw(mempsw);
		memVO.setMememail(mememail);
		memVO.setMempic(mempic);;
		memVO.setMemrandom(memrandom);;

		
		dao.insert(memVO);

		return memVO;
	}

	
	//會員修改個資
	public MemVO updateMem(Integer memno,String mempsw, String memname,
			Integer memgender,String mememail,byte[] mempic) {

		MemVO memVO = new MemVO();

		memVO.setMemno(memno);
		memVO.setMempsw(mempsw);
		memVO.setMemname(memname);
		memVO.setMemgender(memgender);
		memVO.setMememail(mememail);
		memVO.setMempic(mempic);

		dao.update(memVO);

		return memVO;
	}


	public MemVO getOneMem(Integer memno) {
		return dao.findByPrimaryKey(memno);
	}

	public List<MemVO> getAll() {
		return dao.getAll();
	}
	
	//比對資料庫的帳號密碼
	public MemVO getMemPsw(String memaccount,String mempsw){
		return dao.findByMemPsw(memaccount, mempsw);
	}
	
	//修改會員上傳圖片
//	public MemVO getOneMempic(Integer memno) {
//		return dao.uploadpic(memno);
//	}
	
	
	//恭豪
	public Set<StoVO> getStosByMemno(Integer memno){
		return dao.getStosByMemno(memno);		
	}
	//恭豪
	public Set<BuyVO> getBuysByMemno(Integer memno){
		return dao.getBuysByMemno(memno);		
	}
	//bob
	public void updateMempointByMemno(Integer mempoint, Integer memno){
		dao.updateMempointByMemno(mempoint, memno);
	}
	//bob
	public void updateMemstatusByMemno(Integer memstatus, Integer memno){
		dao.updateMemstatusByMemno(memstatus, memno);
	}
	
	//恭豪
		public MemVO updateMemstate(Integer memstate,String memreason,Integer memno) {

			MemVO memVO = new MemVO();

			memVO.setMemno(memno);
			memVO.setMemreason(memreason);
			memVO.setMemstate(memstate);

			dao.updateMemstate(memVO);

			return memVO;
		}
	
}
