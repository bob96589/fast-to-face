package com.mem.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.buy.model.BuyVO;
import com.sto.model.StoVO;

public class MemDAO implements MemDAO_interface {

	// 一個應用程式中,針對一個資料庫 ,共用一個DataSource即可
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/TestDB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = // 新增含圖片
	"INSERT INTO member (memno, memaccount, mempsw, mememail, memstate, mempic, memrandom, mempoint) VALUES (Mem_SEQ.NEXTVAL, ?, ?, ?, 0, ?, ?, 0)";
	private static final String GET_ALL_STMT = // 查詢
	"SELECT * FROM member order by memno";
	private static final String GET_ONE_STMT = // 查詢單一取得所有欄位資料
	"SELECT * FROM member where memno = ?";
	private static final String DELETE = "DELETE FROM member where memno = ?";
	private static final String UPDATE = // 修改會員資料
	"UPDATE member set   mempsw=?, memname=?, memgender=?, mememail=?, memPic=? where memno = ?";
	private static final String GET_account_password_STMT = "SELECT  * FROM member where memaccount = ? AND mempsw=?";
	private static final String GET_MEMONE_STMT = // 查詢單一取得所有欄位資料
	"SELECT * FROM member where memaccount = ? AND mempsw=?";

	// 恭豪
	private static final String GET_STOS_BY_MEMNO = "select * from storerecord where memno = ? order by storetime desc";
	// 恭豪
	private static final String GET_BUYS_BY_MEMNO = "select * from buyrecord where memno = ? order by buytime desc";
	// bob
	private static final String UPDATE_MEMPOINT_BY_MEMNO = "update member set mempoint = ? where memno = ?";
	// bob
	private static final String UPDATE_MEMSTATE_BY_MEMNO = "update member set memstate = ? where memno = ?";
	//恭豪
	private static final String UPDATE_MEMSTATE = "update member set memstate=?,memreason=?,memtime=sysdate where memno=?";

	@Override
	public void insert(MemVO memVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, memVO.getMemaccount());
			pstmt.setString(2, memVO.getMempsw());
			pstmt.setString(3, memVO.getMememail());
			pstmt.setBytes(4, memVO.getMempic());
			pstmt.setString(5, memVO.getMemrandom());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(MemVO memVO) { // 會員修改個資

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setString(1, memVO.getMempsw());
			pstmt.setString(2, memVO.getMemname());
			pstmt.setInt(3, memVO.getMemgender());
			pstmt.setString(4, memVO.getMememail());
			pstmt.setBytes(5, memVO.getMempic());
			pstmt.setInt(6, memVO.getMemno());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer memno) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, memno);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public MemVO findByPrimaryKey(Integer memno) {

		MemVO memVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, memno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVo 也稱為 Domain objects
				memVO = new MemVO();

				memVO.setMemno(rs.getInt("memno"));
				memVO.setMemaccount(rs.getString("memaccount"));
				memVO.setMempsw(rs.getString("mempsw"));
				memVO.setMememail(rs.getString("mememail"));
				memVO.setMemstate(rs.getInt("memstate"));
				memVO.setMempic(rs.getBytes("memPic"));
				memVO.setMemname(rs.getString("memname"));
				memVO.setMemgender(rs.getInt("memgender"));
				memVO.setMemreason(rs.getString("memreason"));
				memVO.setMemtime(rs.getTimestamp("memTime"));
				memVO.setMemrandom(rs.getString("memrandom"));
				memVO.setMempoint(rs.getInt("mempoint"));

			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return memVO;
	}

	@Override
	public List<MemVO> getAll() {
		List<MemVO> list = new ArrayList<MemVO>();
		MemVO memVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// memVO 也稱為 Domain objects
				memVO = new MemVO();
				memVO.setMemno(rs.getInt("memno"));
				memVO.setMemaccount(rs.getString("memaccount"));
				memVO.setMempsw(rs.getString("mempsw"));
				memVO.setMememail(rs.getString("mememail"));
				memVO.setMemstate(rs.getInt("memstate"));
				memVO.setMempic(rs.getBytes("memPic"));
				memVO.setMemname(rs.getString("memname"));
				memVO.setMemgender(rs.getInt("memgender"));
				memVO.setMemreason(rs.getString("memreason"));
				memVO.setMemtime(rs.getTimestamp("memTime"));
				memVO.setMemrandom(rs.getString("memrandom"));
				memVO.setMempoint(rs.getInt("mempoint"));
				list.add(memVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	// 比對登入帳號密碼
	@Override
	public MemVO findByMemPsw(String account, String password) {

		MemVO memVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_account_password_STMT);

			pstmt.setString(1, account);
			pstmt.setString(2, password);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVo 也稱為 Domain objects
				memVO = new MemVO();

				memVO.setMemno(rs.getInt("memno"));
				memVO.setMemaccount(rs.getString("memaccount"));
				memVO.setMempsw(rs.getString("mempsw"));
				memVO.setMememail(rs.getString("mememail"));
				memVO.setMemstate(rs.getInt("memstate"));
				memVO.setMempic(rs.getBytes("memPic"));
				memVO.setMemname(rs.getString("memname"));
				memVO.setMemgender(rs.getInt("memgender"));
				memVO.setMemreason(rs.getString("memreason"));
				memVO.setMemtime(rs.getTimestamp("memTime"));
				memVO.setMemrandom(rs.getString("memrandom"));
				memVO.setMempoint(rs.getInt("mempoint"));

			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return memVO;
	}

	// 恭豪
	@Override
	public Set<StoVO> getStosByMemno(Integer memno) {
		Set<StoVO> set = new LinkedHashSet<StoVO>();
		StoVO stoVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_STOS_BY_MEMNO);
			pstmt.setInt(1, memno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				stoVO = new StoVO();
				stoVO.setStoreno(rs.getInt("storeno"));
				stoVO.setStoreranno(rs.getString("storeranno"));
				stoVO.setStoretime(rs.getTimestamp("storetime"));
				stoVO.setStoreprice(rs.getInt("storeprice"));
				stoVO.setMemno(rs.getInt("memno"));
				set.add(stoVO);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}

	// 恭豪
	@Override
	public Set<BuyVO> getBuysByMemno(Integer memno) {
		Set<BuyVO> set = new LinkedHashSet<BuyVO>();
		BuyVO buyVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_BUYS_BY_MEMNO);
			pstmt.setInt(1, memno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				buyVO = new BuyVO();
				buyVO.setBuyno(rs.getInt("buyno"));
				buyVO.setBuytime(rs.getTimestamp("buytime"));
				buyVO.setBuyprice(rs.getInt("buyprice"));
				buyVO.setBuystate(rs.getInt("buystate"));
				buyVO.setMemno(rs.getInt("memno"));
				buyVO.setCouno(rs.getInt("couno"));
				set.add(buyVO);
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}

	// bob
	@Override
	public void updateMempointByMemno(Integer mempoint, Integer memno) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_MEMPOINT_BY_MEMNO);

			pstmt.setInt(1, mempoint);
			pstmt.setInt(2, memno);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	// bob
	@Override
	public void updateMemstatusByMemno(Integer memstatus, Integer memno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_MEMSTATE_BY_MEMNO);

			pstmt.setInt(1, memstatus);
			pstmt.setInt(2, memno);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}
	
	
	@Override
	public void updateMemstate(MemVO memVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_MEMSTATE);


			pstmt.setInt(1, memVO.getMemstate());
			pstmt.setString(2, memVO.getMemreason());
			pstmt.setInt(3, memVO.getMemno());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	// @Override //修改會員資料取得圖片
	// public MemVO uploadpic(Integer memno) {
	//
	// MemVO memVO = null;
	// Connection con = null;
	// PreparedStatement pstmt = null;
	// ResultSet rs = null;
	//
	// try {
	//
	// con = ds.getConnection();
	// pstmt = con.prepareStatement(GET_ONE_STMT_MEMPIC);
	//
	// pstmt.setInt(1, memno);
	//
	// rs = pstmt.executeQuery();
	//
	// while (rs.next()) {
	//
	// memVO = new MemVO();
	// memVO.setMemno(rs.getInt("memno"));
	// memVO.setMemaccount(rs.getString("memaccount"));
	// memVO.setMemname(rs.getString("memname"));
	// memVO.setMemgender(rs.getInt("memgender"));
	// memVO.setMememail(rs.getString("mememail"));
	// memVO.setMempic(rs.getBytes("memPic"));
	//
	// }
	//
	// // Handle any driver errors
	// } catch (SQLException se) {
	// throw new RuntimeException("A database error occured. "
	// + se.getMessage());
	// // Clean up JDBC resources
	// } finally {
	// if (rs != null) {
	// try {
	// rs.close();
	// } catch (SQLException se) {
	// se.printStackTrace(System.err);
	// }
	// }
	// if (pstmt != null) {
	// try {
	// pstmt.close();
	// } catch (SQLException se) {
	// se.printStackTrace(System.err);
	// }
	// }
	// if (con != null) {
	// try {
	// con.close();
	// } catch (Exception e) {
	// e.printStackTrace(System.err);
	// }
	// }
	// }
	// return memVO;
	// }

}
