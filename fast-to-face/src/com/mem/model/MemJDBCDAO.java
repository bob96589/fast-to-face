package com.mem.model;

import java.util.*;
import java.io.*;
import java.sql.*;

import com.buy.model.BuyVO;
import com.sto.model.StoVO;


public class MemJDBCDAO implements MemDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "user1";
	String passwd = "u111";

		

	private static final String INSERT_STMT = //新增含圖片
			"INSERT INTO member (memno, memaccount, mempsw, mememail, memstate, mempic, memrandom, mempoint) VALUES (Mem_SEQ.NEXTVAL, ?, ?, ?, 0, ?, ?, 0)";
	private static final String GET_ALL_STMT = //查詢
			"SELECT * FROM member order by memno";
	private static final String GET_ONE_STMT =  //查詢單一取得所有欄位資料			
			"SELECT * FROM member where memno = ?";
	private static final String DELETE = 
			"DELETE FROM member where memno = ?";
	private static final String UPDATE = //修改會員資料
			"UPDATE member set mempsw=?, memname=?, memgender=?, mememail=?, memPic=? where memno = ?";
	private static final String GET_account_password_STMT = 
			"SELECT  * FROM member where memaccount = ? AND mempsw=?";
	private static final String GET_MEMONE_STMT =  //查詢單一取得所有欄位資料			
			"SELECT * FROM member where memaccount = ? AND mempsw=?";
	
	// hsu
	private static final String GET_STOS_BY_MEMNO = "select * from storerecord where memno = ? order by storetime desc";
	// hsu
	private static final String GET_BUYS_BY_MEMNO = "select * from buyrecord where memno = ? order by buytime desc";
	//bob
	private static final String UPDATE_MEMPOINT_BY_MEMNO = "update member set mempoint = ? where memno = ?";
	//bob
	private static final String UPDATE_MEMSTATE_BY_MEMNO = "update member set memstate = ? where memno = ?";
	//恭豪
	private static final String UPDATE_MEMSTATE = "update member set memstate=?,memreason=?,memtime=sysdate where memno=?";
	
		
	@Override
	public void insert(MemVO memVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, memVO.getMemaccount());
			pstmt.setString(2, memVO.getMempsw());
			pstmt.setString(3, memVO.getMememail());
			pstmt.setBytes(4, memVO.getMempic());
			pstmt.setString(5, memVO.getMemrandom());			

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(MemVO memVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);

			
			pstmt.setString(1, memVO.getMempsw());
			pstmt.setString(2, memVO.getMemaccount());
			pstmt.setString(3, memVO.getMemname());
			pstmt.setInt(4, memVO.getMemgender());
			pstmt.setString(5, memVO.getMememail());
//			pstmt.setBytes(5, memVO.getMempic());
//			pstmt.setString(6, memVO.getMemreason());
//			pstmt.setTimestamp(7, memVO.getMemtime());
//			pstmt.setInt(8, memVO.getMemVerification());
			pstmt.setInt(6, memVO.getMemno());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer memno) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, memno);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public MemVO findByPrimaryKey(Integer memno) {

		MemVO memVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, memno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				memVO = new MemVO();
				memVO.setMemno(rs.getInt("memno"));
				memVO.setMemaccount(rs.getString("memaccount"));
				memVO.setMempsw(rs.getString("mempsw"));
				memVO.setMememail(rs.getString("mememail"));
				memVO.setMemstate(rs.getInt("memstate"));
				memVO.setMempic(rs.getBytes("memPic"));
				memVO.setMemname(rs.getString("memname"));
				memVO.setMemgender(rs.getInt("memgender"));				
				memVO.setMemreason(rs.getString("memreason"));
				memVO.setMemtime(rs.getTimestamp("memTime"));
				memVO.setMemrandom(rs.getString("memrandom"));
				memVO.setMempoint(rs.getInt("mempoint"));

			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return memVO;
	}

	@Override
	public List<MemVO> getAll() {
		List<MemVO> list = new ArrayList<MemVO>();
		MemVO memVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				memVO = new MemVO();
				memVO.setMemno(rs.getInt("memno"));
				memVO.setMemaccount(rs.getString("memaccount"));
				memVO.setMempsw(rs.getString("mempsw"));
				memVO.setMememail(rs.getString("mememail"));
				memVO.setMemstate(rs.getInt("memstate"));
				memVO.setMempic(rs.getBytes("memPic"));
				memVO.setMemname(rs.getString("memname"));
				memVO.setMemgender(rs.getInt("memgender"));				
				memVO.setMemreason(rs.getString("memreason"));
				memVO.setMemtime(rs.getTimestamp("memTime"));
				memVO.setMemrandom(rs.getString("memrandom"));
				memVO.setMempoint(rs.getInt("mempoint"));
				list.add(memVO);
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	
		@Override
		public MemVO findByMemPsw(String account,String password) {

			MemVO memVO = null;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try {

				Class.forName(driver);
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(GET_account_password_STMT);
	           
				pstmt.setString(1, account);
				pstmt.setString(2, password);

				rs = pstmt.executeQuery();

				while (rs.next()) {
					memVO = new MemVO();
					memVO.setMemno(rs.getInt("memno"));
					memVO.setMemaccount(rs.getString("memaccount"));
					memVO.setMempsw(rs.getString("mempsw"));
					memVO.setMememail(rs.getString("mememail"));
					memVO.setMemstate(rs.getInt("memstate"));
					memVO.setMempic(rs.getBytes("memPic"));
					memVO.setMemname(rs.getString("memname"));
					memVO.setMemgender(rs.getInt("memgender"));				
					memVO.setMemreason(rs.getString("memreason"));
					memVO.setMemtime(rs.getTimestamp("memTime"));
					memVO.setMemrandom(rs.getString("memrandom"));
					memVO.setMempoint(rs.getInt("mempoint"));
				}

				// Handle any driver errors
			}catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return memVO;
		}
		
		
		//hsu
		@Override
		public Set<StoVO> getStosByMemno(Integer memno) {
			Set<StoVO> set = new LinkedHashSet<StoVO>();
			StoVO stoVO = null;

			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try {

				Class.forName(driver);
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(GET_STOS_BY_MEMNO);
				pstmt.setInt(1, memno);

				rs = pstmt.executeQuery();

				while (rs.next()) {				
					stoVO = new StoVO();
					stoVO.setStoreno(rs.getInt("storeno"));
					stoVO.setStoreranno(rs.getString("storeranno"));
					stoVO.setStoretime(rs.getTimestamp("storetime"));
					stoVO.setStoreprice(rs.getInt("storeprice"));
					stoVO.setMemno(rs.getInt("memno"));
					set.add(stoVO);
				}

			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Couldn't load database driver. "
						+ e.getMessage());
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return set;
		}

		//hsu
		@Override
		public Set<BuyVO> getBuysByMemno(Integer memno) {
			Set<BuyVO> set = new LinkedHashSet<BuyVO>();
			BuyVO buyVO = null;

			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try {

				Class.forName(driver);
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(GET_BUYS_BY_MEMNO);
				pstmt.setInt(1, memno);

				rs = pstmt.executeQuery();

				while (rs.next()) {				
					buyVO = new BuyVO();
					buyVO.setBuyno(rs.getInt("buyno"));
					buyVO.setBuytime(rs.getTimestamp("buytime"));
					buyVO.setBuyprice(rs.getInt("buyprice"));
					buyVO.setBuystate(rs.getInt("buystate"));
					buyVO.setMemno(rs.getInt("memno"));
					buyVO.setCouno(rs.getInt("couno"));
					set.add(buyVO);
				}

			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Couldn't load database driver. "
						+ e.getMessage());
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return set;
		}
		
		
		//bob
		@Override
		public void updateMempointByMemno(Integer mempoint, Integer memno) {
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

				Class.forName(driver);
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(UPDATE_MEMPOINT_BY_MEMNO);
				
				pstmt.setInt(1, mempoint);
				pstmt.setInt(2, memno);

				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Couldn't load database driver. "
						+ e.getMessage());
				// Handle any SQL errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			
		}
		
		//bob
		@Override
		public void updateMemstatusByMemno(Integer memstatus, Integer memno) {
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

				Class.forName(driver);
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(UPDATE_MEMSTATE_BY_MEMNO);
				
				pstmt.setInt(1, memstatus);
				pstmt.setInt(2, memno);

				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Couldn't load database driver. "
						+ e.getMessage());
				// Handle any SQL errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			
		}
		
		@Override
		public void updateMemstate(MemVO memVO) {

			Connection con = null;
			PreparedStatement pstmt = null;

			try {
				try {
					Class.forName(driver);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(UPDATE_MEMSTATE);


				pstmt.setInt(1, memVO.getMemstate());
				pstmt.setString(2, memVO.getMemreason());
				pstmt.setInt(3, memVO.getMemno());

				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

		}
	
		
		
	
//		@Override
//		public MemVO uploadpic(Integer memno) {
//
//			MemVO memVO = null;
//			Connection con = null;
//			PreparedStatement pstmt = null;
//			ResultSet rs = null;
//
//			try {
//
//				Class.forName(driver);
//				con = DriverManager.getConnection(url, userid, passwd);
//				pstmt = con.prepareStatement(GET_ONE_STMT_MEMPIC);
//
//				pstmt.setInt(1, memno);
//
//				rs = pstmt.executeQuery();
//
//				while (rs.next()) {
//					
//					memVO = new MemVO();
//					memVO.setMemno(rs.getInt("memno"));
//					memVO.setMemaccount(rs.getString("memaccount"));
//				
//					memVO.setMemname(rs.getString("memname"));
//					memVO.setMemgender(rs.getInt("memgender"));
//					memVO.setMememail(rs.getString("mememail"));
//					memVO.setMempic(rs.getBytes("memPic"));
//
//				}
//
//				// Handle any driver errors
//			} catch (ClassNotFoundException e) {
//				throw new RuntimeException("Couldn't load database driver. "
//						+ e.getMessage());
//				// Handle any SQL errors
//			} catch (SQLException se) {
//				throw new RuntimeException("A database error occured. "
//						+ se.getMessage());
//				// Clean up JDBC resources
//			} finally {
//				if (rs != null) {
//					try {
//						rs.close();
//					} catch (SQLException se) {
//						se.printStackTrace(System.err);
//					}
//				}
//				if (pstmt != null) {
//					try {
//						pstmt.close();
//					} catch (SQLException se) {
//						se.printStackTrace(System.err);
//					}
//				}
//				if (con != null) {
//					try {
//						con.close();
//					} catch (Exception e) {
//						e.printStackTrace(System.err);
//					}
//				}
//			}
//			return memVO;
//		}
	
	
		
	
	
	
	
	
	
	
	
	
	
	
	public static void main(String[] args) throws IOException {
		
		MemJDBCDAO dao = new MemJDBCDAO();

//		FileInputStream in = new FileInputStream(new File("WebContent/Front/mem/images","90002.jpg"));
//		byte[] buffer = new byte[in.available()]; 
		
	
//		java.util.Date du = new java.util.Date();  
	
//		java.sql.Timestamp dt  =  new java.sql.Timestamp(du.getTime());  
		
	//		MemVO memVO1 = new MemVO();	
//		memVO1.setMemaccount("Test002");
//		memVO1.setMempsw("12345");
//		memVO1.setMemname("12defth");
//		memVO1.setMemgender(1);
//		memVO1.setMememail("iii@gmail.com");
//	
//		dao.insert(memVO1);

		//		MemVO memVO2 = new MemVO();
//		memVO2.setMemno(90001);
//		memVO2.setMempsw("987654321");
//		memVO2.setMemname("JIE1");
//		memVO2.setMemgender(1);
//		memVO2.setMememail("ru03g4jie??@gmail.com");
//		memVO2.setMempic(buffer);
//		memVO2.setMemreason("Test update user1");		
//		memVO2.setMemtime(dt);
//		memVO2.setMemVerification(4567);
//		dao.update(memVO2);

	//		dao.delete(90006);

		
		
		//		MemVO memVO3 = dao.uploadpic(90002);	
//		System.out.print(memVO3.getMemno() + ",");
//		System.out.print(memVO3.getMemaccount() + ",");
//		System.out.print(memVO3.getMemname() + ",");
//		System.out.print(memVO3.getMemgender() + ",");
//		System.out.print(memVO3.getMememail() + ",");
//		System.out.print(memVO3.getMempic() + ",");
//		System.out.println("---------------------");
//
//		MemVO memVO4 = dao.findByPrimaryKey(90002);	
//		System.out.print(memVO4.getMemno() + ",");
//		System.out.print(memVO4.getMemaccount() + ",");
//		System.out.print(memVO4.getMemname() + ",");
//		System.out.print(memVO4.getMemgender() + ",");
//		System.out.print(memVO4.getMememail() + ",");
//		System.out.println("---------------------");
		
		
		
		//		List<MemVO> list = dao.getAll();
//		for (MemVO aMem : list) {
//			System.out.print(aMem.getMemno() + ",");
//			System.out.print(aMem.getMemaccount() + ",");	
//			System.out.print(aMem.getMemname() + ",");
//			System.out.print(aMem.getMemgender() + ",");
//			System.out.print(aMem.getMememail() + ",");
//			System.out.print(aMem.getMemstate() + ",");
//			System.out.print(aMem.getMempic() + ",");
//			System.out.print(aMem.getMemreason() + ",");
//			System.out.print(aMem.getMemtime() + ",");
////			System.out.print(aMem.getMemVerification()+"\n");
//			System.out.println("---------------");
//		}
		
		
		
//		dao.updateMempointByMemno(10000, 90001);
		dao.updateMemstatusByMemno(1, 90001);
		
		
		
	}

	


	
	
	
}
