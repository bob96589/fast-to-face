package com.mem.model;

import java.sql.Timestamp;

public class MemVO implements java.io.Serializable{
	private Integer memno;
	private String memaccount;
	private String mempsw;
	private String memname;
	private Integer memgender;
	private String mememail;
	private Integer memstate;
	private byte[] mempic;
	private String memreason;
	private Timestamp memtime;
	private String memrandom;
	private Integer mempoint;
	
	public Integer getMemno() {
		return memno;
	}
	public void setMemno(Integer memno) {
		this.memno = memno;
	}
	public String getMemaccount() {
		return memaccount;
	}
	public void setMemaccount(String memaccount) {
		this.memaccount = memaccount;
	}
	public String getMempsw() {
		return mempsw;
	}
	public void setMempsw(String mempsw) {
		this.mempsw = mempsw;
	}
	public String getMemname() {
		return memname;
	}
	public void setMemname(String memname) {
		this.memname = memname;
	}
	public Integer getMemgender() {
		return memgender;
	}
	public void setMemgender(Integer memgender) {
		this.memgender = memgender;
	}
	public String getMememail() {
		return mememail;
	}
	public void setMememail(String mememail) {
		this.mememail = mememail;
	}
	public Integer getMemstate() {
		return memstate;
	}
	public void setMemstate(Integer memstate) {
		this.memstate = memstate;
	}
	public byte[] getMempic() {
		return mempic;
	}
	public void setMempic(byte[] mempic) {
		this.mempic = mempic;
	}
	public String getMemreason() {
		return memreason;
	}
	public void setMemreason(String memreason) {
		this.memreason = memreason;
	}
	public Timestamp getMemtime() {
		return memtime;
	}
	public void setMemtime(Timestamp memtime) {
		this.memtime = memtime;
	}
	public String getMemrandom() {
		return memrandom;
	}
	public void setMemrandom(String memrandom) {
		this.memrandom = memrandom;
	}
	public Integer getMempoint() {
		return mempoint;
	}
	public void setMempoint(Integer mempoint) {
		this.mempoint = mempoint;
	}
	
	
	

	
	
}
