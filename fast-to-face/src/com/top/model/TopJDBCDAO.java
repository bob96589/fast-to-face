package com.top.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.art.model.ArtVO;

public class TopJDBCDAO implements TopDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "user1";
	String passwd = "u111";
	
	private static final String INSERT_TOP = //輸入資料
	"INSERT INTO topic ( topno,forno,memno,topname,toptime,topstatus) VALUES (TOP_SEQ.NEXTVAL, ?,?,?,sysdate,0)";
	private static final String GET_ALL_STMT = //查詢該表格全部欄位
	"SELECT topno,forno,memno,topname, toptime,topstatus FROM topic  where  topstatus != 2  order by topno";
	private static final String GET_ALL_NEWTOP = //查詢該表格全部欄位
	"SELECT topno,forno,memno,topname, toptime ,topstatus FROM topic where  topstatus != 2  order by topno desc";
	private static final String GET_ONE_STMT = //根據該欄位查詢資料
	"SELECT * FROM topic where topno = ? and topstatus != 2";
	 static final String GET_ONE_STMT2 = //根據該欄位查詢資料
	"SELECT * FROM topic where topno = ? and topstatus != 2";
	private static final String DELETE =    //根據該欄位刪除資料
	"UPDATE topic set topstatus=2  where topno=?";
	private static final String UPDATE =  //更新該表格所有欄位
	"UPDATE topic set forno=?, memno=?, topname=?, toptime=? where topno=?";
	private static final String GET_ALL_TIME = //查詢該表格全部欄位
    "SELECT topno,forno,memno,topname, toptime ,topstatus FROM topic  where  topstatus != 2  order by toptime desc";
	private static final String getShowfor = //查詢XX主題文章下的所有文章
	"select * from article where topno=? and artstate != 2";
	private static final String getShowArtTime =
	"select * from article where topno=? and artstate != 2 and rownum<=1 order by artasktime desc";	
	private static final String GET_MAX_TOPNO = //查詢倒數的TOPNO
    "SELECT max(topno) FROM topic";
	private static final String GET_ONE_NAME =  
	"select * from topic where lower(topname) like ('%'||lower(?)||'%')";
	private static final String GET_ONE_FORNO =  
	"select * from topic where lower(topname) like ('%'||lower(?)||'%') and forno=?"; 
	private static final String GET_ALL_MEMBER = //查詢會員討論區紀錄 
    "SELECT * FROM topic where topno in ( select topno from article where memno= ? ) and topstatus != 2  order by toptime desc";
	private static final String UPDATE_BY_REP =  //檢舉表格-復原主題文章
	"update topic set topstatus = 0 where topno = ?";		
	
	@Override
	public Integer getMaxTopno() {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer topno = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_MAX_TOPNO);	 
			 
			rs = pstmt.executeQuery();
			
			rs.next();
			topno = rs.getInt("max(topno)");
			
			

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return topno;
		
	}
	
	
	
	
	@Override
	public void inserttop(TopVO topVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_TOP);

			pstmt.setInt(1, topVO.getForno());
			pstmt.setInt(2, topVO.getMemno());
			pstmt.setString(3, topVO.getTopname());
		 
			 
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	@Override
	public void update(TopVO topVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			//更新資料須按照SQL指令欄位順序排列 	
			pstmt.setInt(1, topVO.getForno());
			pstmt.setInt(2, topVO.getMemno());
			pstmt.setString(3, topVO.getTopname());
			pstmt.setTimestamp(4, topVO.getToptime());
			pstmt.setInt(5, topVO.getTopno());			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	@Override
	public void delete(Integer topno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, topno);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	@Override
	public TopVO getOnetop(Integer topno) {
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, topno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// forVO 也稱為 Domain objects
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus")); 
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return topVO;		
		 
	}
	@Override
	public List<TopVO> getAll() {
		List<TopVO> list = new ArrayList<TopVO>();
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus")); 
				list.add(topVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;				 
	}	
	
	
	@Override
	public List<TopVO> getallnewtop() {
		List<TopVO> list = new ArrayList<TopVO>();
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_NEWTOP);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus")); 
				list.add(topVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;				 
	}	
	
	
	@Override
	public List<TopVO> getAllTime() {
		List<TopVO> list = new ArrayList<TopVO>();
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_TIME);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus")); 
				list.add(topVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
		
	}
	
	@Override
	public List<ArtVO> getShowfor(Integer topno ){
		List<ArtVO> list = new ArrayList<ArtVO>();
		ArtVO artVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(getShowfor);
			pstmt.setInt(1, topno);
			
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				artVO = new ArtVO();
				artVO.setArtno(rs.getInt("artno"));
				artVO.setMemno(rs.getInt("memno"));
				artVO.setArtasktime(rs.getTimestamp("artasktime"));
				artVO.setArtcon(rs.getString("artcon"));
				artVO.setTopno(rs.getInt("topno")); 
				artVO.setArtstate(rs.getInt("artstate")); 
				list.add(artVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			
			
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		      return list;			 
		 
	}
	
	
	@Override
	public ArtVO getShowArtTime(Integer topno) {   //根據該欄位查詢一筆資料
		ArtVO artVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(getShowArtTime);

			pstmt.setInt(1, topno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// forVO 也稱為 Domain objects
				artVO = new ArtVO();
				artVO.setArtno(rs.getInt("artno"));
				artVO.setMemno(rs.getInt("memno"));
				artVO.setArtasktime(rs.getTimestamp("artasktime"));
				artVO.setTopno(rs.getInt("topno"));
				 		 
			}
		// Handle any driver errors
			} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return artVO;		
		 
	}
	@Override
	public TopVO findByPrimaryKey(Integer topno) {
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT2);

			pstmt.setInt(1, topno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// forVO 也稱為 Domain objects
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus"));  
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return topVO;		
		 
	}
	
	@Override
	public List<TopVO> getAllmember(Integer memno) {   //查詢該表格全部欄位
		List<TopVO> list = new ArrayList<TopVO>();
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			 
			pstmt = con.prepareStatement(GET_ALL_MEMBER);
			pstmt.setInt(1, memno);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus")); 
				list.add(topVO); // Store the row in the list
			}
		// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
			+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		      return list;			 
	}
	
	 
	
	@Override
	public List<TopVO> getOneName(String topname ) {    
		List<TopVO> list = new ArrayList<TopVO>();
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_NAME);

			 
			pstmt.setString(1,topname);

			rs = pstmt.executeQuery();
			
			while(rs.next()){
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus")); 
				list.add(topVO); // Store the row in the list
			}	
			
		// Handle any driver errors
			} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;				 
	}
	
	
	@Override
	public List<TopVO> getOneForno(String topname ,Integer forno  ) {    
		List<TopVO> list = new ArrayList<TopVO>();
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_FORNO);

			pstmt.setString(1,topname);
			pstmt.setInt(2, forno);

			rs = pstmt.executeQuery();

			while(rs.next()){
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus")); 
				
				list.add(topVO); // Store the row in the list
			}	 		 			
		// Handle any driver errors
			} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;				 
	}
	
	
	@Override
	public void updateByRep(Integer topno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE_BY_REP);
			//更新資料須按照SQL指令欄位順序排列 	
			 
			pstmt.setInt(1, topno); 	
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	 
	
//	@Override
//	public int getartcount(Integer topno) {
//		ArtVO artVO = null;
//		Connection con = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		try {
//
//			Class.forName(driver);
//			con = DriverManager.getConnection(url, userid, passwd);
//			pstmt = con.prepareStatement(GET_ART_COUNT);
//
//			pstmt.setInt(1, topno);
//
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//				// forVO 也稱為 Domain objects
//			    artVO = new ArtVO();
//				artVO.setTopno(rs.getInt("topno"));
//				 
//				 
//			}
//
//			// Handle any driver errors
//		} catch (ClassNotFoundException e) {
//			throw new RuntimeException("Couldn't load database driver. "
//					+ e.getMessage());
//			// Handle any SQL errors
//		} catch (SQLException se) {
//			throw new RuntimeException("A database error occured. "
//					+ se.getMessage());
//			// Clean up JDBC resources
//		} finally {
//			if (rs != null) {
//				try {
//					rs.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (pstmt != null) {
//				try {
//					pstmt.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (con != null) {
//				try {
//					con.close();
//				} catch (Exception e) {
//					e.printStackTrace(System.err);
//				}
//			}
//		}
//		return artVO;		
//		 
//	}
//	
	

public static void main(String[] args) {

	TopJDBCDAO dao = new TopJDBCDAO();

	// 新增
//	TopVO topVO1 = new TopVO();
//	topVO1.setForno(150001);
//	topVO1.setMemno(90002);
//	topVO1.setTopname("JAVA好難222");
//	topVO1.setToptime(java.sql.Timestamp.valueOf("2014-10-31 15:00:00"));	 
//	dao.insert(topVO1);

//    
//	// 修改
//	TopVO topVO2 = new TopVO();	
//	topVO2.setTopno(160001);
//	topVO2.setForno(150002);
//	topVO2.setMemno(90001);
//	topVO2.setTopname("JAVA難AAAA");
//	topVO2.setToptime(java.sql.Timestamp.valueOf("2014-11-01 12:00:00"));		
//	dao.update(topVO2);
//	
	dao.updateByRep(160001);

//	// 查詢
//	TopVO topVO3 = dao.findByPrimaryKey(160001);
//	System.out.print(topVO3.getTopno() + ",");
//	System.out.print(topVO3.getForno() + ",");
//	System.out.print(topVO3.getMemno() + ",");
//	System.out.print(topVO3.getTopname() + ",");
//	System.out.print(topVO3.getToptime() + ",");	 
//	System.out.println("---------------------");

//	 查詢
//	List<TopVO> list = dao.getallnewtop();
//	for (TopVO topVO4 : list) {
//		System.out.print(topVO4.getTopno() + ",");
//		System.out.print(topVO4.getForno() + ",");
//		System.out.print(topVO4.getMemno() + ",");
//		System.out.print(topVO4.getTopname() + ",");
//		System.out.print(topVO4.getToptime() + ",");
//		System.out.println();
//	} 
//	
//	
	
	
//	List<ArtVO> artlist = dao.getShowfor(new Integer(160001));
//	for (ArtVO artVO4 : artlist) {
//		System.out.print(artVO4.getArtno() + ",");
//		System.out.print(artVO4.getMemno() + ",");
//		System.out.print(artVO4.getArtasktime() + ",");
//		System.out.print(artVO4.getArtcon() + ",");
//		System.out.print(artVO4.getTopno() + ",");
//		System.out.print(artVO4.getArtstate() + ",");
//		System.out.println();
//	} 
//	
//	 ArtVO artvo1 = dao.getShowArtTime(new Integer(160001));
//	 
//		System.out.print(artvo1.getArtno() + ",");
//		System.out.print(artvo1.getMemno() + ",");
//		System.out.print(artvo1.getArtasktime() + ",");		
//		System.out.print(artvo1.getTopno() + ",");
//		
//		System.out.println();
//	 
//	
//	System.out.println(dao.getMaxTopno());
	
	// 查詢
//	List<TopVO> artlist = dao.getOneName("java");
//	for (TopVO artVO4 : artlist) {
//		System.out.print(artVO4.getArtno() + ",");
//		System.out.print(artVO4.getMemno() + ",");
//		System.out.print(artVO4.getArtasktime() + ",");
//		System.out.print(artVO4.getArtcon() + ",");
//		System.out.print(artVO4.getTopno() + ",");
//		System.out.print(artVO4.getArtstate() + ",");
//		System.out.println();
// 	} 
	
//	List<TopVO> artlist = dao.getOneName("j" );
//	for (TopVO topVO : artlist) {
//		System.out.print(topVO.getTopno() + ",");
//		System.out.print(topVO.getForno() + ",");
//		System.out.print(topVO.getMemno() + ",");
//		System.out.print(topVO.getTopname() + ",");
//		System.out.print(topVO.getToptime() + ",");
//		System.out.print(topVO.getTopstatus() + ",");
//		
//		System.out.println();
//	} 

//	List<TopVO> artlist = dao.getOneForno("j",new Integer(150003));
//	for (TopVO topVO : artlist) {
//		System.out.print(topVO.getTopno() + ",");
//		System.out.print(topVO.getForno() + ",");
//		System.out.print(topVO.getMemno() + ",");
//		System.out.print(topVO.getTopname() + ",");
//		System.out.print(topVO.getToptime() + ",");
//		System.out.print(topVO.getTopstatus() + ",");
//		
//		System.out.println();
//	} 


} 
    
}
