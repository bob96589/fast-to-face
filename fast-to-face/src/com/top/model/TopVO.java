package com.top.model;

 
import java.sql.Timestamp;

public class TopVO implements java.io.Serializable{
	private Integer   topno ;
	private Integer   forno ;
	private Integer   memno ;
	private String    topname ;
	private Timestamp toptime ;
	private Integer   topstatus ; 
	
	public Integer getTopstatus() {
		return topstatus;
	}
	public void setTopstatus(Integer topstatus) {
		this.topstatus = topstatus;
	}
	public Integer getTopno() {
		return topno;
	}
	public void setTopno(Integer topno) {
		this.topno = topno;
	}
	public Integer getForno() {
		return forno;
	}
	public void setForno(Integer forno) {
		this.forno = forno;
	}
	public Integer getMemno() {
		return memno;
	}
	public void setMemno(Integer memno) {
		this.memno = memno;
	}
	public String getTopname() {
		return topname;
	}
	public void setTopname(String topname) {
		this.topname = topname;
	}
	public Timestamp getToptime() {
		return toptime;
	}
	public void setToptime(Timestamp toptime) {
		this.toptime = toptime;
	}
	
	

	
}
