package com.top.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;













import com.art.model.ArtVO;
import com.sun.org.apache.xpath.internal.operations.And;
public class TopDAO implements TopDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/TestDB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	private static final String INSERT_TOP = //輸入資料
	"INSERT INTO topic ( topno,forno,memno,topname,toptime,topstatus) VALUES (TOP_SEQ.NEXTVAL, ?,?,?,sysdate,0)";
	private static final String GET_ALL_TOP = //查詢該表格全部欄位依TOPNO排列
	"SELECT topno,forno,memno,topname, toptime ,topstatus FROM topic where  topstatus != 2  order by topno";
	private static final String GET_ALL_NEWTOP = //查詢該表格全部欄位依TOPTIME排列
	"SELECT topno,forno,memno,topname, toptime ,topstatus FROM topic  where  topstatus != 2 order by topno desc";
	private static final String GET_ONE_TOP = //根據該欄位查詢一筆資料
	"SELECT * FROM topic where topno = ? and topstatus != 2";
	private static final String DELETE =    //根據該欄位刪除一筆資料
	"UPDATE topic set topstatus=2  where topno=?";
	private static final String UPDATE =  //更新該表格所有欄位
	"UPDATE topic set forno=?, memno=?, topname=?, toptime=sysdate where topno=?";
	private static final String GET_ALL_TIME = //查詢所有的主題文章並依照最新發表主題時間排列
	"SELECT topno,forno,memno,topname, toptime ,topstatus FROM topic where topstatus != 2  order by toptime desc";
	private static final String getShowfor = //查詢XX主題文章下的所有文章
	"select * from article where topno=? and artstate != 2 order by artasktime " ;
	private static final String GET_ART_COUNT = //根據該欄位查詢一筆資料
	"select count(*) from article  where topno=? and artstate != 2";
	private static final String getShowArtTime =
	"select * from (select * from article where topno=? and artstate != 2  order by artasktime desc) where  rownum<=1";
	//"select * from article where topno=? and rownum<=1 order by artasktime desc";
	private static final String GET_MAX_TOPNO = //查詢倒數的TOPNO來新增主題用
    "SELECT max(topno) FROM topic  where  topstatus != 2";
	private static final String GET_ONE_STMT2 = //根據該欄位查詢一筆資料
	"SELECT * FROM topic where topno = ? and  topstatus != 2";
	private static final String GET_ONE_NAME =   //關鍵字搜尋全部類別
	 "select * from topic where lower(topname) like ('%'||lower(?)||'%') and topstatus != 2  order by toptime desc"; 
	private static final String GET_ONE_FORNO =  //關鍵字搜尋個別類別
    "select * from topic where lower(topname) like ('%'||lower(?)||'%') and forno=?  and topstatus != 2  order by toptime desc" ;     
	private static final String GET_ALL_MEMBER = //查詢會員討論區紀錄
	"SELECT * FROM topic where topno in ( select topno from article where memno=?) and topstatus != 2  order by toptime desc";
	private static final String UPDATE_BY_REP =  //檢舉表格-復原主題文章
	"update topic set topstatus = 0 where topno = ?";
	@Override
	public Integer getMaxTopno() {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer topno = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_MAX_TOPNO);	 
			 
			rs = pstmt.executeQuery();
			
			rs.next();
			topno = rs.getInt("max(topno)");
			
			

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return topno;
		
	}
	
	
	
	@Override
	public void inserttop(TopVO topVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_TOP);

			pstmt.setInt(1, topVO.getForno());
			pstmt.setInt(2, topVO.getMemno());
			pstmt.setString(3, topVO.getTopname());
			//pstmt.setTimestamp(4, topVO.getToptime());			 
			pstmt.executeUpdate();
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	@Override
	public void update(TopVO topVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			//更新資料須按照SQL指令欄位順序排列 	
			pstmt.setInt(1, topVO.getForno());
			pstmt.setInt(2, topVO.getMemno());
			pstmt.setString(3, topVO.getTopname());
			//pstmt.setTimestamp(4, topVO.getToptime());
			pstmt.setInt(4, topVO.getTopno());			
			pstmt.executeUpdate();
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	@Override
	public void delete(Integer topno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);
			pstmt.setInt(1, topno);
			pstmt.executeUpdate();
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	@Override
	public TopVO getOnetop(Integer topno) {   //根據該欄位查詢一筆資料
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_TOP);

			pstmt.setInt(1, topno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// forVO 也稱為 Domain objects
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus")); 
			}
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return topVO;		
		 
	}
	@Override
	public List<TopVO> getAll() {   //查詢該表格全部欄位
		List<TopVO> list = new ArrayList<TopVO>();
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_TOP);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus")); 
				list.add(topVO); // Store the row in the list
			}
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		      return list;			 
	}	
	
	
	@Override
	public List<TopVO> getallnewtop() {   //查詢該表格全部欄位
		List<TopVO> list = new ArrayList<TopVO>();
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_NEWTOP);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus")); 
				list.add(topVO); // Store the row in the list
			}
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		      return list;			 
	}	
	@Override
	public List<TopVO> getAllTime() {   //查詢該表格全部欄位
		List<TopVO> list = new ArrayList<TopVO>();
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_TIME);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus")); 
				list.add(topVO); // Store the row in the list
			}
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		      return list;			 
	}
	@Override
	public List<ArtVO> getShowfor(Integer topno ){   //查詢XX主題下的所有文章
		List<ArtVO> list = new ArrayList<ArtVO>();
		ArtVO artVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(getShowfor);
			pstmt.setInt(1, topno);
			
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				artVO = new ArtVO();
				artVO.setArtno(rs.getInt("artno"));
				artVO.setMemno(rs.getInt("memno"));
				artVO.setArtasktime(rs.getTimestamp("artasktime"));
				artVO.setArtcon(rs.getString("artcon"));
				artVO.setTopno(rs.getInt("topno")); 
				artVO.setArtstate(rs.getInt("artstate")); 
				 
				list.add(artVO); // Store the row in the list
			}
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		      return list;			 		 
	}
	
	@Override
	public ArtVO getShowArtTime(Integer topno) {   //根據該欄位查詢一筆資料
		ArtVO artVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(getShowArtTime);

			pstmt.setInt(1, topno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// forVO 也稱為 Domain objects
				artVO = new ArtVO();
				artVO.setArtno(rs.getInt("artno"));
				artVO.setMemno(rs.getInt("memno"));
				artVO.setArtasktime(rs.getTimestamp("artasktime"));
				artVO.setTopno(rs.getInt("topno"));
				 		 
			}
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return artVO;		
		 
	}
	
	
	@Override
	public TopVO findByPrimaryKey(Integer topno) {   //根據該欄位查詢一筆資料
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT2);

			pstmt.setInt(1, topno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// forVO 也稱為 Domain objects
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));	
				topVO.setTopstatus(rs.getInt("topstatus")); 
			}
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return topVO;				 
	}
	//查詢會員討論區發文紀錄
	@Override
	public List<TopVO> getAllmember(Integer memno) {   
		List<TopVO> list = new ArrayList<TopVO>();
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;		
		ResultSet rs = null;

		try {
			 
			con = ds.getConnection(); 
			pstmt = con.prepareStatement(GET_ALL_MEMBER);
			pstmt.setInt(1, memno);
			rs = pstmt.executeQuery();
			
			
			while (rs.next()) {
				 
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus")); 
				list.add(topVO); // Store the row in the list
			} 
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		      return list;			 
	}
	
	
	
	//關鍵字搜尋全部類別
	@Override
	public List<TopVO> getOneName(String topname ) {    
		List<TopVO> list = new ArrayList<TopVO>();
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			
			con = ds.getConnection(); 
			pstmt = con.prepareStatement(GET_ONE_NAME);

			 
			pstmt.setString(1,topname);

			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus")); 
				list.add(topVO); // Store the row in the list
			}
		 
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;		
		 
	}
	//關鍵字搜尋個別類別
	@Override
	public List<TopVO> getOneForno(String topname ,Integer forno  ) {    
		List<TopVO> list = new ArrayList<TopVO>();
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			
			con = ds.getConnection(); 
			pstmt = con.prepareStatement(GET_ONE_FORNO);

			pstmt.setString(1,topname);
			pstmt.setInt(2, forno);

			rs = pstmt.executeQuery();
	      while (rs.next()) {
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus")); 
				list.add(topVO); // Store the row in the list
			   }
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;		
		 
	}
	
	@Override
	public void updateByRep (Integer topno) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_BY_REP);

			pstmt.setInt(1, topno);

			pstmt.executeUpdate();
		}	 
			// Handle any SQL errors
		  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

		
		
	
}
