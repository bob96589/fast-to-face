package com.top.model;

import java.util.*;
import java.sql.*;

import com.art.model.ArtVO;

public class TopService {
      private TopDAO_interface dao ;
      public TopService() {
  		dao = new TopDAO();
  	}

  	public TopVO inserttop(Integer forno , Integer memno , String topname 
  		   ) {

  		TopVO topVO = new TopVO();
  		topVO.setForno(forno);
  		topVO.setMemno(memno);
  		topVO.setTopname(topname);
  		//topVO.setToptime(toptime);  		 
  		dao.inserttop(topVO);
  		return topVO;
  	}

  	public TopVO update(Integer topno ,Integer forno , Integer memno , String topname 
  			 ) {


  		TopVO topVO = new TopVO();
  		topVO.setTopno(topno);
  		topVO.setForno(forno);
  		topVO.setMemno(memno);
  		topVO.setTopname(topname);
  		//topVO.setToptime(toptime);  
  		dao.update(topVO);
  		TopVO new_topVO = dao.getOnetop(topno); //重新再撈一次資料 
  		return new_topVO;
  	}

  	public void delete(Integer topno) {
  		dao.delete(topno);
  	}

  	public TopVO getOnetop(Integer topno) {
  		return dao.getOnetop(topno);
  	}

  	public List<TopVO> getAll() {
  		return dao.getAll();
  	}
  	
  	public List<TopVO> getallnewtop() {
  		return dao.getallnewtop();
  	}
  	
  	public List<TopVO> getAllTime() {
  		return dao.getAllTime();
  	}
  	
  	public List<ArtVO> getShowfor(Integer topno) {
  		return dao.getShowfor(topno);
  	}
  	
  	public int getShowfor2(Integer topno) {
  		return dao.getShowfor(topno).size()-1;
  	}
  	
	public ArtVO getShowArtTime(Integer topno) {
  		return dao.getShowArtTime(topno);
  	}
	
	public Integer getMaxTopno(){
		return dao.getMaxTopno();
		
	}
  	
	public TopVO updatetop(Integer topno ,Integer forno , Integer memno , String topname 
 			 ) {


 		TopVO topVO = new TopVO();
 		topVO.setTopno(topno);
 		topVO.setForno(forno);
 		topVO.setMemno(memno);
 		topVO.setTopname(topname);
 		//topVO.setToptime(toptime);  
 		dao.update(topVO);
 		TopVO new_topVO = dao.findByPrimaryKey(topno); //重新再撈一次資料 
 		return new_topVO;
 	}
	
	public List<TopVO> getAllmember(Integer memno) {
  		return dao.getAllmember(memno);
  	}
	
	public  List<TopVO> getOneName(String topname ){        //關鍵字搜尋全部類別
	 
  		return dao.getOneName(topname);
  	}
	
	public  List<TopVO> getOneForno(String topname,Integer forno ){  //關鍵字搜尋個別類別
		 
  		return dao.getOneForno(topname,forno);
  	}
	
	public void updateByRep(Integer topno) {
  		dao.updateByRep(topno);
  	}
	
}
