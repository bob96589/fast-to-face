package com.top.model;

import java.util.List;

import com.art.model.ArtVO;
import com.fro.model.ForVO;

public interface TopDAO_interface   {
	//public void insert(TopVO topVO);
	public void updateByRep(Integer topno);
	public void delete(Integer topno);
	public TopVO getOnetop(Integer topno);
	public List<TopVO> getAll();
	public List<TopVO> getAllTime();
	public List<ArtVO> getShowfor(Integer topno );
	public ArtVO getShowArtTime(Integer topno);
	public void inserttop(TopVO topVO);
	public Integer getMaxTopno();
	public List<TopVO> getallnewtop();
	public TopVO findByPrimaryKey(Integer topno); 
	public List<TopVO> getOneName( String topname );
	public List<TopVO> getOneForno( String topname ,Integer forno);
	public List<TopVO> getAllmember(Integer member ); 
	public void update(TopVO topVO);
}
