package com.app.model;

import java.util.*;

public interface AppDAO_interface {
          public void insert(AppVO appVO);
          public void update(AppVO appVO);
          public void delete(Integer memno,Integer couno);
          public AppVO findByPrimaryKey(Integer memno,Integer couno);
          public List<AppVO> getAll();       
}
