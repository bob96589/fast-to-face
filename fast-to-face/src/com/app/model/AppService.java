package com.app.model;

import java.util.List;

public class AppService {

	private AppDAO_interface dao;

	public AppService() {
		dao = new AppDAO();
	}

	public AppVO addApp(Integer memno,Integer couno,Double appscore) {

		AppVO appVO = new AppVO();
		appVO.setMemno(memno);
		appVO.setCouno(couno);
		appVO.setAppscore(appscore);
		dao.insert(appVO);

		return appVO;
	}

	public AppVO updateApp( Integer memno,Integer couno,Double appscore) {

		AppVO appVO = new AppVO();
		appVO.setMemno(memno);
		appVO.setCouno(couno);
		appVO.setAppscore(appscore);
		dao.update(appVO);

		return appVO;
	}

	public void deleteApp(Integer memno,Integer couno) {
		dao.delete(memno,couno);
	}

	public AppVO getOneApp(Integer memno,Integer couno) {
		return dao.findByPrimaryKey(memno, couno);
	}

	public List<AppVO> getAll() {
		return dao.getAll();
	}
}
