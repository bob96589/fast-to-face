package com.app.model;
import java.sql.Date;

import com.sun.jmx.snmp.Timestamp;

public class AppVO implements java.io.Serializable{
	private Integer memno;
	private Integer couno;
	private Double appscore;
	
	public Integer getMemno() {
		return memno;
	}
	public void setMemno(Integer memno) {
		this.memno = memno;
	}
	public Integer getCouno() {
		return couno;
	}
	public void setCouno(Integer couno) {
		this.couno = couno;
	}
	public Double getAppscore() {
		return appscore;
	}
	public void setAppscore(Double appscore) {
		this.appscore = appscore;
	}
	
	
	
	
}
