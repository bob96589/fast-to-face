package com.app.model;

import java.util.*;
import java.sql.*;

public class AppJDBCDAO implements AppDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "user1";
	String passwd = "u111";

	private static final String INSERT_STMT = 
		"INSERT INTO appraise (memno,couno,appscore) VALUES (?, ?, ?)";
	private static final String GET_ALL_STMT = 
		"SELECT * FROM appraise ";
	private static final String GET_ONE_STMT = 
		"SELECT * FROM appraise where memno=? and couno=?";
	private static final String DELETE = 
		"DELETE FROM appraise where memno=? and couno=?";
	private static final String UPDATE = 
		"UPDATE appraise set appscore=? where memno=? and couno=?";

	@Override
	public void insert(AppVO appVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setInt(1, appVO.getMemno());
			pstmt.setInt(2, appVO.getCouno());
			pstmt.setDouble(3, appVO.getAppscore());
			

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(AppVO appVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setDouble(1, appVO.getAppscore());
			pstmt.setInt(2, appVO.getMemno());
			pstmt.setInt(3, appVO.getCouno());
			

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer memno,Integer couno) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, memno);
			pstmt.setInt(2, couno);
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public AppVO findByPrimaryKey(Integer memno,Integer couno) {

		AppVO appVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, memno);
			pstmt.setInt(2, couno);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				appVO = new AppVO();
				appVO.setMemno(rs.getInt("memno"));
				appVO.setCouno(rs.getInt("couno"));
				appVO.setAppscore(rs.getDouble("appscore"));
				
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return appVO;
	}

	@Override
	public List<AppVO> getAll() {
		List<AppVO> list = new ArrayList<AppVO>();
		AppVO appVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				appVO = new AppVO();
				appVO.setMemno(rs.getInt("memno"));
				appVO.setCouno(rs.getInt("couno"));
				appVO.setAppscore(rs.getDouble("appscore"));
				list.add(appVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	public static void main(String[] args) {

		AppJDBCDAO dao = new AppJDBCDAO();

		
//		AppVO appVO1 = new AppVO();
//		appVO1.setMemno(90003);
//		appVO1.setCouno(10007);
//		appVO1.setAppscore(4.8);
//		dao.insert(appVO1);
//
//		
//		AppVO appVO2 = new AppVO();
//		appVO1.setMemno(90001);
//		appVO1.setMemno(10001);
//		appVO1.setAppscore(4.8);
//		//dao.update(appVO2);
//
//		
//		//dao.delete(90004,10004);

		
		AppVO appVO3 = dao.findByPrimaryKey(90061,10001);
		System.out.print(appVO3);
//		System.out.print(appVO3.getMemno() + ",");
//		System.out.print(appVO3.getCouno() + ",");
//		System.out.print(appVO3.getAppscore() + ",");
//		System.out.println("---------------------");
//
//		
//		List<AppVO> list = dao.getAll();
//		for (AppVO aQue : list) {
//			System.out.print(aQue.getMemno() + ",");
//			System.out.print(aQue.getCouno() + ",");
//			System.out.print(aQue.getAppscore() + ",");
//			System.out.println();
//		}
	}
}