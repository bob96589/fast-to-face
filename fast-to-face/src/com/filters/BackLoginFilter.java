package com.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.acc.model.AccVO;

public class BackLoginFilter implements Filter {

private FilterConfig config;
	
	public void init(FilterConfig config){
		this.config = config;
	}
	
	public void destroy(){
		config = null;
	}
    
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException{
		
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		
		HttpSession session = req.getSession();
		AccVO accVO = (AccVO)session.getAttribute("accVO"); 
		if (accVO == null) {
			res.sendRedirect(req.getContextPath() + "/back/backlogin/backlogin.jsp");
			return;
		}else{
			chain.doFilter(request,response);
		}
	}
   

}
