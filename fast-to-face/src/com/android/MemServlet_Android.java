package com.android;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mem.model.MemService;
import com.mem.model.MemVO;

public class MemServlet_Android extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest rq, HttpServletResponse rp)
			throws ServletException, IOException {
		ServletContext context = getServletContext();
		String contentType = context.getInitParameter("contentType");

		String name = rq.getParameter("name"); // 1接參數
		String password = rq.getParameter("password"); // 2接參數

		MemService memService = new MemService();
		MemVO memVO = memService.getMemPsw(name, password); // 3找vo

		Gson gson = new Gson();
		String memVOStr = gson.toJson(memVO);

		rp.setContentType(contentType);
		PrintWriter out = rp.getWriter();
		out.println(memVOStr); // 4給我vo
		out.close();

	}

	public void doPost(HttpServletRequest rq, HttpServletResponse rp)
			throws ServletException, IOException {
		doGet(rq, rp);
	}

}
