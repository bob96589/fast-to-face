package com.android;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.buy.model.BuyService;
import com.content.model.ConVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.fin.model.FinService;
import com.google.gson.Gson;
import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.unit.model.UnitService;
import com.unit.model.UnitVO;

@SuppressWarnings("serial")
public class CouBuying_Android extends HttpServlet {

	public void doGet(HttpServletRequest rq, HttpServletResponse rp)
			throws ServletException, IOException {
		doPost(rq, rp);

	}

	public void doPost(HttpServletRequest rq, HttpServletResponse rp)
			throws ServletException, IOException {

		ServletContext context = getServletContext();
		String contentType = context.getInitParameter("contentType");
		String outStr = "";

		Gson gson = new Gson();

		String action = rq.getParameter("buying");
		if (action.equals("buyCou")) {
			Integer memno = new Integer(rq.getParameter("memno"));
			Integer couno = new Integer(rq.getParameter("couno"));
System.out.println("3333333333"+memno);
System.out.println("444444444"+couno);
			// CouVO
			CouService couSvc = new CouService();
			CouVO couVO = couSvc.getOneCou(couno);

			// MemVO
			MemService memSvc = new MemService();
			MemVO memVO = memSvc.getOneMem(memno);

			// pay
			memSvc.updateMempointByMemno(
					memVO.getMempoint() - couVO.getCoudiscount(),
					memVO.getMemno());

			// add buy record
			BuyService buySvc = new BuyService();
			buySvc.addBuy(couVO.getCoudiscount(), memVO.getMemno(), couno);

			// add finish record
			FinService finSvc = new FinService();
			Set<UnitVO> unitVOSet = couSvc.getUnitsByCouno(couno);

			for (UnitVO unitVO : unitVOSet) {
				UnitService unitSvc = new UnitService();
				Set<ConVO> conVOSet = unitSvc.getConsByUnitno(unitVO
						.getUnitno());
				for (ConVO conVO : conVOSet) {
					finSvc.addFin(memVO.getMemno(), conVO.getConno());
				}
			}
			MemVO mem = memSvc.getOneMem(memno);
			outStr = gson.toJson(mem);
			System.out.println("1111111111111"+mem);
		}
		rp.setContentType(contentType);
		PrintWriter out = rp.getWriter();
		out.println(outStr);
		out.close();
	}
}
