package com.android;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.sto.model.StoService;
import com.sto.model.StoVO;

/**
 * Servlet implementation class StoServlet_Android
 */
public class StoServlet_Android extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public StoServlet_Android() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest rq, HttpServletResponse rp)
			throws ServletException, IOException {
		doPost(rq, rp);
	}

	protected void doPost(HttpServletRequest rq, HttpServletResponse rp)
			throws ServletException, IOException {
		ServletContext context = getServletContext();
		String contentType = context.getInitParameter("contentType");
		String outStr = "";
		Gson gson = new Gson();
		Integer storeprice = new Integer(rq.getParameter("storeprice"));
		Integer memno = new Integer(rq.getParameter("memno"));
System.out.println(storeprice+memno+"22222222222222222");
		MemService memSvc = new MemService();
		MemVO memVO = memSvc.getOneMem(memno);
		// add money

		memSvc.updateMempointByMemno(memVO.getMempoint() + storeprice,
				memVO.getMemno());
		MemVO newmemVO = memSvc.getOneMem(memno);
		outStr = gson.toJson(newmemVO);
		rp.setContentType(contentType);
		PrintWriter out = rp.getWriter();
		out.println(outStr);
		out.close();
	}

}
