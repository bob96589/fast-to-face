package com.android;

import java.util.List;

import com.buy.model.BuyVO;
import com.cou.model.CouVO;


public class MemVO_Android {
			private List<BuyVO> buyVOList;
			private List<CouVO> couVOList;
			
			
			public List<CouVO> getCouVOList() {
				return couVOList;
			}
			public void setCouVOList(List<CouVO> couVOList) {
				this.couVOList = couVOList;
			}
			public List<BuyVO> getBuyVOList() {
				return buyVOList;
			}
			public void setBuyVOList(List<BuyVO> buyVOList) {
				this.buyVOList = buyVOList;
			}
			
	
		 
}
