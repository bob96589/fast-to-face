package com.android;

import javax.servlet.*;
import javax.servlet.http.*;

import com.google.gson.Gson;
import com.pro.model.ProService;
import com.pro.model.ProVO;

import java.util.List;
import java.io.*;

public class ProServlet_Android extends HttpServlet {
	@SuppressWarnings("serial")
	public void doGet(HttpServletRequest rq, HttpServletResponse rp)
			throws ServletException, IOException {
		doPost(rq, rp);

	}

	public void doPost(HttpServletRequest rq, HttpServletResponse rp)
			throws ServletException, IOException {

		ServletContext context = getServletContext();
		String contentType = context.getInitParameter("contentType");
		String outStr = "";

		// 資料來源為Web專案本身
		// ProductDAO productDAO = new ProductDAOFileImpl(context);
		// 資料來源為DB
		ProService proService = new ProService();

		Gson gson = new Gson();

		String action = rq.getParameter("imageSize");
		if (action.equals("small")) {
			List<ProVO> proVO = proService.getAll();
			// 將所有照片縮小
			// for (int i = 0; i < products.size(); i++) {
			// Product product = products.get(i);
			// product.setImage(ImageUtil.shrink(product.getImage(), 100));
			// }
			outStr = gson.toJson(proVO);

		}
		// else if (action.equals("large")) {
		// String couno = rq.getParameter("id");
		// int id = Integer.parseInt(couno);
		// CouVO couVO = couService.getOneCou(id);
		// outStr = gson.toJson(couVO);
		// }
		rp.setContentType(contentType);
		PrintWriter out = rp.getWriter();
		out.println(outStr);
		out.close();
	}
}
