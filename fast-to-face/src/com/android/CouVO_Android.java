package com.android;



import java.util.List;

import com.acc.model.AccVO;
import com.content.model.ConVO;
import com.cou.model.CouVO;
import com.unit.model.UnitVO;


public class CouVO_Android {
	private CouVO couVO;
	private AccVO accVO;
	private List<UnitVO> UnitVOList;
	private List<ConVO> conVOList;
	
	
	public List<ConVO> getConVOList() {
		return conVOList;
	}
	public void setConVOList(List<ConVO> conVOList) {
		this.conVOList = conVOList;
	}
	public List<UnitVO> getUnitVOList() {
		return UnitVOList;
	}
	public void setUnitVOList(List<UnitVO> unitVOList) {
		UnitVOList = unitVOList;
	}
	public CouVO getCouVO() {
		return couVO;
	}
	public void setCouVO(CouVO couVO) {
		this.couVO = couVO;
	}
	public AccVO getAccVO() {
		return accVO;
	}
	public void setAccVO(AccVO accVO) {
		this.accVO = accVO;
	}

}
