package com.android;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.*;
import javax.servlet.http.*;

import com.buy.model.BuyVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.google.gson.Gson;
import com.mem.model.MemService;

public class MemServletBuy_Android extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest rq, HttpServletResponse rp)
			throws ServletException, IOException {
		doPost(rq, rp);
	}

	@SuppressWarnings("null")
	public void doPost(HttpServletRequest rq, HttpServletResponse rp)
			throws ServletException, IOException {

		ServletContext context = getServletContext();
		String contentType = context.getInitParameter("contentType");
		String outStr = "";
		CouService couService = new CouService();
		Gson gson = new Gson();
		String action = rq.getParameter("action");
		List<CouVO> couVOList = new ArrayList<CouVO>();
		if ("getBuyByMem".equals(action)) {
			Integer memno = new Integer(rq.getParameter("memno"));

			MemService buySvc = new MemService();
			Set<BuyVO> buyVOSet = buySvc.getBuysByMemno(memno);
			
			List<BuyVO> buyVOList = new ArrayList<BuyVO>(buyVOSet);

			for (BuyVO buyVO : buyVOList) {

				couVOList.add(couService.getOneCou(buyVO.getCouno()));

			}

			
			
			outStr = gson.toJson(couVOList);

		}
		rp.setContentType(contentType);
		PrintWriter outbuy = rp.getWriter();
		outbuy.println(outStr);
		outbuy.close();

	}

}
