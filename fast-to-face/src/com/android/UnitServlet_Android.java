package com.android;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.content.model.ConVO;
import com.google.gson.Gson;
import com.unit.model.UnitService;

@SuppressWarnings("serial")
public class UnitServlet_Android extends HttpServlet {

	public void doGet(HttpServletRequest rq, HttpServletResponse rp)
			throws ServletException, IOException {
		doPost(rq, rp);

	}

	public void doPost(HttpServletRequest rq, HttpServletResponse rp)
			throws ServletException, IOException {

		ServletContext context = getServletContext();
		String contentType = context.getInitParameter("contentType");
		String outStr = "";
		UnitService usv = new UnitService();
		Gson gson = new Gson();

String action = rq.getParameter("imageSize");
		if (action.equals("conList")) {
			
			String UniId = rq.getParameter("id");
			System.out.println("2222"+UniId);
			int unitno= Integer.parseInt(UniId);
			Set<ConVO> conVOSet = usv.getConsByUnitno(unitno);
			List<ConVO> conList = new ArrayList<ConVO>(conVOSet);
			
			outStr = gson.toJson(conList);
			System.out.println("1111"+conList);
		}
		rp.setContentType(contentType);
		PrintWriter out = rp.getWriter();
		System.out.println("3333333"+outStr);
		out.println(outStr);
		out.close();
	}
}