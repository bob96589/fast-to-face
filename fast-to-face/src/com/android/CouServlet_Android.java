package com.android;

import javax.servlet.*;
import javax.servlet.http.*;

import com.acc.model.AccService;
import com.acc.model.AccVO;
import com.buy.model.BuyVO;
import com.content.model.ConVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.google.gson.Gson;
import com.mem.model.MemService;
import com.android.MemVO_Android;
import com.unit.model.UnitVO;
import com.android.CouVO_Android;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.io.*;

public class CouServlet_Android extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest rq, HttpServletResponse rp)
			throws ServletException, IOException {
		doPost(rq, rp);

	}

	public void doPost(HttpServletRequest rq, HttpServletResponse rp)
			throws ServletException, IOException {

		ServletContext context = getServletContext();
		String contentType = context.getInitParameter("contentType");
		String outStr = "";

		// 資料來源為Web專案本身
		// ProductDAO productDAO = new ProductDAOFileImpl(context);
		// 資料來源為DB
		CouService couService = new CouService();

		Gson gson = new Gson();

		String action = rq.getParameter("imageSize");
		if (action.equals("small")) {
			Set<CouVO> couVOSet = couService.getCousByCoustate(3);
			List<CouVO> couVOList = new ArrayList<CouVO>(couVOSet);

			Integer memno = new Integer(rq.getParameter("memno"));

			MemService buySvc = new MemService();
			Set<BuyVO> buyVOSet = buySvc.getBuysByMemno(memno);
			List<BuyVO> buyVOList = new ArrayList<BuyVO>(buyVOSet);
			System.out.println(buyVOList);

			MemVO_Android memVO_Android = new MemVO_Android();
			memVO_Android.setCouVOList(couVOList);
			memVO_Android.setBuyVOList(buyVOList);

			// 將所有照片縮小
			// for (int i = 0; i < products.size(); i++) {
			// Product product = products.get(i);
			// product.setImage(ImageUtil.shrink(product.getImage(), 100));
			// }
			outStr = gson.toJson(memVO_Android);
		}
		if (action.equals("unitList")) {
			String couId = rq.getParameter("id");
			int couno = Integer.parseInt(couId);

			Set<UnitVO> setUnitVO = couService.getUnitsByCouno(couno);

			List<UnitVO> unitList = new ArrayList<UnitVO>(setUnitVO);
			outStr = gson.toJson(unitList);
		} else if (action.equals("large")) {
			AccService accSvc = new AccService();
			String couId = rq.getParameter("id");
			int couno = Integer.parseInt(couId);
			CouVO couVO = couService.getOneCou(couno);

			List<Object> wholeCourseWithoutAssList = couService
					.getWholeCouByCounoWithoutAss(couno);

			// CouVO couVObj = (CouVO) wholeCourseWithoutAssList.get(0);

			List<Set<ConVO>> conVOSetList = (List<Set<ConVO>>) wholeCourseWithoutAssList
					.get(2);

			List<ConVO> conVOList = new ArrayList<ConVO>(conVOSetList.get(0));// 1
			// ConVO conVO = conVOList.get(0);
			// System.out.println(conVO);

			AccVO accVO = accSvc.getOneAcc(couVO.getAccno());// 2
			Set<UnitVO> setUnitVO = couService.getUnitsByCouno(couno);

			List<UnitVO> unitVOList = new ArrayList<UnitVO>(setUnitVO);// 3
			//
			// List<Object> listCouAcc = new ArrayList<Object>();
			// listCouAcc.add(couVO);
			// listCouAcc.add(accVO);

			// Object[]oo=new Object[2];
			// oo[0]=couVO;
			//
			// CouVO couVO = (CouVO)listCouAcc.get(0);
			// AccVO accVO = (AccVO)list.get(1);
			// List<CouVO_Android> couAcclist = new ArrayList<CouVO_Android>();
			CouVO_Android androidCou = new CouVO_Android();
			androidCou.setCouVO(couVO);
			androidCou.setAccVO(accVO);
			androidCou.setUnitVOList(unitVOList);
			androidCou.setConVOList(conVOList);

			// couAcclist.add(androidCou);

			outStr = gson.toJson(androidCou);
			// outStr = gson.toJson(accVO);

		}
		rp.setContentType(contentType);
		PrintWriter out = rp.getWriter();
		out.println(outStr);
		out.close();
	}
}
