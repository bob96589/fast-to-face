package com.fro.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.top.model.TopVO;

public class ForJDBCDAO implements ForDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "user1";
	String passwd = "u111";

	// private static final String INSERT_STMT = //輸入資料
	// "INSERT INTO forum ( forno,fortype) VALUES (FOR_SEQ.NEXTVAL, ?)";
	 private static final String GET_ALL_STMT = //查詢該表格全部欄位
	 "SELECT  forno,fortype FROM forum order by forno";
	private static final String GET_ONE_STMT = //根據該欄位查詢資料
			"SELECT  * FROM forum where forno = ?";
	// private static final String DELETE = //根據該欄位刪除資料
	// "DELETE FROM forum where forno = ?";
	 private static final String UPDATE = //更新該表格所有欄位
	 "UPDATE forum set  fortype=? where forno = ?  ";

	private static final String getShowtop = 
	"select * from topic where forno=? and topstatus != 2 ";
 
	
	@Override
	
	public List<TopVO> getShowtop(Integer forno) {
		List<TopVO> list = new ArrayList<TopVO>();
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(getShowtop);
			pstmt.setInt(1, forno);

			rs = pstmt.executeQuery();

			while (rs.next()) {

				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus"));
				list.add(topVO); // Store the row in the list
			}
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		}

		finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;

	}

	@Override
	public List<ForVO> getAll() {

		List<ForVO> list = new ArrayList<ForVO>();
		ForVO forVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {

				forVO = new ForVO();
				forVO.setForno(rs.getInt("forno"));
				forVO.setFortype(rs.getString("fortype"));
				list.add(forVO); // Store the row in the list
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	
	@Override
	public ForVO findByPrimaryKey(Integer forno) {
		ForVO forVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, forno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// forVO 也稱為 Domain objects
				forVO = new ForVO();
				forVO.setForno(rs.getInt("forno"));
				forVO.setFortype(rs.getString("fortype"));
				 
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return forVO;		
		 
	}
	
	@Override
	public void update(ForVO forVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			//更新資料須按照SQL指令欄位順序排列 	
			pstmt.setString(1, forVO.getFortype());
			pstmt.setInt(2, forVO.getForno()); 
			 
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	

	//
	// @Override
	// public void insert(ForVO forVO) {
	// Connection con = null;
	// PreparedStatement pstmt = null;
	//
	// try {
	//
	// Class.forName(driver);
	// con = DriverManager.getConnection(url, userid, passwd);
	// pstmt = con.prepareStatement(INSERT_STMT);
	//
	// pstmt.setString(1, forVO.getFortype());
	// pstmt.executeUpdate();
	//
	// // Handle any driver errors
	// } catch (ClassNotFoundException e) {
	// throw new RuntimeException("Couldn't load database driver. "
	// + e.getMessage());
	// // Handle any SQL errors
	// } catch (SQLException se) {
	// throw new RuntimeException("A database error occured. "
	// + se.getMessage());
	// // Clean up JDBC resources
	// } finally {
	// if (pstmt != null) {
	// try {
	// pstmt.close();
	// } catch (SQLException se) {
	// se.printStackTrace(System.err);
	// }
	// }
	// if (con != null) {
	// try {
	// con.close();
	// } catch (Exception e) {
	// e.printStackTrace(System.err);
	// }
	// }
	// }
	//
	// }
	//
	  
	 
	
	// }
	// @Override
	// public void delete(Integer forno) {
	// Connection con = null;
	// PreparedStatement pstmt = null;
	//
	// try {
	//
	// Class.forName(driver);
	// con = DriverManager.getConnection(url, userid, passwd);
	// pstmt = con.prepareStatement(DELETE);
	//
	// pstmt.setInt(1, forno);
	//
	// pstmt.executeUpdate();
	//
	// // Handle any driver errors
	// } catch (ClassNotFoundException e) {
	// throw new RuntimeException("Couldn't load database driver. "
	// + e.getMessage());
	// // Handle any SQL errors
	// } catch (SQLException se) {
	// throw new RuntimeException("A database error occured. "
	// + se.getMessage());
	// // Clean up JDBC resources
	// } finally {
	// if (pstmt != null) {
	// try {
	// pstmt.close();
	// } catch (SQLException se) {
	// se.printStackTrace(System.err);
	// }
	// }
	// if (con != null) {
	// try {
	// con.close();
	// } catch (Exception e) {
	// e.printStackTrace(System.err);
	// }
	// }
	// }
	//
	// }
	// @Override
	// public ForVO getShowtop(Integer forno) {
	// ForVO forVO = null;
	// Connection con = null;
	// PreparedStatement pstmt = null;
	// ResultSet rs = null;
	//
	// try {
	//
	// Class.forName(driver);
	// con = DriverManager.getConnection(url, userid, passwd);
	// pstmt = con.prepareStatement(GET_ONE_STMT);
	//
	// pstmt.setInt(1, forno);
	//
	// rs = pstmt.executeQuery();
	//
	// while (rs.next()) {
	// // forVO 也稱為 Domain objects
	// forVO = new ForVO();
	// forVO.setForno(rs.getInt("forno"));
	// forVO.setFortype(rs.getString("fortype"));
	//
	// }
	//
	// // Handle any driver errors
	// } catch (ClassNotFoundException e) {
	// throw new RuntimeException("Couldn't load database driver. "
	// + e.getMessage());
	// // Handle any SQL errors
	// } catch (SQLException se) {
	// throw new RuntimeException("A database error occured. "
	// + se.getMessage());
	// // Clean up JDBC resources
	// } finally {
	// if (rs != null) {
	// try {
	// rs.close();
	// } catch (SQLException se) {
	// se.printStackTrace(System.err);
	// }
	// }
	// if (pstmt != null) {
	// try {
	// pstmt.close();
	// } catch (SQLException se) {
	// se.printStackTrace(System.err);
	// }
	// }
	// if (con != null) {
	// try {
	// con.close();
	// } catch (Exception e) {
	// e.printStackTrace(System.err);
	// }
	// }
	// }
	// return forVO;
	//
	// }
	// @Override
	// public List<ForVO> getAll() {
	//
	// List<ForVO> list = new ArrayList<ForVO>();
	// ForVO forVO = null;
	// Connection con = null;
	// PreparedStatement pstmt = null;
	// ResultSet rs = null;
	//
	// try {
	//
	// Class.forName(driver);
	// con = DriverManager.getConnection(url, userid, passwd);
	// pstmt = con.prepareStatement(GET_ALL_STMT);
	// rs = pstmt.executeQuery();
	//
	// while (rs.next()) {
	//
	// forVO = new ForVO();
	// forVO.setForno(rs.getInt("forno"));
	// forVO.setFortype(rs.getString("fortype"));
	// list.add(forVO); // Store the row in the list
	// }
	//
	// // Handle any driver errors
	// } catch (ClassNotFoundException e) {
	// throw new RuntimeException("Couldn't load database driver. "
	// + e.getMessage());
	// // Handle any SQL errors
	// } catch (SQLException se) {
	// throw new RuntimeException("A database error occured. "
	// + se.getMessage());
	// // Clean up JDBC resources
	// } finally {
	// if (rs != null) {
	// try {
	// rs.close();
	// } catch (SQLException se) {
	// se.printStackTrace(System.err);
	// }
	// }
	// if (pstmt != null) {
	// try {
	// pstmt.close();
	// } catch (SQLException se) {
	// se.printStackTrace(System.err);
	// }
	// }
	// if (con != null) {
	// try {
	// con.close();
	// } catch (Exception e) {
	// e.printStackTrace(System.err);
	// }
	// }
	// }
	// return list;
	// }
	public static void main(String[] args) {
		ForJDBCDAO dao = new ForJDBCDAO();
		// 新增
		// ForVO forVO1 = new ForVO();
		// forVO1.setForno(150001);
		// forVO1.setFortype("aaaa");
		// dao.insert(forVO1);
		//
		//
		// // 修改
		// ForVO forVO2 = new ForVO();
		// forVO2.setForno(150004);
		// forVO2.setFortype("bbbbb");
		// dao.update(forVO2);

		// // 刪除
		// dao.delete(150004);

		// // 查詢
		// ForVO forVO3 = dao.findByPrimaryKey(150001);
		// System.out.print(forVO3.getForno() + ",");
		// System.out.print(forVO3.getFortype() + ",");
		// System.out.println("---------------------");

		// 查詢
		List<ForVO> list = dao.getAll();
		for (ForVO forVO4 : list) {
			System.out.print(forVO4.getForno() + ",");
			System.out.print(forVO4.getFortype() + ",");
			System.out.println();
		}
		
//		
//		// 查詢
//		List<TopVO> list = dao.getShowtop(new Integer(150001));
//		for (TopVO topVO4 : list) {
//			System.out.print(topVO4.getTopno() + ",");
//			System.out.print(topVO4.getForno() + ",");
//			System.out.print(topVO4.getMemno() + ",");
//			System.out.print(topVO4.getTopname() + ",");
//			System.out.print(topVO4.getToptime() + ",");
//			System.out.println();
//		} 
	}

}
