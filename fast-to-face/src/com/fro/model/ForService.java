package com.fro.model;

import java.util.*;
import java.sql.*;

import com.top.model.TopVO;

public class ForService {
      private ForDAO_interface dao ;
      public ForService() {
  		dao = new ForDAO();
  	}
  

  	public List<ForVO> getAll() {
  		return dao.getAll();
  	}
  	public List<TopVO> getShowtop(Integer forno ) {   //顯示主題文章
  		return dao.getShowtop(forno);
  	}
  	public ForVO getOneFor(Integer forno) {
  		return dao.findByPrimaryKey(forno);
  	}
  	  
  	public ForVO update( Integer forno ,String fortype) {

  		ForVO forVO = new ForVO();
  		forVO.setForno(forno);	
  		forVO.setFortype(fortype);
  			
  		 
  		dao.update(forVO);
  		return forVO;
  	}

  	
	
}
