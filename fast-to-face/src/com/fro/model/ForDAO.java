package com.fro.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

 











import com.top.model.TopVO;
 
public class ForDAO implements ForDAO_interface{
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/TestDB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	//查詢XX主題下的所有主題文章
	private static final String getShowtop =    //顯示主題文章
			
	"select * from topic where forno=? and topstatus != 2 order by toptime desc";
//	private static final String GET_ALL_STMT = //查詢該表格全部欄位
//	"SELECT topno,forno,memno,topname, toptime FROM topic order by topno";
	private static final String GET_ALL_STMT = //查詢該表格全部欄位
	"SELECT  forno,fortype FROM forum order by forno";
	
	private static final String GET_ONE_STMT = //根據該欄位查詢資料
	"SELECT  * FROM forum where forno = ?";
	
	private static final String UPDATE =  //更新該表格所有欄位
	"UPDATE forum set  fortype=? where forno = ?";
 
	
	@Override
	public List<TopVO> getShowtop(Integer forno ){    //顯示該類別下所有主題文章
		List<TopVO> list = new ArrayList<TopVO>();
		TopVO topVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(getShowtop);
			pstmt.setInt(1, forno);
			
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				topVO = new TopVO();
				topVO.setTopno(rs.getInt("topno"));
				topVO.setForno(rs.getInt("forno"));
				topVO.setMemno(rs.getInt("memno"));
				topVO.setTopname(rs.getString("topname"));
				topVO.setToptime(rs.getTimestamp("toptime"));
				topVO.setTopstatus(rs.getInt("topstatus"));
				list.add(topVO); // Store the row in the list
			}
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		      return list;			 
		 
	}
	@Override
	public List<ForVO> getAll() {
		 
		List<ForVO> list = new ArrayList<ForVO>();
		ForVO forVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				forVO = new ForVO();
				forVO.setForno(rs.getInt("forno"));
				forVO.setFortype(rs.getString("fortype"));				 
				list.add(forVO); // Store the row in the list
			}
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;				 
	}
	
	@Override
	public ForVO findByPrimaryKey(Integer forno) {
		ForVO forVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);
			pstmt.setInt(1, forno);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				// forVO 也稱為 Domain objects
				forVO = new ForVO();
				forVO.setForno(rs.getInt("forno"));
				forVO.setFortype(rs.getString("fortype"));				 
			}
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return forVO;		
		 
	}
	
	@Override
	public void update(ForVO forVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			//更新資料須按照SQL指令欄位順序排列 	
		
			pstmt.setString(1, forVO.getFortype());
			pstmt.setInt(2, forVO.getForno());  
			 
			pstmt.executeUpdate();
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}	
	}
	
	 
	
	
}
