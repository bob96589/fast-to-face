package com.fro.model;

public class ForVO implements java.io.Serializable{
	private Integer   forno ;
	private String    fortype ;
	
	
	public Integer getForno() {
		return forno;
	}
	public void setForno(Integer forno) {
		this.forno = forno;
	}
	public String getFortype() {
		return fortype;
	}
	public void setFortype(String fortype) {
		this.fortype = fortype;
	}

	
	
	
}
