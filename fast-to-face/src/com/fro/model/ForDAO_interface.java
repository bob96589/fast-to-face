package com.fro.model;

import java.util.List;
import java.util.Set;

import com.top.model.TopVO;

public interface ForDAO_interface {
	 
	public List<ForVO> getAll();
	public ForVO findByPrimaryKey(Integer forno);
	public List<TopVO> getShowtop(Integer forno );
	public void update(ForVO forVO); 
	
}