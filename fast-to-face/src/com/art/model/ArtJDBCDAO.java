package com.art.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ArtJDBCDAO implements ArtDAO_interface{
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "user1";
	String passwd = "u111";
	
	private static final String INSERT_ART = //輸入資料
	"INSERT INTO Article (artno,memno,artasktime,artcon,topno,artstate) VALUES (ART_SEQ.NEXTVAL,?,sysdate,?,?,0)";
	private static final String INSERT_TOP = //輸入資料
	"INSERT INTO Article (artno,memno,artasktime,artcon,topno,artstate) VALUES (ART_SEQ.NEXTVAL,?,sysdate,?,?,0)";
	private static final String GET_ALL_STMT = //查詢該表格全部欄位
	"SELECT * FROM Article where artstate != 2 order by artno";
	private static final String GET_ONE_STMT = //根據該欄位查詢資料
	"SELECT * FROM Article where artno = ? and artstate != 2";
	private static final String GET_ONE_ART_BYREP = //檢舉表格-查詢文章內容
	"SELECT * FROM Article where artno = ? ";
//	private static final String DELETE = //根據該欄位刪除資料
//	"DELETE FROM Article where artno = ?";
	private static final String DELETE = //移除該篇文章
	"update article set artstate = 2 where artno = ?";
	private static final String REPDELETE = //檢舉該篇文章
	"update article set artstate = 1 where artno = ?";
	private static final String UPDATEREP = //根據該欄位刪除資料
	"update article set artstate = 0 where artno = ?";
	private static final String UPDATE =  //更新該表格所有欄位
	"UPDATE Article set memno=?, artasktime=?, artcon=?, topno=?, artstate=? where artno = ?";
	
	
	@Override
	public void insertart(ArtVO artVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_ART);			 
			pstmt.setInt(1, artVO.getMemno());						
		//	pstmt.setTimestamp(2, artVO.getArtasktime());
			pstmt.setString(2, artVO.getArtcon());
			pstmt.setInt(3, artVO.getTopno()); 
		//	pstmt.setInt(5, artVO.getArtstate());
			pstmt.executeUpdate();
			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}
	
	
	@Override
	public void inserttop(ArtVO artVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_TOP);			 
			pstmt.setInt(1, artVO.getMemno());						
		//  pstmt.setTimestamp(2, artVO.getArtasktime());
			pstmt.setString(2, artVO.getArtcon());
			pstmt.setInt(3, artVO.getTopno()); 
       //   pstmt.setInt(5, artVO.getArtstate());
			pstmt.executeUpdate();
			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}
	@Override
	public void update(ArtVO artVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);	
			//更新資料須按照SQL指令欄位順序排列 	
			pstmt.setInt(1, artVO.getMemno());						
			pstmt.setTimestamp(2, artVO.getArtasktime());
			pstmt.setString(3, artVO.getArtcon());
			pstmt.setInt(4, artVO.getTopno()); 
			pstmt.setInt(5, artVO.getArtstate());
			pstmt.setInt(6, artVO.getArtno());			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	@Override
	public void delete(Integer artno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, artno);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	
	
	@Override
	public void repdelete(Integer artno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(REPDELETE);

			pstmt.setInt(1, artno);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	@Override
	public void updaterep (Integer artno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATEREP);

			pstmt.setInt(1, artno);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	@Override
	public ArtVO findByPrimaryKey(Integer artno) {
		ArtVO artVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, artno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// artVO 也稱為 Domain objects
				artVO = new ArtVO();
				artVO.setArtno(rs.getInt("artno"));
				artVO.setMemno(rs.getInt("memno"));
				artVO.setArtasktime(rs.getTimestamp("artasktime"));
				artVO.setArtcon(rs.getString("artcon"));
				artVO.setTopno(rs.getInt("topno")); 
				artVO.setArtstate(rs.getInt("artstate")); 
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return artVO;		
		 
	}
	
	
	@Override
	public ArtVO getOneArtByRep(Integer artno) {
		ArtVO artVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_ART_BYREP);

			pstmt.setInt(1, artno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// artVO 也稱為 Domain objects
				artVO = new ArtVO();
				artVO.setArtno(rs.getInt("artno"));
				artVO.setMemno(rs.getInt("memno"));
				artVO.setArtasktime(rs.getTimestamp("artasktime"));
				artVO.setArtcon(rs.getString("artcon"));
				artVO.setTopno(rs.getInt("topno")); 
				artVO.setArtstate(rs.getInt("artstate")); 
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return artVO;		
		 
	}
	@Override
	public List<ArtVO> getAll() {
		List<ArtVO> list = new ArrayList<ArtVO>();
		ArtVO artVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				artVO = new ArtVO();
				artVO.setArtno(rs.getInt("artno"));
				artVO.setMemno(rs.getInt("memno"));
				artVO.setArtasktime(rs.getTimestamp("artasktime"));
				artVO.setArtcon(rs.getString("artcon"));
				artVO.setTopno(rs.getInt("topno")); 
				artVO.setArtstate(rs.getInt("artstate")); 
				list.add(artVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	
	
	public static void main(String[] args) {

		ArtJDBCDAO dao = new ArtJDBCDAO();

////		// 新增
//		ArtVO artVO1= new ArtVO();
//		artVO1.setMemno(90001);
//	//	artVO1.setArtasktime(java.sql.Timestamp.valueOf("2014-11-01 03:00:00"));		
//		artVO1.setArtcon("JAVA好難5");
//		artVO1.setTopno(160001);
//	//	artVO1.setArtstate(0); 
//		dao.insertart (artVO1);
		
//	    
//		// 修改
//		ArtVO artVO2 = new ArtVO();	
//		artVO2.setArtno(170001);
//		artVO2.setMemno(90001);
//		artVO2.setArtasktime(java.sql.Timestamp.valueOf("2014-11-01 12:00:00"));
//		artVO2.setArtcon("JAVA難AAAA");
//		artVO2.setTopno(160001);
//		artVO2.setArtstate(0);
//		dao.update(artVO2);
//		
//		// 刪除
		//dao.delete(170004);
		dao.repdelete(170004);

//		// 查詢
//		ArtVO artVO3 = dao.findByPrimaryKey(170002);
//		System.out.print(artVO3.getArtno() + ",");
//		System.out.print(artVO3.getMemno() + ",");
//		System.out.print(artVO3.getArtasktime() + ",");
//		System.out.print(artVO3.getArtcon() + ",");
//		System.out.print(artVO3.getTopno() + ",");
//		System.out.print(artVO3.getArtstate() + ",");
//		System.out.println("---------------------");

	 	// 查詢
//		List<ArtVO> list=dao.getAll();
//		for (ArtVO artVO4 : list) {
//			System.out.print(artVO4.getArtno() + ",");
//			System.out.print(artVO4.getMemno() + ",");
//			System.out.print(artVO4.getArtasktime() + ",");
//			System.out.print(artVO4.getArtcon() + ",");
//			System.out.print(artVO4.getTopno() + ",");
//			System.out.print(artVO4.getArtstate() + ",");
//			System.out.println();
//		}  
//		 
	   }
	
}
