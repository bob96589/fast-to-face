package com.art.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ArtDAO implements ArtDAO_interface{
	
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/TestDB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private static final String INSERT_ART = //輸入資料   //文章回文
	"INSERT INTO Article (artno,memno,artasktime,artcon,topno,artstate) VALUES (ART_SEQ.NEXTVAL,?,sysdate,?,?,0)";
	private static final String INSERT_TOP = //輸入資料
	"INSERT INTO Article (artno,memno,artasktime,artcon,topno,artstate) VALUES (ART_SEQ.NEXTVAL,?,sysdate,?,?,0)";
	private static final String GET_ALL_STMT = //查詢該表格全部欄位
	"SELECT * FROM Article where artstate != 2 order by artno";
	private static final String GET_ONE_ART = //根據編號搜尋該欄位資料
	"SELECT * FROM Article where artno = ? and artstate != 2";
	private static final String GET_ONE_ART_BYREP = //檢舉表格-查詢文章內容
	"SELECT * FROM Article where artno = ? ";
	private static final String DELETE = //移除該篇文章
	"update article set artstate = 2 where artno = ?";
	private static final String REPDELETE = //檢舉該篇文章
	"update article set artstate = 1 where artno = ?";
	private static final String UPDATEREP = //復原檢舉表單中資料
	"update article set artstate = 0 where artno = ?";
	private static final String UPDATE =  //更新該表格所有欄位
	"UPDATE Article set memno=?, artasktime=?, artcon=?, topno=?, artstate=? where artno = ?";
	
	@Override
	public void insertart(ArtVO artVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {		
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_ART);			 
			pstmt.setInt(1, artVO.getMemno());						
			//pstmt.setTimestamp(2, artVO.getArtasktime());
			pstmt.setString(2, artVO.getArtcon());
			pstmt.setInt(3, artVO.getTopno()); 
		//	pstmt.setInt(4, artVO.getArtstate());
			pstmt.executeUpdate();			 
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}
	
	
	@Override
	public void inserttop(ArtVO artVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {		
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_TOP);			 
			pstmt.setInt(1, artVO.getMemno());						
			//pstmt.setTimestamp(2, artVO.getArtasktime());
			pstmt.setString(2, artVO.getArtcon());
			pstmt.setInt(3, artVO.getTopno()); 
			//pstmt.setInt(4, artVO.getArtstate());
			pstmt.executeUpdate();			 
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}
	@Override
	public void update(ArtVO artVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {			
			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);	
			//更新資料須按照SQL指令欄位順序排列 	
			pstmt.setInt(1, artVO.getMemno());						
			//pstmt.setTimestamp(2, artVO.getArtasktime());
			pstmt.setString(2, artVO.getArtcon());
			pstmt.setInt(3, artVO.getTopno()); 
		//	pstmt.setInt(4, artVO.getArtstate());
			pstmt.setInt(4, artVO.getArtno());			
			pstmt.executeUpdate();			 
		}  
			// Handle any SQL errors
		  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	@Override
	public void delete(Integer artno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {	
			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);
			pstmt.setInt(1, artno);
			pstmt.executeUpdate();		 
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	
	
	@Override
	public void repdelete(Integer artno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {	
			con = ds.getConnection();
			pstmt = con.prepareStatement(REPDELETE);
			pstmt.setInt(1, artno);
			pstmt.executeUpdate();		 
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	
	@Override
	public void updaterep (Integer artno) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATEREP);

			pstmt.setInt(1, artno);

			pstmt.executeUpdate();
		}	 
			// Handle any SQL errors
		  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	@Override
	public ArtVO findByPrimaryKey(Integer artno) {
		ArtVO artVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {	
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_ART);

			pstmt.setInt(1, artno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// artVO 也稱為 Domain objects
				artVO = new ArtVO();
				artVO.setArtno(rs.getInt("artno"));
				artVO.setMemno(rs.getInt("memno"));
				artVO.setArtasktime(rs.getTimestamp("artasktime"));
				artVO.setArtcon(rs.getString("artcon"));
				artVO.setTopno(rs.getInt("topno")); 
				artVO.setArtstate(rs.getInt("artstate")); 
			}			
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return artVO;		
		 
	}
	
	
	@Override
	public ArtVO getOneArtByRep(Integer artno) {
		ArtVO artVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {	
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_ART_BYREP);

			pstmt.setInt(1, artno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// artVO 也稱為 Domain objects
				artVO = new ArtVO();
				artVO.setArtno(rs.getInt("artno"));
				artVO.setMemno(rs.getInt("memno"));
				artVO.setArtasktime(rs.getTimestamp("artasktime"));
				artVO.setArtcon(rs.getString("artcon"));
				artVO.setTopno(rs.getInt("topno")); 
				artVO.setArtstate(rs.getInt("artstate")); 
			 
			}			
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return artVO;		
		 
	}
	@Override
	public List<ArtVO> getAll() {
		List<ArtVO> list = new ArrayList<ArtVO>();
		ArtVO artVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				 
				artVO = new ArtVO();
				artVO.setArtno(rs.getInt("artno"));
				artVO.setMemno(rs.getInt("memno"));
				artVO.setArtasktime(rs.getTimestamp("artasktime"));
				artVO.setArtcon(rs.getString("artcon"));
				artVO.setTopno(rs.getInt("topno")); 
				artVO.setArtstate(rs.getInt("artstate")); 
				list.add(artVO); // Store the row in the list
			}			
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	
	
}
