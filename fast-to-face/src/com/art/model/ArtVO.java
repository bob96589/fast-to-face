package com.art.model;

import java.sql.Timestamp;

public class ArtVO implements java.io.Serializable{
	
	private Integer    artno ;
	private Integer    memno ;
	private Timestamp  artasktime ;
	private String     artcon ;
	private Integer    topno ;
	private Integer    artstate ;
	
	public Integer getArtno() {
		return artno;
	}
	public void setArtno(Integer artno) {
		this.artno = artno;
	}
	public Integer getMemno() {
		return memno;
	}
	public void setMemno(Integer memno) {
		this.memno = memno;
	}
	public Timestamp getArtasktime() {
		return artasktime;
	}
	public void setArtasktime(Timestamp artasktime) {
		this.artasktime = artasktime;
	}
	public String getArtcon() {
		return artcon;
	}
	public void setArtcon(String artcon) {
		this.artcon = artcon;
	}
	public Integer getTopno() {
		return topno;
	}
	public void setTopno(Integer topno) {
		this.topno = topno;
	}
	public Integer getArtstate() {
		return artstate;
	}
	public void setArtstate(Integer artstate) {
		this.artstate = artstate;
	}
	
	
}
