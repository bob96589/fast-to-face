package com.art.model;

import java.util.List;

public interface ArtDAO_interface {
//	public void insert(ArtVO artVO);
	public void update(ArtVO artVO);
	public void delete(Integer artno);
	public void repdelete(Integer artno);
	public ArtVO findByPrimaryKey(Integer artno);
	public ArtVO getOneArtByRep(Integer artno);
	public List<ArtVO> getAll();
	public void insertart(ArtVO artVO);
	public void inserttop(ArtVO artVO);
	public void updaterep(Integer artno);
}
