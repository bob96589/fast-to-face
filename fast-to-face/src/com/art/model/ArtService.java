package com.art.model;

import java.util.*;
import java.sql.*;

public class ArtService {
      private ArtDAO_interface dao ;
      public ArtService() {
  		dao = new ArtDAO();
  	}
      
      
  	public ArtVO insertart(Integer memno   , String artcon ,
  			Integer topno  ) {

  		ArtVO artVO = new ArtVO();
  		artVO.setMemno(memno);  		 
  		artVO.setArtcon(artcon);
  		artVO.setTopno(topno); 	    		 
  		dao.insertart(artVO);
  		return artVO;
  	} 

  	public ArtVO inserttop(Integer memno   , String artcon ,
  			Integer topno  ) {

  		ArtVO artVO = new ArtVO();
  		artVO.setMemno(memno);  		 
  		artVO.setArtcon(artcon);
  		artVO.setTopno(topno);  	  	 
  		dao.insertart(artVO);
  		return artVO;
  	}
  	
  

  	public ArtVO update(Integer artno,Integer memno , Timestamp artasktime , String artcon ,
  			Integer topno , Integer artstate) {

  		ArtVO artVO = new ArtVO();
  		artVO.setArtno(artno);
  		artVO.setMemno(memno);
  		artVO.setArtasktime(artasktime);
  		artVO.setArtcon(artcon);
  		artVO.setTopno(topno);
  		artVO.setArtstate(artstate);
  		dao.update(artVO);
  		return artVO;
  	}

  	public void delete(Integer artno) {
  		dao.delete(artno);
  	}
  	
  	public void repdelete(Integer artno) {
  		dao.repdelete(artno);
  	}


  	public ArtVO getOneArt(Integer artno) {
  		return dao.findByPrimaryKey(artno);
  	}

  	
  	public ArtVO getOneArtByRep(Integer artno) {
  		return dao.getOneArtByRep(artno);
  	}
  	
  	public List<ArtVO> getAll() {
  		return dao.getAll();
  	}
  	
  	public void updaterep(Integer artno) {
  		dao.updaterep(artno);
  	}
		
}
