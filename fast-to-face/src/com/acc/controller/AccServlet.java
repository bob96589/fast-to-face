package com.acc.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.acc.model.AccService;
import com.acc.model.AccVO;
import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.oreilly.servlet.MultipartRequest;

public class AccServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		String contentType = req.getContentType();
		String action = null;
		MultipartRequest multi = null;

		if (contentType != null
				&& contentType.startsWith("multipart/form-data")) {
			multi = new MultipartRequest(req, getServletContext().getRealPath(
					"/shared/temp"), 5 * 1024 * 1024, "UTF-8");
			action = multi.getParameter("action");
		} else {
			req.setCharacterEncoding("UTF-8");
			action = req.getParameter("action");
		}
		
		
		if ("login_check".equals(action)) {
			PrintWriter out = res.getWriter();
			String acc = req.getParameter("acc").trim();			
			String accpsw = req.getParameter("accpsw").trim();
			
			AccVO accVO = new AccVO();
			AccService AccSvc = new AccService();
			accVO = AccSvc.findByAccPsw(acc, accpsw);
			
			if (accVO == null) { 	
							
				out.write("false");
				return; 
			}
		}

		if ("getAll".equals(action)) {
			/*************************** 開始查詢資料 ****************************************/
			AccService accSvc = new AccService();
			List<AccVO> list = accSvc.getAll();
			/*************************** 查詢完成,準備轉交(Send the Success view) *************/
			HttpSession session = req.getSession();
			session.setAttribute("list", list); // 資料庫取出的list物件,存入session
			// Send the Success view
			String url = "/back/backaccount_management/backaccount_management.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交backaccount_management.jsp
			successView.forward(req, res);
			return;
		}

		if ("getOne_For_Update".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				// accno
				Integer accno = new Integer(req.getParameter("accno"));

				AccService accSvc = new AccService();
				AccVO accVO = accSvc.getOneAcc(accno);

				req.setAttribute("accVO", accVO);
				String url = "/back/backaccount_management/backaccount_update.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back/backaccount_management/backaccount_management.jsp");
				failureView.forward(req, res);
			}
		}

		if ("update".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				// accno
				Integer accno = new Integer(multi.getParameter("accno").trim());

				// accname
				String accname = multi.getParameter("accname").trim();
				if (accname == null || (accname.trim()).length() == 0) {
					errorMsgs.add("請輸入姓名");
				}

				// accpic
				byte[] buffer = null;
				File accpic = multi.getFile("accpic");
				if (accpic != null) {
					// add user's image
					InputStream in = new FileInputStream(accpic);
					buffer = new byte[in.available()];
					in.read(buffer);
					accpic.delete();
					in.close();
				} else {
					// read accpic from DB
					AccService accSvc = new AccService();
					AccVO accVO = accSvc.getOneAcc(accno);
					buffer = accVO.getAccpic();
				}

				// accintro
				String accintro = multi.getParameter("accintro");
				if (accintro == null || (accintro.trim()).length() == 0) {
					errorMsgs.add("請輸入簡介");
				}

				// accemail
				String accemail = multi.getParameter("accemail").trim();
				if (accemail == null || (accemail.trim()).length() == 0) {
					errorMsgs.add("請輸入Email");
				} else {
					if (!isValidEmail(accemail)) {
						errorMsgs.add("Email格式不正確");
					}else{
						AccService accSvc = new AccService();
						List<AccVO> accVOList = accSvc.getAll();
						for(AccVO accVO : accVOList){
							if(accemail.equals(accVO.getAccemail()) && !accno.equals(accVO.getAccno())){
								errorMsgs.add("信箱已使用");	
								break;
							}				
						}
					}
				}

				AccVO accVO = new AccVO();
				accVO.setAccno(accno);
				accVO.setAccname(accname);
				accVO.setAccpic(buffer);
				accVO.setAccintro(accintro);
				accVO.setAccemail(accemail);

				if (!errorMsgs.isEmpty()) {
					req.setAttribute("accVO", accVO);
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back/backaccount_management/backaccount_update.jsp");
					failureView.forward(req, res);
					return;
				}

				AccService accSvc = new AccService();
				accSvc.updateAcc(accno, accname, buffer, accintro, accemail);

				req.setAttribute("accVO", accVO);
				String url = "/back/backaccount_management/backaccount_management.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back/backaccount_management/backaccount_update.jsp");
				failureView.forward(req, res);
			}

		}

		

		if ("update_accpsw".equals(action) || "update_accpsw_first".equals(action)) {
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
//			try {

				Integer accno = new Integer(req.getParameter("accno").trim());
				String accpsw = req.getParameter("accpsw").trim();
				String accpsw2 = req.getParameter("accpsw2").trim();
				if ((accpsw == null || (accpsw.trim()).length() == 0)) {
					errorMsgs.add("請輸入更改密碼");
				}
				if ((accpsw2 == null || (accpsw2.trim()).length() == 0)) {
					errorMsgs.add("請輸入確認更改密碼");
				}
				if (errorMsgs.isEmpty()) {
					if(!accpsw.equals(accpsw2)){
						errorMsgs.add("更改密碼、確認更改密碼不同");
					}
				}
				
				if (!errorMsgs.isEmpty()) {
					String url = null;
					if("update_accpsw_first".equals(action)){
						url = "/back/backaccount_management/backaccount_updatePsw_first.jsp";
					}else{
						url = "/back/backaccount_management/backaccount_updatePsw.jsp";
					}
					
					RequestDispatcher failureView = req.getRequestDispatcher(url);
					failureView.forward(req, res);
					return;
				}

				AccService accSvc = new AccService();
				accSvc.updatePsw(accno, accpsw);

				String url = "/back/backaccount_management/backaccount_updatePsw_success.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
				successView.forward(req, res);
				
//			} catch (Exception e) {
//				errorMsgs.add(e.getMessage());
//				RequestDispatcher failureView = req.getRequestDispatcher("/back/backaccount_management/backaccount_updatePsw.jsp");
//				failureView.forward(req, res);
//				return;
//			}
		}

		// 帳號新增
		if ("insert".equals(action)) { // 來自addMem.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {

				/*********************** 1.接收請求參數 - 輸入格式的錯誤處理 *************************/

				// acc
				String acc = multi.getParameter("acc").trim();
				if (acc == null || (acc.trim()).length() == 0) {
					errorMsgs.add("請輸入帳號");
				} else {
					if (acc.getBytes().length != acc.length()) {
						errorMsgs.add("帳號請輸入英文或數字");
					} else {
						if (acc.length() < 3) {
							errorMsgs.add("帳號含\"FTF\"至少六碼");
						}
					}
				}

				// accpsw
				String accpsw = "111111";

				// accrole
				Integer accrole = new Integer(multi.getParameter("accrole")
						.trim());

				// accname
				String accname = multi.getParameter("accname").trim();
				if (accname == null || (accname.trim()).length() == 0) {
					errorMsgs.add("請輸入姓名");
				}

				// accpic
				byte[] buffer = null;
				File accpic = multi.getFile("accpic");
				if (accpic != null) {
					// add user's image
					InputStream in = new FileInputStream(accpic);
					buffer = new byte[in.available()];
					in.read(buffer);
					accpic.delete();
					in.close();
				} else {
					// add default image
					FileInputStream in = new FileInputStream(new File(
							getServletContext().getRealPath(
									"/shared/image/noPhoto.jpg")));
					buffer = new byte[in.available()];
					in.read(buffer);
					in.close();
				}

				// accintro
				String accintro = multi.getParameter("accintro");
				if (accintro == null || (accintro.trim()).length() == 0) {
					errorMsgs.add("請輸入簡介");
				}

				// accemail
				String accemail = multi.getParameter("accemail").trim();
				if (accemail == null || (accemail.trim()).length() == 0) {
					errorMsgs.add("請輸入Email");
				} else {
					if (!isValidEmail(accemail)) {
						errorMsgs.add("Email格式不正確");
					}else{
						AccService accSvc = new AccService();
						List<AccVO> accVOList = accSvc.getAll();
						for(AccVO accVO : accVOList){
							if(accemail.equals(accVO.getAccemail())){
								errorMsgs.add("信箱已使用");	
								break;
							}
						}
					}
				}

				AccVO accVO = new AccVO();
				accVO.setAcc(acc);
				accVO.setAccpsw(accpsw);
				accVO.setAccrole(accrole);
				accVO.setAccname(accname);
				accVO.setAccpic(buffer);
				accVO.setAccintro(accintro);
				accVO.setAccemail(accemail);

				if (!errorMsgs.isEmpty()) {
					req.setAttribute("accVO", accVO);
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back/backaccount_management/backaccount_insert.jsp");
					failureView.forward(req, res);
					return;
				}

				/*************************** 2.開始新增資料 ***************************************/

				AccService accSvc = new AccService();
				accVO = accSvc.addAcc("FTF" + acc, accpsw, accrole, accname,
						buffer, accintro, accemail);

				/*************************** 3.新增完成,準備轉交(Send the Success view) ***********/

				String url = "/back/backaccount_management/backaccount_management.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交backaccount_management.jsp
				successView.forward(req, res);

				/*************************** 其他可能的錯誤處理 **********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back/backaccount_management/backaccount_insert.jsp");
				failureView.forward(req, res);
			}
		}

		if ("getAccpic".equals(action)) {

			/*********************** 1.接收請求參數 - 輸入格式的錯誤處理 *************************/
			Integer accno = new Integer(req.getParameter("accno"));
			/*************************** 開始查詢資料 ****************************************/
			AccService accSvc = new AccService();
			AccVO accVO = accSvc.getOneAcc(accno);

			byte[] accpic = accVO.getAccpic();

			res.setContentType("image/png");
			ServletOutputStream out = res.getOutputStream();
			out.write(accpic);
			out.flush();

		}

		if ("login_back".equals(action)) {
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			/*********************** 1.接收請求參數 - 輸入格式的錯誤處理 *****************************/
			
			String acc = req.getParameter("acc").trim();
			if (acc == null || (acc.trim()).length() == 0) {
				errorMsgs.add("請輸入帳號");
			}
			String accpsw = req.getParameter("accpsw").trim();
			if (accpsw == null || (accpsw.trim()).length() == 0) {
				errorMsgs.add("請輸入密碼");
			}
			
			AccVO accVO = new AccVO();
			accVO.setAcc(acc);
			
			if (!errorMsgs.isEmpty()) {
				req.setAttribute("accVO", accVO);
				RequestDispatcher failureView = req.getRequestDispatcher("/back/backlogin/backlogin.jsp");
				failureView.forward(req, res);
				return; 
			}
			
			/*************************** 開始查詢資料 *****************************************/
			
			AccService AccSvc = new AccService();
			accVO = AccSvc.findByAccPsw(acc, accpsw);
			
			/*************************** 3.查詢完成,準備轉交(Send the Success view) ************/
			if (accVO == null) { 
				errorMsgs.add("登入失敗，帳號密碼有誤");
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req.getRequestDispatcher("/back/backlogin/backlogin.jsp");
					failureView.forward(req, res);
					return; 
				}
			} else { 
				HttpSession session = req.getSession();
				session.setAttribute("accVO", accVO);
				
				String url = null;
				if("111111".equals(accVO.getAccpsw())){
					url = "/back/backaccount_management/backaccount_updatePsw_first.jsp";
				}else{
					url = "/back/backlogin/backlogin_success.jsp";
				}
				
				RequestDispatcher successView = req.getRequestDispatcher(url); 
				successView.forward(req, res);
			}
		}
		
		if("logout_back".equals(action)){
			HttpSession session = req.getSession();
			session.removeAttribute("accVO");
			
			String url = "/back/backlogin/backlogout_success.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url); 
			successView.forward(req, res);
		}
		
	}

	public static boolean isValidEmail(String email) {
		if (email == null) {
			return false;
		}
		String emailPattern = "^([\\w]+)(([-\\.][\\w]+)?)*@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		return email.matches(emailPattern);
	}

}
