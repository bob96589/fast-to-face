package com.acc.model;

import java.util.*;

import com.cou.model.CouVO;

public interface AccDAO_interface {
	public void insert(AccVO accVO);
	public void update(AccVO accVO);
	public void delete(Integer accno);
	public AccVO findByPrimaryKey(Integer accno);
	public List<AccVO> getAll();
	
	//�d���[��
	public Set<CouVO> getCousByAccno(Integer accno);
	//jie
	public void updatePsw(Integer accno, String accpsw);
	public AccVO findByAccPsw(String account,String password);
}
