package com.acc.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.cou.model.CouVO;

public class AccDAO implements AccDAO_interface {

	// 一個應用程式中,針對一個資料庫 ,共用一個DataSource即可
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/TestDB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
			"INSERT INTO account (accno, acc, accpsw, accrole, accname, accpic, accintro, accemail) VALUES (Acc_SEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?)";
	private static final String GET_ALL_STMT = 
			"SELECT * FROM account order by accno";
	private static final String GET_ONE_STMT = 
			"SELECT * FROM account where accno = ?";
	private static final String DELETE = 
			"DELETE FROM account where accno = ?";
	private static final String UPDATE = 
			"UPDATE account set accname=?, accpic=?, accintro=?, accemail=? where accno = ?";
	
	
	//康縣加的
	private static final String GET_COUS_BY_ACCNO = 
			"select * from course where accno = ? and coustate != -1";
	//jie
	private static final String UPDATE_ACCPSW = 
			"UPDATE account set accpsw = ? where accno = ?";
	private static final String GET_account_password_STMT = 
			"SELECT * FROM account where acc = ? AND accpsw = ?";
	
	@Override
	public void insert(AccVO accVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);  //新增一筆資料

			pstmt.setString(1, accVO.getAcc());
			pstmt.setString(2, accVO.getAccpsw());
			pstmt.setInt(3, accVO.getAccrole());
			pstmt.setString(4, accVO.getAccname());
			pstmt.setBytes(5, accVO.getAccpic());
			pstmt.setString(6, accVO.getAccintro());
			pstmt.setString(7, accVO.getAccemail());
			
			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(AccVO accVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE); 

			pstmt.setString(1, accVO.getAccname());
			pstmt.setBytes(2, accVO.getAccpic());
			pstmt.setString(3, accVO.getAccintro());
			pstmt.setString(4, accVO.getAccemail());
			pstmt.setInt(5, accVO.getAccno());
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer accno) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE); 
			pstmt.setInt(1, accno); 
			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public AccVO findByPrimaryKey(Integer accno) {

		AccVO accVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, accno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				accVO = new AccVO();
				accVO.setAccno(rs.getInt("accno"));
				accVO.setAcc(rs.getString("acc"));
				accVO.setAccpsw(rs.getString("accpsw"));
				accVO.setAccrole(rs.getInt("accrole"));
				accVO.setAccname(rs.getString("accname"));
				accVO.setAccpic(rs.getBytes("accpic"));
				accVO.setAccintro(rs.getString("accintro"));
				accVO.setAccemail(rs.getString("accemail"));
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return accVO;
	}

	@Override
	public List<AccVO> getAll() {
		List<AccVO> list = new ArrayList<AccVO>();
		AccVO accVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);  
			rs = pstmt.executeQuery();

			while (rs.next()) {
				accVO = new AccVO();
				accVO.setAccno(rs.getInt("accno"));
				accVO.setAcc(rs.getString("acc"));
				accVO.setAccpsw(rs.getString("accpsw"));
				accVO.setAccrole(rs.getInt("accrole"));
				accVO.setAccname(rs.getString("accname"));
				accVO.setAccpic(rs.getBytes("accpic"));
				accVO.setAccintro(rs.getString("accintro"));
				accVO.setAccemail(rs.getString("accemail"));
				list.add(accVO); 
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	
	//康縣加的
	@Override
	public Set<CouVO> getCousByAccno(Integer accno) {
		Set<CouVO> set = new LinkedHashSet<CouVO>();
		CouVO couVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_COUS_BY_ACCNO);
			pstmt.setInt(1, accno);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				couVO = new CouVO();
				couVO.setCouno(rs.getInt("couno"));
				couVO.setCouname(rs.getString("couname"));
				couVO.setCouintro(rs.getString("couintro"));
				couVO.setCoupic(rs.getBytes("coupic"));
				couVO.setCoubadge(rs.getBytes("coubadge"));
				couVO.setCouprice(rs.getInt("couprice"));
				couVO.setCoudiscount(rs.getInt("coudiscount"));
				couVO.setCouishot(rs.getInt("couishot"));
				couVO.setAccno(rs.getInt("accno"));
				couVO.setCoustate(rs.getInt("coustate"));
				couVO.setProno(rs.getInt("prono"));
				couVO.setCouupdate(rs.getTimestamp("couupdate"));
				set.add(couVO);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}
	
	
	
	@Override
	public void updatePsw(Integer accno, String accpsw) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_ACCPSW);

			pstmt.setString(1, accpsw);
			pstmt.setInt(2, accno);
			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	
	
	@Override
	public AccVO findByAccPsw(String acc, String accpsw) {

		AccVO accVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_account_password_STMT);

			pstmt.setString(1, acc);
			pstmt.setString(2, accpsw);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				accVO = new AccVO();
				accVO.setAccno(rs.getInt("accno"));
				accVO.setAcc(rs.getString("acc"));
				accVO.setAccpsw(rs.getString("accpsw"));
				accVO.setAccrole(rs.getInt("accrole"));
				accVO.setAccname(rs.getString("accname"));
				accVO.setAccpic(rs.getBytes("accpic"));
				accVO.setAccintro(rs.getString("accintro"));
				accVO.setAccemail(rs.getString("accemail"));
			}
			

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return accVO;
	}
}
