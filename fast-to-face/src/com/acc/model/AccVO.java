package com.acc.model;

public class AccVO {
	private Integer accno;
	private String acc;
	private String accpsw;
	private String accname;
	private String accintro;
	private Integer accrole;
	private byte[] accpic;
	private String accemail;
	
	public Integer getAccno() {
		return accno;
	}
	public void setAccno(Integer accno) {
		this.accno = accno;
	}
	public String getAcc() {
		return acc;
	}
	public void setAcc(String acc) {
		this.acc = acc;
	}
	public String getAccpsw() {
		return accpsw;
	}
	public void setAccpsw(String accpsw) {
		this.accpsw = accpsw;
	}
	public String getAccname() {
		return accname;
	}
	public void setAccname(String accname) {
		this.accname = accname;
	}
	public String getAccintro() {
		return accintro;
	}
	public void setAccintro(String accintro) {
		this.accintro = accintro;
	}
	public Integer getAccrole() {
		return accrole;
	}
	public void setAccrole(Integer accrole) {
		this.accrole = accrole;
	}
	public byte[] getAccpic() {
		return accpic;
	}
	public void setAccpic(byte[] accpic) {
		this.accpic = accpic;
	}
	public String getAccemail() {
		return accemail;
	}
	public void setAccemail(String accemail) {
		this.accemail = accemail;
	}
	
	
	
	
	
	
}
