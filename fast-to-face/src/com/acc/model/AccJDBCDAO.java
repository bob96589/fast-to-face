package com.acc.model;

import java.util.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;

import com.cou.model.CouVO;

public class AccJDBCDAO implements AccDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "user1";
	String passwd = "u111";

	private static final String INSERT_STMT = 
			"INSERT INTO account (accno, acc, accpsw, accrole, accname, accpic, accintro, accemail) VALUES (Acc_SEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?)";
	private static final String GET_ALL_STMT = 
			"SELECT * FROM account order by accno";
	private static final String GET_ONE_STMT = 
			"SELECT * FROM account where accno = ?";
	private static final String DELETE = 
			"DELETE FROM account where accno = ?";
	private static final String UPDATE = 
			"UPDATE account set accname=?, accpic=?, accintro=?, accemail=? where accno = ?";
	
	
	//康縣加的
	private static final String GET_COUS_BY_ACCNO = 
			"select * from course where accno = ? and coustate != -1";
	//jie
	private static final String UPDATE_ACCPSW = 
			"UPDATE account set accpsw = ? where accno = ?";
	private static final String GET_account_password_STMT = 
			"SELECT * FROM account where acc = ? AND accpsw = ?";
	
	@Override
	public void insert(AccVO accVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);  //新增一筆資料

			pstmt.setString(1, accVO.getAcc());
			pstmt.setString(2, accVO.getAccpsw());
			pstmt.setInt(3, accVO.getAccrole());
			pstmt.setString(4, accVO.getAccname());
			pstmt.setBytes(5, accVO.getAccpic());
			pstmt.setString(6, accVO.getAccintro());
			pstmt.setString(7, accVO.getAccemail());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(AccVO accVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE); 

			pstmt.setString(1, accVO.getAccname());
			pstmt.setBytes(2, accVO.getAccpic());
			pstmt.setString(3, accVO.getAccintro());
			pstmt.setString(4, accVO.getAccemail());
			pstmt.setInt(5, accVO.getAccno());
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer accno) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);   //刪除資料

			pstmt.setInt(1, accno);     //以帳號編號為依據

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public AccVO findByPrimaryKey(Integer accno) {

		AccVO accVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);   //查詢取得單筆資料

			pstmt.setInt(1, accno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				accVO = new AccVO();
				accVO.setAccno(rs.getInt("accno"));
				accVO.setAcc(rs.getString("acc"));
				accVO.setAccpsw(rs.getString("accpsw"));
				accVO.setAccrole(rs.getInt("accrole"));
				accVO.setAccname(rs.getString("accname"));
				accVO.setAccpic(rs.getBytes("accpic"));
				accVO.setAccintro(rs.getString("accintro"));
				accVO.setAccemail(rs.getString("accemail"));
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return accVO;
	}

	@Override
	public List<AccVO> getAll() {
		List<AccVO> list = new ArrayList<AccVO>();
		AccVO accVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);  
			rs = pstmt.executeQuery();

			while (rs.next()) {
				accVO = new AccVO();
				accVO.setAccno(rs.getInt("accno"));
				accVO.setAcc(rs.getString("acc"));
				accVO.setAccpsw(rs.getString("accpsw"));
				accVO.setAccrole(rs.getInt("accrole"));
				accVO.setAccname(rs.getString("accname"));
				accVO.setAccpic(rs.getBytes("accpic"));
				accVO.setAccintro(rs.getString("accintro"));
				accVO.setAccemail(rs.getString("accemail"));
				list.add(accVO); 
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	
	//康縣加的
	@Override
	public Set<CouVO> getCousByAccno(Integer accno) {
		Set<CouVO> set = new LinkedHashSet<CouVO>();
		CouVO couVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_COUS_BY_ACCNO);
			pstmt.setInt(1, accno);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				couVO = new CouVO();
				couVO.setCouno(rs.getInt("couno"));
				couVO.setCouname(rs.getString("couname"));
				couVO.setCouintro(rs.getString("couintro"));
				couVO.setCoupic(rs.getBytes("coupic"));
				couVO.setCoubadge(rs.getBytes("coubadge"));
				couVO.setCouprice(rs.getInt("couprice"));
				couVO.setCoudiscount(rs.getInt("coudiscount"));
				couVO.setCouishot(rs.getInt("couishot"));
				couVO.setAccno(rs.getInt("accno"));
				couVO.setCoustate(rs.getInt("coustate"));
				couVO.setProno(rs.getInt("prono"));
				couVO.setCouupdate(rs.getTimestamp("couupdate"));
				set.add(couVO);
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}
	
	
	@Override
	public void updatePsw(Integer accno, String accpsw) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE_ACCPSW);

			pstmt.setString(1, accpsw);
			pstmt.setInt(2, accno);
			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	
	
	@Override
	public AccVO findByAccPsw(String acc, String accpsw) {

		AccVO accVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_account_password_STMT);

			pstmt.setString(1, acc);
			pstmt.setString(2, accpsw);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				accVO = new AccVO();
				accVO.setAccno(rs.getInt("accno"));
				accVO.setAcc(rs.getString("acc"));
				accVO.setAccpsw(rs.getString("accpsw"));
				accVO.setAccrole(rs.getInt("accrole"));
				accVO.setAccname(rs.getString("accname"));
				accVO.setAccpic(rs.getBytes("accpic"));
				accVO.setAccintro(rs.getString("accintro"));
				accVO.setAccemail(rs.getString("accemail"));
			}

		}catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return accVO;
	}

	public static void main(String[] args) throws IOException{

		AccJDBCDAO dao = new AccJDBCDAO();

		FileInputStream in = new FileInputStream(new File("C:/pic/111.jpg"));
		byte[] buffer = new byte[in.available()];  // 設置buffer大小
		in.read(buffer);     //將資料讀(倒)入buffer
//		
//		//新增
//		AccVO accVO1 = new AccVO();
//		accVO1.setAcc("TestAcc");
//		accVO1.setAccpsw("78787878");
//		accVO1.setAccrole(1);
//		accVO1.setAccname("吳小志");
//		accVO1.setAccpic(buffer);
//		accVO1.setAccintro("Test_G4_Acc");
//		accVO1.setAccemail("wewew@cc");
//		dao.insert(accVO1);
//
		// 修改
//		AccVO accVO2 = new AccVO();
//		accVO2.setAccno(100006);
//		accVO2.setAccname("小城");
//		accVO2.setAccpic(buffer);
//		accVO2.setAccintro("test_update2");
//		dao.update(accVO2);
//
		// 刪除
//		dao.delete(110005);
//
		// 查詢
//		AccVO accVO3 = dao.findByPrimaryKey(100002);
//		System.out.println(accVO3.getAccno() + ",");
//		System.out.println(accVO3.getAcc() + ",");
//		System.out.println(accVO3.getAccpsw() + ",");
//		System.out.println(accVO3.getAccrole() + ",");
//		System.out.println(accVO3.getAccname() + ",");
//		System.out.println(accVO3.getAccpic()+"\n");
//		System.out.println(accVO3.getAccintro() + ",");
//		System.out.println(accVO3.getAccemail() + ",");
//		System.out.println("---------------------");
//
		// 查詢
//		List<AccVO> list = dao.getAll();
//		for (AccVO aAcc : list) {
//			System.out.print(aAcc.getAccno() + ",");
//			System.out.print(aAcc.getAcc() + ",");
//			System.out.print(aAcc.getAccpsw() + ",");
//			System.out.print(aAcc.getAccrole() + ",");
//			System.out.print(aAcc.getAccname() + ",");
//			System.out.print(aAcc.getAccpic() + ",");	
//			System.out.print(aAcc.getAccintro() + ",");
//			System.out.print(aAcc.getAccemail() + ",");
//			System.out.println();
//		}
//		
//		
//		
//		//康縣加的
//		Set<CouVO> CouSet = dao.getCousByAccno(100002);
//		for (CouVO p : CouSet) {
//			System.out.println(p.getCouno());
//			System.out.println(p.getCouname());
//			System.out.println(p.getCouintro());
//			System.out.println(p.getCouprice());
//			System.out.println(p.getCoudiscount());
//			System.out.println(p.getCouishot());
//			System.out.println(p.getAccno());
//			System.out.println(p.getCoustate());
//			System.out.println(p.getProno());
//			System.out.println(p.getCouupdate());
//			System.out.println("---------------------");
//		}
		
		
		
		

//		dao.updatePsw(100004, "121213");
		
//		AccVO accVO = dao.findByAccPsw("peter3", "121212");
//		System.out.print(accVO.getAccno() + ",");
//		System.out.print(accVO.getAcc() + ",");
//		System.out.print(accVO.getAccpsw() + ",");
//		System.out.print(accVO.getAccrole() + ",");
//		System.out.print(accVO.getAccname() + ",");
//		System.out.println(accVO.getAccpic()+"\n");
//		System.out.print(accVO.getAccintro() + ",");
//		System.out.print(accVO.getAccemail() + ",");
//		System.out.println("---------------------");	
		
		
	
	
	
	
	
	
	
	}

	
}
