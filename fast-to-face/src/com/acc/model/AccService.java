package com.acc.model;

import java.util.List;
import java.util.Set;

import com.cou.model.CouVO;

public class AccService {

	private AccDAO_interface dao;

	public AccService() {
		dao = new AccDAO();
	}

	public AccVO addAcc(String acc, String accpsw, Integer accrole, String accname, byte[] accpic, String accintro, String accemail) {

		AccVO accVO = new AccVO();

		accVO.setAcc(acc);
		accVO.setAccpsw(accpsw);
		accVO.setAccrole(accrole);
		accVO.setAccname(accname);
		accVO.setAccpic(accpic);
		accVO.setAccintro(accintro);
		accVO.setAccemail(accemail);
		dao.insert(accVO);

		return accVO;
	}

	public AccVO updateAcc(Integer accno, String accname, byte[] accpic, String accintro, String accemail) {

		AccVO accVO = new AccVO();

		accVO.setAccno(accno);
		accVO.setAccname(accname);
		accVO.setAccpic(accpic);
		accVO.setAccintro(accintro);
		accVO.setAccemail(accemail);
		dao.update(accVO);

		return accVO;
	}

	public void deleteAcc(Integer accno) {
		dao.delete(accno);
	}

	public AccVO getOneAcc(Integer accno) {
		return dao.findByPrimaryKey(accno);
	}

	public List<AccVO> getAll() {
		return dao.getAll();
	}
	
	//�d���[��
	public Set<CouVO> getCousByAccno(Integer accno){
		return dao.getCousByAccno(accno);		
	}
	//jie
	public void updatePsw(Integer accno, String accpsw){
		dao.updatePsw(accno, accpsw);
	}
	public AccVO findByAccPsw(String acc, String accpsw){
		return dao.findByAccPsw(acc, accpsw);
	}
	
}
