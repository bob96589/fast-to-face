package com.unit.model;

import java.util.List;
import java.util.Set;

import com.content.model.ConVO;

public interface UnitDAO_interface {
	public void insert(UnitVO unitVO);
    public void update(UnitVO unitVO);
    public void delete(Integer unitno);
    public UnitVO findByPrimaryKey(Integer unitno);
    public List<UnitVO> getAll();
    
    public Set<ConVO> getConsByUnitno(Integer unitno);
    public Set<ConVO> getConsByUnitnoWithoutConvideo(Integer unitno);
    public Integer getMaxConorderByUnitno(Integer unitno);
    public Integer getConnoByUnitnoConorder(Integer unitno, Integer conorder);
    public void updateUnitorder(Integer unitno, Integer unitorder);
    
  

}
