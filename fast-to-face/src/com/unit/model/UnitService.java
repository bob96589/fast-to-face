package com.unit.model;

import java.util.List;
import java.util.Set;

import com.content.model.ConVO;

public class UnitService {
	private UnitDAO_interface dao;

	public UnitService() {
		dao = new UnitDAO();
	}

	public UnitVO addUnit(Integer couno, String unitname, Integer unitorder) {

		UnitVO unitVO = new UnitVO();

		unitVO.setCouno(couno);
		unitVO.setUnitname(unitname);
		unitVO.setUnitorder(unitorder);
		dao.insert(unitVO);

		return unitVO;
	}

	public UnitVO updateUnit(Integer unitno, String unitname) {

		UnitVO unitVO = new UnitVO();

		unitVO.setUnitno(unitno);
		unitVO.setUnitname(unitname);
		dao.update(unitVO);

		return unitVO;
	}

	public void deleteUnit(Integer unitno) {
		dao.delete(unitno);
	}

	public UnitVO getOneUnit(Integer unitno) {
		return dao.findByPrimaryKey(unitno);
	}

	public List<UnitVO> getAll() {
		return dao.getAll();
	}

	public Set<ConVO> getConsByUnitno(Integer unitno){
		return dao.getConsByUnitno(unitno);		
	}
	
	public Set<ConVO> getConsByUnitnoWithoutConvideo(Integer unitno){
		return dao.getConsByUnitnoWithoutConvideo(unitno);
	}
	
	public Integer getMaxConorderByUnitno(Integer unitno){
		return dao.getMaxConorderByUnitno(unitno);		
	}	
	
	public Integer getConnoByUnitnoConorder(Integer unitno, Integer conorder){
		return dao.getConnoByUnitnoConorder(unitno, conorder);
	}
	
	public void updateUnitorder(Integer unitno, Integer unitorder){
		dao.updateUnitorder(unitno, unitorder);
	}
	
	
}
