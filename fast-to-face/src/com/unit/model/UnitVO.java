package com.unit.model;

public class UnitVO  implements java.io.Serializable {
	private Integer unitno;
	private Integer couno;
	private String unitname;
	private Integer unitorder;
	
	public Integer getUnitno() {
		return unitno;
	}
	public void setUnitno(Integer unitno) {
		this.unitno = unitno;
	}
	public Integer getCouno() {
		return couno;
	}
	public void setCouno(Integer couno) {
		this.couno = couno;
	}
	public String getUnitname() {
		return unitname;
	}
	public void setUnitname(String unitname) {
		this.unitname = unitname;
	}
	public Integer getUnitorder() {
		return unitorder;
	}
	public void setUnitorder(Integer unitorder) {
		this.unitorder = unitorder;
	}
	
	
	
}
