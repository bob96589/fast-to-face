package com.unit.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.content.model.ConVO;

public class UnitJDBCDAO implements UnitDAO_interface {
	
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "user1";
	String passwd = "u111";

	private static final String INSERT_STMT = 
			"INSERT INTO unit (unitno,couno,unitname,unitorder) VALUES (unit_seq.nextval, ?, ?, ?)";
	private static final String GET_ALL_STMT = 
			"SELECT * FROM unit order where unitorder != -1 by unitno";
	private static final String GET_ONE_STMT = 
			"SELECT * FROM unit where unitno = ?";
	private static final String DELETE = 
			"UPDATE unit set unitorder = -1 where unitno = ?";
	private static final String UPDATE = 
			"UPDATE unit set unitname=? where unitno = ?";
	
	private static final String GET_CONS_BY_UNITNO = 
			"select * from content where unitno = ? and conorder != -1 order by conorder";
	private static final String GET_CONS_BY_UNITNO_WITHOUT_CONVIDEO = 
			"select conno, conname, contype, unitno, conorder from content where unitno = ? and conorder != -1 order by conorder";
	private static final String GET_MAX_CONORDER_BY_UNITNO = 
			"select max(conorder) from content where unitno = ?";
	private static final String GET_CON_BY_UNITNO_CONORDER = 
			"select conno from content where unitno = ? and conorder = ?";
	private static final String UPDATE_UNITORDER = 
			"UPDATE unit set unitorder=? where unitno = ?";
	
	
	
	@Override
	public void insert(UnitVO unitVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setInt(1, unitVO.getCouno());
			pstmt.setString(2, unitVO.getUnitname());
			pstmt.setInt(3, unitVO.getUnitorder());

			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(UnitVO unitVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setString(1, unitVO.getUnitname());
			pstmt.setInt(2, unitVO.getUnitno());

			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer unitno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, unitno);

			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public UnitVO findByPrimaryKey(Integer unitno) {
		UnitVO unitVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, unitno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				unitVO = new UnitVO();
				unitVO.setUnitno(rs.getInt("unitno"));
				unitVO.setCouno(rs.getInt("couno"));
				unitVO.setUnitname(rs.getString("unitname"));
				unitVO.setUnitorder(rs.getInt("unitorder"));
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return unitVO;
	}

	@Override
	public List<UnitVO> getAll() {
		List<UnitVO> list = new ArrayList<UnitVO>();
		UnitVO unitVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				unitVO = new UnitVO();
				unitVO.setUnitno(rs.getInt("unitno"));
				unitVO.setCouno(rs.getInt("couno"));
				unitVO.setUnitname(rs.getString("unitname"));
				unitVO.setUnitorder(rs.getInt("unitorder"));
				list.add(unitVO); // Store the row in the list
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	
	@Override
	public Set<ConVO> getConsByUnitno(Integer unitno) {
		Set<ConVO> set = new LinkedHashSet<ConVO>();
		ConVO conVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_CONS_BY_UNITNO);
			pstmt.setInt(1, unitno);		
			
			rs = pstmt.executeQuery();			

			while (rs.next()) {				
				conVO = new ConVO();
				conVO.setConno(rs.getInt("conno"));
				conVO.setConname(rs.getString("conname"));
				conVO.setContype(rs.getString("contype"));
				conVO.setUnitno(rs.getInt("unitno"));
				conVO.setConvideo(rs.getString("convideo"));
				conVO.setConorder(rs.getInt("conorder"));				
				set.add(conVO);
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}
	
	@Override
	public Set<ConVO> getConsByUnitnoWithoutConvideo(Integer unitno) {
		Set<ConVO> set = new LinkedHashSet<ConVO>();
		ConVO conVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_CONS_BY_UNITNO_WITHOUT_CONVIDEO);
			pstmt.setInt(1, unitno);		
				
			
			rs = pstmt.executeQuery();			

			while (rs.next()) {				
				conVO = new ConVO();
				conVO.setConno(rs.getInt("conno"));
				conVO.setConname(rs.getString("conname"));
				conVO.setContype(rs.getString("contype"));
				conVO.setUnitno(rs.getInt("unitno"));
				conVO.setConorder(rs.getInt("conorder"));				
				set.add(conVO);
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}
	
	
	@Override
	public Integer getMaxConorderByUnitno(Integer unitno) {
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer conorder = null;

		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_MAX_CONORDER_BY_UNITNO);
			pstmt.setInt(1, unitno);		
				
			
			rs = pstmt.executeQuery();			

			rs.next();
			conorder = rs.getInt(1);
			

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return conorder;
	}
	
	
	
	@Override
	public Integer getConnoByUnitnoConorder(Integer unitno, Integer conorder) {
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Integer conno = null;

		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_CON_BY_UNITNO_CONORDER);
			pstmt.setInt(1, unitno);		
			pstmt.setInt(2, conorder);			
			
			rs = pstmt.executeQuery();			

			rs.next();
			conno = rs.getInt(1);
			

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return conno;
	}
	
	
	@Override
	public void updateUnitorder(Integer unitno, Integer unitorder) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE_UNITORDER);

			pstmt.setInt(1, unitorder);
			pstmt.setInt(2, unitno);

			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	
	

	
	

	public static void main(String[] args) {

		UnitJDBCDAO dao = new UnitJDBCDAO();

		
//		UnitVO vo1 = new UnitVO();
//
//		vo1.setCouno(10003);
//		vo1.setUnitname("�}�C");
//		vo1.setUnitorder(1);
//		dao.insert(vo1);
//
//	
//		UnitVO vo2 = new UnitVO();
//
//		vo2.setUnitno(30001);
//		vo2.setUnitname("�}�C1");
//		vo2.setUnitorder(2);
//		dao.update(vo2);
//
//	
//		dao.delete(30012);
//
//		
//		UnitVO vo = dao.findByPrimaryKey(30001);
//		System.out.println(vo.getUnitno());
//		System.out.println(vo.getCouno());
//		System.out.println(vo.getUnitname());
//		System.out.println(vo.getUnitorder());
//		System.out.println("---------------------");
//
//		
//		List<UnitVO> list = dao.getAll();
//		for (UnitVO p : list) {
//			System.out.println(p.getUnitno());
//			System.out.println(p.getCouno());
//			System.out.println(p.getUnitname());
//			System.out.println(p.getUnitorder());
//			System.out.println("---------------------");
//		}
		
		
//		Set<ConVO> conSet = dao.getConsByUnitno(30018);
//		for (ConVO p : conSet) {
//			System.out.println(p.getConno());
//			System.out.println(p.getConname());
//			System.out.println(p.getContype());
//			System.out.println(p.getUnitno());
//			System.out.println(p.getConvideo());
//			System.out.println(p.getConorder());
//			System.out.println("---------------------");
//		}
//		
//		Set<ConVO> conSet2 = dao.getConsByUnitnoWithoutConvideo(30018);
//		for (ConVO p : conSet2) {
//			System.out.println(p.getConno());
//			System.out.println(p.getConname());
//			System.out.println(p.getContype());
//			System.out.println(p.getUnitno());
//			System.out.println(p.getConorder());
//			System.out.println("---------------------");
//		}
		
//		System.out.println(dao.getMaxConorderByUnitno(30002));
//		System.out.println(dao.getConnoByUnitnoConorder(30002,2));
		dao.updateUnitorder(30023, 5);

	}







	

}
