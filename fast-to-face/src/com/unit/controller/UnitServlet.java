package com.unit.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;




















import com.acc.model.AccService;
import com.acc.model.AccVO;
import com.ass.model.AssVO;
import com.content.model.ConService;
import com.content.model.ConVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.unit.model.*;

public class UnitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public void doGet(HttpServletRequest req, HttpServletResponse res)throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
			
		
		if ("insert".equals(action)) { 
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");	
			
			//couno
			Integer	couno = new Integer(req.getParameter("couno").trim());

			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
							
				//unitname
				String unitname = req.getParameter("unitname").trim();				
				if (unitname == null || (unitname.trim()).length() == 0) {
					errorMsgs.add("請輸入單元名稱");
				}
				
				CouService couSvc = new CouService();
				Integer unitorder = couSvc.getMaxUnitorderByCouno(couno) + 1;			
				
				UnitVO unitVO = new UnitVO();
				unitVO.setCouno(couno);
				unitVO.setUnitname(unitname);
				unitVO.setUnitorder(unitorder);			
												
				if (!errorMsgs.isEmpty()) {
					
					//跳頁準備
					Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
					req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
					req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
					req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 					
					
					List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);				
					req.setAttribute("couVO", wholeCourseList.get(0)); 
					req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
					req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
					req.setAttribute("assVOSetListList", wholeCourseList.get(3)); 				
					
					req.setAttribute("unitVO", unitVO);
					RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				
				UnitService unitSvc = new UnitService();
				unitVO = unitSvc.addUnit(couno, unitname, unitorder);				
				
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				
				//跳頁準備
				Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
				req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
				req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
				req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
				
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);				
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3)); 
				
				RequestDispatcher successView = req.getRequestDispatcher(requestURL);
				successView.forward(req, res);				
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				//跳頁準備
				CouService couSvc = new CouService();
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);					
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3)); 
				
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
				failureView.forward(req, res);
			}
		}		
		
				

		
		
		if ("update".equals(action)) { 
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");	
			Integer	couno = new Integer(req.getParameter("couno").trim());
		
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				
				//unitno
				Integer unitno = new Integer(req.getParameter("unitno").trim());
				
				//unitname			
				String unitname = req.getParameter("unitname").trim();
				if (unitname == null || (unitname.trim()).length() == 0) {
					errorMsgs.add("請輸入單元名稱");
				}
				
				
				if (!errorMsgs.isEmpty()) {
					//跳頁準備
					CouService couSvc = new CouService();
					List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);				
					req.setAttribute("couVO", wholeCourseList.get(0)); 
					req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
					req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
					req.setAttribute("assVOSetListList", wholeCourseList.get(3)); 
					
					RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
					failureView.forward(req, res);
					return; 
				}
				
				/***************************2.開始修改資料*****************************************/
				
				UnitService unitSvc = new UnitService();
				unitSvc.updateUnit(unitno, unitname);
								
				/***************************3.修改完成,準備轉交(Send the Success view)*************/	
				
				//跳頁準備
				CouService couSvc = new CouService();
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);					
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3)); 
								
				RequestDispatcher successView = req.getRequestDispatcher(requestURL);
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("dd");
				failureView.forward(req, res);
			}
		}		
		
		if ("delete".equals(action)) { 
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");
			Integer	couno = new Integer(req.getParameter("couno").trim());
	
			try {
				/***************************1.接收請求參數***************************************/
				Integer unitno = new Integer(req.getParameter("unitno"));
				
				/***************************2.開始刪除資料***************************************/
				UnitService unitSvc = new UnitService();
				unitSvc.deleteUnit(unitno);
				
				//reorder
				CouService couSvc = new CouService();
				Set<UnitVO> unitVOSet = couSvc.getUnitsByCouno(couno);
				int i = 1;
				for(UnitVO unitVO : unitVOSet){					
					unitSvc.updateUnitorder(unitVO.getUnitno(), i++);					
				}			
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/								
				
				//跳頁準備
				Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
				req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
				req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
				req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
				
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);					
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3)); 
				
				RequestDispatcher successView = req.getRequestDispatcher(requestURL);
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				//跳頁準備
				CouService couSvc = new CouService();
				Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
				req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
				req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
				req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
				
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);					
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3)); 
				
				errorMsgs.add(e.getMessage());
				RequestDispatcher successView = req.getRequestDispatcher(requestURL);
				successView.forward(req, res);
			}
		}
		
		
		
		if ("upward".equals(action) || "downward".equals(action)) { 
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");
			Integer	couno = new Integer(req.getParameter("couno").trim());
	
			try {
				/***************************1.接收請求參數***************************************/
				Integer unitno = new Integer(req.getParameter("unitno"));
				Integer unitorder = new Integer(req.getParameter("unitorder"));
				
				/***************************2.開始刪除資料***************************************/
				
				if ("upward".equals(action)) {
					//get previous unitno
					CouService couSvc = new CouService();
					Integer previousUnitno = couSvc.getUnitnoByCounoUnitorder(couno, unitorder - 1);
					//exchange unitorder				
					UnitService unitSvc = new UnitService();
					unitSvc.updateUnitorder(unitno, unitorder - 1);
					unitSvc.updateUnitorder(previousUnitno, unitorder);
				}else{
					//get next unitno
					CouService couSvc = new CouService();
					Integer nextUnitno = couSvc.getUnitnoByCounoUnitorder(couno, unitorder + 1);
					//exchange unitorder				
					UnitService unitSvc = new UnitService();
					unitSvc.updateUnitorder(unitno, unitorder + 1);
					unitSvc.updateUnitorder(nextUnitno, unitorder);		
				}
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/								
				
				//跳頁準備
				CouService couSvc = new CouService();
				Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
				req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
				req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
				req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
				
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);					
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3)); 
				
				RequestDispatcher successView = req.getRequestDispatcher(requestURL);
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				//跳頁準備
				CouService couSvc = new CouService();
				Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
				req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
				req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
				req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
				
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);					
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3)); 
				
				errorMsgs.add(e.getMessage());
				RequestDispatcher successView = req.getRequestDispatcher(requestURL);
				successView.forward(req, res);
			}
		}		
	}
}
