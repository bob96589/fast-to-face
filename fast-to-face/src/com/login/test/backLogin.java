package com.login.test;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.acc.model.AccService;
import com.acc.model.AccVO;

public class backLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	
	
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		if ("teacher".equals(action)) {
			AccService accSvc = new AccService();
			AccVO accVO = accSvc.getOneAcc(100002);
			
			HttpSession session = req.getSession();
			session.setAttribute("accVO", accVO);
		}
		
		if ("manager".equals(action)) {
			AccService accSvc = new AccService();
			AccVO accVO = accSvc.getOneAcc(100001);
			
			HttpSession session = req.getSession();
			session.setAttribute("accVO", accVO);			
		}
	
	}

}
