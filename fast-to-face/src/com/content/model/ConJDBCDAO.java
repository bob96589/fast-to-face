package com.content.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.ass.model.AssVO;

public class ConJDBCDAO implements ConDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:xe";
	String userid = "user1";
	String passwd = "u111";
	
	private static final String INSERT_STMT = 
			"INSERT INTO content (conno, conname, contype, unitno, convideo, conorder)"
			+ "VALUES (con_seq.nextval, ?, ?, ?, ?, ?)";
	private static final String GET_ALL_STMT = 
			"SELECT * FROM content where conorder != -1 order by conno";
	private static final String GET_ONE_STMT = 
			"SELECT * FROM content where conno = ?";
	private static final String DELETE = 
			"UPDATE content set conorder=-1 where conno = ?";
	private static final String UPDATE = 
			"UPDATE content set conname=?,convideo=? where conno = ?";
	
	private static final String GET_ASSS_BY_CONNO = 
			"select * from assessment where conno = ?";	
	private static final String UPDATE_CONORDER = 
			"UPDATE content set conorder=? where conno = ?";
	private static final String INSERT_ASS = 
			"INSERT INTO assessment (assno, assques, assans, assansa, assansb, assansc, assansd, conno)"
			+ "VALUES (ass_seq.nextval, ?, ?, ?, ?, ?, ?, ?)";
	
	
	@Override
	public Integer insert(ConVO conVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		Integer keyno = null;
		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			String[] column = {"conno"};
			pstmt = con.prepareStatement(INSERT_STMT, column);
						
			pstmt.setString(1, conVO.getConname());
			pstmt.setString(2, conVO.getContype());
			pstmt.setInt(3, conVO.getUnitno());
			pstmt.setString(4, conVO.getConvideo());			
			pstmt.setInt(5, conVO.getConorder());				

			pstmt.executeUpdate();
			
			ResultSet rsKeys = pstmt.getGeneratedKeys();
			
			if(rsKeys.next()){
				keyno = rsKeys.getInt(1);				
			}		

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
		return keyno;
	}
	
	
	@Override
	public void insertConAss(ConVO conVO, Set<AssVO> assVOSet) {
		Connection con = null;
		PreparedStatement pstmtCon = null;
		PreparedStatement pstmtAss = null;
		Integer conno = null;
		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			String[] column = {"conno"};
			pstmtCon = con.prepareStatement(INSERT_STMT, column);
			pstmtAss = con.prepareStatement(INSERT_ASS);
			con.setAutoCommit(false);
						
			pstmtCon.setString(1, conVO.getConname());
			pstmtCon.setString(2, conVO.getContype());
			pstmtCon.setInt(3, conVO.getUnitno());
			pstmtCon.setString(4, conVO.getConvideo());			
			pstmtCon.setInt(5, conVO.getConorder());
			pstmtCon.executeUpdate();
			
			ResultSet rsKeys = pstmtCon.getGeneratedKeys();			
			if(rsKeys.next()){
				conno = rsKeys.getInt(1);				
			}
			
			for(AssVO assVO : assVOSet){				
				pstmtAss.setString(1, assVO.getAssques());
				pstmtAss.setString(2, assVO.getAssans());
				pstmtAss.setString(3, assVO.getAssansa());
				pstmtAss.setString(4, assVO.getAssansb());
				pstmtAss.setString(5, assVO.getAssansc());
				pstmtAss.setString(6, assVO.getAssansd());
				pstmtAss.setInt(7, conno);
				pstmtAss.executeUpdate();				
			}
			
			con.commit();		
			

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());
		} catch (SQLException se) {
			if(con != null){
				try {
					System.err.println("A database error occured. " + se.getMessage());
					System.err.println("Transaction is being rolled back.");
					con.rollback();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		} finally {
			if (pstmtCon != null) {
				try {
					pstmtCon.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmtAss != null) {
				try {
					pstmtAss.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}

	@Override
	public void update(ConVO conVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
						
			pstmt.setString(1, conVO.getConname());
			pstmt.setString(2, conVO.getConvideo());	
			pstmt.setInt(3, conVO.getConno());

			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer conno) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, conno);
			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public ConVO findByPrimaryKey(Integer conno) {
		ConVO conVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, conno);

			rs = pstmt.executeQuery();
			
			while (rs.next()) {						
				conVO = new ConVO();
				conVO.setConno(rs.getInt("conno"));
				conVO.setConname(rs.getString("conname"));
				conVO.setContype(rs.getString("contype"));
				conVO.setUnitno(rs.getInt("unitno"));
				conVO.setConvideo(rs.getString("convideo"));
				conVO.setConorder(rs.getInt("conorder"));				
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return conVO;
	}

	@Override
	public List<ConVO> getAll() {
		List<ConVO> list = new ArrayList<ConVO>();
		ConVO conVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {				
				conVO = new ConVO();
				conVO.setConno(rs.getInt("conno"));
				conVO.setConname(rs.getString("conname"));
				conVO.setContype(rs.getString("contype"));
				conVO.setUnitno(rs.getInt("unitno"));
				conVO.setConvideo(rs.getString("convideo"));
				conVO.setConorder(rs.getInt("conorder"));				
				list.add(conVO);
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	
	@Override
	public Set<AssVO> getAsssByConno(Integer conno) {
		Set<AssVO> set = new LinkedHashSet<AssVO>();
		AssVO assVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ASSS_BY_CONNO);
			pstmt.setInt(1, conno);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				assVO = new AssVO();
				assVO.setAssno(rs.getInt("assno"));
				assVO.setAssques(rs.getString("assques"));
				assVO.setAssans(rs.getString("assans"));
				assVO.setAssansa(rs.getString("assansa"));
				assVO.setAssansb(rs.getString("assansb"));
				assVO.setAssansc(rs.getString("assansc"));
				assVO.setAssansd(rs.getString("assansd"));
				assVO.setConno(rs.getInt("conno"));
				set.add(assVO); // Store the row in the list
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}
	
	
	@Override
	public void updateConorder(Integer conno, Integer conorder) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE_CONORDER);
								
			pstmt.setInt(1, conorder);
			pstmt.setInt(2, conno);

			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	
	
	
	
	
	
	public static void main(String[] args) {
		
		ConJDBCDAO dao = new ConJDBCDAO();	
		System.out.println(String.valueOf(System.currentTimeMillis()));

		
//		try {
		
//			ConVO vo1 = new ConVO();
//			
//			FileInputStream in = new FileInputStream(new File("C:/pic/111.mp4"));
//			byte[] buffer = new byte[in.available()];
//			in.read(buffer);
//			
//			vo1.setConname("MANAGER");
//			vo1.setContype("�d�ç�");
//			vo1.setUnitno(30003);
//			vo1.setConvideo(buffer);
//			vo1.setConorder(5);		
//			System.out.println(dao.insert(vo1));
//			in.close();
//
//		
//			ConVO vo2 = new ConVO();
//			
//			FileInputStream in2 = new FileInputStream(new File("C:/pic/222.mp4"));
//			byte[] buffer2 = new byte[in2.available()];
//			in2.read(buffer2);
//			
//			vo2.setConno(40055);
//			vo2.setConname("MANAGER2");
//			vo2.setConvideo(buffer2);
//			vo2.setConorder(2);
//			dao.update(vo2);
//			in2.close();
//			
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}		
//
//
//		dao.delete(40055);
//		
//	
//		ConVO vo = dao.findByPrimaryKey(40001);
//		System.out.println(vo.getConno());
//		System.out.println(vo.getConname());
//		System.out.println(vo.getContype());
//		System.out.println(vo.getUnitno());
//		System.out.println(vo.getConvideo());
//		System.out.println(vo.getConorder());					
//		System.out.println("---------------------");
//		
//		
//		List<ConVO> list = dao.getAll();
//		for (ConVO p : list) {
//			System.out.println(p.getConno());
//			System.out.println(p.getConname());
//			System.out.println(p.getContype());
//			System.out.println(p.getUnitno());
//			System.out.println(p.getConvideo());
//			System.out.println(p.getConorder());
//			System.out.println("---------------------");
//		}
		
//		Set<AssVO> assSet = dao.getAsssByConno(40054);
//		for (AssVO p : assSet) {
//			System.out.println(p.getAssno());
//			System.out.println(p.getAssques());
//			System.out.println(p.getAssans());
//			System.out.println(p.getAssansa());
//			System.out.println(p.getAssansb());
//			System.out.println(p.getAssansc());
//			System.out.println(p.getAssansd());
//			System.out.println(p.getConno());
//			System.out.println("---------------------");
//		}
		
		
//		dao.updateConorder(40001, 9);
		
		
		ConVO vo = new ConVO();
		vo.setConname("MANAGER");
		vo.setContype("�d�ç�");
		vo.setUnitno(30003);
		vo.setConvideo(null);
		vo.setConorder(5);	
		
		Set<AssVO> assVOSet = new LinkedHashSet<AssVO>();
		AssVO vo1 = new AssVO();
		vo1.setAssques("���D2");
		vo1.setAssans("a2");
		vo1.setAssansa("a2");
		vo1.setAssansb("b2");
		vo1.setAssansc("c2");
		vo1.setAssansd("d2");
		assVOSet.add(vo1);
		
		AssVO vo2 = new AssVO();
		vo2.setAssques("���D");
		vo2.setAssans("a");
		vo2.setAssansa("a");
		vo2.setAssansb("b");
		vo2.setAssansc("c");
		vo2.setAssansd("d");
		assVOSet.add(vo2);
		
		dao.insertConAss(vo, assVOSet);
	}


	
	
	





}
