package com.content.model;

import java.util.List;
import java.util.Set;

import com.ass.model.AssVO;

public class ConService {
	private ConDAO_interface dao;

	public ConService() {
		dao = new ConDAO();
	}

	public ConVO addCon(String conname, String contype, Integer unitno, String convideo, Integer conorder) {

		ConVO conVO = new ConVO();

		conVO.setConname(conname);
		conVO.setContype(contype);
		conVO.setUnitno(unitno);
		conVO.setConvideo(convideo);
		conVO.setConorder(conorder);
		Integer conno = dao.insert(conVO);
		conVO.setConno(conno);

		return conVO;
	}
	
	public void insertConAss(ConVO conVO, Set<AssVO> assVOSet){
		dao.insertConAss(conVO, assVOSet);
	}

	public ConVO updateCon(Integer conno, String conname, String convideo) {

		ConVO conVO = new ConVO();

		conVO.setConno(conno);
		conVO.setConname(conname);
		conVO.setConvideo(convideo);
		dao.update(conVO);

		return conVO;
	}

	public void deleteCon(Integer conno) {
		dao.delete(conno);
	}

	public ConVO getOneCon(Integer conno) {
		return dao.findByPrimaryKey(conno);
	}

	public List<ConVO> getAll() {
		return dao.getAll();
	}

	public Set<AssVO> getAsssByConno(Integer conno){
		return dao.getAsssByConno(conno);
	}
	
	public void updateConorder(Integer conno, Integer conorder){
		dao.updateConorder(conno, conorder);
	}




}
