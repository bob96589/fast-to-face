package com.content.model;

public class ConVO  implements java.io.Serializable {
		
	private Integer conno;
	private String conname;
	private String contype;
	private Integer unitno;
	private String convideo;
	private Integer conorder;
	
	public Integer getConno() {
		return conno;
	}
	public void setConno(Integer conno) {
		this.conno = conno;
	}
	public String getConname() {
		return conname;
	}
	public void setConname(String conname) {
		this.conname = conname;
	}
	public String getContype() {
		return contype;
	}
	public void setContype(String contype) {
		this.contype = contype;
	}
	public Integer getUnitno() {
		return unitno;
	}
	public void setUnitno(Integer unitno) {
		this.unitno = unitno;
	}
	public String getConvideo() {
		return convideo;
	}
	public void setConvideo(String convideo) {
		this.convideo = convideo;
	}
	public Integer getConorder() {
		return conorder;
	}
	public void setConorder(Integer conorder) {
		this.conorder = conorder;
	}
	
	
	

	
	

}
