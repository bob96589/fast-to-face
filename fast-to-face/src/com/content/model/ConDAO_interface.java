package com.content.model;

import java.util.List;
import java.util.Set;

import com.ass.model.AssVO;

public interface ConDAO_interface {
	public Integer insert(ConVO conVO);
    public void update(ConVO conVO);
    public void delete(Integer conno);
    public ConVO findByPrimaryKey(Integer conno);
    public List<ConVO> getAll();
    
    public Set<AssVO> getAsssByConno(Integer conno);
    public void updateConorder(Integer conno, Integer conorder);
    public void insertConAss(ConVO conVO, Set<AssVO> assVOSet);
   

}
