package com.content.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.acc.model.AccService;
import com.acc.model.AccVO;
import com.ass.model.AssService;
import com.ass.model.AssVO;
import com.buy.model.BuyVO;
import com.content.model.ConService;
import com.content.model.ConVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.fin.model.FinService;
import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.oreilly.servlet.MultipartRequest;
import com.pro.model.ProService;
import com.pro.model.ProVO;
import com.unit.model.UnitService;
import com.unit.model.UnitVO;

public class ConServlet extends HttpServlet {
       
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		String contentType = req.getContentType();
		String action = null;
		MultipartRequest multi = null;
		if(contentType!=null && contentType.startsWith("multipart/form-data")){
			multi = new MultipartRequest(req, getServletContext().getRealPath("/shared/video"), 15*1024*1024, "UTF-8");			
			action = multi.getParameter("action");				
		}else{
			req.setCharacterEncoding("UTF-8");
			action = req.getParameter("action");			
		}
		
		
		if ("update_video".equals(action)) { 
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = multi.getParameter("requestURL");
			Integer couno = new Integer(multi.getParameter("couno"));
		
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				//conno
				Integer	conno = new Integer(multi.getParameter("conno").trim());
				
				//conname
				String conname = multi.getParameter("conname");
				if (conname == null || (conname.trim()).length() == 0) {
					errorMsgs.add("請輸入學程名稱");
				}					
							
				//convideo				
				String convideo = multi.getFilesystemName("convideo");
				if(convideo != null){
					File file = multi.getFile("convideo");
					String newFileName = String.valueOf(System.currentTimeMillis()) + ".mp4";
					File newFile = new File(file.getParent(), newFileName);
					newFile.delete();
					file.renameTo(newFile);
					convideo = newFileName;
					
					//delete existing video on server
					ConService conSvc = new ConService();
					ConVO conVO = conSvc.getOneCon(conno);
					String deletedVideo = conVO.getConvideo();
					File deletedFile = new File(file.getParent(), deletedVideo);
					deletedFile.delete();					
					
				}else{
					//read convideo from DB
					ConService conSvc = new ConService();
					ConVO conVO = conSvc.getOneCon(conno);
					convideo = conVO.getConvideo();
				}			
				
				
				ConVO conVO = new ConVO();
				conVO.setConno(conno);
				conVO.setConname(conname);
				conVO.setConvideo(convideo);
				
				if (!errorMsgs.isEmpty()) {
					
					//跳頁準備
					CouService couSvc = new CouService();
					
					Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
					req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
					req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
					req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
					
					List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);	
					req.setAttribute("conno", conno);
					req.setAttribute("couVO", wholeCourseList.get(0)); 
					req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
					req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
					req.setAttribute("assVOSetListList", wholeCourseList.get(3));
					
					req.setAttribute("conVO", conVO);
					req.setAttribute("requestURL", requestURL);
					RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始修改資料*****************************************/
				ConService conSvc = new ConService();
				conVO = conSvc.updateCon(conno, conname, convideo);
				
				/***************************3.修改完成,準備轉交(Send the Success view)*************/			
				
				req.setAttribute("conVO", conVO);
				
				//跳頁準備
				CouService couSvc = new CouService();
				
				Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
				req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
				req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
				req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
				
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);	
				req.setAttribute("conno", conno);
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3));
				
				RequestDispatcher successView = req.getRequestDispatcher(requestURL); 
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("AAAA");
				failureView.forward(req, res);
			}
		}
		
		
		
		
		if ("updata_con_and_ass".equals(action)) { 				
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");
			Integer couno = new Integer(req.getParameter("couno"));

			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
				
				
				//conno
				Integer	conno = new Integer(req.getParameter("conno").trim());
				
				//conname
				String conname = req.getParameter("conname");
				if (conname == null || (conname.trim()).length() == 0) {
					errorMsgs.add("請輸入單元內容名稱");
				}			
								
				//convideo
				String convideo = null;				
													
				ConVO conVO = new ConVO();
				conVO.setConno(conno);
				conVO.setConname(conname);
				conVO.setConvideo(convideo);
				
				List<AssVO> assVOList = new ArrayList<AssVO>();
				
				for(int i = 1; i <= 2; i++){
					Integer	assno = new Integer(req.getParameter("assno" + i).trim());
					String assques = req.getParameter("assques" + i);
					if (assques == null || (assques.trim()).length() == 0) {
						errorMsgs.add("請輸入單元內容名稱");
					}	
					String assans = req.getParameter("assans" + i);
					if (assans == null || (assans.trim()).length() == 0) {
						errorMsgs.add("請輸入單元內容名稱");
					}	
					String assansa = req.getParameter("assansa" + i);
					if (assansa == null || (assansa.trim()).length() == 0) {
						errorMsgs.add("請輸入單元內容名稱");
					}	
					String assansb = req.getParameter("assansb" + i);
					if (assansb == null || (assansb.trim()).length() == 0) {
						errorMsgs.add("請輸入單元內容名稱");
					}	
					String assansc = req.getParameter("assansc" + i);
					if (assansc == null || (assansc.trim()).length() == 0) {
						errorMsgs.add("請輸入單元內容名稱");
					}	
					String assansd = req.getParameter("assansd" + i);
					if (assansd == null || (assansd.trim()).length() == 0) {
						errorMsgs.add("請輸入單元內容名稱");
					}
					
					AssVO assVO = new AssVO();					
					assVO.setAssno(assno);
					assVO.setAssques(assques);
					assVO.setAssans(assans);
					assVO.setAssansa(assansa);
					assVO.setAssansb(assansb);
					assVO.setAssansc(assansc);
					assVO.setAssansd(assansd);
					assVO.setConno(conno);					
					assVOList.add(assVO);									
				}
				
				if (!errorMsgs.isEmpty()) {
					
					//跳頁準備
					CouService couSvc = new CouService();
					List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);	
					req.setAttribute("conno", conno);
					req.setAttribute("couVO", wholeCourseList.get(0)); 
					req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
					req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
					req.setAttribute("assVOSetListList", wholeCourseList.get(3));
					
					req.setAttribute("conVO", conVO); 
					RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
					failureView.forward(req, res);
					return;
				}
				
				
				/***************************2.開始新增資料***************************************/
								
				ConService conSvc = new ConService();
				conVO = conSvc.updateCon(conno, conname, convideo);
				
				AssService assSvc = new AssService();
				assSvc.updateAss(assVOList.get(0));
				assSvc.updateAss(assVOList.get(1));									
				
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				
				//跳頁準備
				CouService couSvc = new CouService();
				
				Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
				req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
				req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
				req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
				
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);	
				req.setAttribute("conno", conno);
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3));
				
				RequestDispatcher successView = req.getRequestDispatcher(requestURL); 
				successView.forward(req, res);				
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("abc");
				failureView.forward(req, res);
			}
		}
		
		
		
		
		
		
		if ("insert_video".equals(action)) { 				
			
				List<String> errorMsgs = new LinkedList<String>();
				req.setAttribute("errorMsgs", errorMsgs);
				
				String requestURL = multi.getParameter("requestURL");
				Integer couno = new Integer(multi.getParameter("couno"));

				try {
					/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
					
					//conname
					String conname = multi.getParameter("conname");
					if (conname == null || (conname.trim()).length() == 0) {
						errorMsgs.add("請輸入學程名稱");
					}					
					
					//contype
					String contype = multi.getParameter("contype").trim();
					
					//unitno
					Integer	unitno = new Integer(multi.getParameter("unitno").trim());
					
					
					//convideo
					String convideo = multi.getFilesystemName("convideo");
					if(convideo != null){
						File file = multi.getFile("convideo");
						String newFileName = String.valueOf(System.currentTimeMillis()) + ".mp4";
						File newFile = new File(file.getParent(), newFileName);
						newFile.delete();
						file.renameTo(newFile);
						convideo = newFileName;	
					}else{						
						errorMsgs.add("請選擇檔案");
					}					
					
					//conorder
					UnitService unitSvc = new UnitService();
					Integer	conorder = unitSvc.getMaxConorderByUnitno(unitno) + 1;

					ConVO conVO = new ConVO();
					conVO.setConname(conname);
					conVO.setContype(contype);
					conVO.setUnitno(unitno);
					conVO.setConvideo(convideo);

					if (!errorMsgs.isEmpty()) {
						//跳頁準備
						CouService couSvc = new CouService();
						
						Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
						req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
						req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
						req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
						
						List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);	
						req.setAttribute("couVO", wholeCourseList.get(0)); 
						req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
						req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
						req.setAttribute("assVOSetListList", wholeCourseList.get(3)); 
						
						req.setAttribute("conVO", conVO); 
						RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
						failureView.forward(req, res);
						return;
					}
					
					/***************************2.開始新增資料***************************************/
					ConService conSvc = new ConService();
					conVO = conSvc.addCon(conname, contype, unitno, convideo, conorder);
					Integer conno = conVO.getConno();
					
					/***************************3.新增完成,準備轉交(Send the Success view)***********/
					
					//跳頁準備
					CouService couSvc = new CouService();
					
					Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
					req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
					req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
					req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
					
					List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);	
					req.setAttribute("conno", conno);
					req.setAttribute("couVO", wholeCourseList.get(0)); 
					req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
					req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
					req.setAttribute("assVOSetListList", wholeCourseList.get(3)); 
					
					RequestDispatcher successView = req.getRequestDispatcher(requestURL); 
					successView.forward(req, res);				
					
					/***************************其他可能的錯誤處理**********************************/
				} catch (Exception e) {
					errorMsgs.add(e.getMessage());
					RequestDispatcher failureView = req.getRequestDispatcher("abc");
					failureView.forward(req, res);
				}
		}
		
		
		
		
		if ("insert_con_and_ass".equals(action)) { 				
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");
			Integer couno = new Integer(req.getParameter("couno"));
			Integer conno = null;

			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
				
				//conname
				String conname = req.getParameter("conname");
				if (conname == null || (conname.trim()).length() == 0) {
					errorMsgs.add("請輸入單元內容名稱");
				}					
				
				//contype
				String contype = req.getParameter("contype").trim();
				
				//unitno
				Integer	unitno = new Integer(req.getParameter("unitno").trim());
								
				//convideo
				String convideo = null;				
									
				//conorder
				UnitService unitSvc = new UnitService();
				Integer	conorder = unitSvc.getMaxConorderByUnitno(unitno) + 1;

				ConVO conVO = new ConVO();
				conVO.setConname(conname);
				conVO.setContype(contype);
				conVO.setUnitno(unitno);
				conVO.setConvideo(convideo);
				conVO.setConorder(conorder);
				
				
				/***************************2.開始新增資料***************************************/
				
				
			
				
				Set<AssVO> assVOSet = new LinkedHashSet<AssVO>();
				
				for(int i = 1; i <= 2; i++){
					String assques = req.getParameter("assques" + i);
					if (assques == null || (assques.trim()).length() == 0) {
						errorMsgs.add("請輸入單元內容名稱");
					}	
					String assans = req.getParameter("assans" + i);
					if (assans == null || (assans.trim()).length() == 0) {
						errorMsgs.add("請輸入單元內容名稱");
					}	
					String assansa = req.getParameter("assansa" + i);
					if (assansa == null || (assansa.trim()).length() == 0) {
						errorMsgs.add("請輸入單元內容名稱");
					}	
					String assansb = req.getParameter("assansb" + i);
					if (assansb == null || (assansb.trim()).length() == 0) {
						errorMsgs.add("請輸入單元內容名稱");
					}	
					String assansc = req.getParameter("assansc" + i);
					if (assansc == null || (assansc.trim()).length() == 0) {
						errorMsgs.add("請輸入單元內容名稱");
					}	
					String assansd = req.getParameter("assansd" + i);
					if (assansd == null || (assansd.trim()).length() == 0) {
						errorMsgs.add("請輸入單元內容名稱");
					}
					
					AssVO assVO = new AssVO();
					assVO.setAssques(assques);
					assVO.setAssans(assans);
					assVO.setAssansa(assansa);
					assVO.setAssansb(assansb);
					assVO.setAssansc(assansc);
					assVO.setAssansd(assansd);
					assVO.setConno(conno);					
					assVOSet.add(assVO);									
				}
				
				
				if (!errorMsgs.isEmpty()) {
									
					//跳頁準備
					CouService couSvc = new CouService();
					
					Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
					req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
					req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
					req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
					
					List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);		
					req.setAttribute("couVO", wholeCourseList.get(0)); 
					req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
					req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
					req.setAttribute("assVOSetListList", wholeCourseList.get(3));
					
					req.setAttribute("pageFrom", "type_1");
					
					req.setAttribute("conVO", conVO); 
					RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				
				ConService conSvc = new ConService();
				conSvc.insertConAss(conVO, assVOSet);				
				
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				
				//跳頁準備
				CouService couSvc = new CouService();
				
				Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
				req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
				req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
				req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
				
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);	
				req.setAttribute("conno", conno);
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3));
				
				RequestDispatcher successView = req.getRequestDispatcher(requestURL); 
				successView.forward(req, res);				
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("abc");
				failureView.forward(req, res);
			}
	}
		
		
		if ("delete".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");
			Integer couno = new Integer(req.getParameter("couno"));
			Integer unitno = new Integer(req.getParameter("unitno"));

			try {
				/***************************1.接收請求參數***************************************/
				Integer conno = new Integer(req.getParameter("conno"));
				
				/***************************2.開始刪除資料***************************************/
				
				ConService conSvc = new ConService();				
				conSvc.deleteCon(conno);				
				
				//reorder 
				UnitService unitSvc = new UnitService();
				Set<ConVO> conVOSet = unitSvc.getConsByUnitno(unitno);
				int i = 1;
				for(ConVO conVObj : conVOSet){
					conSvc.updateConorder(conVObj.getConno(), i++);					
				}			
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/
				
				//跳頁準備
				CouService couSvc = new CouService();
				
				Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
				req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
				req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
				req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
				
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);		
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3));
				
				RequestDispatcher successView = req.getRequestDispatcher(requestURL);
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				//跳頁準備
				CouService couSvc = new CouService();
				
				Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
				req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
				req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
				req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
				
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);			
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3));
				
				errorMsgs.add("刪除資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
				failureView.forward(req, res);
			}
		}
		
		
		
		if ("upward".equals(action) || "downward".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");
			Integer couno = new Integer(req.getParameter("couno"));
			Integer conno = new Integer(req.getParameter("conno"));

			try {
				/***************************1.接收請求參數***************************************/
				Integer unitno = new Integer(req.getParameter("unitno"));
				
				Integer conorder = new Integer(req.getParameter("conorder"));
				
				/***************************2.開始刪除資料***************************************/
				if("upward".equals(action)){
					//find previous conno
					UnitService unitSvc = new UnitService();
					Integer previousConno = unitSvc.getConnoByUnitnoConorder(unitno, conorder-1);
					
					//exchange conorder
					ConService conSvc = new ConService();
					conSvc.updateConorder(conno, conorder-1);
					conSvc.updateConorder(previousConno, conorder);						
				}else{
					//find next conno
					UnitService unitSvc = new UnitService();
					Integer nextConno = unitSvc.getConnoByUnitnoConorder(unitno, conorder+1);
					
					//exchange conorder
					ConService conSvc = new ConService();
					conSvc.updateConorder(conno, conorder+1);
					conSvc.updateConorder(nextConno, conorder);					
				}				
									
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/
				
				//跳頁準備
				CouService couSvc = new CouService();
				
				Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
				req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
				req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
				req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
				
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);		
				req.setAttribute("conno", conno); 
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3));
				
				RequestDispatcher successView = req.getRequestDispatcher(requestURL);
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				//跳頁準備
				CouService couSvc = new CouService();
				
				Map<String, Integer> CouDetailCountMap = couSvc.getCouDetailCountByCouno(couno);		
				req.setAttribute("unitCount", CouDetailCountMap.get("unitCount")); 
				req.setAttribute("videoCount", CouDetailCountMap.get("videoCount")); 
				req.setAttribute("assCount", CouDetailCountMap.get("assCount")); 
				
				List<Object> wholeCourseList = couSvc.getWholeCouByCouno(couno);	
				req.setAttribute("conno", conno);
				req.setAttribute("couVO", wholeCourseList.get(0)); 
				req.setAttribute("unitVOSet", wholeCourseList.get(1)); 
				req.setAttribute("conVOSetList", wholeCourseList.get(2)); 
				req.setAttribute("assVOSetListList", wholeCourseList.get(3));
				
				errorMsgs.add("刪除資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher(requestURL);
				failureView.forward(req, res);
			}
		}
		
		
		
		
		
		
		/***************************易鈞加的部分**********************************/
		if ("getAssConvideo".equals(action)) { 
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			HttpSession session = req.getSession();
			MemVO memVO = (MemVO)session.getAttribute("memVO");
		
			/***************************1.接收請求參數****************************************/
			Integer conno = new Integer(req.getParameter("conno"));
			Integer couno = new Integer(req.getParameter("couno"));
//			Integer assno = new Integer(req.getParameter("assno"));
			
			String contype = req.getParameter("contype");
//			解決傳送編碼的問題
			String contype2 = new String(contype.getBytes("ISO-8859-1"),"UTF-8");
			
			
			//guest or member
			if (memVO != null) {
				MemService memSvc = new MemService();
				Set<BuyVO> buyVOSet = memSvc.getBuysByMemno(memVO.getMemno());
				//member or buy member
				for (BuyVO buyVO : buyVOSet) {
					if (buyVO.getCouno().equals(couno)) {
						req.setAttribute("alreadyBought", "alreadyBought");
					}
				}
			}
			
			/***************************2.開始查詢資料****************************************/
			ConService conassSvc = new ConService();
			Set<AssVO> assVOSet = conassSvc.getAsssByConno(conno);
			
			/*判斷影片評量完成方法*/
			if("影片".equals(contype2)){
				FinService finSvc= new FinService();
				finSvc.updateisfinish(memVO.getMemno(), conno);				
			}
			
			ConService conSvc = new ConService();
			ConVO conVO = conSvc.getOneCon(conno);
			
			String convideo = conVO.getConvideo();
			
			CouService couSvc = new CouService();
			CouVO couVO = couSvc.getOneCou(couno);
			Set<UnitVO> unitVOSet = couSvc.getUnitsByCouno(couno);
			
			List<Set<ConVO>> conVOSetList = new ArrayList<Set<ConVO>>();
			
			for(UnitVO unitVO : unitVOSet){
				UnitService unitSvc = new UnitService();
				Set<ConVO> conVOSet = unitSvc.getConsByUnitno(unitVO.getUnitno());					
				conVOSetList.add(conVOSet);					
			}
			
			AccService accSvc = new AccService();
			AccVO accVO = accSvc.getOneAcc(couVO.getAccno());
			/***************************3.查詢完成,準備轉交(Send the Success view)*************/
			
			req.setAttribute("couVO", couVO); 
			req.setAttribute("accVO", accVO); 
			req.setAttribute("unitVOSet", unitVOSet); 
			req.setAttribute("conVOSetList", conVOSetList); 
			req.setAttribute("conVO", conVO); 
			req.setAttribute("convideo", convideo);
			req.setAttribute("assVOSet", assVOSet);
			
			String url = "/front/course_detail/couDetail2.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
			successView.forward(req, res);
			

	}
		if ("getOneAssConvideo".equals(action)) { 
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			HttpSession session = req.getSession();
			MemVO memVO = (MemVO)session.getAttribute("memVO");
		
			/***************************1.接收請求參數****************************************/
			Integer conno = new Integer(req.getParameter("conno"));
			Integer couno = new Integer(req.getParameter("couno"));
			
			String contype = req.getParameter("contype");
//			解決傳送編碼的問題
			String contype2 = new String(contype.getBytes("ISO-8859-1"),"UTF-8");
			
			String assans1 = req.getParameter("assans1");
			String assans2 = req.getParameter("assans2");
//			Integer assno = new Integer(req.getParameter("assno"));
			

			
			/***************************2.開始查詢資料****************************************/
			ConService conassSvc = new ConService();
			Set<AssVO> assVOSet = conassSvc.getAsssByConno(conno);
			List<AssVO> assVOList = new ArrayList<AssVO>(assVOSet);
			
			
			int rightAns=0;//看影片
			/*判斷影片評量完成方法*/
			if("影片".equals(contype2)){
				FinService finSvc= new FinService();
				finSvc.updateisfinish(memVO.getMemno(), conno);
				
			}else{
				if(assVOList.get(0).getAssans().equals(assans1)&&assVOList.get(1).getAssans().equals(assans2))
					rightAns=1;
				else	
					rightAns=2;
			}
			
			if(rightAns==1){//對
				FinService finSvc= new FinService();
				finSvc.updateisfinish(memVO.getMemno(), conno);
				req.setAttribute("rightAns", rightAns);//1
				
			}else if(rightAns==2){//錯
				req.setAttribute("rightAns", rightAns);//2
				
			}
			else{
				req.setAttribute("rightAns", rightAns);//0
			
			}
//			System.out.println(contype2);
			
			ConService conSvc = new ConService();
			ConVO conVO = conSvc.getOneCon(conno);
			
			String convideo = conVO.getConvideo();
			
			CouService couSvc = new CouService();
			CouVO couVO = couSvc.getOneCou(couno);
			Set<UnitVO> unitVOSet = couSvc.getUnitsByCouno(couno);
			
			List<Set<ConVO>> conVOSetList = new ArrayList<Set<ConVO>>();
			
			for(UnitVO unitVO : unitVOSet){
				UnitService unitSvc = new UnitService();
				Set<ConVO> conVOSet = unitSvc.getConsByUnitno(unitVO.getUnitno());					
				conVOSetList.add(conVOSet);					
			}
			
			AccService accSvc = new AccService();
			AccVO accVO = accSvc.getOneAcc(couVO.getAccno());
			
			
			/***************************3.查詢完成,準備轉交(Send the Success view)*************/
			
			req.setAttribute("couVO", couVO); 
			req.setAttribute("accVO", accVO); 
			req.setAttribute("unitVOSet", unitVOSet); 
			req.setAttribute("conVOSetList", conVOSetList); 
			req.setAttribute("conVO", conVO); 
			req.setAttribute("convideo", convideo); 
			req.setAttribute("assVOSet", assVOSet);
			
			String url = "/front/course_detail/couDetail2.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
			successView.forward(req, res);
			

		}
	
	}
}
