package com.fin.model;

public class FinVO {
	private Integer memno;
	private Integer conno;
	private Integer isfinish;
	
	public Integer getMemno() {
		return memno;
	}
	public void setMemno(Integer memno) {
		this.memno = memno;
	}
	public Integer getConno() {
		return conno;
	}
	public void setConno(Integer conno) {
		this.conno = conno;
	}
	public Integer getIsfinish() {
		return isfinish;
	}
	public void setIsfinish(Integer isfinish) {
		this.isfinish = isfinish;
	}
	
	
	
	
	
}
