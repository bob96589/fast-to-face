package com.fin.model;

import java.util.*;
import java.sql.*;

public class FinJDBCDAO implements FinDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "user1";
	String passwd = "u111";
	
	//modified by bob
	private static final String INSERT_STMT = 
			"INSERT INTO finishRecord (memno, conno, isfinish) VALUES (?, ?, 0)";
	private static final String GET_ALL_STMT = 
			"SELECT memno,conno,isfinish FROM finishRecord order by memno";
	private static final String GET_ONE_STMT = 
			"SELECT memno,conno,isfinish FROM finishRecord where memno = ? AND conno=?";
	private static final String DELETE = 
			"DELETE FROM finishRecord where memno = ? AND conno=?";
	private static final String UPDATE = 
			"UPDATE finishRecord set isfinish=? where memno = ? AND conno=?";
	
	//modified by 易鈞
	private static final String UPDATE_ISFINISH = 
	"UPDATE finishRecord set isfinish=1 where memno=? and conno = ?  ";	
	
	
	//modified by bob
	@Override
	public void insert(FinVO finVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setInt(1, finVO.getMemno());
			pstmt.setInt(2, finVO.getConno());	

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(FinVO finVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setInt(1, finVO.getIsfinish());
			pstmt.setInt(2, finVO.getMemno());
			pstmt.setInt(3, finVO.getConno());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer memno,Integer conno) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, memno);
			pstmt.setInt(2, conno);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public FinVO findByPrimaryKey(Integer memno,Integer conno) {

		FinVO finVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, memno);
			pstmt.setInt(2, conno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVo 也稱為 Domain objects
				finVO = new FinVO();
				finVO.setMemno(rs.getInt("memno"));
				finVO.setConno(rs.getInt("conno"));
				finVO.setIsfinish(rs.getInt("isfinish"));
				

			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return finVO;
	}

	@Override
	public List<FinVO> getAll() {
		List<FinVO> list = new ArrayList<FinVO>();
		FinVO finVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVO 也稱為 Domain objects
				finVO = new FinVO();
				finVO.setMemno(rs.getInt("memno"));
				finVO.setConno(rs.getInt("conno"));
				finVO.setIsfinish(rs.getInt("isfinish"));
				
				list.add(finVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public void updateisfinish(Integer memno,Integer conno) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE_ISFINISH);

			
			pstmt.setInt(1, memno);
			pstmt.setInt(2, conno);

			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	public static void main(String[] args) {

		FinJDBCDAO dao = new FinJDBCDAO();

//		//新增
//		FinVO finVO1 = new FinVO();
//		finVO1.setMemno(90001);
//		finVO1.setConno(40005);
//		dao.insert(finVO1);

		// 修改
//		FinVO finVO2 = new FinVO();
//		finVO2.setIsfinsh(1);
//		finVO2.setMemno(90001);
//		finVO2.setConno(40005);
//		dao.update(finVO2);

		// 刪除
//		dao.delete(90001,40005);

		// 查詢
//		FinVO finVO3 = dao.findByPrimaryKey(90001,40004);
//		System.out.print(finVO3.getMemno() + ",");
//		System.out.print(finVO3.getConno() + ",");
//		System.out.print(finVO3.getIsfinsh() + ","+"\n");
//		
//		System.out.println("---------------------");

		// 查詢
//		List<FinVO> list = dao.getAll();
//		for (FinVO aMem : list) {
//			System.out.print(aMem.getMemno() + ",");
//			System.out.print(aMem.getConno() + ",");
//			System.out.print(aMem.getIsfinsh() + ","+"\n");
//			System.out.println("---------------------");
//		}
	}
}
