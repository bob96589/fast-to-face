package com.fin.model;

import java.util.List;
import java.util.Set;

import com.buy.model.BuyVO;




public class FinService {

	private FinDAO_interface dao;

	public FinService() {
		dao = new FinDAO();
	}

	public List<FinVO> getAll() {
		return dao.getAll();
	}

	public FinVO getOneFin(Integer memno,Integer conno) {
		return dao.findByPrimaryKey(memno,conno);
	}


	public void deleteFin(Integer memno,Integer conno) {
		dao.delete(memno,conno);
	}
	
	
	//modified by bob
	public FinVO addFin(Integer memno, Integer conno) {
		FinVO finVO = new FinVO();
		finVO.setMemno(memno);
		finVO.setConno(conno);
		
		dao.insert(finVO);
		return finVO;
	}
	//modified by hsieh
	public void updateisfinish(Integer memno, Integer conno){
		
		dao.updateisfinish(memno, conno);
		
	}
	
	
}
