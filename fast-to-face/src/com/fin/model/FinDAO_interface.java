package com.fin.model;

import java.util.*;

public interface FinDAO_interface {
	public void insert(FinVO finVO);
	public void update(FinVO finVO);
	public void delete(Integer memno,Integer conno);
	public FinVO findByPrimaryKey(Integer memno,Integer conno);
	public List<FinVO> getAll();
	
	//hsieh
	public void updateisfinish(Integer memno,Integer conno);
}
