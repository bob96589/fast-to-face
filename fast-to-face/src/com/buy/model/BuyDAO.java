package com.buy.model;

import java.util.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class BuyDAO  implements BuyDAO_interface{
	private static DataSource ds = null;
	static{
		try{
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/TestDB");
		} catch (NamingException e){
			e.printStackTrace();
		}
	}

	//modified by bob
		private static final String INSERT_STMT = 
			"INSERT INTO BuyRecord (buyno,buytime,buyprice,buystate,memno,couno) VALUES (Buy_seq.NEXTVAL, sysdate, ?, 0, ?, ?)";
	private static final String GET_ALL_STMT = 
		"SELECT buyno,buytime,buyprice,buystate,memno,couno FROM BuyRecord order by buyno";
	private static final String GET_ONE_STMT = 
		"SELECT buyno,buytime,buyprice,buystate,memno,couno FROM BuyRecord where buyno = ?";
	private static final String DELETE = 
		"DELETE FROM BuyRecord where buyno = ?";
	private static final String UPDATE = 
		"UPDATE BuyRecord set buytime=?, buyprice=?, buystate=?, memno=?, couno=? where buyno = ?";
	
	private static final String GET_RANGE_BY_MEMNO_STMT = 
			"SELECT * FROM BuyRecord where buytime >= ? and buytime <= ? and memno = ? order by buytime desc";
	
	

	//modified by bob
		@Override
		public int insert(BuyVO buyVO) {
			int updateCount = 0;
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(INSERT_STMT);
				
				pstmt.setInt(1, buyVO.getBuyprice());
				pstmt.setInt(2, buyVO.getMemno());
				pstmt.setInt(3, buyVO.getCouno());			

				updateCount = pstmt.executeUpdate();			
				

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return updateCount;
		}

	@Override
	public int update(BuyVO buyVO) {
		int updateCount = 0;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setTimestamp(1, buyVO.getBuytime());
			pstmt.setInt(2, buyVO.getBuyprice());
			pstmt.setInt(3, buyVO.getBuystate());
			pstmt.setInt(4, buyVO.getMemno());
			pstmt.setInt(5, buyVO.getCouno());
			pstmt.setInt(6, buyVO.getBuyno());

			updateCount = pstmt.executeUpdate();

			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return updateCount;
	}

	@Override
	public int delete(Integer buyno) {
		int updateCount = 0;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);
			
			pstmt.setInt(1, buyno);
			
			updateCount = pstmt.executeUpdate();

			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return updateCount;
	}

	@Override
	public BuyVO findByPrimaryKey(Integer buyno) {

		BuyVO buyVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setInt(1, buyno);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// advVo is Domain objects
				buyVO = new BuyVO();
				buyVO.setBuyno(rs.getInt("buyno"));
				buyVO.setBuytime(rs.getTimestamp("buytime"));
				buyVO.setBuyprice(rs.getInt("buyprice"));
				buyVO.setBuystate(rs.getInt("buystate"));
				buyVO.setMemno(rs.getInt("memno"));
				buyVO.setCouno(rs.getInt("couno"));
			}

			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return buyVO;
	}

	@Override
	public List<BuyVO> getAll() {
		List<BuyVO> list = new ArrayList<BuyVO>();
		BuyVO buyVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// advVO is Domain objects
				buyVO = new BuyVO();
				buyVO.setBuyno(rs.getInt("buyno"));
				buyVO.setBuytime(rs.getTimestamp("buytime"));
				buyVO.setBuyprice(rs.getInt("buyprice"));
				buyVO.setBuystate(rs.getInt("buystate"));
				buyVO.setMemno(rs.getInt("memno"));
				buyVO.setCouno(rs.getInt("couno"));
				list.add(buyVO); // Store the row in the vector
			}

			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	
	@Override
	public List<BuyVO> getRangeByMemno(Timestamp d1, Timestamp d2, Integer memno) {
		List<BuyVO> list = new ArrayList<BuyVO>();
		BuyVO buyVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_RANGE_BY_MEMNO_STMT);
			pstmt.setTimestamp(1,d1);
			pstmt.setTimestamp(2,d2);
			pstmt.setInt(3,memno);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				buyVO = new BuyVO();
				buyVO.setBuyno(rs.getInt("buyno"));
				buyVO.setBuytime(rs.getTimestamp("buytime"));
				buyVO.setBuyprice(rs.getInt("buyprice"));
				buyVO.setBuystate(rs.getInt("buystate"));
				buyVO.setMemno(rs.getInt("memno"));
				buyVO.setCouno(rs.getInt("couno"));
				list.add(buyVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
}