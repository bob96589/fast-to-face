package com.buy.model;

import java.util.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;

public class BuyJDBCDAO  implements BuyDAO_interface{
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:xe";
	String userid = "user1";
	String passwd = "u111";
	
	//modified by bob
	private static final String INSERT_STMT = 
		"INSERT INTO BuyRecord (buyno,buytime,buyprice,buystate,memno,couno) VALUES (Buy_seq.NEXTVAL, sysdate, ?, 0, ?, ?)";
	private static final String GET_ALL_STMT = 
		"SELECT buyno,buytime,buyprice,buystate,memno,couno FROM BuyRecord order by buyno";
	private static final String GET_ONE_STMT = 
		"SELECT buyno,buytime,buyprice,buystate,memno,couno FROM BuyRecord where buyno = ?";
	private static final String DELETE = 
		"DELETE FROM BuyRecord where buyno = ?";
	private static final String UPDATE = 
		"UPDATE BuyRecord set buytime=?, buyprice=?, buystate=?, memno=?, couno=? where buyno = ?";
	
	private static final String GET_RANGE_BY_MEMNO_STMT = 
			"SELECT * FROM BuyRecord where buytime >= ? and buytime <= ? and memno = ? order by buytime desc";
	
	
	
	//modified by bob
	@Override
	public int insert(BuyVO buyVO) {
		int updateCount = 0;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);			
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setInt(1, buyVO.getBuyprice());
			pstmt.setInt(2, buyVO.getMemno());
			pstmt.setInt(3, buyVO.getCouno());			

			updateCount = pstmt.executeUpdate();			
			

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return updateCount;
	}

	@Override
	public int update(BuyVO buyVO) {
		int updateCount = 0;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setTimestamp(1, buyVO.getBuytime());
			pstmt.setInt(2, buyVO.getBuyprice());
			pstmt.setInt(3, buyVO.getBuystate());
			pstmt.setInt(4, buyVO.getMemno());
			pstmt.setInt(5, buyVO.getCouno());
			pstmt.setInt(6, buyVO.getBuyno());

			updateCount = pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return updateCount;
	}

	@Override
	public int delete(Integer buyno) {
		int updateCount = 0;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);
			
			pstmt.setInt(1, buyno);
			
			updateCount = pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return updateCount;
	}

	@Override
	public BuyVO findByPrimaryKey(Integer buyno) {

		BuyVO buyVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setInt(1, buyno);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				buyVO = new BuyVO();
				buyVO.setBuyno(rs.getInt("buyno"));
				buyVO.setBuytime(rs.getTimestamp("buytime"));
				buyVO.setBuyprice(rs.getInt("buyprice"));
				buyVO.setBuystate(rs.getInt("buystate"));
				buyVO.setMemno(rs.getInt("memno"));
				buyVO.setCouno(rs.getInt("couno"));
				
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return buyVO;
	}

	@Override
	public List<BuyVO> getAll() {
		List<BuyVO> list = new ArrayList<BuyVO>();
		BuyVO buyVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// advVO is Domain objects
				buyVO = new BuyVO();
				buyVO.setBuyno(rs.getInt("buyno"));
				buyVO.setBuytime(rs.getTimestamp("buytime"));
				buyVO.setBuyprice(rs.getInt("buyprice"));
				buyVO.setBuystate(rs.getInt("buystate"));
				buyVO.setMemno(rs.getInt("memno"));
				buyVO.setCouno(rs.getInt("couno"));
				list.add(buyVO); // Store the row in the vector
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	
	
	@Override
	public List<BuyVO> getRangeByMemno(Timestamp d1, Timestamp d2, Integer memno) {
		List<BuyVO> list = new ArrayList<BuyVO>();
		BuyVO buyVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_RANGE_BY_MEMNO_STMT);
			pstmt.setTimestamp(1,d1);
			pstmt.setTimestamp(2,d2);
			pstmt.setInt(3,memno);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				buyVO = new BuyVO();
				buyVO.setBuyno(rs.getInt("buyno"));
				buyVO.setBuytime(rs.getTimestamp("buytime"));
				buyVO.setBuyprice(rs.getInt("buyprice"));
				buyVO.setBuystate(rs.getInt("buystate"));
				buyVO.setMemno(rs.getInt("memno"));
				buyVO.setCouno(rs.getInt("couno"));
				list.add(buyVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	
	
	
	
	
	
	public static void main(String[] args) {
		BuyJDBCDAO dao = new BuyJDBCDAO();

//		 BuyVO buyVO1 = new BuyVO();
//		 buyVO1.setBuytime(Timestamp.valueOf("2014-10-30 21:21:21"));
//		 buyVO1.setBuyprice(1000);
//		 buyVO1.setBuystate(1);
//		 buyVO1.setMemno(90005);
//		 buyVO1.setCouno(10009);
//		 int updateCount_insert = dao.insert(buyVO1);
//		 System.out.println(updateCount_insert);
				

//		 BuyVO buyVO2 = new BuyVO();
//		 buyVO2.setBuyno(80006);
//		 buyVO2.setBuytime(Timestamp.valueOf("2002-01-01 20:25:23"));	
//		 buyVO2.setBuyprice(100);
//		 buyVO2.setBuystate(0);
//		 buyVO2.setMemno(90001);
//		 buyVO2.setCouno(10001);
//		 int updateCount_update = dao.update(buyVO2);
//		 System.out.println(updateCount_update);
				

//		 int updateCount_delete = dao.delete(80007);
//		 System.out.println(updateCount_delete);

//		BuyVO advVO3 = dao.findByPrimaryKey(80012);
//		System.out.print(advVO3.getBuyno() + ",");
//		System.out.print(advVO3.getBuytime() + ",");
//		System.out.print(advVO3.getBuyprice() + ",");
//		System.out.print(advVO3.getBuystate() + ",");
//		System.out.print(advVO3.getMemno() + ",");
//		System.out.print(advVO3.getCouno() + ",");
//		System.out.println("---------------------");

//		List<BuyVO> list = dao.getAll();
//		for (BuyVO aBuy : list) {
//			System.out.print(aBuy.getBuyno() + ",");
//			System.out.print(aBuy.getBuytime() + ",");
//			System.out.print(aBuy.getBuyprice() + ",");
//			System.out.print(aBuy.getBuystate() + ",");
//			System.out.print(aBuy.getMemno() + ",");
//			System.out.print(aBuy.getCouno() + ",");
//			System.out.println();
//		}
		
		
//		BuyVO buyVO = new BuyVO();
//		buyVO.setBuyprice(1000);
//		buyVO.setMemno(90005);
//		buyVO.setCouno(10009);
//		
//		dao.insert(buyVO);

	}

}
