package com.buy.model;

import java.sql.*;

public class BuyVO implements java.io.Serializable{
	private Integer buyno;
	private Timestamp buytime;
	private Integer buyprice;
	private Integer buystate;
	private Integer memno;
	private Integer couno;
	public Integer getBuyno() {
		return buyno;
	}
	public void setBuyno(Integer buyno) {
		this.buyno = buyno;
	}
	public Timestamp getBuytime() {
		return buytime;
	}
	public void setBuytime(Timestamp buytime) {
		this.buytime = buytime;
	}
	public Integer getBuyprice() {
		return buyprice;
	}
	public void setBuyprice(Integer buyprice) {
		this.buyprice = buyprice;
	}
	public Integer getBuystate() {
		return buystate;
	}
	public void setBuystate(Integer buystate) {
		this.buystate = buystate;
	}
	public Integer getMemno() {
		return memno;
	}
	public void setMemno(Integer memno) {
		this.memno = memno;
	}
	public Integer getCouno() {
		return couno;
	}
	public void setCouno(Integer couno) {
		this.couno = couno;
	}
	
}
