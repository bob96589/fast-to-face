package com.buy.model;

import java.sql.Timestamp;
import java.util.*;

public interface BuyDAO_interface {
	public int insert(BuyVO buyVO);
	public int update(BuyVO buyVO);
	public int delete(Integer buyno);
	public BuyVO findByPrimaryKey(Integer buyno);
	public List<BuyVO> getAll();
		
	public List<BuyVO> getRangeByMemno(Timestamp d1, Timestamp d2, Integer memno);
}