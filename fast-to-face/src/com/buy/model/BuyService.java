package com.buy.model;

import java.sql.Timestamp;
import java.util.List;

public class BuyService {

	private BuyDAO_interface dao;

	public BuyService() {
		dao = new BuyDAO();
	}
	//modified by bob
	public BuyVO addBuy(Integer buyprice, Integer memno, Integer couno) {

		BuyVO buyVO = new BuyVO();

		buyVO.setBuyprice(buyprice);
		buyVO.setMemno(memno);
		buyVO.setCouno(couno);
		
		dao.insert(buyVO);

		return buyVO;
	}

	public BuyVO updateBuy(Integer buyno,Timestamp buytime,Integer buyprice,Integer buystate) {

		BuyVO buyVO = new BuyVO();

		buyVO.setBuyno(buyno);
		buyVO.setBuytime(buytime);
		buyVO.setBuyprice(buyprice);
		buyVO.setBuystate(buystate);
		dao.update(buyVO);

		return buyVO;
	}

	public void deleteBuy(Integer buyno) {
		dao.delete(buyno);
	}

	public BuyVO getOneBuy(Integer buyno) {
		return dao.findByPrimaryKey(buyno);
	}

	public List<BuyVO> getAll() {
		return dao.getAll();
	}
	
	
	public List<BuyVO> getRangeByMemno(Timestamp d1, Timestamp d2, Integer memno){
		return dao.getRangeByMemno(d1,d2,memno);
	}
}
