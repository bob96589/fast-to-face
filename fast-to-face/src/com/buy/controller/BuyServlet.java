package com.buy.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.buy.model.BuyService;
import com.buy.model.BuyVO;
import com.mem.model.MemService;
import com.mem.model.MemVO;

public class BuyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
		
		
		
		
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		if ("getBuyList".equals(action)) {
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String whichPage = req.getParameter("whichPage");			
			HttpSession session= req.getSession();
			MemVO memVO = (MemVO)session.getAttribute("memVO");
			
//			try {
				/*************************** 1.接收請求參數 - 輸入格式的錯誤處理 **********************/
								
				
				String formStr = req.getParameter("from");
				String toStr = req.getParameter("to");
				
				if(formStr == null || (formStr.trim()).length() == 0){
					
					//getAll
					MemService memSvc = new MemService();
					Set<BuyVO> buyVOSet = memSvc.getBuysByMemno(memVO.getMemno());					
					req.setAttribute("buyVOSet", buyVOSet);
					
				}else{
					//getPart
					
					
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");					
					//from
					Timestamp from = null;					
					try {
						
						Date fromDate = formatter.parse(formStr);
						from = new Timestamp(fromDate.getTime());
					} catch (Exception e) {
						errorMsgs.add("日期格式轉換錯誤");
					}					
					//to
					Timestamp to = null;					
					try {
						Date toDate = formatter.parse(toStr);
						to = new Timestamp(toDate.getTime());
					} catch (Exception e) {
						errorMsgs.add("日期格式轉換錯誤");
					}
					
					BuyService buySvc = new BuyService();
					List<BuyVO> buyVOList = buySvc.getRangeByMemno(from, to, memVO.getMemno());
					Set<BuyVO> buyVOSet = new LinkedHashSet<BuyVO>(buyVOList);
					req.setAttribute("buyVOSet", buyVOSet);					
					
				}
							
				String url = "/front/buy_record/listBuyByMemno.jsp";
				req.setAttribute("formStr", formStr);
				req.setAttribute("toStr", toStr);
				req.setAttribute("whichPage", whichPage);
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				/*************************** 其他可能的錯誤處理 *************************************/
//			} catch (Exception e) {
//				errorMsgs.add("無法取得資料:" + e.getMessage());
//				RequestDispatcher failureView = req
//						.getRequestDispatcher("/front/buy/listBuyByMemno.jsp");
//				failureView.forward(req, res);
//			}
		}
		
	}

}
