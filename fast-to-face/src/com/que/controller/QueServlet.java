package com.que.controller;

import java.io.*;
import java.sql.Timestamp;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.acc.model.AccService;
import com.acc.model.AccVO;
import com.cou.model.CouService;
import com.cou.model.CouVO;
import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.que.model.QueService;
import com.que.model.QueVO;

public class QueServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		
		if ("getOne_For_Display".equals(action)) { // 來自select_page.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String str = req.getParameter("quesno");
				if (str == null || (str.trim()).length() == 0) {
					errorMsgs.add("請輸入問題編號");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/que/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				Integer quesno = null;
				try {
					quesno = new Integer(str);
				} catch (Exception e) {
					errorMsgs.add("問題編號格式不正確");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/que/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************2.開始查詢資料*****************************************/
				QueService queSvc = new QueService();
				QueVO queVO = queSvc.getOneQue(quesno);
				if (queVO == null) {
					errorMsgs.add("查無資料");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/que/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************3.查詢完成,準備轉交(Send the Success view)*************/
				req.setAttribute("queVO", queVO); // 資料庫取出的empVO物件,存入req
				String url = "/que/listOneQue.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/que/select_page.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("getOne_For_Update".equals(action)) { // 來自listAllEmp.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				/***************************1.接收請求參數****************************************/
				Integer quesno = new Integer(req.getParameter("quesno"));
				
				/***************************2.開始查詢資料****************************************/
				QueService queSvc = new QueService();
				QueVO queVO = queSvc.getOneQue(quesno);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("queVO", queVO);         // 資料庫取出的empVO物件,存入req
				String url = "/que/update_que_input.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/que/listAllQue.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("update".equals(action)) { // 來自update_emp_input.jsp的請求
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
		
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				Integer quesno = new Integer(req.getParameter("quesno").trim());
//				Integer couno = new Integer(req.getParameter("couno").trim());		
//				Integer memno = new Integer(req.getParameter("memno").trim());
//				java.sql.Timestamp quesasktime = null;
				
//				java.sql.Timestamp quesrestime = null;
				
//				try {
//					quesasktime = java.sql.Timestamp.valueOf(req.getParameter("quesasktime").trim());
//				} catch (IllegalArgumentException e) {
//					quesasktime=new java.sql.Timestamp(System.currentTimeMillis());
//					errorMsgs.add("請輸入日期!");
//				}
//
//				String quesaskcon = null;
//				try {
//					quesaskcon = new String(req.getParameter("quesaskcon").trim());
//				} catch (NullPointerException e) {
//					quesaskcon = null;
//					errorMsgs.add("問題不可為空白.");
//				}
				
//				try {
//					quesrestime = java.sql.Timestamp.valueOf(req.getParameter("quesrestime").trim());
//				} catch (IllegalArgumentException e) {
//					quesrestime=new java.sql.Timestamp(System.currentTimeMillis());
//					errorMsgs.add("請輸入日期!");
//				}
				
				String quesrescon = null;
				try {
					quesrescon = new String(req.getParameter("quesrescon").trim());
				} catch (NullPointerException e) {
					quesrescon = null;
					errorMsgs.add("問題不可為空白.");
				}

				//Integer deptno = new Integer(req.getParameter("deptno").trim());

				QueVO queVO = new QueVO();
				queVO.setQuesno(quesno);
//				queVO.setCouno(couno);
//				queVO.setMemno(memno);
//				queVO.setQuesasktime(quesasktime);
//				queVO.setQuesaskcon(quesaskcon);
//				queVO.setQuesrestime(quesrestime);
				queVO.setQuesrescon(quesrescon);
				//queVO.setDeptno(deptno);

				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("queVO", queVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back/question_response/listAllQue.jsp");
					failureView.forward(req, res);
					return; //程式中斷
				}
				
				/***************************2.開始修改資料*****************************************/
				QueService queSvc = new QueService();
				queVO = queSvc.updateQue(quesno,quesrescon);
				
				/***************************3.修改完成,準備轉交(Send the Success view)*************/
				req.setAttribute("queVO", queVO); // 資料庫update成功後,正確的的empVO物件,存入req
				String url = "/back/question_response/listAllQue.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 修改成功後,轉交listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back/question_response/listAllQue.jsp");
				failureView.forward(req, res);
			}
		}

        if ("insert".equals(action)) { // 來自addQue.jsp的請求  
			
			List<String> errorMsgs = new LinkedList<String>();			
			req.setAttribute("errorMsgs", errorMsgs);
			
			HttpSession session = req.getSession();
			MemVO memVO = (MemVO)session.getAttribute("memVO");			
			
			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
				
				Integer couno = new Integer(req.getParameter("couno"));

				String	quesaskcon = new String(req.getParameter("quesaskcon").trim());
				if (quesaskcon == null || (quesaskcon.trim()).length() == 0) {
					errorMsgs.add("請輸入問題內容");
				}
				
				QueVO queVO = new QueVO();
				
				queVO.setQuesaskcon(quesaskcon);

				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("queVO", queVO);
					RequestDispatcher failureView = req.getRequestDispatcher("/couDetail/cou.do?"+req.getParameter("reURL"));
					failureView.forward(req, res);
					return;
				}
			
				/***************************2.開始新增資料***************************************/
				QueService queSvc = new QueService();
				queVO = queSvc.addQue(couno, memVO.getMemno(), quesaskcon);
				
				
				/***************************3.新增完成,準備轉交(Send the Success view)***********/

				
				String url = "/couDetail/cou.do?"+req.getParameter("reURL");
				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
				successView.forward(req, res);				
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/couDetail/cou.do?"+req.getParameter("reURL"));
				failureView.forward(req, res);
			}
		}
        
		
		if ("delete".equals(action)) { // 來自listAllEmp.jsp

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
	
			try {
				/***************************1.接收請求參數***************************************/
				Integer quesno = new Integer(req.getParameter("quesno"));
				
				/***************************2.開始刪除資料***************************************/
				QueService queSvc = new QueService();
				queSvc.deleteQue(quesno);
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/								
				String url = "/back/question_response/listAllQue.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 刪除成功後,轉交回送出刪除的來源網頁
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("刪除資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back/question_response/listAllQue.jsp");
				failureView.forward(req, res);
			}
		}
	}
}
