package com.que.model;

import java.util.List;
import java.util.Set;
import java.sql.Timestamp;

public class QueService {

	private QueDAO_interface dao;

	public QueService() {
		dao = new QueDAO();
	}

	public QueVO addQue(Integer couno, Integer memno, 
			String quesaskcon) {

		QueVO queVO = new QueVO();

		queVO.setCouno(couno);
		queVO.setMemno(memno);
//		queVO.setQuesasktime(quesasktime);
		queVO.setQuesaskcon(quesaskcon);
//		queVO.setQuesrestime(quesrestime);
//		queVO.setQuesrescon(quesrescon);
		dao.insert(queVO);

		return queVO;
	}

	public QueVO updateQue(Integer quesno, String quesrescon) {

		QueVO queVO = new QueVO();
		queVO.setQuesno(quesno);
//		queVO.setCouno(couno);
//		queVO.setMemno(memno);
//		queVO.setQuesasktime(quesasktime);
//		queVO.setQuesaskcon(quesaskcon);
//		queVO.setQuesrestime(quesrestime);
		queVO.setQuesrescon(quesrescon);
		dao.update(queVO);

		return queVO;
	}

	public void deleteQue(Integer quesno) {
		dao.delete(quesno);
	}

	public QueVO getOneQue(Integer quesno) {
		return dao.findByPrimaryKey(quesno);
	}

	public List<QueVO> getAll() {
		return dao.getAll();
	}
	
	public Set<QueVO> getQueByCouno(Integer couno){
		return dao.getQueByCouno(couno); 
	 }
	
	public Set<QueVO> getQueByMemno(Integer memno){
		return dao.getQueByMemno(memno);  
	}
}
