package com.que.model;

import java.util.*;

public interface QueDAO_interface {
          public void insert(QueVO queVO);
          public void update(QueVO queVO);
          public void delete(Integer quesno);
          public QueVO findByPrimaryKey(Integer quesno);
          public List<QueVO> getAll();
          public Set<QueVO> getQueByCouno(Integer couno);
          public Set<QueVO> getQueByMemno(Integer memno);
}
