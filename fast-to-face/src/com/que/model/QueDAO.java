package com.que.model;

import java.util.*;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.cou.model.CouVO;



public class QueDAO implements QueDAO_interface {

	
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/TestDB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
			"INSERT INTO question (quesno,couno,memno,quesasktime,quesaskcon) VALUES (qus_seq.NEXTVAL, ?, ?, sysdate, ?)";
		private static final String GET_ALL_STMT = 
			"SELECT quesno,couno,memno, quesasktime, quesaskcon, quesrestime,quesrescon FROM question order by quesasktime desc";
		private static final String GET_ONE_STMT = 
			"SELECT quesno,couno,memno, quesasktime, quesaskcon, quesrestime,quesrescon FROM question where quesno = ?";
		private static final String DELETE = 
			"DELETE FROM question where quesno = ?";
		private static final String UPDATE = 
			"UPDATE question set quesrestime=sysdate,quesrescon=? where quesno = ?";
		private static final String GET_QUESTION_BY_COUNO = 
				"select * from question where couno = ? order by quesasktime desc" ;
		
		private static final String GET_QUESTION_BY_MEMNO = 
				"select * from question where memno = ? order by quesasktime desc";
	@Override
	public void insert(QueVO queVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setInt(1, queVO.getCouno());
			pstmt.setInt(2, queVO.getMemno());
			pstmt.setString(3, queVO.getQuesaskcon());
		


			pstmt.executeUpdate();

			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(QueVO queVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setString(1, queVO.getQuesrescon());
			pstmt.setInt(2, queVO.getQuesno());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer quesno) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, quesno);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public QueVO findByPrimaryKey(Integer quesno) {

		QueVO queVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, quesno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// quepVo  Domain objects
				queVO = new QueVO();
				queVO.setQuesno(rs.getInt("quesno"));
				queVO.setCouno(rs.getInt("couno"));
				queVO.setMemno(rs.getInt("memno"));
				queVO.setQuesasktime(rs.getTimestamp("quesasktime"));
				queVO.setQuesaskcon(rs.getString("quesaskcon"));
				queVO.setQuesrestime(rs.getTimestamp("quesrestime"));
				queVO.setQuesrescon(rs.getString("quesrescon"));
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return queVO;
	}

	@Override
	public List<QueVO> getAll() {
		List<QueVO> list = new ArrayList<QueVO>();
		QueVO queVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				queVO = new QueVO();
				queVO.setQuesno(rs.getInt("quesno"));
				queVO.setCouno(rs.getInt("couno"));
				queVO.setMemno(rs.getInt("memno"));
				queVO.setQuesasktime(rs.getTimestamp("quesasktime"));
				queVO.setQuesaskcon(rs.getString("quesaskcon"));
				queVO.setQuesrestime(rs.getTimestamp("quesrestime"));
				queVO.setQuesrescon(rs.getString("quesrescon"));
				list.add(queVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public Set<QueVO> getQueByCouno(Integer couno) {
		Set<QueVO> set = new LinkedHashSet<QueVO>();
		QueVO queVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_QUESTION_BY_COUNO);
			pstmt.setInt(1, couno);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// quepVo  Domain objects
				queVO = new QueVO();
				queVO.setQuesno(rs.getInt("quesno"));
				queVO.setCouno(rs.getInt("couno"));
				queVO.setMemno(rs.getInt("memno"));
				queVO.setQuesasktime(rs.getTimestamp("quesasktime"));
				queVO.setQuesaskcon(rs.getString("quesaskcon"));
				queVO.setQuesrestime(rs.getTimestamp("quesrestime"));
				queVO.setQuesrescon(rs.getString("quesrescon"));
				set.add(queVO);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}
	
	@Override
	public Set<QueVO> getQueByMemno(Integer memno) {
		Set<QueVO> set = new LinkedHashSet<QueVO>();
		QueVO queVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_QUESTION_BY_MEMNO);
			pstmt.setInt(1, memno);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// quepVo  Domain objects
				queVO = new QueVO();
				queVO.setQuesno(rs.getInt("quesno"));
				queVO.setCouno(rs.getInt("couno"));
				queVO.setMemno(rs.getInt("memno"));
				queVO.setQuesasktime(rs.getTimestamp("quesasktime"));
				queVO.setQuesaskcon(rs.getString("quesaskcon"));
				queVO.setQuesrestime(rs.getTimestamp("quesrestime"));
				queVO.setQuesrescon(rs.getString("quesrescon"));
				set.add(queVO);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}
	
}