package com.que.model;
import java.sql.Date;
import java.sql.Timestamp;

public class QueVO implements java.io.Serializable{
	private Integer quesno;
	private Integer couno;
	private Integer memno;
	private Timestamp quesasktime;
	private String quesaskcon;
	private Timestamp quesrestime;
	private String quesrescon;
	public Integer getQuesno() {
		return quesno;
	}
	public void setQuesno(Integer quesno) {
		this.quesno = quesno;
	}
	public Integer getCouno() {
		return couno;
	}
	public void setCouno(Integer couno) {
		this.couno = couno;
	}
	public Integer getMemno() {
		return memno;
	}
	public void setMemno(Integer memno) {
		this.memno = memno;
	}
	public Timestamp getQuesasktime() {
		return quesasktime;
	}
	public void setQuesasktime(Timestamp quesasktime) {
		this.quesasktime = quesasktime;
	}
	public String getQuesaskcon() {
		return quesaskcon;
	}
	public void setQuesaskcon(String quesaskcon) {
		this.quesaskcon = quesaskcon;
	}
	public Timestamp getQuesrestime() {
		return quesrestime;
	}
	public void setQuesrestime(Timestamp quesrestime) {
		this.quesrestime = quesrestime;
	}
	public String getQuesrescon() {
		return quesrescon;
	}
	public void setQuesrescon(String quesrescon) {
		this.quesrescon = quesrescon;
	}
	
	
}
