package com.que.model;

import java.util.*;
import java.sql.*;

public class QueJDBCDAO implements QueDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "user2";
	String passwd = "u222";

	private static final String INSERT_STMT = 
		"INSERT INTO question (quesno,couno,memno,quesasktime,quesaskcon,quesrestime,quesrescon) VALUES (qus_seq.NEXTVAL, ?, ?, ?, ?, ?, ?)";
	private static final String GET_ALL_STMT = 
		"SELECT quesno,couno,memno,quesasktime, quesaskcon, quesrestime,quesrescon FROM question order by quesno";
	private static final String GET_ONE_STMT = 
		"SELECT quesno,couno,memno, quesasktime, quesaskcon, quesrestime,quesrescon FROM question where quesno = ?";
	private static final String DELETE = 
		"DELETE FROM question where quesno = ?";
	private static final String UPDATE = 
		"UPDATE question set couno=?,memno=?, quesasktime=?, quesaskcon=?,quesrestime=?,quesrescon=? where quesno = ?";
	private static final String GET_QUESTION_BY_COUNO = 
			"select * from question where couno = ?";
	
	private static final String GET_QUESTION_BY_MEMNO = 
			"select * from question where memno = ?";
	@Override
	public void insert(QueVO queVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setInt(1, queVO.getCouno());
			pstmt.setInt(2, queVO.getMemno());
			pstmt.setTimestamp(3, queVO.getQuesasktime());
			pstmt.setString(4, queVO.getQuesaskcon());
			pstmt.setTimestamp(5, queVO.getQuesrestime());
			pstmt.setString(6, queVO.getQuesrescon());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(QueVO queVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setInt(1, queVO.getCouno());
			pstmt.setInt(2, queVO.getMemno());
			pstmt.setTimestamp(3, queVO.getQuesasktime());
			pstmt.setString(4, queVO.getQuesaskcon());
			pstmt.setTimestamp(5, queVO.getQuesrestime());
			pstmt.setString(6, queVO.getQuesrescon());
			pstmt.setInt(7, queVO.getQuesno());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(Integer quesno) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, quesno);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public QueVO findByPrimaryKey(Integer quesno) {

		QueVO queVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, quesno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				queVO = new QueVO();
				queVO.setQuesno(rs.getInt("quesno"));
				queVO.setCouno(rs.getInt("couno"));
				queVO.setMemno(rs.getInt("memno"));
				queVO.setQuesasktime(rs.getTimestamp("quesasktime"));
				queVO.setQuesaskcon(rs.getString("quesaskcon"));
				queVO.setQuesrestime(rs.getTimestamp("quesrestime"));
				queVO.setQuesrescon(rs.getString("quesrescon"));
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return queVO;
	}

	@Override
	public List<QueVO> getAll() {
		List<QueVO> list = new ArrayList<QueVO>();
		QueVO queVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
			
				queVO = new QueVO();
				queVO.setQuesno(rs.getInt("quesno"));
				queVO.setCouno(rs.getInt("couno"));
				queVO.setMemno(rs.getInt("memno"));
				queVO.setQuesasktime(rs.getTimestamp("quesasktime"));
				queVO.setQuesaskcon(rs.getString("quesaskcon"));
				queVO.setQuesrestime(rs.getTimestamp("quesrestime"));
				queVO.setQuesrescon(rs.getString("quesrescon"));
				list.add(queVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public Set<QueVO> getQueByCouno(Integer couno) {
		Set<QueVO> set = new LinkedHashSet<QueVO>();
		QueVO queVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_QUESTION_BY_COUNO);
			pstmt.setInt(1, couno);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// quepVo  Domain objects
				queVO = new QueVO();
				queVO.setQuesno(rs.getInt("quesno"));
				queVO.setCouno(rs.getInt("couno"));
				queVO.setMemno(rs.getInt("memno"));
				queVO.setQuesasktime(rs.getTimestamp("quesasktime"));
				queVO.setQuesaskcon(rs.getString("quesaskcon"));
				queVO.setQuesrestime(rs.getTimestamp("quesrestime"));
				queVO.setQuesrescon(rs.getString("quesrescon"));
				set.add(queVO);
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}

	
	@Override
	public Set<QueVO> getQueByMemno(Integer memno) {
		Set<QueVO> set = new LinkedHashSet<QueVO>();
		QueVO queVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_QUESTION_BY_MEMNO);
			pstmt.setInt(1, memno);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// quepVo  Domain objects
				queVO = new QueVO();
				queVO.setQuesno(rs.getInt("quesno"));
				queVO.setCouno(rs.getInt("couno"));
				queVO.setMemno(rs.getInt("memno"));
				queVO.setQuesasktime(rs.getTimestamp("quesasktime"));
				queVO.setQuesaskcon(rs.getString("quesaskcon"));
				queVO.setQuesrestime(rs.getTimestamp("quesrestime"));
				queVO.setQuesrescon(rs.getString("quesrescon"));
				set.add(queVO);
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}

	public static void main(String[] args) {

		QueJDBCDAO dao = new QueJDBCDAO();

		
//		QueVO queVO1 = new QueVO();
//		queVO1.setCouno(10001);
//		queVO1.setMemno(90001);
//		queVO1.setQuesasktime(java.sql.Date.valueOf("2014-10-31"));
//		queVO1.setQuesaskcon("我有問題?");
//		queVO1.setQuesrestime(java.sql.Date.valueOf("2005-10-31"));
//		queVO1.setQuesrescon("我有問題?");
//		dao.insert(queVO1);
//
//		
//		QueVO queVO2 = new QueVO();
//		queVO1.setCouno(10001);
//		queVO1.setMemno(90001);
//		queVO1.setQuesasktime(java.sql.Date.valueOf("2014-10-31"));
//		queVO1.setQuesaskcon("我有問題?");
//		queVO1.setQuesrestime(java.sql.Date.valueOf("2005-10-31"));
//		queVO1.setQuesrescon("我有問題?");
//		dao.update(queVO2);
//
//		
//		dao.delete(130002);

		
		QueVO queVO3 = dao.findByPrimaryKey(130001);
		System.out.print(queVO3.getQuesno() + ",");
		System.out.print(queVO3.getCouno() + ",");
		System.out.print(queVO3.getMemno() + ",");
		System.out.print(queVO3.getQuesasktime() + ",");
		System.out.print(queVO3.getQuesaskcon() + ",");
		System.out.print(queVO3.getQuesrestime() + ",");
		System.out.println(queVO3.getQuesrescon());
		System.out.println("---------------------");

		
		List<QueVO> list = dao.getAll();
		for (QueVO aQue : list) {
			System.out.print(aQue.getQuesno() + ",");
			System.out.print(aQue.getCouno() + ",");
			System.out.print(aQue.getMemno() + ",");
			System.out.print(aQue.getQuesasktime() + ",");
			System.out.print(aQue.getQuesaskcon() + ",");
			System.out.print(aQue.getQuesrestime() + ",");
			System.out.print(aQue.getQuesrescon());
			System.out.println();
		
		}
		System.out.println("---------------------");
		
		Set<QueVO> queVOset = dao.getQueByCouno(10001);
		for (QueVO queVOset1 : queVOset) {
			System.out.print(queVOset1.getQuesno() + ",");
			System.out.print(queVOset1.getCouno() + ",");
			System.out.print(queVOset1.getMemno() + ",");
			System.out.print(queVOset1.getQuesasktime() + ",");
			System.out.print(queVOset1.getQuesaskcon() + ",");
			System.out.print(queVOset1.getQuesrestime() + ",");
			System.out.print(queVOset1.getQuesrescon());
			System.out.println();
		}
	}
}