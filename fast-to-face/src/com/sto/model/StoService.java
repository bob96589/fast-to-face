package com.sto.model;

import java.sql.Timestamp;
import java.util.List;

public class StoService {

	private StoDAO_interface dao;

	public StoService() {
		dao = new StoDAO();
	}

	public StoVO addSto(String storeranno,Integer storeprice,Integer memno) {

		StoVO stoVO = new StoVO();

		stoVO.setStoreranno(storeranno);
		stoVO.setStoreprice(storeprice);
		stoVO.setMemno(memno);
		dao.insert(stoVO);

		return stoVO;
	}

	public StoVO updateSto(Integer storeno,String storeranno,Timestamp storetime,Integer storeprice,Integer memno) {

		StoVO stoVO = new StoVO();

		stoVO.setStoreno(storeno);
		stoVO.setStoreranno(storeranno);
		stoVO.setStoreprice(storeprice);
		stoVO.setMemno(memno);
		dao.update(stoVO);

		return stoVO;
	}

	public void deleteSto(Integer storeno) {
		dao.delete(storeno);
	}

	public StoVO getOneSto(Integer storeno) {
		return dao.findByPrimaryKey(storeno);
	}

	public List<StoVO> getAll() {
		return dao.getAll();
	}
	public List<StoVO> getRange(java.sql.Timestamp d1,java.sql.Timestamp d2){
		return dao.getRange(d1,d2);
	}
	//bob
	public List<StoVO> getRangeByMemno(Timestamp d1, Timestamp d2, Integer memno){
		return dao.getRangeByMemno(d1,d2,memno);
	}
}
