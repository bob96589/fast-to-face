package com.sto.model;

import java.util.*;
import java.util.Date;
import java.sql.*;

public class StoJDBCDAO implements StoDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "user1";
	String passwd = "u111";

	private static final String INSERT_STMT = 
			"INSERT INTO StoreRecord (storeno,storeranno,storetime,storeprice,memno) VALUES (Sto_seq.NEXTVAL, ?, ?, ?, ?)";
		private static final String GET_ALL_STMT = 
			"SELECT storeno,storeranno,storetime,storeprice,memno FROM StoreRecord order by storeno";
		private static final String GET_ONE_STMT = 
			"SELECT storeno,storeranno,storetime,storeprice,memno FROM StoreRecord where storeno = ?";
		private static final String DELETE = 
			"DELETE FROM StoreRecord where storeno = ?";
		private static final String UPDATE = 
			"UPDATE StoreRecord set storeranno=?, storetime=?, storeprice=?, memno=? where storeno = ?";
		private static final String GET_RANGE_STMT = 
				"SELECT * FROM StoreRecord where storetime between ? and ?";
		//bob
		private static final String GET_RANGE_BY_MEMNO_STMT = 
				"SELECT * FROM StoreRecord where storetime >= ? and storetime <= ? and memno = ? order by storetime desc";

	@Override
	public int insert(StoVO stoVO) {
		int updateCount = 0;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, stoVO.getStoreranno());
			pstmt.setTimestamp(2, stoVO.getStoretime());
			pstmt.setInt(3, stoVO.getStoreprice());
			pstmt.setInt(4, stoVO.getMemno());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return updateCount;
	}

	@Override
	public int update(StoVO stoVO) {
		int updateCount = 0;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setString(1, stoVO.getStoreranno());
			pstmt.setTimestamp(2, stoVO.getStoretime());
			pstmt.setInt(3, stoVO.getStoreprice());
			pstmt.setInt(4, stoVO.getMemno());
			pstmt.setInt(5, stoVO.getStoreno());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return updateCount;
	}

	@Override
	public int delete(Integer storeno) {
		int updateCount = 0;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setInt(1, storeno);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return updateCount;
	}

	@Override
	public StoVO findByPrimaryKey(Integer storeno) {

		StoVO stoVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setInt(1, storeno);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVo 也稱為 Domain objects
				stoVO = new StoVO();
				stoVO.setStoreno(rs.getInt("storeno"));
				stoVO.setStoreranno(rs.getString("storeranno"));
				stoVO.setStoretime(rs.getTimestamp("storetime"));
				stoVO.setStoreprice(rs.getInt("storeprice"));
				stoVO.setMemno(rs.getInt("memno"));
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return stoVO;
	}

	@Override
	public List<StoVO> getAll() {
		List<StoVO> list = new ArrayList<StoVO>();
		StoVO stoVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVO 也稱為 Domain objects
				stoVO = new StoVO();
				stoVO.setStoreno(rs.getInt("storeno"));
				stoVO.setStoreranno(rs.getString("storeranno"));
				stoVO.setStoretime(rs.getTimestamp("storetime"));
				stoVO.setStoreprice(rs.getInt("storeprice"));
				stoVO.setMemno(rs.getInt("memno"));
				list.add(stoVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<StoVO> getRange(java.sql.Timestamp d1,java.sql.Timestamp d2) {
		List<StoVO> list = new ArrayList<StoVO>();
		StoVO stoVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_RANGE_STMT);
			pstmt.setTimestamp(1,d1);
			pstmt.setTimestamp(2,d2);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				stoVO = new StoVO();
				stoVO.setStoreno(rs.getInt("storeno"));
				stoVO.setStoreranno(rs.getString("storeranno"));
				stoVO.setStoretime(rs.getTimestamp("storetime"));
				stoVO.setStoreprice(rs.getInt("storeprice"));
				stoVO.setMemno(rs.getInt("memno"));
				list.add(stoVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	
	//bob
	@Override
	public List<StoVO> getRangeByMemno(Timestamp d1, Timestamp d2, Integer memno) {
		List<StoVO> list = new ArrayList<StoVO>();
		StoVO stoVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_RANGE_BY_MEMNO_STMT);
			pstmt.setTimestamp(1,d1);
			pstmt.setTimestamp(2,d2);
			pstmt.setInt(3,memno);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				stoVO = new StoVO();
				stoVO.setStoreno(rs.getInt("storeno"));
				stoVO.setStoreranno(rs.getString("storeranno"));
				stoVO.setStoretime(rs.getTimestamp("storetime"));
				stoVO.setStoreprice(rs.getInt("storeprice"));
				stoVO.setMemno(rs.getInt("memno"));
				list.add(stoVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	public static void main(String[] args) {

		StoJDBCDAO dao = new StoJDBCDAO();

//		// 新增
//		StoVO stoVO1 = new StoVO();
//		stoVO1.setStoreranno("FTF0000062");
//		stoVO1.setStoretime(java.sql.Timestamp.valueOf("2014-11-01 10:22:48"));
//		stoVO1.setStoreprice(300);
//		stoVO1.setMemno(90001);
//		dao.insert(stoVO1);
//
//		// 修改
//		StoVO stoVO2 = new StoVO();
//		stoVO2.setStoreno(190001);
//		stoVO2.setStoreranno("FTF0000002");
//		stoVO2.setStoretime(java.sql.Timestamp.valueOf("2014-11-01 10:22:48"));
//		stoVO2.setStoreprice(3000);
//		stoVO2.setMemno(90002);
//		dao.update(stoVO2);
//
//		// 刪除
//		dao.delete(190004);
//
//		// 查詢
//		StoVO stoVO3 = dao.findByPrimaryKey(190001);
//		System.out.print(stoVO3.getStoreno() + ",");
//		System.out.print(stoVO3.getStoreranno() + ",");
//		System.out.print(stoVO3.getStoretime() + ",");
//		System.out.print(stoVO3.getStoreprice() + ",");
//		System.out.print(stoVO3.getMemno() + ",");
//		System.out.println("---------------------");

		// 查詢
//		List<StoVO> list = dao.getAll();
//		for (StoVO aSto : list) {
//			System.out.print(aSto.getStoreno() + ",");
//			System.out.print(aSto.getStoreranno() + ",");
//			System.out.print(aSto.getStoretime() + ",");
//			System.out.print(aSto.getStoreprice() + ",");
//			System.out.print(aSto.getMemno() + ",");
//			System.out.println();
//		}
		
//		List<StoVO> list = dao.getRange(java.sql.Timestamp.valueOf("2014-07-11 18:17:18.0"),java.sql.Timestamp.valueOf("2014-10-01 18:19:48.0"));
//		for (StoVO aSto : list) {
//			System.out.print(aSto.getStoreno() + ",");
//			System.out.print(aSto.getStoreranno() + ",");
//			System.out.print(aSto.getStoretime() + ",");
//			System.out.print(aSto.getStoreprice() + ",");
//			System.out.print(aSto.getMemno() + ",");
//			System.out.println();
//		}
		
		
		
		List<StoVO> list = dao.getRangeByMemno(java.sql.Timestamp.valueOf("2014-07-11 18:17:18.0"),java.sql.Timestamp.valueOf("2014-10-01 18:19:48.0"),90003);
		for (StoVO aSto : list) {
			System.out.print(aSto.getStoreno() + ",");
			System.out.print(aSto.getStoreranno() + ",");
			System.out.print(aSto.getStoretime() + ",");
			System.out.print(aSto.getStoreprice() + ",");
			System.out.print(aSto.getMemno() + ",");
			System.out.println();
		}
		
		
	}

	
}