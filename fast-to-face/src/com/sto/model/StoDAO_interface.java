package com.sto.model;

import java.util.*;

public interface StoDAO_interface {
	public int insert(StoVO stoVO);
	public int update(StoVO stoVO);
	public int delete(Integer stono);
	public StoVO findByPrimaryKey(Integer stono);
	public List<StoVO> getAll();
	public List<StoVO> getRange(java.sql.Timestamp d1,java.sql.Timestamp d2);
	public List<StoVO> getRangeByMemno(java.sql.Timestamp d1,java.sql.Timestamp d2, Integer memno);
//	public List<StoVO> getWeek();
//	public List<StoVO> getMonth();
//	public List<StoVO> getSeason();
}
