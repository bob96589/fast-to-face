package com.sto.model;

import java.util.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class StoDAO  implements StoDAO_interface{
	private static DataSource ds = null;
	static{
		try{
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/TestDB");
		} catch (NamingException e){
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = 
			"INSERT INTO StoreRecord (storeno,storeranno,storetime,storeprice,memno) VALUES (Sto_seq.NEXTVAL, ?, sysdate, ?,?)";
	private static final String GET_ALL_STMT = 
		"SELECT storeno,storeranno,storetime,storeprice,memno FROM StoreRecord order by storeno";
	private static final String GET_ONE_STMT = 
		"SELECT storeno,storeranno,storetime,storeprice,memno FROM StoreRecord where storeno = ?";
	private static final String DELETE = 
		"DELETE FROM StoreRecord where storeno = ?";
	private static final String UPDATE = 
		"UPDATE StoreRecord set storeranno=?, storetime=?, storeprice=?, memno=? where storeno = ?";
	private static final String GET_RANGE_STMT = 
		"SELECT * FROM StoreRecord where storetime between ? and ?";
	//bob
	private static final String GET_RANGE_BY_MEMNO_STMT = 
			"SELECT * FROM StoreRecord where storetime >= ? and storetime <= ? and memno = ? order by storetime desc";

	@Override
	public int insert(StoVO stoVO) {
		int updateCount = 0;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);	
						
			pstmt.setString(1,stoVO.getStoreranno());
			pstmt.setInt(2, stoVO.getStoreprice());
			pstmt.setInt(3, stoVO.getMemno());

			updateCount = pstmt.executeUpdate();

			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return updateCount;
	}

	@Override
	public int update(StoVO stoVO) {
		int updateCount = 0;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, stoVO.getStoreranno());
			pstmt.setTimestamp(2, stoVO.getStoretime());
			pstmt.setInt(3, stoVO.getStoreprice());
			pstmt.setInt(4, stoVO.getMemno());
			pstmt.setInt(5, stoVO.getStoreno());

			updateCount = pstmt.executeUpdate();

			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return updateCount;
	}

	@Override
	public int delete(Integer storeno) {
		int updateCount = 0;
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);
			
			pstmt.setInt(1, storeno);
			
			updateCount = pstmt.executeUpdate();

			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return updateCount;
	}

	@Override
	public StoVO findByPrimaryKey(Integer storeno) {

		StoVO stoVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setInt(1, storeno);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// advVo  也稱為Domain objects
				stoVO = new StoVO();
				stoVO.setStoreno(rs.getInt("storeno"));
				stoVO.setStoreranno(rs.getString("storeranno"));
				stoVO.setStoretime(rs.getTimestamp("storetime"));
				stoVO.setStoreprice(rs.getInt("storeprice"));
				stoVO.setMemno(rs.getInt("memno"));
				
			}

			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return stoVO;
	}

	@Override
	public List<StoVO> getAll() {
		List<StoVO> list = new ArrayList<StoVO>();
		StoVO stoVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// stoVO  也稱為Domain objects
				stoVO = new StoVO();
				stoVO.setStoreno(rs.getInt("storeno"));
				stoVO.setStoreranno(rs.getString("storeranno"));
				stoVO.setStoretime(rs.getTimestamp("storetime"));
				stoVO.setStoreprice(rs.getInt("storeprice"));
				stoVO.setMemno(rs.getInt("memno"));
				list.add(stoVO);// Store the row in the vector
			}

			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	@Override
	public List<StoVO> getRange(java.sql.Timestamp d1, java.sql.Timestamp d2) {
		List<StoVO> list = new ArrayList<StoVO>();
		StoVO stoVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_RANGE_STMT);
			pstmt.setTimestamp(1, d1);
			pstmt.setTimestamp(2, d2);			
			rs = pstmt.executeQuery();
			
			
			while (rs.next()) {
				// stoVO  也稱為Domain objects
				stoVO = new StoVO();
				stoVO.setStoreno(rs.getInt("storeno"));
				stoVO.setStoreranno(rs.getString("storeranno"));
				stoVO.setStoretime(rs.getTimestamp("storetime"));
				stoVO.setStoreprice(rs.getInt("storeprice"));
				stoVO.setMemno(rs.getInt("memno"));
				list.add(stoVO); // Store the row in the vector
			}

			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	//bob
	@Override
	public List<StoVO> getRangeByMemno(Timestamp d1, Timestamp d2, Integer memno) {
		List<StoVO> list = new ArrayList<StoVO>();
		StoVO stoVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_RANGE_BY_MEMNO_STMT);
			pstmt.setTimestamp(1,d1);
			pstmt.setTimestamp(2,d2);
			pstmt.setInt(3,memno);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				stoVO = new StoVO();
				stoVO.setStoreno(rs.getInt("storeno"));
				stoVO.setStoreranno(rs.getString("storeranno"));
				stoVO.setStoretime(rs.getTimestamp("storetime"));
				stoVO.setStoreprice(rs.getInt("storeprice"));
				stoVO.setMemno(rs.getInt("memno"));
				list.add(stoVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	
	
	
	
//	@Override
//	public List<StoVO> getWeek() {
//		List<StoVO> list = new ArrayList<StoVO>();
//		StoVO stoVO = null;
//
//		Connection con = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		try {
//
//			con = ds.getConnection();
//			pstmt = con.prepareStatement(GET_ALL_STMT);
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//				// stoVO  也稱為Domain objects
//				stoVO = new StoVO();
//				stoVO.setStoreno(rs.getInt("storeno"));
//				stoVO.setStoreranno(rs.getString("storeranno"));
//				stoVO.setStoretime(rs.getTimestamp("storetime"));
//				stoVO.setStoreprice(rs.getInt("storeprice"));
//				list.add(stoVO); // Store the row in the vector
//			}
//
//			// Handle any SQL errors
//		} catch (SQLException se) {
//			throw new RuntimeException("A database error occured. "
//					+ se.getMessage());
//			// Clean up JDBC resources
//		} finally {
//			if (rs != null) {
//				try {
//					rs.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (pstmt != null) {
//				try {
//					pstmt.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (con != null) {
//				try {
//					con.close();
//				} catch (Exception e) {
//					e.printStackTrace(System.err);
//				}
//			}
//		}
//		return list;
//	}
//	@Override
//	public List<StoVO> getMonth() {
//		List<StoVO> list = new ArrayList<StoVO>();
//		StoVO stoVO = null;
//
//		Connection con = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		try {
//
//			con = ds.getConnection();
//			pstmt = con.prepareStatement(GET_ALL_STMT);
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//				// stoVO  也稱為Domain objects
//				stoVO = new StoVO();
//				stoVO.setStoreno(rs.getInt("storeno"));
//				stoVO.setStoreranno(rs.getString("storeranno"));
//				stoVO.setStoretime(rs.getTimestamp("storetime"));
//				stoVO.setStoreprice(rs.getInt("storeprice"));
//				list.add(stoVO); // Store the row in the vector
//			}
//
//			// Handle any SQL errors
//		} catch (SQLException se) {
//			throw new RuntimeException("A database error occured. "
//					+ se.getMessage());
//			// Clean up JDBC resources
//		} finally {
//			if (rs != null) {
//				try {
//					rs.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (pstmt != null) {
//				try {
//					pstmt.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (con != null) {
//				try {
//					con.close();
//				} catch (Exception e) {
//					e.printStackTrace(System.err);
//				}
//			}
//		}
//		return list;
//	}
//	@Override
//	public List<StoVO> getSeason() {
//		List<StoVO> list = new ArrayList<StoVO>();
//		StoVO stoVO = null;
//
//		Connection con = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		try {
//
//			con = ds.getConnection();
//			pstmt = con.prepareStatement(GET_ALL_STMT);
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//				// stoVO  也稱為Domain objects
//				stoVO = new StoVO();
//				stoVO.setStoreno(rs.getInt("storeno"));
//				stoVO.setStoreranno(rs.getString("storeranno"));
//				stoVO.setStoretime(rs.getTimestamp("storetime"));
//				stoVO.setStoreprice(rs.getInt("storeprice"));
//				list.add(stoVO); // Store the row in the vector
//			}
//
//			// Handle any SQL errors
//		} catch (SQLException se) {
//			throw new RuntimeException("A database error occured. "
//					+ se.getMessage());
//			// Clean up JDBC resources
//		} finally {
//			if (rs != null) {
//				try {
//					rs.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (pstmt != null) {
//				try {
//					pstmt.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (con != null) {
//				try {
//					con.close();
//				} catch (Exception e) {
//					e.printStackTrace(System.err);
//				}
//			}
//		}
//		return list;
//	}
}
