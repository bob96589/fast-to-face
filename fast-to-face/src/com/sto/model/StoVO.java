package com.sto.model;

import java.sql.*;

public class StoVO implements java.io.Serializable{
	private Integer storeno;
	private String storeranno;
	private Timestamp storetime;
	private Integer storeprice;
	private Integer memno;
	public Integer getStoreno() {
		return storeno;
	}
	public void setStoreno(Integer storeno) {
		this.storeno = storeno;
	}
	public String getStoreranno() {
		return storeranno;
	}
	public void setStoreranno(String storeranno) {
		this.storeranno = storeranno;
	}
	public Timestamp getStoretime() {
		return storetime;
	}
	public void setStoretime(Timestamp storetime) {
		this.storetime = storetime;
	}
	public Integer getStoreprice() {
		return storeprice;
	}
	public void setStoreprice(Integer storeprice) {
		this.storeprice = storeprice;
	}
	public Integer getMemno() {
		return memno;
	}
	public void setMemno(Integer memno) {
		this.memno = memno;
	}
	
	
}
