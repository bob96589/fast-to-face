package com.sto.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mem.model.MemService;
import com.mem.model.MemVO;
import com.sto.model.StoService;
import com.sto.model.StoVO;
public class StoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		if ("getStoList".equals(action)) {
						
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String whichPage = req.getParameter("whichPage");			
			HttpSession session= req.getSession();
			MemVO memVO = (MemVO)session.getAttribute("memVO");
			
			try {
				/*************************** 1.接收請求參數 - 輸入格式的錯誤處理 **********************/
								
				
				String formStr = req.getParameter("from");
				String toStr = req.getParameter("to");
				
				if(formStr == null || (formStr.trim()).length() == 0){
					
					//getAll
					MemService memSvc = new MemService();
					Set<StoVO> stoVOSet = memSvc.getStosByMemno(memVO.getMemno());					
					req.setAttribute("stoVOSet", stoVOSet);
					
				}else{
					//getPart
					
					
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");					
					//from
					Timestamp from = null;					
					try {
						
						Date fromDate = formatter.parse(formStr);
						from = new Timestamp(fromDate.getTime());
					} catch (Exception e) {
						errorMsgs.add("日期格式轉換錯誤");
					}					
					//to
					Timestamp to = null;					
					try {
						Date toDate = formatter.parse(toStr);
						to = new Timestamp(toDate.getTime());
					} catch (Exception e) {
						errorMsgs.add("日期格式轉換錯誤");
					}
					
					StoService stoSvc = new StoService();
					List<StoVO> stoVOList = stoSvc.getRangeByMemno(from, to, memVO.getMemno());
					Set<StoVO> stoVOSet = new LinkedHashSet<StoVO>(stoVOList);
					req.setAttribute("stoVOSet", stoVOSet);					
					
				}
							
				String url = "/front/store_record/listStoByMemno.jsp";
				req.setAttribute("formStr", formStr);
				req.setAttribute("toStr", toStr);
				req.setAttribute("whichPage", whichPage);
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				/*************************** 其他可能的錯誤處理 *************************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front/sto/listStoByMemno.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		
		if("Store_Point".equals(action)){
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
				
			HttpSession session= req.getSession();
			MemVO memVO = (MemVO)session.getAttribute("memVO");
			
			try {
				/*************************** 1.接收請求參數 - 輸入格式的錯誤處理 **********************/
				String storeranno = req.getParameter("storeranno");
				Integer storeprice = new Integer(req.getParameter("storeprice"));	
				Integer memno = new Integer(req.getParameter("memno"));
				StoVO stoVO = new StoVO();
				stoVO.setStoreranno(storeranno);
				stoVO.setStoreprice(storeprice);
				stoVO.setMemno(memno);
				
				/*************************** 2.開始新增資料 *****************************************/
				//add store record
				StoService stoSvc = new StoService();
				stoVO = stoSvc.addSto(storeranno,storeprice,memno);
				//add money
				MemService memSvc = new MemService();
				memSvc.updateMempointByMemno(memVO.getMempoint() + storeprice, memVO.getMemno());
				DateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH點mm分ss秒");
				Date date = new Date();
				String time = dateFormat.format(date);
				Integer money = stoVO.getStoreprice();
				String member = memVO.getMemname();
				if(member == null){
					member = memVO.getMemaccount();
				}
			 	String[] tel ={"0985604253"};
			 	String message = "親愛的"+member+"您好，Fast to Face教學平台於"+time+"恭喜您儲值點數"+money+"點成功，祝福您學習愉快!";
			 	Send se = new Send();
			 	se.sendMessage(tel , message);
				 
				/*************************** 其他可能的錯誤處理 *************************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front/store_record/listStoByMemno.jsp");
				failureView.forward(req, res);
			}
		
		}
		
	}

}
