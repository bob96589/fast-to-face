<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<title>Insert title here</title>
		
		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/my_course/css/myCourse.css">
		<script>$(function(){$(".customized_tab #first").addClass("active");});	</script>
	</head>
	<body>
		<%@ include file="/shared/pages/front_header_nav.file" %>
		<%@ include file="/shared/pages/myCourse_top.file" %>
		<h3 class="myaccount_title">�ڪ��ҵ{</h3>
		<table  class="table table-hover">
			<c:forEach var="couVO" items="${couVOList}" varStatus="couStatus">
				<tr>
					<td>
						<img src="<%=request.getContextPath()%>/cou/cou.do?couno=${couVO.couno}&action=getCoupic" class="course_img" />
					</td>
					<td>
					<table id="inner_table">
						<tr>
							<td colspan="2">
								<div class="center_title">
									<a href="<%=request.getContextPath()%>/couDetail/cou.do?couno=${couVO.couno}&action=getCouDetail">${couVO.couname}</a>
								</div>
							</td>
						</tr>
						<tr height="50">
							<td colspan="2">
								<div class="center_desc">${couVO.couintro}</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="progress progress_bar">
    								<div class="progress-bar progress-bar-custom" role="progressbar" aria-valuenow="${completePercentageList[couStatus.index]}" aria-valuemin="0" aria-valuemax="100" style="width:${completePercentageList[couStatus.index]}%;">
        								
								    </div>
								    
								</div>
							</td>
							<td><span class="center_desc">&nbsp;${completePercentageList[couStatus.index]}%</span></td>
						</tr>
					</table>						
					</td>				
				</tr>
			</c:forEach>
		</table>
		
		
		
		<%@ include file="/shared/pages/myCourse_bottom.file" %>
		<%@ include file="/shared/pages/front_footer.file" %>
	</body>
</html>