<%@page import="com.mem.model.MemVO"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.*"%>
<%@ page import="com.que.model.*"%>
<%@page import="java.util.Set"%>
<%@page import="com.cou.model.*"%>
<%@page import="com.acc.model.*"%>
<%@page import="com.mem.model.*"%>
<%@page import="com.buy.model.*"%>
<%-- 此頁練習採用 EL 的寫法取值 --%>

<%

	MemVO memVO = (MemVO)session.getAttribute("memVO");
	MemService memSvc = new MemService();
	Set<BuyVO> buyVOSet = memSvc.getBuysByMemno(memVO.getMemno());
	pageContext.setAttribute("buyVOSet", buyVOSet);
	System.out.print(buyVOSet);
// 	AccService accSvc = new AccService();
// 	Set<CouVO> couVOSet = accSvc.getCousByAccno(100002);
// 	pageContext.setAttribute("couVOSet", couVOSet);

	CouService couSvc = new CouService();
	List<CouVO> list = couSvc.getAll();
	pageContext.setAttribute("list", list);
	
	QueVO queVO = (QueVO) request.getAttribute("queVO");
	QueService queSvc = new QueService();
	Set<QueVO> queVOSet = queSvc.getQueByMemno(memVO.getMemno());
	pageContext.setAttribute("queVOSet", queVOSet);
%>

<html>
	<head>
		<title>回覆專區</title>
		
		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">

		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/my_course/css/jquery-ui.css">
		<script>$(function(){$(".customized_tab #second").addClass("active");});	</script>
		<script>
			$(function() {
				$("#accordion").accordion();
			});
		</script>
		<style>
		*{
			font-family:Microsoft JhengHei;
		}
		</style>



	</head>
	<body bgcolor='white'>
		<%@ include file="/shared/pages/front_header_nav.file"%>
		<%@ include file="/shared/pages/myCourse_top.file"%>
		<h3 class="myaccount_title">發問紀錄</h3>
		

		<%-- 錯誤表列 --%>
		<c:if test="${not empty errorMsgs}">
			<font color='red'>請修正以下錯誤:
				<ul>
					<c:forEach var="message" items="${errorMsgs}">
						<li>${message}</li>
					</c:forEach>
				</ul>
			</font>
		</c:if>


	 </br>
	
		<div id="accordion" style="text-align:center;">
			
			<c:forEach var="couVO" items="${list}">
			
			<c:forEach var="buyVO" items="${buyVOSet}">
			<c:if test="${couVO.couno==buyVO.couno}">
			<h3 style="width:100%; background-color:rgb(225,237,233);color:#333; font-family: Microsoft JhengHei;">${couVO.couname}</h3>
			<div style="width:100%;">
				<p>
					<table class="table table-bordered" style="width:780px; font-size:16px;">
					<tr class="titleBgcolor">
						<th style="text-align:center;width:120px;">發問日期</th>
						<th style="text-align:center;width:270px;">問題</th>
						<th style="text-align:center;width:120px;">回覆日期</th>
						<th style="text-align:center;width:270px;">回覆</th>
						</tr>
						<c:forEach var="queVO" items="${queVOSet}">
						<c:if test="${queVO.couno==couVO.couno}">
					<tr>
						<td style="text-align:center;">
							<fmt:formatDate value="${queVO.quesasktime}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate>
						</td>
						<td style="word-break:break-all">${queVO.quesaskcon}</td>
						<td style="text-align:center;">
							<fmt:formatDate value="${queVO.quesrestime}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate>
						</td>
						<td style="word-break:break-all">${queVO.quesrescon}</td>
					</tr>
						</c:if>
						</c:forEach>
					</table>
				</p>
			</div>
			</c:if>
			</c:forEach>
			
			</c:forEach>
		</div>




	<%@ include file="/shared/pages/myCourse_bottom.file"%>
	<%@ include file="/shared/pages/front_footer.file"%>
</body>
</html>
