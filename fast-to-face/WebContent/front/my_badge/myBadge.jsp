<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<title>Insert title here</title>
		
		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">
		
		<!-- self -->
		<script>$(function(){$(".customized_tab #third").addClass("active");});	</script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/my_badge/css/myBadge.css">
		
	</head>
	<body>
		<%@ include file="/shared/pages/front_header_nav.file" %>
		<%@ include file="/shared/pages/myAccount_top.file" %>
		<h3 class="myaccount_title">�ڪ�����</h3>
		<c:forEach var="couVO" items="${couVOList}" varStatus="couStatus">
<%-- 			<c:if test="${completePercentageList[couStatus.index]==100}"> --%>
<%-- 			<c:if test="${true}"> --%>
				<div id="table_wrapper">
					<table  ${(completePercentageList[couStatus.index]==100)?'':'class="grayscale"'}">
						<tr>
							<td>
								<img src="<%=request.getContextPath()%>/cou/cou.do?couno=${couVO.couno}&action=getCoubadge" height="180"/>
							</td>
						</tr>
						<tr>
							<td class="center_desc">${couVO.couname}</td>
						</tr>
						<tr>
							<td class="hightlight">���V����</td>
						</tr>				
					</table>
				</div>			
<%-- 			</c:if> --%>
		</c:forEach>
		<div style="clear:both;"></div>		
		<%@ include file="/shared/pages/myAccount_bottom.file" %>
		<%@ include file="/shared/pages/front_footer.file" %>
	</body>
</html>