<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<title>Insert title here</title>
		
		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/about_us/css/aboutUs.css">
		
	</head>
	<body>
		<%@ include file="/shared/pages/front_header_nav.file" %>
		<table>
			<tr>
				<td id="line">				
					<img src="<%=request.getContextPath()%>/shared/image/logo.png"/>
				</td>
				<td>
					<div class="center_title">關於Fast to Face</div>
					<div class="center_desc">Fast to Face為資策會所經營的新一代學習服務，我們有30年的培訓經驗，在快速變化的資通訊專業世界裡，您碰到不足的知識或技術，我們嘗試來協助及補給。我們相信透過學習可以改變一個人的價值，Fast to Face希望在您職涯成長的路上貢獻一點力量，透過老師們用心錄製的課程及各式學習活動安排與指引，協助您在專業及技能上學習的更好，在職涯發展路上不斷提升自我價值。</div>
				</td>			
			</tr>	
		</table>	
		<%@ include file="/shared/pages/front_footer.file" %>
	</body>
</html>