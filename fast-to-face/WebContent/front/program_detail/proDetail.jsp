<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<title>Insert title here</title>
		
		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/program_detail/css/proDetail.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/star-rating.css">
		<script src="<%=request.getContextPath()%>/shared/js/star-rating.js"></script>
	</head>
	<body>
		<%@ include file="/shared/pages/front_header_nav.file" %>
			<div id="intro_section">
				<table>
					<tr>
						<td>
							<img src="<%=request.getContextPath()%>/pro/pro.do?prono=${proVO.prono}&action=getPropic" class="program_img"/>
						</td>
						<td>
							<ul id="intro_list">
								<li class="center_title">${proVO.proname}</li>
								<li class="center_desc">${proVO.prointro}</li>
								<c:if test="${!alreadyBought}">
								<li class="center_price">原價：<del>${proVO.proprice}</del>&nbsp;&nbsp;特價：${proVO.prodiscount}</li>
								<li><a href="<%=request.getContextPath()%>/proBuying/pro.do?action=getOneForBuying&prono=${proVO.prono}" class="btn btn-primary"><span class="glyphicon glyphicon-shopping-cart"></span> 購買</a></li>
								</c:if>
								<c:if test="${alreadyBought}">
								<li class="center_price">已購買</li>
								</c:if>
							</ul>
						</td>																		
					</tr>								
				</table>				
			</div>
			<hr/>
			<div id="list_section">	
				<div class="center_header">
					學程所屬課程
				</div>
				<div>						
					<table class="table">
						<c:forEach var="couVO" items="${couVOSet}" varStatus="couStatus">
							<tr>
								<td>
									<img src="<%=request.getContextPath()%>/cou/cou.do?couno=${couVO.couno}&action=getCoupic" class="course_img"/>
								</td>
								<td>
									<div class="center_title"><a href="<%=request.getContextPath()%>/couDetail/cou.do?couno=${couVO.couno}&action=getCouDetail">${couVO.couname}</a></div>
									<div class="center_desc">${couVO.couintro}</div>
									<div>
										<input id="star" class="rating" value="${averageAppscoreList[couStatus.index]}" data-show-clear="false" data-show-caption="false" data-size="xs"  readonly/>
									</div>
								</td>
								<td><c:if test="${!alreadyBought}">
									<div class="center_price">原價：<del>${couVO.couprice}</del></div>
									<div class="center_price">特價：${couVO.coudiscount}</div>
									</c:if>
									<c:if test="${alreadyBought}">
									<div class="center_price">已購買</div>
									</c:if>
								</td>									
							</tr>
						</c:forEach>												
					</table>
				</div>						
			</div>	
		<%@ include file="/shared/pages/front_footer.file" %>
	</body>
</html>