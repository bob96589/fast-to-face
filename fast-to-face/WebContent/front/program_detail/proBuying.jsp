<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
	<head>
		<title>Insert title here</title>
		
		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/program_detail/css/proBuying.css">
		
	</head>
	<body>
		<%@ include file="/shared/pages/front_header_nav.file" %>	
		<div class="center_header">
			確認購買
		</div>
		<div id="item_section" class="center_title">
			<table class="table table-bordered">
				<tr>
					<td>學程</td>
					<td>學程說明</td>					
					<td>原價</td>
					<td>特價</td>									
				</tr>
				<tr>
					<td rowspan="2">
						<img src="<%=request.getContextPath()%>/pro/pro.do?prono=${proVO.prono}&action=getPropic" class="program_img"/>
					</td>
					<td>
						<ul id="intro_list">										
							<li class="center_title">${proVO.proname}</li>
							<li class="center_desc">${proVO.prointro}</li>
						</ul>
					</td>					
					<td rowspan="2">${proVO.proprice}</td>
					<td rowspan="2" class="red">${proVO.prodiscount}</td>								
				</tr>
				<tr>
					<td>
						<ul id="intro_list">										
							<c:forEach var="couVO" items="${couVOSet}">
								<li>
									<img src="<%=request.getContextPath()%>/cou/cou.do?couno=${couVO.couno}&action=getCoupic" class="course_img"/>
									<span>${couVO.couname}</span>
									<c:forEach var="boughtCouVO" items="${boughtCouVO}">
										<c:if test="${couVO.couno == boughtCouVO.couno}">
											<span class="red">(此課程已購買)</span>
										</c:if>
									</c:forEach>
								</li>
							</c:forEach>
						</ul>
					</td>
				</tr>								
			</table>				
		</div>
		<hr/>
	<div>
		<div style="float: left;">
			<table class="borderless">
				<tr>
					<td colspan="2">${(memVO.memname==null)? memVO.memaccount : memVO.memname}，你好</td>
				</tr>
				<tr>
					<td>你的點數尚餘：</td>
					<td class="big">${memVO.mempoint}</td>
				</tr>
				<tr>
					<td>本次消費：</td>
					<td class="red big">${proVO.prodiscount}</td>
				</tr>
				<tr>
					<td>剩餘點數為：</td>
					<td class="big">${memVO.mempoint - proVO.prodiscount}</td>
				</tr>
				<c:if test="${fn:length(boughtCouVO) gt 0}">
					<tr>
						<td colspan="2" class="red">本學程中，有${fn:length(boughtCouVO)}課程已購買，是否確認購買?</td>
					</tr>
				</c:if>
				<tr>
					<td><c:if test="${memVO.mempoint >= proVO.prodiscount}">
							<form method="post"
								action="<%=request.getContextPath()%>/proBuying/pro.do">
								<button class="btn btn-primary"
									onclick="javascript:return alert('購買成功')">
									<span class="glyphicon glyphicon-ok"></span> 確認購買
								</button>
								<input type="hidden" name="action" value="buying" /> <input
									type="hidden" name="prono" value="${proVO.prono}" />
							</form>
						</c:if> <c:if test="${memVO.mempoint < proVO.prodiscount}">
							<form method="post"
								action="<%=request.getContextPath()%>/front/sto/store_member.jsp">
								<button class="btn btn-primary">
									<span class="glyphicon glyphicon-share-alt"></span> 點數不足，請先儲值
								</button>
								<input type="hidden" name="proBuyingQueryString"
									value="${pageContext.request.queryString}" />
							</form>
						</c:if></td>
					<td>
						<form method="post"
							action="<%=request.getContextPath()%>/proDetail/pro.do">
							<button class="btn btn-warning">
								<span class="glyphicon glyphicon-remove"></span> 取消
							</button>
							<input type="hidden" name="action" value="getProDetail" /> <input
								type="hidden" name="prono" value="${proVO.prono}" />
						</form>
					</td>
				</tr>
			</table>
		</div>
		<div style="width: 400px; margin-left: 420px;">
			<div class="well">
				<b>法律小常識:</b><br> 根據《消保法》第19條第1項規定：「郵購買賣之消費者，對所收受之商品不願買受時，<font
				color="red">得於收受商品後7日內</font>，退回商品或以書面通知企業經營者解除買賣契約，無須說明理由及負擔任何費用或價款
				。」，而同法第2條明定：「郵購買賣指『企業經營者』以電視、型錄、<b><u>網際網路</u></b>或其他類似之方法，使消費者未能檢
				視商品而與企業經營者所為之賣。」故只要網路賣家為針對一般消費者型式的「網路商城」，等同於企業經營者，消費者向這類賣家買
				東西時，可享有《消保法》的保障，享有7天鑑賞期！
			</div>
		</div>
	</div>
	<%@ include file="/shared/pages/front_footer.file" %>
	</body>
</html>