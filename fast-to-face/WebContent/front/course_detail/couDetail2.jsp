<%@page import="com.content.model.ConVO"%>
<%@page import="com.unit.model.UnitVO"%>
<%@page import="java.util.Set"%>
<%@page import="com.ass.model.AssVO"%>
<%@page import="com.content.model.*"%>
<%@page import="com.unit.model.*"%>
<%@page import="java.util.List"%>
<%@page import="com.cou.model.*"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<html>
<head>
<title>Insert title here</title>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/front/course_detail/css/jquery-ui.css">
<!-- <script -->
<%-- 	src="<%=request.getContextPath()%>/front/course_detail/js/jquery-1.10.2.js"></script> --%>


<!--  		back shared file -->

<script
	src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js">
	
</script>
<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/shared/css/back.css">
<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>

<!-- self -->
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/front/course_detail/css/style.css">
<script
	src="<%=request.getContextPath()%>/front/course_detail/js/index2.js"></script>

</head>
<body>
	
	<div id="wrapper">
	<div id="header">
		<div id="logo">
			<h2>Fast to Face</h2>
		</div>
			</br>
			<p style="font-size:16px;"><a href="<%=request.getContextPath()%>/couDetail/cou.do?couno=${couVO.couno}&action=getCouDetail"><b>回課程列表</b></a></p>
		</div>
	</div>
	
	<!-- end #menu -->
	<div id="page">
	<div id="page-bgtop">
	<div id="page-bgbtm">
		<div id="content">
			<div class="post">
			<div class="post-bgtop">
			<div class="post-bgbtm">
				<h2 class="title">${conVO.conname}
				
				<c:choose>
				<c:when test="${rightAns==1}">
					<img src="<%=request.getContextPath()%>/front/course_detail/image/pass.png" width="100" height="45"  class="content_img"/>

					</c:when>
				<c:when test="${rightAns==2}">
					<img src="<%=request.getContextPath()%>/front/course_detail/image/Go-2.png" width="40" height="25" z-index="2" class="content_img"/>

					</c:when>
					<c:otherwise>
					</c:otherwise>
					</c:choose>
				</h2>	
				<p class="meta"></p>
				<div class="entry">
				<p>
					<c:if test="${conVO.contype=='影片'||conVO.contype=='影片 '}">
					<input type="hidden" name="conno" value="${conVO.conno}" />
					<input type="hidden" name="contype" value="${conVO.contype}"/>

						
						<div id=video>
							<video id="myVideo" width="920" height="500" controls>
								<source
									src="<%=request.getContextPath()%>/shared/video/${conVO.convideo}"
									type="video/mp4">
							</video>
						</div>
						</c:if>
					
					
					<c:if test="${conVO.contype=='評量'||conVO.contype=='評量 '}">
				<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/con/con.do">
					<c:forEach var="assVO" items="${assVOSet}" varStatus="a">
						<div id="tbcontent">
						<table class="borderless" style="font-size:16px;font-weight:bold;color:#428bca">
							
							<tr>							    
								<th height="10">題目</th>
								<td>${a.count}.${assVO.assques}</td>
							</tr>
							<tr>
								<th></th>
								<td><input type="radio" name="assans${a.count}" value="A">(A)${assVO.assansa}</td>
							</tr>
							<tr>
								<th></th>
								<td><input type="radio" name="assans${a.count}" value="B">(B)${assVO.assansb}</td>
							</tr>
							<tr>
								<th></th>
								<td><input type="radio" name="assans${a.count}" value="C">(C)${assVO.assansc}</td>
							</tr>
							<tr>
								<th></th>
								<td><input type="radio" name="assans${a.count}" value="D">(D)${assVO.assansd}</td>
							</tr>
							
						</table>
						</div>						
						<hr/>
					</c:forEach>
					<div id="btsubmit">
					
					
						<input type="submit" value="提交" onclick="showDiv()" id="openPass">
			    		<input type="hidden" name="conno" value="${conVO.conno}">
			    		<input type="hidden" name="contype" value="${conVO.contype}">
			    		<input type="hidden" name="couno" value="${couVO.couno}">
			   	 		<input type="hidden" name="action"value="getOneAssConvideo">
									   	 	
			   	 	</div>
					</FORM>
				</c:if>
				</p>
				</div>
			</div>
			</div>
			</div>
			
		<div style="clear: both;">&nbsp;</div>
		</div>
		<!-- end #content -->
		<div id="sidebar">
			<ul>
				<li>
					<c:forEach var="unitVO" items="${unitVOSet}" varStatus="unitStatus">
					<h2 style='height:50px'>${unitVO.unitname}</h2>
					<ul>
						<c:forEach var="conVO" items="${conVOSetList[unitStatus.index]}" varStatus="s">
							<li>
								<a href="<%=request.getContextPath()%>/con/con.do?couno=${couVO.couno}&conno=${conVO.conno}&contype=${conVO.contype}&action=getOneAssConvideo">${conVO.conname}</a>
							</li>
						</c:forEach>	
					</ul>
					</c:forEach>
				</li>				
			</ul>
		</div>

		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	</div>
	</div>
	<!-- end #page -->
	
	<!-- end #footer -->
</body>
</html>