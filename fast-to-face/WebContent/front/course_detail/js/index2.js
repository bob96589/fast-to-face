 $(function() {
    $( "#menu" ).menu({
      items: "> :not(.ui-widget-header)"
    });
  });
   $(function() {
    $( "#tabs" ).tabs();
  });
   
  $(function() {
    // run the currently selected effect
    function runEffect() {
      // get effect type from
      var selectedEffect = "slide";
 
      // most effect types need no options passed by default
      var options = {};
      // some effects have required parameters
      if ( selectedEffect === "scale" ) {
        options = { percent: 0 };
      } else if ( selectedEffect === "size" ) {
        options = { to: { width: 200, height: 60 } };
      }
 
      // run the effect
      $( "#effect" ).toggle( selectedEffect, options, 500 );
    };
 
    // set effect from select menu value
    $( ".button" ).click(function() {
	  
    	$("#effect").hide();
	  runEffect();
	  $( ".toggler" ).html($(this).attr("id"));
    });
  });