<%@page import="com.content.model.ConVO"%>
<%@page import="com.unit.model.UnitVO"%>
<%@page import="com.acc.model.AccVO"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.List"%>
<%@page import="com.cou.model.CouVO"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
	<head>
		<title>Insert title here</title>
		
		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/course_detail/css/couDetail.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/star-rating.css">
		<script src="<%=request.getContextPath()%>/shared/js/star-rating.js"></script>
		<script src="<%=request.getContextPath()%>/front/course_detail/js/video30s.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/course_detail/css/video30s.css">
	</head>
	<body>
		<%@ include file="/shared/pages/front_header_nav.file" %>
		<div id="intro_section">
			<table>
				<tr>
					<td>
						<img src="<%=request.getContextPath()%>/cou/cou.do?couno=${couVO.couno}&action=getCoupic" class="course_img"/>
					</td>
					<td>
						<ul id="intro_list">
							<li class="center_title">${couVO.couname}</li>
							<li class="center_desc">${couVO.couintro}</li>
							<c:if test="${(identity=='unpurchased')||(identity=='appraised')}">
								<li>
									<table>
										<tr>
											<td>
												<input id="star" class="rating" value="${averageAppscore}" data-show-clear="false" data-show-caption="true" data-size="sm"  readonly/>
											</td>
											<td class="center_desc">，共${appraiseCount}人評價課程</td>										
										</tr>								
									</table>									
								</li>
							</c:if>
							<c:if test="${identity=='purchased'}">
								<li>
									<form method="post" action="<%=request.getContextPath()%>/couBuying/cou.do">
										<table>
											<tr>
												<td width="190">
													<input name="appscore" class="rating" data-step="0.5" data-show-clear="false" data-size="sm"/>												
												</td>
												<td>
													<input  type="submit" class="btn btn-primary btn-xs" value="給評">
													<input type="hidden" name="action" value="insert"/>
													<input type="hidden" name="reURL" value="${pageContext.request.queryString}">		
													<input type="hidden" name="couno" value="${couVO.couno}"/>		
													<input type="hidden" name="memno" value="${memVO.memno}"/>												
												</td>
											</tr>										
										</table>																	
									</form>									
								</li>
							</c:if>
							<c:if test="${identity=='unpurchased'}">
								<li class="center_price">
									原價：<del>${couVO.couprice}</del>&nbsp;&nbsp;特價：${couVO.coudiscount}
								</li>
								<li>
									<a href="<%=request.getContextPath()%>/couBuying/cou.do?action=getOneForBuying&couno=${couVO.couno}" class="btn btn-primary"><span class="glyphicon glyphicon-shopping-cart"></span> 購買</a>									
								</li>
							</c:if>
							<c:if test="${(identity=='purchased')||(identity=='appraised')}">								
								<li class="center_price">已購買此課程</li>
							</c:if>															
						</ul>
					</td>																		
				</tr>								
			</table>				
		</div>
		<hr/>
		<div id="list_section">	
			<div class="center_header">
				課程內容
			</div>
			<c:forEach var="unitVO" items="${unitVOSet}" varStatus="s">						
				<div class="panel panel-default unit_list">
				<!-- Default panel contents -->
					<div class="panel-heading">單元${s.count}：${unitVO.unitname}</div> 
					<table class="table">
						<c:forEach var="conVO" items="${conVOSetList[s.index]}">
							<tr>
								<td>
								<c:if test="${!empty finVOSet}">
									<c:forEach var="finVO" items="${finVOSet}">
										<c:if test="${finVO.conno==conVO.conno}">
											<c:choose>
												<c:when test="${finVO.isfinish==1}">
													<c:if test="${conVO.contype=='影片'}">
														<img src="<%=request.getContextPath()%>/shared/image/video_complete.png" class="content_img"/>
													</c:if>
													<c:if test="${conVO.contype=='評量'}">
														<img src="<%=request.getContextPath()%>/shared/image/ass_complete.png" class="content_img"/>
													</c:if>
												</c:when>
												<c:otherwise>
													<c:if test="${conVO.contype=='影片'}">
														<img src="<%=request.getContextPath()%>/shared/image/video.png" class="content_img"/>
													</c:if>
													<c:if test="${conVO.contype=='評量'}">
														<img src="<%=request.getContextPath()%>/shared/image/ass.png" class="content_img"/>
													</c:if>
												</c:otherwise>
											</c:choose>
										</c:if>
									</c:forEach>
								</c:if>
								<c:if test="${empty finVOSet}">
									<c:if test="${conVO.contype=='影片'}">
										<img src="<%=request.getContextPath()%>/shared/image/video.png" class="content_img"/>
									</c:if>
									<c:if test="${conVO.contype=='評量'}">
										<img src="<%=request.getContextPath()%>/shared/image/ass.png" class="content_img"/>
									</c:if>
								</c:if>
								</td>
								<td style="font-size:16px;">
								
									<!-- 	易鈞加的								 --> 
									<c:if test="${(identity=='purchased')||(identity=='appraised')}">
										<a href="<%=request.getContextPath()%>/con/con.do?couno=${couVO.couno}&conno=${conVO.conno}&contype=${conVO.contype}&action=getAssConvideo">${conVO.conname}</a>
									</c:if>
									<c:if test="${(identity=='unpurchased')}">
										<c:if test="${conVO.contype=='影片'||conVO.contype=='影片 '}">
											<a>${conVO.conname}</a>
	
											<button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#${conVO.conno}">預覽影片</button>
											<div class="modal fade customized_modal" id="${conVO.conno}" tabindex="-1"
												role="dialog" aria-labelledby="myLargeModalLabel"
												aria-hidden="true">
												<div class="modal-dialog modal-lg">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close exit" data-dismiss="modal">
																<span aria-hidden="true">&times;</span>
																<span class="sr-only">Close</span>
															</button>
															<h4 class="modal-title">${conVO.conno}&nbsp-&nbsp${conVO.contype}&nbsp&nbsp${conVO.conname}</h4>
														</div>
														<div class="modal-body">
															<video class="video30s controller" width="865">
																<source src="<%=request.getContextPath()%>/shared/video/${conVO.convideo}" type="video/mp4">
															</video>
														</div>
													</div>
												</div>
											</div>
										</c:if>
										<c:if test="${conVO.contype=='評量'||conVO.contype=='評量 '}">
											<a>${conVO.conname}</a>
										</c:if>
									</c:if>
								
								
								</td>										
							</tr>							
						</c:forEach> 
					</table>							
				</div>						
			</c:forEach>					
		</div>					
		<div id="teacher_section">
			<ul>
				<li class="center_header">
					老師簡介
				</li>
				<li>
					<img src="<%=request.getContextPath()%>/cou/cou.do?accno=${accVO.accno}&action=getAccpic" class="img-circle teacher_img"/>
				</li>
				<li class="center_desc">${accVO.accname}</li>
				<li class="center_desc">${accVO.accintro}</li>							
				<li>
					<c:if test="${(identity=='purchased')||(identity=='appraised')}">
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">發問問題</button>
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  							<div class="modal-dialog">
    							<div class="modal-content">
      								<div class="modal-header">
        								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        								<h4 class="modal-title" id="myModalLabel">發問問題</h4>
     								</div>
     								<form method="post" action="<%=request.getContextPath()%>/que/que.do" name="form1">
	      								<div class="modal-body">
											<textarea id="quesaskcon" class="form-control" rows="16" placeholder="請輸入你的問題" name="quesaskcon" value="${queVO.quesaskcon}"></textarea>
										</div>
									    <div class="modal-footer">
									    	<input type="hidden" name="action" value="insert">
											<input type="hidden" name="reURL" value="${pageContext.request.queryString}">
											<button type="button" class="btn btn-warning btn-xs" id="magic_button"><span class="glyphicon glyphicon-star"></span></button>
											<input type="submit" class="btn btn-primary" value="送出">
											<input type="hidden" name="couno" value="${couVO.couno}" /> 
								      	</div>
								      	<script>
											$(function(){
												$("#magic_button").click(function(){									
													$("#quesaskcon").val("請問JAVA口訣是什麼?");
												});
											});
										</script>
							      	</form>      
    							</div>
  							</div>
						</div>
						
					</c:if>
				</li>							
			</ul>						
		</div>
		<div style="clear:both;"></div>
		<%@ include file="/shared/pages/front_footer.file" %>
	</body>
</html>