<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.*" %>
<%@ page import="com.pro.model.*" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%
	
	ProService proSvc = new ProService();
	Set<ProVO> proVOSet = proSvc.getProsByProstate(1);
	List<ProVO> list = new ArrayList<ProVO>(proVOSet);
	pageContext.setAttribute("list", list);ProVO proVO =(ProVO)request.getAttribute("proVO");
%>
<%
	response.setHeader("Cache-Control","no-store"); //HTTP 1.1
	response.setHeader("Pragma","no-cache");        //HTTP 1.0
	response.setDateHeader ("Expires", 0);
%>
<!DOCTYPE html>
<html>
 	<head>
		<title>流覽學程列表</title>	
		
		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/program_list/css/program_list.css"> 					
	</head>
	<body>
		<%@ include file="/shared/pages/front_header_nav.file"%>
		
					<div class="center_header">學程列表(<%= list.size() %>)</div>					
					<div id="list_section">						
						<div>
							<%@ include file="pages/page1.file"%>									
							<!-- Table -->
							<table class="table table-hover">
								<c:forEach var="proVO" items="${list}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
								<tr>
									<td>
										<a href="<%=request.getContextPath()%>/proDetail/pro.do?prono=${proVO.prono}&action=getProDetail">
										<img src="<%=request.getContextPath()%>/pro/pro.do?prono=${proVO.prono}&action=getPropic" class="program_img"/></a>
									</td>
									<td>
										<div class="center_title">
										<a href="<%=request.getContextPath()%>/proDetail/pro.do?prono=${proVO.prono}&action=getProDetail">${proVO.proname}</a></div>
										<div class="center_desc">${proVO.prointro}</div>
									</td>
																
								</tr>
								</c:forEach>		
							</table>
							<%@ include file="pages/page2.file"%>	
						</div>
					</div>
			
		<%@ include file="/shared/pages/front_footer.file"%>
	</body>
</html>