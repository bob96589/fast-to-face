<%@page import="com.pro.model.ProVO"%>
<%@page import="com.pro.model.ProService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Set"%>
<%@page import="com.cou.model.*"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%
	ProService proSvc = new ProService();
	Set<ProVO> proVOSet = proSvc.getHotPros();
	List<ProVO> proVOList = new ArrayList<ProVO>(proVOSet); 
	Collections.shuffle(proVOList);
	pageContext.setAttribute("proVOList", proVOList);
%>

 


<%
	CouService couSvc = new CouService();
	Set<CouVO> couVOSet = couSvc.getHotCous();
	List<CouVO> couVOList = new ArrayList<CouVO>(couVOSet); 
	Collections.shuffle(couVOList);
	pageContext.setAttribute("couVOList", couVOList);
%>







<html>
	<head>
		<title>Insert title here</title>
		
		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/main_page/css/main_page.css">
	</head>
	<body>
		<%@ include file="/shared/pages/front_header_nav.file" %>
	
	
		<div id="img_section">
			<jsp:include page="/front/adv/showAdv.jsp" flush="true" />
		</div>
		<div id="program_section">
			<div class="center_header">
					熱門學程
			</div>
			<div id="program">
				<c:forEach var="proVO" items="${proVOList}">
					<a class="program_box" href="<%=request.getContextPath()%>/proDetail/pro.do?prono=${proVO.prono}&action=getProDetail">
						<img src="<%=request.getContextPath()%>/pro/pro.do?prono=${proVO.prono}&action=getPropic" class="program_img"  width="280" />
						<div class="center_title">${proVO.proname}</div>
						<div class="center_desc">${proVO.prointro}</div>
					</a>					
				</c:forEach>				
				<div style="clear:both;"></div>
			</div>
		</div>
		<hr/>
		<div id="course_section">
			<div class="center_header">
				熱門課程
			</div>
			<div id="course">
				<c:forEach var="couVO" items="${couVOList}">
					<a class="course_box" href="<%=request.getContextPath()%>/couDetail/cou.do?couno=${couVO.couno}&action=getCouDetail">
						<img src="<%=request.getContextPath()%>/cou/cou.do?couno=${couVO.couno}&action=getCoupic" class="course_img"/>
						<div class="course_text">
							<div class="center_title">${couVO.couname}</div>
							<div class="center_desc">${couVO.couintro}</div>
						</div>
						<div style="clear:both;"></div>	
					</a>
				</c:forEach>									
				<div style="clear:both;"></div>	
			</div>														
		</div>
		<hr/>					
		<div id="discuss_section">
			<div class="center_header">
					最新討論
			</div>
			
			<div id="discuss_content">
				<jsp:include page="/front/front-mainx/index.jsp" flush="true" />				
			</div>
		</div>
		<%@ include file="/shared/pages/front_footer.file" %>
	</body>
</html>