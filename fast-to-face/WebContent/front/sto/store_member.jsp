<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.*" %>
<%@ page import="com.sto.model.*" %>
<%@ page import="com.mem.model.*" %>
<%@ page trimDirectiveWhitespaces="true" %>

<%
	response.setHeader("Cache-Control","no-store"); //HTTP 1.1
	response.setHeader("Pragma","no-cache");        //HTTP 1.0
	response.setDateHeader ("Expires", 0);
%>
<jsp:useBean id="memSvc" class="com.mem.model.MemService"/>
<c:set var="memVO" value="${memSvc.getOneMem(sessionScope.memVO.memno)}" scope="session"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title>儲值點數</title>

<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">



<!-- self -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/front/sto/css/store_member.css">
<style type="text/css">
.well{
    background: rgba(200, 255, 255, 0.5);
}
</style>
<script type="text/javascript" src="<%=request.getContextPath()%>/front/sto/js/confirm_price.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/front/sto/js/confirm_ranno.js"></script>
<script>$(function(){$(".customized_tab #second").addClass("active");});	</script>
<script type="text/javascript" >
$(function(){
	$("#confirmPoint").click(function(){
		var get_action = $(this).next("input[type=hidden]").val();
		var get_point = $("#showprice").children(".transformValue").html();
// 		var get_memno = 90002;
		var get_memno = ${memVO.memno};	//session用
		var	get_ranno = $("#confirmRanno").val();
		
		
		
		if(get_ranno == ""){
			alert("請輸入儲值序號");			
			return false;	
		}
		if(get_ranno.length != 15){
			alert("儲值序號長度不符");			
			return false;
		}
		if(get_point == 100){
			if(get_ranno.charAt(13) != 0 || get_ranno.charAt(14) != 0){
				alert("請輸入面額100專屬序號");
				return false;				
			}
		}
		if(get_point == 350){
			if(get_ranno.charAt(13) != 0 || get_ranno.charAt(14) != 1){
				alert("請輸入面額350專屬序號");
				return false;				
			}
		}
		if(get_point == 500){
			if(get_ranno.charAt(13) != 1 || get_ranno.charAt(14) != 0){
				alert("請輸入面額500專屬序號");
				return false;				
			}
		}
		if(get_point == 1000){
			if(get_ranno.charAt(13) != 1 || get_ranno.charAt(14) != 1){
				alert("請輸入面額1000專屬序號");
				return false;				
			}
		}
		
		
		if(get_ranno.charCodeAt(1) != 84){
			alert("儲值序號錯誤");			
			return false;
		}else if(get_ranno.charCodeAt(2) < 65 || get_ranno.charCodeAt(2) > 90){
			alert("儲值序號錯誤");			
			return false;
		}else if(get_ranno.charAt(3) != 0 && get_ranno.charAt(3) != 1 && get_ranno.charAt(3) != 2){
			alert("儲值序號錯誤");			
			return false;
		}else if(get_ranno.charCodeAt(0) != 70 || get_ranno.charCodeAt(2) != 70){
			alert("儲值序號錯誤");			
			return false;
		}else{  
			$.post("<%=request.getContextPath()%>/sto/sto.do",
			{action:get_action,storeprice:get_point,memno:get_memno,storeranno:get_ranno},
			
			function(){
				var storeRecordPage = "<%=request.getContextPath()%>/sto/sto.do?action=getStoList"
				var couBuyingPage = "<%=request.getContextPath()%>/couBuying/cou.do?${param.couBuyingQueryString}";
				var proBuyingPage = "<%=request.getContextPath()%>/proBuying/pro.do?${param.proBuyingQueryString}";
				if("${param.couBuyingQueryString}"!=""){
					alert("儲值成功，系統即將轉至購買課程頁面!");
					window.location.href = couBuyingPage;
				}else if("${param.proBuyingQueryString}"!=""){
					alert("儲值成功，系統即將轉至購買學程頁面!");
					window.location.href = proBuyingPage;
				}else{
					alert("感謝您的支持，系統即將轉至儲值紀錄頁面!");
					window.location.href = storeRecordPage;
				}
			}
			);
		}
	});
});
</script>

</head>
<body>
<%@ include file="/shared/pages/front_header_nav.file"%>
<%@ include file="/shared/pages/myAccount_top.file" %>
<h3 class="myaccount_title">儲值點數</h3>			

<div class="container"><div class="well" style="color:'#dddddd';">
<div class="panel-group" id="accordion">
	<font size="5">您目前的點數為<span><font color="red">${sessionScope.memVO.mempoint}</font></span>點</font>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
             	<b><i>Step 1</i></b>&nbsp;&nbsp;:&nbsp;儲值金額點數
            </h4>
          </div>
          <div id="collapse1" class="panel-collapse collapse in">
            <div class="panel-body">
				<table class="table table-hover">
					<tr class="success">
						<th></th>
						<th>點數</th>
						<th>金額</th>
						<th></th>												
					</tr>
					<tr>
						<td></td>
						<td>100點</td>
						<td>100元</td>
						<td>
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
							<input type="button" class="btn btn-success" name="n100" id="n100" value="儲值"></a>
							<input type="hidden" name="action" value="">
						</td>
					</tr>
					<tr>
						<td></td>
						<td>350點</td>
						<td>350元</td>
						<td>
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
							<input type="button" class="btn btn-success" name="n350" id="n350" value="儲值"></a>
							<input type="hidden" name="action" value="">
						</td>
					</tr>
					<tr>
						<td></td>	
						<td>500點</td>
						<td>500元</td>
						<td>
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
							<input type="button" class="btn btn-success" name="n500" id="n500" value="儲值"></a>
							<input type="hidden" name="action" value="">
						</td>
					</tr>
					<tr>
						<td></td>	
						<td>1000點</td>
						<td>1000元</td>
						<td>
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
							<input type="button" class="btn btn-success" name="n1000" id="n1000" value="儲值"></a>
							<input type="hidden" name="action" value="">
						</td>
					</tr>
				</table>
			</div>
          </div>
        </div>
   
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              	<b><i>Step 2</i></b>&nbsp;&nbsp;:&nbsp;確認交易金額
            </h4>
          </div>
          <div id="collapse2" class="panel-collapse collapse">
            <div class="panel-body">
            	<table class="table table-hover">
					<tr class="success">
						<th></th><th></th><th></th>						
						<th>點數</th><th></th><th></th>
						<th>金額</th><th></th><th></th>											
						<th></th>											
						<th></th>											
					</tr>
					<tr>
						<td></td><td></td><td></td>
						<td><span id="showpoint"></span>點</td><td></td><td></td>
						<td><span id="showprice"></span>元</td><td></td><td></td>
						<td><span>
							<a data-toggle="collapse" id="confirmMoney"  data-parent="#accordion" href="#collapse3">
							<input type="button" class="btn btn-success"  name="" value="確認金額"></a>
						</span></td>
						<td><span>
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
							<input type="button" class="btn btn-warning" name="" value="取消金額"></a>
						</span></td>
					</tr>
				</table>	
				
				
			</div>
          </div>
        </div>
        
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              	<b><i>Step 3</i></b>&nbsp;&nbsp;:&nbsp;輸入儲值序號
            </h4>
          </div>
          <div id="collapse3" class="panel-collapse collapse">
            <div class="panel-body">
				<%-- 錯誤表列 --%>
				<c:if test="${not empty errorMsgs}">
					<font color='red'>請修正以下錯誤:
						<ul>
							<c:forEach var="message" items="${errorMsgs}">
								<li>${message}</li>
							</c:forEach>
						</ul>
					</font>
				</c:if>
				點數儲值須知: 
				<div class="">
                    <ul class="wayToPay">
                        <li class="L"><font color="green"><b>現金點數</b></font>：為會員自行儲值獲得的點數。</li>
                        <li class="L"><font color="red"><b>紅利點數</b></font>：為會員透過活動或討論所獲得的官方贈送點數，比值與現金點數相同。</li>                  
                    </ul>
                </div>
                 	請輸入儲值序號:	
                <form method="post" action="<%=request.getContextPath()%>/sto/sto.do">				
					<input type="text" name="numID"  class="" id="confirmRanno" maxlength="15" required>
					<input type="button" value="檢查序號" onclick="init()" class="btn btn-xs btn-info">				
					<div>
						<span>
							<button type="button" class="btn btn-success"  id="confirmPoint" name="">確認儲值</button>				
							<input type="hidden" name="action"  value="Store_Point">
						</span>
						<span>
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
							<input type="button" class="btn btn-warning" name="" value="取消儲值"></a>
						</span>
					</div>
				</form>
			</div>
          </div>
        </div>
        
</div> </div>
</div>




<%@ include file="/shared/pages/myAccount_bottom.file" %>
<%@ include file="/shared/pages/front_footer.file"%>

</body>
</html>