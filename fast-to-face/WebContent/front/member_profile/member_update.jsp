<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.mem.model.*"%>





<html>
	<head>
		<title>會員 基本資料</title>
		
		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/member_profile/css/member_basic.css">
		<script>$(function(){$(".customized_tab #first").addClass("active");});	</script>
		
		
<style type="text/css">
 #preview
    {
        /*width:80%;
        height:80%;*/
        width: 182px !important;
        width: 182px;
        max-width: 182px;
       
        border: 1px dashed #d1d0d0;
        text-align: center;
    }
</style>    
<script>      
//載入本機圖片
function preview(file) {

    if (file.files && file.files[0]) {
        var reader = new FileReader();
        reader.onload = function (evt) {
            $("#preview").html( '<img width="180px" src="' + evt.target.result + '" />');
        }
        $("#preview").show();
        reader.readAsDataURL(file.files[0]);
    }
}

</script>  
		
		

	</head>
	<body>
		<%@ include file="/shared/pages/front_header_nav.file"%>
		<%@ include file="/shared/pages/myAccount_top.file" %>
			<form action="<%=request.getContextPath()%>/front/mem/mem.do"  method="post" enctype="multipart/form-data">
			<div id="header_wrapper">
				<h3 id="header" class="myaccount_title">基本資料</h3>
				<input type="submit" class="btn btn-warning btn-sm" value="儲存">
				<input type="hidden" name="action" value="update">
				<input type="hidden" name="memno" value="${memVO.memno}">
			</div>
			<table class="table">
				<tr>
					<td>
						<div id="preview" >
							<img src="<%=request.getContextPath()%>/front/mem/mem.do?memno=${memVO.memno}&action=getMempic" width="180"/>
						</div>
						<input id="uploadImage" type="file" name="mempic" class="fimg1" onchange="preview(this)" />
					</td>				
					<td  width="500">
						<table class="borderless">
							<c:if test="${not empty errorMsgs}">
								<tr>
									<td colspan="2">
										<font color='red'>請修正以下錯誤:
											<ul>
												<c:forEach var="message" items="${errorMsgs}">
												<li>${message}</li>
												</c:forEach>
											</ul>
										</font>
									</td>
								</tr>
							</c:if>
							
							
							<tr>
								<td>帳號</td>
								<td>${sessionScope.memVO.memaccount}</td>
							</tr>
							<tr>
								<td>密碼</td>
								<td>
									<input type="password" name="mempsw" value="${memVO.mempsw}"/>
								</td>
							</tr>
							<tr>
								<td>確認密碼</td>
								<td>
									<input type="password" name="mempsw2" value="${memVO.mempsw}"/>
								</td>
							</tr>
							<tr>
								<td>姓名</td>
								<td>
									<input type="text" name="memname" value="${memVO.memname}"/>
								</td>
							</tr>
							<tr>
								<td>信箱</td>
								<td>
									<input type="text" name="mememail" value="${memVO.mememail}"/>
								</td>
							</tr>
							<tr>
								<td>性別</td>
								<td>
									<input type="radio" name="memgender" value="0" ${(memVO.memgender==0)?'checked':''}/>不公開&nbsp;&nbsp;&nbsp;
									<input type="radio" name="memgender" value="1" ${(memVO.memgender==1)?'checked':''}/>男&nbsp;&nbsp;&nbsp;
									<input type="radio" name="memgender" value="2" ${(memVO.memgender==2)?'checked':''}/>女
								</td>
							</tr>
						</table>				
					</td>				
				</tr>			
			</table>
			</form>		
		<%@ include file="/shared/pages/myAccount_bottom.file" %>				
		<%@ include file="/shared/pages/front_footer.file"%>
	</body>
</html>