<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.mem.model.*"%>





<html>
	<head>
		<title>會員 基本資料</title>
		
		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/member_profile/css/member_basic.css">
		<script>$(function(){$(".customized_tab #first").addClass("active");});	</script>
	</head>
	<body>
		<%@ include file="/shared/pages/front_header_nav.file"%>
		<%@ include file="/shared/pages/myAccount_top.file" %>
			<div id="header_wrapper">
				<h3 id="header" class="myaccount_title">基本資料</h3>
				<a href="<%=request.getContextPath()%>/front/member_profile/member_update.jsp" id="button" class="btn btn-success btn-sm">修改基本資料</a>
			</div>
			<table class="table">
				<tr>
					<td>
						<img src="<%=request.getContextPath()%>/front/mem/mem.do?memno=${memVO.memno}&action=getMempic" width="180"/>
					</td>				
					<td width="500">
						<table class="borderless">
							<tr>
								<td>帳號</td>
								<td>${memVO.memaccount}</td>
							</tr>
							<tr>
								<td>密碼</td>
								<td>●●●●●●</td>
							</tr>
							<tr>
								<td>姓名</td>
								<td>${memVO.memname}</td>
							</tr>
							<tr>
								<td>信箱</td>
								<td>${memVO.mememail}</td>
							</tr>
							<tr>
								<td>性別</td>
								<td>
									${(memVO.memgender==0)?'不公開':''}
									${(memVO.memgender==1)?'男':''}
									${(memVO.memgender==2)?'女':''}
								</td>
							</tr>
						</table>				
					</td>				
				</tr>			
			</table>		
		<%@ include file="/shared/pages/myAccount_bottom.file" %>				
		<%@ include file="/shared/pages/front_footer.file"%>
	</body>
</html>