<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*" %>
<%@ page import="com.buy.model.*" %>
<%@ page import="com.mem.model.*" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%
Set<BuyVO> buyVOSet = (Set<BuyVO>)request.getAttribute("buyVOSet");
%>

<jsp:useBean id="couSvc" scope="page" class="com.cou.model.CouService" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>購買紀錄查詢</title>

<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">


		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/buy_record/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/buy_record/css/jquery-ui.css">
		<script src="<%=request.getContextPath()%>/front/buy_record/js/datepicker.js"></script>
		<script>$(function(){$(".customized_tab #fourth").addClass("active");});	</script>

</head>
<body>
	<%@ include file="/shared/pages/front_header_nav.file"%>
	<%@ include file="/shared/pages/myAccount_top.file" %>
	<h3 class="myaccount_title">購買紀錄</h3>
	
				<%-- 錯誤表列 --%>
				<c:if test="${not empty errorMsgs}">
					<font color='red'>請修正以下錯誤:
						<ul>
							<c:forEach var="message" items="${errorMsgs}">
								<li>${message}</li>
							</c:forEach>
						</ul>
					</font>
				</c:if>
				
				<div class="breadcrumb">
								<h4><i>購買時間搜尋:</i></h4>
								<form method="post" action="<%=request.getContextPath()%>/buy/buy.do">
									<label for="from">起始日期:</label>
									<input type="text" id="from" name="from" placeholder="月份/日期/西元年" style="width:130px;height:30px;" required> 
									<label for="to">終止日期:</label>
									<input type="text" id="to" name="to" placeholder="月份/日期/西元年" style="width:130px;height:30px;" required> 
									<div style="float:right;"><button type="submit" class="btn btn-info btn-sm">查詢</button></div>
									<input type="hidden" name="action" value="getBuyList">
								</form>  	
							</div>
				<%@ include file="pages/page1.file"%>
				&nbsp;&nbsp;&nbsp;
				<div class="panel panel-default">

					<!-- Table -->
					<table class="table table-hover table_center">
						<tr align='center' valign='middle' class="success">
							<th>會員名稱</th>
							<th>課程名稱</th>
							<th>購買編號</th>
							<th>購買狀態</th>
							<th>購買時間</th>
							<th>購買金額(NT)</th>
						</tr>
			 			<c:forEach var="buyVO" items="${buyVOSet}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
							<tr style="font-size:16px;">
								<td style="width:150px;">${memVO.memname}</td>
								<td width="330">
									<c:forEach var="couVO" items="${couSvc.all}">
										<c:if test="${buyVO.couno==couVO.couno}">
											${couVO.couname}
										</c:if>
									</c:forEach>
								</td>
								<td width="150">${buyVO.buyno}</td>
								<td width="150">${(buyVO.buystate==0)?"已購買":"未購買"}</td>
								<td width="150">
									<fmt:formatDate value="${buyVO.buytime}"  pattern="yyyy-MM-dd HH:mm:ss" />
								</td>
								<td width="190">${buyVO.buyprice}元</td>
							</tr>
						</c:forEach>
					</table>
					</div>
					<%@ include file="pages/page2.file"%>
					
	<%@ include file="/shared/pages/myAccount_bottom.file" %>
	<%@ include file="/shared/pages/front_footer.file"%>
</body>
</html>
