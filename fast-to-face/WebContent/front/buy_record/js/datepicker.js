/**
 * 
 */

$(function() {
		$("#from").datepicker({
			showButtonPanel : true,
			onClose : function(selectedDate) {
				$("#to").datepicker("option", "minDate", selectedDate);
			}
		});
		$("#to").datepicker({
			showButtonPanel : true,
			onClose : function(selectedDate) {
				$("#from").datepicker("option", "maxDate", selectedDate);
			}
		});
	});