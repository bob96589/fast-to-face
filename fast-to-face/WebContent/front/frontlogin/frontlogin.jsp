<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- <%@ page errorPage="error.jsp" %> --%>


<html>
	<head>
		<title>Fast to Face-Login</title>
		
		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/frontlogin/css/frontlogin.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/frontlogin/css/demo.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/frontlogin/css/style.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/frontlogin/css/animate-custom.css">
		
		
		
	</head>
	<body>
		<%@ include file="/shared/pages/front_header_nav.file"%>			
			<div id="container_demo" >
			<div id="wrapper">
                        <div id="login" class="animate form">
                            <form  action="<%=request.getContextPath()%>/front/mem/mem.do" method="POST" autocomplete="on">
								<h1 class="login_title"><b>會員登入</b></h1>
								<c:if test="${not empty errorMsgs}">
									<ul>
										<c:forEach var="errorMsgs" items="${errorMsgs}">
											<li class="center_desc">${errorMsgs}</li>
										</c:forEach>
									</ul>
								</c:if>
                                <p> 
                                     <label for="account" class="uname" data-icon="u">帳號:</label>
                                    <input type="text" name="account" size="15" class="form-control" placeholder="請輸入帳號" id="username" required="required"/>
                                </p>
                                <p> 
                                    <label for="password" class="youpasswd" data-icon="p">密碼:</label>
                                    <input type="password" name="password" size="15" class="form-control" placeholder="請輸入密碼" id="password" required="required"/> 
                                </p>
                                <p class="login button"> 
                               		<input type="hidden" name="action" value="login_front"/> 
                                    <input type="submit" value="登入" /> 
								</p>
                                <p class="change_link">
									還不是會員?
									<a href="<%=request.getContextPath()%>/front/member_register/member_register.jsp" class="to_register">註冊</a>
								</p>
								
                            </form>
                        </div>
			</div>
			</div>
		<%@ include file="/shared/pages/front_footer.file"%>	
	</body>
</html>