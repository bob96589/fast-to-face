<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<title>Insert title here</title>
		
		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/frontlogin/css/loginSuccess.css">
		
	</head>
	<body>
		<%@ include file="/shared/pages/front_header_nav.file"%>		
		<center class="welcome_title"><h3>登入成功</h3></center>
		<center class="center_decs">${(memVO.memname==null)? memVO.memaccount : memVO.memname}你好，歡迎來到Fast to Face。</center>
		<center class="center_decs">
			本頁面過5秒後，將自動為您轉至
			<a href="<%=request.getContextPath()%>/front/main_page/main_page.jsp">首頁</a>。
		</center>
		<c:if test="${sessionScope.memVO.memstate==2}">
			<center class="center_decs" style="color:red;">注意!!你在討論區的發言權已被停止。</center>
		</c:if>
		<% response.setHeader("Refresh", "5;URL=" + request.getContextPath() + "/front/main_page/main_page.jsp"); %>
		<%@ include file="/shared/pages/front_footer.file"%>	
	</body>
</html>