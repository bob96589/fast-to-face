<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.sto.model.*" %>
<%@ page import="com.mem.model.*" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%
	Set<StoVO> stoVOSet = (Set<StoVO>)request.getAttribute("stoVOSet");
%>
<%
	response.setHeader("Cache-Control","no-store"); //HTTP 1.1
	response.setHeader("Pragma","no-cache");        //HTTP 1.0
	response.setDateHeader ("Expires", 0);
%>
<%
	MemService memSvc2 = new MemService();
	MemVO memVO = (MemVO)session.getAttribute("memVO");
	memVO = memSvc2.getOneMem(memVO.getMemno());
	session.setAttribute("memVO", memVO);
%>
<html>
	<head>
		<title>儲值記錄查詢</title>


		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">


		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/store_record/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/store_record/css/jquery-ui.css">
		<script src="<%=request.getContextPath()%>/front/store_record/js/datepicker.js"></script>
		<script>$(function(){$(".customized_tab #fifth").addClass("active");});	</script>

	</head>
	<body>
		<%@ include file="/shared/pages/front_header_nav.file"%>
		<%@ include file="/shared/pages/myAccount_top.file" %>
		<h3 class="myaccount_title">儲值紀錄</h3>	
			
							<%-- 錯誤表列 --%>
							<c:if test="${not empty errorMsgs}">
								<font color='red'>請修正以下錯誤:
									<ul>
										<c:forEach var="message" items="${errorMsgs}">
											<li>${message}</li>
										</c:forEach>
									</ul>
								</font>
							</c:if>
							
							<div class="breadcrumb">
								<h4><i>儲值時間搜尋:</i></h4>
								<form method="post" action="<%=request.getContextPath()%>/sto/sto.do">
									<label for="from">起始日期:</label>
									<input type="text" id="from" name="from" placeholder="月份/日期/西元年" style="width:130px;height:30px;" required /> 
									<label for="to">終止日期:</label>
									<input type="text" id="to" name="to" placeholder="月份/日期/西元年" style="width:130px;height:30px;" required /> 
									<div style="float:right;"><button type="submit" class="btn btn-info btn-sm">查詢</button></div>
									<input type="hidden" name="action" value="getStoList">
								</form>  	
							</div>
							<%@ include file="pages/page1.file"%>
							&nbsp;&nbsp;&nbsp;
							<div class="panel panel-default">
								<!-- Table -->
								<table class="table table-hover table_center" >
									<tr align='center' valign='middle' class="success">
										<th>會員名稱</th>
										<th>儲值編號</th>
										<th>儲值序號</th>
										<th>儲值時間</th>
										<th>儲值點數</th>
									</tr>
									<c:forEach var="stoVO" items="${stoVOSet}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>" >
										<tr style="font-size:16px;">
											<td>${memVO.memname}</td>
											<td width="150">${stoVO.storeno}</td>									
											<td width="200">${stoVO.storeranno}</td>
											<td width="190">
												<fmt:formatDate value="${stoVO.storetime}"  pattern="yyyy-MM-dd HH:mm:ss" />
											</td>
											<td width="150">${stoVO.storeprice}元</td>											
										</tr>
									</c:forEach>
								</table>															
							</div>
							<%@ include file="pages/page2.file" %>
							
							
							
		<%@ include file="/shared/pages/myAccount_bottom.file" %>				
		<%@ include file="/shared/pages/front_footer.file"%>
	</body>
</html>
