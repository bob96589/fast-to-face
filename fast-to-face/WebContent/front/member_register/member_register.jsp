<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<title>註冊</title>
		
	<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">
		
		<!-- self -->
		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/member_register/css/demo.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/member_register/css/registerStyle.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/member_register/css/animate-custom.css">
		<script src="<%=request.getContextPath()%>/front/member_register/js/memhecked.js"></script>
	<script language="javascript">
		window.onload=function (){
			alert("請詳細閱讀使用條約並勾選");
			  var check=document.getElementById("check");
			  var reg=document.getElementById("reg");
			  check.onclick=function(){
			    if(check.checked==true){
			    	alert("請詳實填寫帳號、電子信箱及密碼!");
			       reg.disabled=false;
			    }
			    else{
			    	reg.disabled=true;
			    }
			  }
			}
	</script>
			
	</head>
	<body>
		<%@ include file="/shared/pages/front_header_nav.file"%>							
            <div id="container_demo" >                 
                <div id="wrapper">
                   <div id="register" class="animate form">

		               <form METHOD="post" action="<%=request.getContextPath() %>/front/mem/mem.do" autocomplete="on">
		               <h1>會員註冊</h1> 
		              		<%-- 錯誤表列 --%>
							<c:if test="${not empty errorMsgs}">
								<font color='red'>請修正以下錯誤:
									<ul>
										<c:forEach var="errorMsgs" items="${errorMsgs}">
											<li>${errorMsgs}</li>
										</c:forEach>
									</ul>
								</font>
							</c:if>	 
		                   <p> 
		                       <label for="memaccount" class="uname" data-icon="u">帳號:</label>
		                       <input type="text" name="memaccount" size="30" value="${memVO.memaccount}" id="usernamesignup" placeholder="mysuperusername690" />
		                      	<span id="loadingIMG"></span>
		                   </p>
		                   <p> 
		                       <label for="mememail" class="youmail" data-icon="e" >電子郵件:</label>
		                       <input type="text" name="mememail" size="30" value="${memVO.mememail}" id="emailsignup" placeholder="mysupermail@mail.com"/> 
 		                   	   <span ></span>
		                   </p>
		                   <p> 
		                       <label for="mempsw" class="youpasswd" data-icon="p">密碼:</label>
		                       <input type="password" name="mempsw" size="30" value="" id="passwordsignup"  placeholder="eg. X8df!90EO"/>
		                       <span></span>
		                   </p>
		                   <p> 
		                       <label for="pswchecked" class="youpasswd" data-icon="p">確認密碼:</label>
		                       <input type="password" name="pswchecked" size="30" value="" id="passwordsignup_confirm"  placeholder="eg. X8df!90EO"/>
		                       <span></span>
		                   </p>
		                   <p>
		                   		<lable><input type="checkbox"    id="check"  required>
		                   			&nbsp; 我已詳閱<<a href="<%=request.getContextPath() %>/front/member_register/member_usetreaty.jsp" >使用條款</a>>
		                   		</lable>
		                   </p>
		                   
		                   <p class="signin button"> 
								<input type="submit" value="註冊" id="reg"  disabled />
								<input type="hidden" name="action" value="insert">  
				           </p>
				          
					       <p class="change_link">已經是會員?
					       			<a href="<%=request.getContextPath() %>/front/frontlogin/frontlogin.jsp" class="to_register">登入</a>
							</p>
		               </form>
<%-- 		               </c:forEach> --%>
<%-- 		               </c:if> --%>
           </div>
       </div>
    </div>  
  			<%@ include file="/shared/pages/front_footer.file"%>	
	</body>
</html>