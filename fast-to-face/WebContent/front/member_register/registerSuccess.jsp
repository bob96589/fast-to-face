<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<title>Insert title here</title>
		
		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/member_register/css/registerSuccess.css">
		
	</head>
	<body>
		<%@ include file="/shared/pages/front_header_nav.file"%>		
		<center class="welcome_title"><h3>註冊成功，您的帳號尚未開通</h3></center>
		<center class="center_decs">${memVO.memaccount}你好，請至您的E-mail收取帳號認證信，並輸入驗證碼。</center>
		<form action="<%=request.getContextPath()%>/memValidation/mem.do" method="post">
			<center><input type="text" name="memrandom"/></center>
			<center class="center_decs" id="errorMsgs">${errorMsgs}</center>
			<center>
				<input type="submit" class="btn btn-success btn-sm" value="開通此帳號"/>
				<input type="hidden" name="action" value="updateMemstate"/>
				<input type="hidden" name="memno" value="${memVO.memno}"/>
			</center>
		</form>
		<%@ include file="/shared/pages/front_footer.file"%>	
	</body>
</html>