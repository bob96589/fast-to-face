<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor" %>
<%-- 此頁練習採用 EL 的寫法取值 --%>
<%@ page import="java.util.*"%>
<%@ page import="com.top.model.*"%>
<%@ page import="com.fro.model.*"%>
<%@ page import="com.mem.model.*"%>
<%@ page import="com.art.model.*"%>
<%@ page import="com.rep.model.*"%>     
<%  
	List<ArtVO> artVOlist = (List<ArtVO>)request.getAttribute("artVOlist");
%> 	
<html>
<head>
<!-- <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script> -->
<script src="<%= request.getContextPath() %>/ckeditor/ckeditor.js"></script>
<!-- <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script> -->
<link rel="stylesheet " href="css/front-mainx.css">
<script>
      window.onload =(function(){
    	  $(".art-ckeditor").hide(); });
    //  $(document).ready(function(){
	//  $(".art-ckeditor").hide(10); });
      
     function show(){    	 
   	  $(".art-ckeditor").show(100); 
    	 };
     function hide(){    	 
      $(".art-ckeditor").hide(100); 
    	  };
    	 
</script> 
  
<title>文章列表</title>

<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">

</head>
<body bgcolor='white'>
<%@ include file="/shared/pages/front_header_nav.file" %>
    <jsp:include page="header3.jsp" />      	
   <!--錯誤表列  --> 
	<c:if test="${not empty errorMsgs}">
		<font color='red'>
			<ul>
				<c:forEach var="message" items="${errorMsgs}">
					<li>${message}</li>
				</c:forEach>
			</ul>
		</font>
	</c:if>     	 	 	  
    <div  align="center" >
    <table>
	   <tr>
	     <td><%@ include file="pages/article-page1.file"%> </td>	    
	     <td width="1000px" align="center"><%@ include file="pages/article-page2.file"%>
	     </td>	     
	   </tr>
	</table> 
	<table id="article-table1">
	   <tr>	       
	     <td width="980px"><主題>${topVO.topname}  
	     </td>
	     <td> 
	     	<c:if test="${(sessionScope.memVO.memstate != 2) && (sessionScope.memVO != null)}">	
	     		<button onclick="show()">回覆文章</button>
	     	</c:if> 
	     </td>
	   </tr>
	</table>
	<table   class="table table-bordered" > 		 			
		<c:forEach var="artVO" items="${artVOlist}"  begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">																  
		<tr class="active">
		  <td>
		  	<c:forEach var="memVO" items="${memVOlist}">
			  	<c:if test="${artVO.memno==memVO.memno}">
			  		<table>
				  		<tr>
				  			<td>
					    		<img src="<%=request.getContextPath()%>/front/mem/mem.do?memno=${memVO.memno}&action=getMempic" width="100" style="padding:10px;margin:0px 10px;" />
							</td>
							<td valign="bottom">
								<div >作者:${(memVO.memname==null)? memVO.memaccount : memVO.memname}</div>
								<p>發文時間:<fmt:formatDate value="${artVO.artasktime}"  pattern="yyyy-MM-dd HH:mm:ss" />
								<c:if test="${(sessionScope.memVO.memstate != 2) && (sessionScope.memVO != null)}">	
								<c:if test="${!empty sessionScope.memVO.memno && sessionScope.memVO.memno != memVO.memno}"> <!-- 未登入隱藏檢舉鈕 -->
								<button type="button" class="btn btn-default btn-sm" data-toggle="modal"   data-target="#report${artVO.artno}" <c:if test="${sessionScope.memVO.memstate== 2}"> disabled </c:if> >檢舉</button>
								</c:if> 
								</c:if><br/><!-- 檢舉用燈箱 -->
								  <div class="modal fade" id="report${artVO.artno}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
											  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
											  <h4 class="modal-title" id="myModalLabel">檢舉</h4>
										    </div> 
											  <form method="post" action="<%=request.getContextPath()%>/front/front-mainx/front.do">
										       <div class="modal-body">								  			
											   <textarea rows="15" id="protextarea"  class="form-control" name="repcon" placeholder="請輸入檢舉原因" required></textarea>									       		
										       </div>										       
										       <div class="modal-footer">								       	
						        			    <button type="submit" class="btn btn-primary">檢舉</button>
						        			    <button type="button" class="btn btn-warning btn-xs"  id="artbutton"><span class="glyphicon glyphicon-star"></span></button>
							      			     <input type="hidden" name="action" value="insertrep">
							      			     <input type="hidden" name="artno" value="${artVO.artno}">
							      			      <input type="hidden" name="memno2" value="${sessionScope.memVO.memno}">
											     <input type="hidden" name="memno" value="${artVO.memno}">
											     <input type="hidden" name="topno" value="${artVO.topno}">       
											     <input type="hidden" name="whichPage" value="${param.whichPage}">
						      			       </div>
						      			       <script>
						      			      		 $(function(){							
                                                 	$("#artbutton").click(function(){					
                                                   	$(".form-control").val("發文內容不清楚!");
                                                      	});
                                                           });	
						      			       </script>
					      				      </form>
										 </div>
									</div>
								</div>
							</td>
						</tr>
					</table>		
				</c:if> 			    							
		    </c:forEach>
				    		 
		  </td>			 
		 </tr>
		 <tr id="article-table2">
			<td style="word-break:break-all">
				${artVO.artcon}
		    </td>
		</tr>		  		   									   			
		</c:forEach>		 
	  </table >
	 <div style="padding-top:20px">
	  <%@ include file="pages/article-page2.file"%>
	 </div>				
    </div>
    <div>
     <p><%@ include file="art-ckeditor.jsp"%>
    </div>
<%@ include file="/shared/pages/front_footer.file" %>     
</body>
</html>
