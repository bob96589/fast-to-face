<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor" %>
<%@ page import="com.art.model.*"%>
<%@ page import="com.fro.model.*"%>
<%@ page import="com.top.model.*"%>
<%	 
    ArtVO artVO = (ArtVO) request.getAttribute("ArtVO");   
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 

<script src="<%= request.getContextPath() %>/ckeditor/ckeditor.js"></script>
<link rel="stylesheet " href="css/front-mainx.css">
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title>文章回覆</title>
</head>
<body>
<!--    錯誤表列 
<c:if test="${not empty errorMsgs}">
	<font color='red'>請修正以下錯誤:
	<ul>
		<c:forEach var="message" items="${errorMsgs}">
			<li>${message}</li>
		</c:forEach>
	</ul>
	</font>
</c:if>--> 
    <div id="article-ckeditor" class="art-ckeditor">
     <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front/front-mainx/front.do">
     <table>     	   
       <tr>
         <td id="article-ckeditor">	 
	     <textarea  id="editor2" name="artcon"  rows="10" cols="10">請輸入內容...</textarea>  <!--  name="artcon" 設定欄位名稱 -->
         <ckeditor:replace replace="editor2" basePath="/ckeditor/" />         
         <br>
         </td>
       </tr>
         <tr id="article-ckeditor">
         <td>         
	          <input type="button" value="取消" onclick="hide()">	
	          <input type="hidden" name="whichPage" value="<%= request.getParameter("whichPage") %>">      
	          <input type="hidden" name="action" value="insertart">
	          <input type="hidden" name="memno2" value="${sessionScope.memVO.memno}">           
	          <input type="hidden" name="topno" value="${topVO.topno}">
	          <input type="submit" value="送出" onclick="hide()">
	          <button type="button" class="btn btn-warning btn-xs" id="art_button"><span class="glyphicon glyphicon-star"></span></button>	        
         </td> 
         </tr>
      </table>
      </FORM>
           <script>
			$(function(){							
				$("#art_button").click(function(){														 
					CKEDITOR.instances['editor2'].setData(
					"<h3>推薦你可以到中壢資策會去報名</h3><br>\
					 <h1>Java雲端服務開發技術養成班</h1><br>\
                     <h3>中壢資策會  師資優良 環境優美 是非常適合學習的好地方喔!!</h3>");					
			      	});
			  });
		</script>	
    </div>	
</body>
</html>