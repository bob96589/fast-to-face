<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor" %>

<%--日期格式化標籤 --%>
<%@ page import="java.util.*"%>
<%@ page import="com.top.model.*"%>
<%@ page import="com.fro.model.*"%>
<%@ page import="com.mem.model.*"%>
<%@ page import="com.art.model.*"%>
<%@ page import="com.rep.model.*"%>
<%
 	List<TopVO> list = (List<TopVO>)request.getAttribute("list");   
	
%>
 
<html>
<head>

<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">

<!-- self -->
<script>$(function(){$(".customized_tab #sixth").addClass("active");});</script>

<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title>Insert title here</title>
</head>
<body>
<%@ include file="/shared/pages/front_header_nav.file" %>
<%@ include file="/shared/pages/myAccount_top.file" %>
<h3 class="myaccount_title">討論區發文紀錄</h3>
<div align="center">
    	<table >
    		<tr >		   
    			<td><%@ include file="pages/member-page1.file"%></td> 			     		
   		   </tr>    	
    	</table>
    	<div class="panel panel-default">    	   	        	
		<table   class="table" id="table1">
			<tr valign='middle' id="table1" class="success">
				<th style="text-align:center"  >主題</th>
				<th style="text-align:center"  >回覆數</th>
				<th style="text-align:center"  >作者</th>
				<th style="text-align:center"  >最新回應</th>
		    </tr>			 			
			<c:forEach var="topVO" items="${list}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>" varStatus="s">
				<tr align='center' id="topic-td">
					<td id="topic-table2" align='left' class="center_title">
						<a href="<%=request.getContextPath()%>/front/front-mainx/front.do?topno=${topVO.topno}&action=showart">
						<c:if test="${topVO.forno==150001}">
						<img src="<%=request.getContextPath()%>/front/front-mainx/images/10001.png" width="20px">
						</c:if>						 
						<c:if test="${topVO.forno==150002}">
						<img src="<%=request.getContextPath()%>/front/front-mainx/images/10005.png" width="20px">
						</c:if>
						<c:if test="${topVO.forno==150003}">
						<img src="<%=request.getContextPath()%>/front/front-mainx/images/10007.png" width="20px">
						</c:if>
						${topVO.topname}</a>
					</td>
					<jsp:useBean id="topService" scope="page" class="com.top.model.TopService"/>
					<td class="center_title">${topService.getShowfor2(topVO.topno)}</td>
					<td style="font-size:14px;">
						<fmt:formatDate value="${topVO.toptime}"  pattern="yyyy-MM-dd HH:mm:ss" /><br>						
						<c:forEach var="memVO" items="${memVOlist}">						
							<c:if test="${topVO.memno==memVO.memno}">
								${memVO.memname} 
							</c:if>
						</c:forEach></td>												
						<td style="font-size:14px;">
						 <jsp:useBean id="arttime" scope="page" class="com.top.model.TopService"/>
						 <fmt:formatDate value="${arttime.getShowArtTime(topVO.topno).artasktime}"  pattern="yyyy-MM-dd HH:mm:ss" /><br>						 																 						    							
						   <c:forEach var="memVO" items="${memVOlist}">						
						   <c:if test="${arttime.getShowArtTime(topVO.topno).memno==memVO.memno}">
						    ${memVO.memname} 
						   </c:if>
						   </c:forEach>																					   					
					    </td>
				</tr>				 				
			</c:forEach>
		</table>
		</div>	
		<%@ include file="pages/member-page2.file"%>			            	  
		   <div style="padding-top:20px">
		   
		   </div>		    		 	
	</div>
<%@ include file="/shared/pages/myAccount_bottom.file" %>
<%@ include file="/shared/pages/front_footer.file" %>
</body>
</html>