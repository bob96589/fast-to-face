<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor" %>

<%--日期格式化標籤 --%>
<%@ page import="java.util.*"%>
<%@ page import="com.top.model.*"%>
<%@ page import="com.fro.model.*"%>
<%@ page import="com.mem.model.*"%>
<%@ page import="com.art.model.*"%>
<%@ page import="com.rep.model.*"%>
 <jsp:useBean id="arttime" scope="page" class="com.top.model.TopService"/>
<%-- 此頁練習採用 EL 的寫法取值 --%>
<%
 	List<TopVO> list = (List<TopVO>)request.getAttribute("list"); 
     
%>
<html>
<head>

<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">

<!-- self -->
<link rel="stylesheet " href="css/front-mainx.css">

<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title>主題文章列表</title>
</head>
<body>
<%@ include file="/shared/pages/front_header_nav.file" %>
  <div >
    <jsp:include page="header.jsp" /> 
     <form method="post" action="<%=request.getContextPath()%>/front/front-mainx/front.do">	 				        
      <div class="col-lg-6">  
       <div class="input-group" style="width:1000px;margin-left:300px">          	
          搜尋於:<select size="1" name="forno" style=height:25px >
         <option  value="1">全部的討論區</option>
    	  <c:forEach var="forVO" items="${forlist}">		 
	       <option value="${forVO.forno}">${forVO.fortype}</option>	      	       
	      </c:forEach> 	      	  
         </select> 
         <input type="text" style="width:250px;margin-left:10px;margin-right:10px;height:25px" placeholder="請輸入關鍵字..." name=topname value="${topVO.topname}">          
         <input type="hidden" name="action" value="SelectTop">
         <button class="btn btn-default" type="submit"  >搜尋</button>       
       </div>   
      </div>
     </form>  
    <br/>
    <hr>   
    <div align="center">
    	<table >
    		<tr >
    		    <td width="300px"><%@ include file="pages/top-page1.file"%></td>
    			<td width="500px" align="center"><%@ include file="pages/top-page2.file"%></td>
    			<td style="padding-left:230px"> 
    			<c:if test="${(sessionScope.memVO.memstate != 2) && (sessionScope.memVO != null)}">		      
    				<input type="button" class="btn btn-default"   value="發文"   onclick="window.location.href ='<%=request.getContextPath()%>/front/front-mainx/top-ckeditor.jsp?forno=${forVO.forno}'" />    			      
    			</c:if> 
    			</td>	    		
   		   </tr>    	
    	</table>
    	<div class="panel panel-default">    	   	        	
		<table  class="table table-striped">
			<tr valign='middle'  class="customized_row">
				<th style="text-align:center"  >主題  </th>
				<th style="text-align:center"  >回覆數</th>
				<th style="text-align:center"  >作者  </th>
				<th style="text-align:center"  >最新回應</th>
		    </tr>		   	
			<c:forEach var="topVO" items="${list}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>" varStatus="s">
				 
				<tr align='center' id="topic-td">
					<td id="topic-table2" >
						<a href="<%=request.getContextPath()%>/front/front-mainx/front.do?topno=${topVO.topno}&action=showart">
						<c:if test="${topVO.forno==150001}">
						<img src="<%=request.getContextPath()%>/front/front-mainx/images/10001.png" width="25px">
						</c:if>						 
						<c:if test="${topVO.forno==150002}">
						<img src="<%=request.getContextPath()%>/front/front-mainx/images/10005.png" width="25px">
						</c:if>
						<c:if test="${topVO.forno==150003}">
						<img src="<%=request.getContextPath()%>/front/front-mainx/images/10007.png" width="25px">
						</c:if>
						${topVO.topname}</a>
					</td>
					<jsp:useBean id="topService" scope="page" class="com.top.model.TopService"/>
					<td style="vertical-align:middle">${topService.getShowfor2(topVO.topno)}</td>
					<td >
						<fmt:formatDate value="${topVO.toptime}"  pattern="yyyy-MM-dd HH:mm:ss" /><br>						
						<c:forEach var="memVO" items="${memVOlist}">						
							<c:if test="${topVO.memno==memVO.memno}">
						      ${(memVO.memname==null)? memVO.memaccount : memVO.memname}
							</c:if>
						</c:forEach></td>												
						<td  >						  
						 <fmt:formatDate value="${arttime.getShowArtTime(topVO.topno).artasktime}"  pattern="yyyy-MM-dd HH:mm:ss" /><br>						 																 						    							
						   <c:forEach var="memVO" items="${memVOlist}">						
						   <c:if test="${arttime.getShowArtTime(topVO.topno).memno==memVO.memno}">
						      ${(memVO.memname==null)? memVO.memaccount : memVO.memname} 
						   </c:if>
						   </c:forEach>																					   					
					    </td>
				   </tr>
			  </c:forEach> 
		   </table>
		   </div>		  
		  <div style="padding-top:20px">
	     <%@ include file="pages/top-page2.file"%>
	   </div>		    		 	
	</div> 
  </div>	
<%@ include file="/shared/pages/front_footer.file" %>
</body>
</html>