<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor" %>
<%@ page import="com.art.model.*"%>
<%@ page import="com.fro.model.*"%>
<%@ page import="com.top.model.*"%>
<%


	TopVO topVO = (TopVO) request.getAttribute("TopVO");
    ArtVO artVO = (ArtVO) request.getAttribute("ArtVO");
    
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">

<!-- self -->
<script src="<%= request.getContextPath() %>/ckeditor/ckeditor.js"></script>
<link rel="stylesheet " href="css/front-mainx.css">

<meta http-equiv="Content-Type" content="text/html;">
<title>Insert title here</title>
</head>
<body>
<%@ include file="/shared/pages/front_header_nav.file" %>
     <%-- 錯誤表列 --%>
<c:if test="${not empty errorMsgs}">
	<font color='red'>請修正以下錯誤:
	<ul>
		<c:forEach var="message" items="${errorMsgs}">
			<li>${message}</li>
		</c:forEach>
	</ul>
	</font>
</c:if>
    <div id="article-ckeditor">
     <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front/front-mainx/front.do">
     <table>     
	   <tr>
	    <td>	         
	        主題名稱:
		    <input type="TEXT" name="topname" size="45" id="protopname"
			value="${topVO.topname}" />
	        
		    主題類別: 
		    <jsp:useBean id="forSvc" scope="page"
			class="com.fro.model.ForService" />				
			<font color=red><b>*</b></font>
			<select size="1" name="forno">
				<c:forEach var="forVO" items="${forSvc.all}">
					<option value="${forVO.forno}" ${(topVO.forno==forVO.forno)? 'selected':'' }>${forVO.fortype}</option>
				</c:forEach>
		    </select>
		  </td>					    
	   </tr>
       <tr>
         <td id="article-ckeditor">	 
	     <textarea  id="editor1" name="artcon" class="prockeditor" rows="10" cols="10">請輸入內容...</textarea>  <!--  name="artcon" 設定欄位名稱 -->
         <ckeditor:replace replace="editor1" basePath="/ckeditor/" />         
         <br>
         </td>
       </tr>
       <tr>
         <td>  
	          <input type="button" value="取消" onclick="javascript:history.back()">		             
	          <input type="hidden" name="action" value="insertArt">
	          <input type="hidden" name="memno" value="${memVO.memno}">
	          <input type="hidden" name="whichPage" value="<%= request.getParameter("whichPage") %>">
	          <input type="submit" value="送出" <c:if test="${sessionScope.memVO.memstate== 2}"> disabled </c:if>>	        
              <button type="button" class="btn btn-warning btn-xs" id="top_button"><span class="glyphicon glyphicon-star"></span></button>	
         </td> 
       </tr>
        
      </table>
      </FORM>
       <script>
			$(function(){							
				$("#top_button").click(function(){									
					$("#protopname").val("快來拜讀吧~超實用的SCJP 6.0認證教戰手冊(第二版)");
					CKEDITOR.instances['editor1'].setData(
							"<h1>SCJP 6.0認證教戰手冊(第二版)</h1><br>\
							內容特色:<br>\
							1.Oracle SCJP認證考試介紹：詳細介紹Oracle SCJP 6.0認證考試的範圍與考試題型，並一步一步地帶領讀者在VUE官方網站上註冊個人資料與訂定考場。列出VUE在台灣所有合法考場的名稱與地址。<br>\
							2.Java基本語法介紹：以日常生活所熟悉的事物當作範例來解說Java基本語法，完全不需要強記。<br>\
							3.Java物件導向觀念說明：作者以業界專案設計的經驗來說明物件導向的觀念。例如抽象類別、介面由系統分析師來撰寫的原因。<br>\
							4.Java重要類別庫說明與應用：將Oracle SCJP認證考試所涵蓋的類別庫及其重要方法都列表加以說明，並將應用方式以範例完整呈現。<br>\
							5.300題完全擬真試題詳解：保證符合Oracle SCJP 6.0的考試範圍與命題方向，並將考題分析與解題技巧錄製成『影音光碟』，讓讀者即使在家，也能夠隨時聽講，以熟練考試題型而順利取得SCJP國際證照。<br>\
							<br>\
							作者介紹:<br>\
							作者 黃彬華<br>\
							學歷：英國Essex大學Computer Science碩士<br>\
							現任：台中科大、清雲、開南、萬能等大學資管系 兼任講師<br>\
							　　　奇科電腦、資策會、自強基金會、巨匠電腦等教育訓練中心 兼任講師<br>\
							曾任：宏達國際電子股份有限公司（HTC） 智慧型手機專案經理<br>\
							　　　寶成國際供應鏈研發中心 課長級系統分析師<br>\
							　　　中華映管股份有限公司 CIM工程師<br>\
							<br>\
							章節目錄:<br>\
							第1章 SCJP認證考試介紹<br>\
							　1-1 Java證照<br>\
							　1-2 SCJP 6.0認證考試範圍<br>\
							　1-3 上網報名考試<br>\
							第2章 Java導論<br>\
							　2-1 傳統程式與Java程式的差異<br>\
							　2-2 Java開發工具<br>\
							　2-3 Java檔案的編譯與執行<br>\
							　2-4 Java程式內容簡介<br>\
							　2-5 Java基本類型<br>\
							　2-6 基本類型的轉型<br>\
							　練習題<br>\
							第3章 基本運算符號<br>\
							　3-1 運算符號介紹<br>\
							　3-2 算數運算符號<br>\
							　3-3 文字串接符號<br>\
							　3-4 指派運算符號<br>\
							　3-5 比較運算符號<br>\
							　3-6 邏輯運算符號<br>\
							　練習題<br>\
							第4章 條件與迴圈控制<br>\
							　4-1 條件控制<br>\
							　4-2 迴圈控制<br>\
							　4-3 特殊流程的處理<br>\
							第5章 陣列的應用<br>\
							　5-1 陣列基本概念<br>\
							　5-2 1維陣列介紹<br>\
							　5-3 1維陣列元素的取出<br>\
							　5-4 1維陣列的排序與搜尋<br>\
							　5-5 陣列的傳址與傳值<br>\
							　5-6 2維陣列的宣告與存取<br>\
							　練習題<br>\
							第6章 物件導向基本觀念<br>\
							　6-1 物件導向概論<br>\
							　6-2 Java方法<br>\
							　6-3 建構式概論<br>\
							　6-4 物件陣列<br>\
							　6-5 繼承<br>\
							　6-6 Object類別<br>\
							　練習題<br>\
							第7章 物件導向進階觀念<br>\
							　7-1 Java套件<br>\
							　7-2 編譯Java專案套件<br>\
							　7-3 存取修飾詞的使用<br>\
							　7-4 封裝<br>\
							　7-5 抽象類別<br>\
							　7-6 介面<br>\
							　7-7 Java的繼承與實作<br>\
							　7-8 物件的轉型<br>\
							　7-9 多型<br>\
							　練習題<br>\
							第8章 生命期的探討與列舉類型<br>\
							　8-1 變數種類與其生命期<br>\
							　8-2 方法種類與呼叫方式<br>\
							　8-3 類別成員的匯入<br>\
							　8-4 main()方法的參數傳遞與系統屬性設定<br>\
							　8-5 內部類別<br>\
							　8-6 Java列舉類型<br>\
							　8-7 資源回收機制<br>\
							　練習題<br>\
							第9章 例外事件的產生與處理<br>\
							　9-1 執行上的錯誤<br>\
							　9-2 Java例外事件與處理機制<br>\
							　9-3 使用throw自行產生例外事件<br>\
							　9-4 自訂例外類別<br>\
							　9-5 使用throws拋出例外事件<br>\
							　9-6 RuntimeException與Checked Exception<br>\
							　9-7 測試程式與AssertionError錯誤事件<br>\
							　9-8 StackOverflowError錯誤事件<br>\
							　練習題<br>\
							第10章 資料的輸入與輸出<br>\
							　10-1 基本輸入與輸出的觀念<br>\
							　10-2 Console類別<br>\
							　10-3 File類別<br>\
							　10-4 存取檔案內容<br>\
							　10-5 多重串接<br>\
							　練習題<br>\
							第11章 常用資料類型與其格式化<br>\
							　11-1 數字類型<br>\
							　11-2 文字類型<br>\
							　11-3 日期/時間類型<br>\
							　11-4 數字與日期/時間格式設定<br>\
							　11-5 規則運算式與相關類別<br>\
							　練習題<br>\
							第12章 集合與泛型<br>\
							　12-1 集合與陣列的比較<br>\
							　12-2 Collection介面與Collections類別<br>\
							　12-3 泛型<br>\
							　12-4 各種集合的特色<br>\
							　12-5 Map的功能與架構<br>\
							　練習題<br>\
							第13章 多執行緒程式設計<br>\
							　13-1 多執行緒簡介<br>\
							　13-2 Java執行緒與Thread類別<br>\
							　13-3 Runnable介面<br>\
							　13-4 執行緒的同步性與安全性<br>\
							　13-5 執行緒的互動處理<br>\
							　練習題<br>\
							第14章 SCJP 6.0完全擬真試題與解析(共244題)<br>\
							附錄A 使用Eclipse開啟本書範例<br>\
							　A-1 Eclipse – Java整合開發工具<br>\
							　A-2 下載Eclipse<br>\
							　A-3 安裝Eclipse<br>\
							　A-4 建立Java專案並載入本書範例<br>\
							<img src='http://www.gotop.com.tw/Waweb2004/WawebImages/BookXL/AER007331.jpg'>");					
			      	});
			});
		</script>	
    </div>
<%@ include file="/shared/pages/front_footer.file" %>	
</body>
</html>