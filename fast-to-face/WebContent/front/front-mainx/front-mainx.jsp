<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%--日期格式化標籤 --%>
<%@ page import="java.util.*"%>
<%@ page import="com.top.model.*"%>
<%@ page import="com.fro.model.*"%>
<jsp:useBean id="forSvc" scope="page" class="com.fro.model.ForService" />
<%-- <%
	ForService forSvc = new ForService();
	List<ForVO> forVO = forSvc.getAll();
	pageContext.setAttribute("forVO", forVO);
%> --%>

<!DOCTYPE html>
<html>
<head>

<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">

<!-- self -->
<link rel="stylesheet" href="css/discuss.css">
<link rel="stylesheet " href="../../css/mainX.css">
<link rel="stylesheet " href="css/front-mainx.css">
<script src="main.js"></script>
<title>版規公告</title>
</head>
<body>
<%@ include file="/shared/pages/front_header_nav.file" %>
			<jsp:include page="header1.jsp" />
			<div>
			  		 
			  <table id="mainx-table2" border="1" bordercolor="#CCCCFF">	
			  	
			  	<tr><td id="mainx-table3"><p>【刪文】
                         <p>第1條．分類錯誤。
                         <p>第2條．引言含有違規內容。
                         <p>第3條．與本板主題無關，文章內容與本板討論方向不符。
                         <p>第4條．請充實文章內容。文章內容未達10個繁體中文字 感謝文 純推文 火星文 補字文 私信文 標點符號 .. 。
                         <p>第5條．注音文。標題+內文 含有超過3字注音(ㄚ、ㄧ 視情況處理)，少見字可用同音字標示發音。     
                         <p>第6條．同串連續2篇(含)以上回應，避免洗板請自行使用編輯功能。板務會依據其發文意圖自行判斷。
                         <p>第7條．重複2次以上發文，包含同子板與不同子板，內容相同或類似之文章
                         <p>【鎖帳號】
                         <p>第1條．文章內容含謾罵，人身攻擊等字眼。
                         <p>第2條．發表限制級之圖片、言論或連結。  
　　　                   <p>第3條．於站內從事現金交易。
                         <p>第4條．幻想文：過於誇張，虛構不實文章，由板務主觀認定。
                         <p>第5條．未經同意公布他人個資、信件、訊息者。
                         <p>第6條．濫用檢舉系統。經板務通知後，仍然蓄意檢舉不違規之文章。    
                        </td>
                    </tr>
             </table>
			</div>			
<%@ include file="/shared/pages/front_footer.file" %>
</body>
</html>
