<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor" %>
<%@ page import="com.art.model.*"%>
<%@ page import="com.fro.model.*"%>
<%@ page import="com.top.model.*"%>
<%


	TopVO topVO = (TopVO) request.getAttribute("TopVO");
    ArtVO artVO = (ArtVO) request.getAttribute("ArtVO");
    
%>
 
  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="<%= request.getContextPath() %>/ckeditor/ckeditor.js"></script>

<link rel="stylesheet " href="css/front-mainx.css">
<meta http-equiv="Content-Type" content="text/html;">
<title>Insert title here</title>
</head>
<body> 
     <%-- 錯誤表列 --%>
<c:if test="${not empty errorMsgs}">
	<font color='red'>請修正以下錯誤:
	<ul>
		<c:forEach var="message" items="${errorMsgs}">
			<li>${message}</li>
		</c:forEach>
	</ul>
	</font>
</c:if>
    <div id="article-ckeditor">
     <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front/front-mainx/front.do">
      <table>     	   
        <tr>
          <td id="article-ckeditor">	 
	      <textarea  id="editor1" name="repcon"  rows="10" cols="10"></textarea>  <!--  name="artcon" 設定欄位名稱 -->
          <ckeditor:replace replace="editor1" basePath="/ckeditor/" />                  
          </td>
         </tr>
         <tr>
         <td>  
   			 <input type="hidden" name="artno" value="${artVO.artno}">
			  <input type="hidden" name="memno" value="${memVO.memno}">
		     <input type="hidden" name="topno" value="${artVO.topno}">       
		     <input type="hidden" name="whichPage" value="${param.whichPage}">   <!-- 取得分頁 -->     
	         <input type="button" value="取消">	      
	         <input type="hidden" name="action" value="insertrep">
	         <input type="submit" value="送出">	        
         </td> 
       </tr>
      </table>
     </FORM>	   
   </div>	
</body>
</html>