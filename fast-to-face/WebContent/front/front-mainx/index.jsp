<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%--日期格式化標籤 --%>
<%@ page import="java.util.*"%>
<%@ page import="com.top.model.*"%>
<%@ page import="com.fro.model.*"%>

<%-- 此頁練習採用 EL 的寫法取值 --%>
<%
	TopService topSvc = new TopService();
	List<TopVO> list = topSvc.getallnewtop();
	pageContext.setAttribute("list", list);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<link rel="stylesheet " href="css/front-mainx.css">
<title>Insert title here</title>
</head>
<body><%@ include file="pages/indexpage1.file"%>
    
    <div align="center">
	<table class="table table-striped" style="font-size:16px;">
		<tr valign='middle' align='center'>
			<th>類別名稱</th>
			<th>主題</th>
			<th>作者</th>
			<th>發表日期</th>			 
		</tr>
	<!--  	<jsp:useBean id="top1Svc" scope="page" class="com.top.model.TopService" /> -->
			
		<jsp:useBean id="memSvc" scope="page" class="com.mem.model.MemService" />
		<jsp:useBean id="for1Svc" scope="page" 	class="com.fro.model.ForService" />		
		<c:forEach var="topVO" items="${list}"    begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
			<tr align='left'>
				<td><c:forEach var="forVO" items="${for1Svc.all}">
						<c:if test="${topVO.forno==forVO.forno}">						
	                   ${forVO.fortype}
                    </c:if>
					</c:forEach></td>
				<td align="left"><a href="<%=request.getContextPath()%>/front/front-mainx/front.do?action=showtop&forno=${topVO.forno}&topno=${topVO.topno}">${topVO.topname}</a>
				</td>
				<td><c:forEach var="memVO" items="${memSvc.all}">
						<c:if test="${topVO.memno==memVO.memno}">						
	                   ${memVO.memname}
                    </c:if>
					</c:forEach></td>
				<td><fmt:formatDate value="${topVO.toptime}"
				     pattern="yyyy-MM-dd HH:mm:ss" /></td>				 
			</tr>
		</c:forEach>
	</table>
  </div>
</body>
</html>