<%@page import="com.mem.model.MemService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*" %>
<%@ page import="com.cou.model.*" %>
<%@ page import="com.pro.model.*" %>
<%@ page import="com.app.model.*" %>
<%@ page trimDirectiveWhitespaces="true" %>

<%
	List<CouVO> couVOList = (List<CouVO>)request.getAttribute("couVOList");
	request.getAttribute("requestURL");
	response.setHeader("Cache-Control","no-store"); //HTTP 1.1
	response.setHeader("Pragma","no-cache");        //HTTP 1.0
	response.setDateHeader ("Expires", 0);
%>

<!DOCTYPE html>
<html>
 	<head>
     	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">	
		<title>流覽課程列表</title>
		
		<!-- front shared file -->
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/shared/image/favicon.ico">
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap2.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/front.css">
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/course_list/css/course_list.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/star-rating.css">
		<script src="<%=request.getContextPath()%>/shared/js/star-rating.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/store_record/css/jquery-ui.css">
		<script type="text/javascript" src="<%=request.getContextPath()%>/front/course_list/js/autocomplete.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/front/course_list/css/autocomplete.css">
		<script>
		$(function() {
		    var data = [ 
		      { label: "Loop", category: "Java物件導向程式語言" },
		      { label: "Array", category: "Java物件導向程式語言" },
		      { label: "Constructor", category: "Java物件導向程式語言" },
		      { label: "Canvas", category: "HTML5/CSS3網頁開發" },
		      { label: "Drag and Drop", category: "HTML5/CSS3網頁開發" },
		      { label: "CSS3", category: "HTML5/CSS3網頁開發" },
		      { label: "Google地圖", category: "Google Android APP開發" },
		      { label: "ER-Model", category: "MySQL資料庫" },
		      { label: "3NF/BCNF", category: "MySQL資料庫" }
		    ];
		 
		    $( "#Search_Keyword" ).catcomplete({
		      delay: 0,
		      source: data
		    });
		  });
		  </script>	
	</head>
	<body>
	<%@ include file="/shared/pages/front_header_nav.file"%>
		<div class="center_header">課程列表(<%= couVOList.size() %>)</div>
		<div id="list_section">
			<div>
				<%@ include file="pages/page1.file"%>
				<table  class="table table-hover">
					<c:forEach var="couVO" items="${couVOList}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>" varStatus="couStatus">
						<tr>
							<td>
								<img src="<%=request.getContextPath()%>/cou/cou.do?couno=${couVO.couno}&action=getCoupic" class="course_img" />
							</td>
							<td>
								<div class="center_title">
									<a href="<%=request.getContextPath()%>/couDetail/cou.do?couno=${couVO.couno}&action=getCouDetail">${couVO.couname}</a>
								</div>
								<div class="center_desc">${couVO.couintro}</div>
								<div>
									<input id="star" class="rating" value="${averageAppscoreList[couStatus.index]}" data-show-clear="false" data-show-caption="false" data-size="xs"  readonly/>
								</div>
							</td>
							<td>
								<c:if test="${boughtYetList[couStatus.index]}">
									<div class="center_price">已購買</div>
								</c:if>
								<c:if test="${!boughtYetList[couStatus.index]}">
									<div class="center_price">原價：<del>${couVO.couprice}</del></div>
									<div class="center_price">特價：${couVO.coudiscount}</div>
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</table>
				<%@ include file="pages/page2.file"%>
			</div>
		</div>
		<div id="category_section">
			<div class="well">
				<div id="category_title">課程分類</div>				
				<form class="form-horizontal" method="post" action="<%=request.getContextPath()%>/coulist/cou.do">
					<div class="form-group has-feedback">
						<div class="col-sm-10" style="width:240px;">
							<input type="text" class="form-control" id="Search_Keyword" name="prono" onchange="submit()" placeholder="搜尋您要的關鍵字" value="${param.couintro}" required>
							<input type="hidden" name="action" value="getCouList">
							<input type="hidden" name="requestURL" value="<%=request.getServletPath()%>" />
							<input type="hidden" name="whichPage" value="<%=whichPage%>" />	
							
							<!-- 按鈕1 -->
							<span class="glyphicon glyphicon-search form-control-feedback" onclick="submit()"></span>
							<script>
 								function submit() { 
  						   			document.getElementById("Search_Keyword")[0].submit(); 
  								}
							</script>
							
							<!-- 按鈕2 -->
<!-- 							<button class="glyphicon glyphicon-search form-control-feedback" type="submit"></button> -->
											
						</div>
					</div>
				</form>
				<table class="table">
					<c:forEach var="proVO" items="${proVOList}" varStatus="proStatus">
						<tr>
							<td>
								<div>
									<a href="<%=request.getContextPath()%>/coulist/cou.do?prono=${proVO.prono}&action=getCouList">${proVO.proname}(${couCountList[proStatus.index]})</a>
								</div>
							</td>
						</tr>
					</c:forEach>
					<tr>
						<td>
							<div>
								<a href="<%=request.getContextPath()%>/coulist/cou.do?prono=all&action=getCouList">所有課程(${couCountSum})</a>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div style="clear:both;"></div>
		<%@ include file="/shared/pages/front_footer.file"%>	
	</body>
	
</html>