<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.mem.model.*"%>
<%-- 此頁練習採用 EL 的寫法取值 --%>

<%
    MemService memSvc = new MemService();
    List<MemVO> list = memSvc.getAll();
    pageContext.setAttribute("list",list);
    
%>

<html>
<head>
<title>Fast to Face-所有會員資料 - listAllMem.jsp</title>
</head>
<body bgcolor='white'>
	<b><font color=red></font></b>
	<table>
		<tr>
			<td>
				<h3>所有會員資料 - listAllMem.jsp</h3> <a href="<%=request.getContextPath()%>/select_page.jsp"><img
					src="image/back1.gif" width="100" height="32" border="0">回首頁</a>
			</td>
		</tr>
	</table>

	<%-- 錯誤表列 --%>
	<c:if test="${not empty errorMsgs}">
		<font color='red'>請修正以下錯誤:
			<ul>
				<c:forEach var="message" items="${errorMsgs}">
					<li>${message}</li>
				</c:forEach>
			</ul>
		</font>
	</c:if>

	<table border='1' bordercolor='#CCCCFF' width='800'>
		<tr>
			<th>會員編號</th>
			<th>帳號</th>
			<th>會員姓名</th>
			<th>性別</th>
			<th>電子信箱</th>
			<th>圖片</th>
			<th>修改</th>

		</tr>
		<%@ include file="page/page1.file"%>
		<c:forEach var="memVO" items="${list}" begin="<%=pageIndex%>"
			end="<%=pageIndex+rowsPerPage-1%>">
			<tr align='center' valign='middle'}>
				<td>${memVO.memno}</td>
				<td>${memVO.memaccount}</td>
				<%-- 			<td>${memVO.mempsw}</td> --%>
				<td>${memVO.memname}</td>
				<%-- 			<td>${memVO.memgender}</td> --%>
				<td><c:choose>
						<c:when test="${memVO.memgender==0}">
								男
						</c:when>
						<c:otherwise>
							女
						</c:otherwise>
					</c:choose></td>
				<td>${memVO.mememail}</td>
				<%-- 			<td>${memVO.memstate}</td> --%>
				<td><img
					src="<%=request.getContextPath()%>/front/mem/mem.do?memno=${memVO.memno}&action=getMempic"
					width="100" height="100" /></td>
			    <td>
				<form method="post" action="<%=request.getContextPath()%>/front/mem/mem.do">
					<input type="submit" value="修改">
					<input type="hidden" name="memno"  value="${memVO.memno}">
					<input type="hidden" name="action" value="getOne_For_Update">
				</form>
				</td>
<!-- 				<td> -->
<%-- 					<form method="post" action="<%=request.getContextPath()%>/Front/mem/mem.do"> --%>
<!-- 						<input type="submit" value="刪除"> -->
<%-- 						<input type="hidden" name="memno" value="${memVO.memno}"> --%>
<!-- 						<input type="hidden" name="action" value="delete"> -->
<!-- 					</form> -->
<!-- 				</td> -->
			</tr>
		</c:forEach>
	</table>
	<%@ include file="page/page2.file"%>
</body>
</html>
