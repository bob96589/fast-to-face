<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<%@ page import="com.mem.model.*"%>
<%

MemVO memVO = (MemVO) request.getAttribute("memVO");

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title>會員資料修改</title>
</head>
<script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	document.getElementById("img2").addEventListener('change',function(){

		var file =this.files[0]; 
		
		var reader = new FileReader();
		 reader.readAsDataURL(file);
		 reader.onload = function(e) {
			 
			
			 document.getElementById("showPic").innerHTML="<img src= '"+this.result+"'' width='200px' height='200px'/>";
		 }
		
	},false);
});

</script>


<body>
	<table>
		<tr>
			<td>
				<h3>會員資料修改 - update_mem.jsp</h3>
			</td>
			<td><a href="<%=request.getContextPath()%>/select_page.jsp">回首頁</a></td>
		</tr>
	</table>
	<hr>
	<HR>
	<h3>資料修改:</h3>
<%-- 錯誤表列 --%>
<c:if test="${not empty errorMsgs}">
	<font color='red'>請修正以下錯誤:
	<ul>
		<c:forEach var="errorMsgs" items="${errorMsgs}">
			<li>${errorMsgs}</li>
		</c:forEach>
	</ul>
	</font>
</c:if>
	<HR>
	<form method="post" action="<%=request.getContextPath()%>/front/mem/mem.do" enctype="multipart/form-data">
		<table border="0">
			<tr>
				<td>會員編號:<font color=red><b>*</b></font></td>
				<td><%=memVO.getMemno()%></td>
			</tr>
			<tr>
				<td>會員帳號:<font color=red><b>*</b></font></td>
				<td><%=memVO.getMemaccount()%></td>
			</tr>
			<tr>
				<td>密碼:<font color=red><b>*</b></font></td>
				<td>
					<input type="password" name="mempsw" size="45" 
							value="<%=memVO.getMempsw()%>"/>
				</td>
			</tr>
			<tr>
				<td>會員姓名:</td>
				<td><input type="TEXT" name="memname" size="45"
						value="<%=memVO.getMemname()%>" /></td>
			</tr>
			<tr>
				<td>性別:</td>
				<td><input type="radio" name="memgender" value="0" checked />男
				<input type="radio" name="memgender" value="1" />女</td>
			</tr>
			<tr>
				<td>電子信箱:<font color=red><b>*</b></font></td>
				<td><input type="TEXT" name="mememail" size="50"
				value="<%=memVO.getMememail()%>" /></td>
			</tr>
			<tr>
				<td>圖片:</td>
				<td>
					<div><span id="showPic"><img
					src="<%=request.getContextPath()%>/front/mem/mem.do?memno=${memVO.memno}&action=getMempic" /></span></div>
					<input type="file" id="img2" name="mempic" accept="image/*" />
				</td>
			</tr>			
		</table>
		<br>
	<input type="hidden" name="action" value="update">
	<input type="hidden" name="memno" value="<%=memVO.getMemno()%>">
	<input type="submit" value="送出修改">
	</form>
</body>
</html>