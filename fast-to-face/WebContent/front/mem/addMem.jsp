<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.mem.model.*"%>
<%
MemVO memVO = (MemVO) request.getAttribute("memVO");
%>

<html>
<head>
<title>會員資料新增 - addMem.jsp</title>
</head>
<link rel="stylesheet" type="text/css" href="js/calendar.css">
<script language="JavaScript" src="js/calendarcode.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	document.getElementById("img1").addEventListener('change',function(){

		var file =this.files[0]; 
		
		var reader = new FileReader();
		 reader.readAsDataURL(file);
		 reader.onload = function(e) {
			 
			
			 document.getElementById("showPic").innerHTML="<img src= '"+this.result+"'' width='200px' height='200px'/>";
		 }
		
	},false);
});

</script>

<div id="popupcalendar" class="text"></div>

<body bgcolor='white'>

	<table>
		<tr>
			<td>
				<h3>會員資料新增 - addMem.jsp</h3>
			</td>
			<td><a href="<%=request.getContextPath()%>/select_page.jsp">回首頁</a></td>
		</tr>
	</table>

	<h3>會員資料:</h3>
	<%-- 錯誤表列 --%>
	<c:if test="${not empty errorMsgs}">
		<font color='red'>請修正以下錯誤:
			<ul>
				<c:forEach var="errorMsgs" items="${errorMsgs}">
					<li>${errorMsgs}</li>
				</c:forEach>
			</ul>
		</font>
	</c:if>

<hr>

	<FORM METHOD="post" ACTION="<%=request.getContextPath() %>/front/mem/mem.do"    enctype="multipart/form-data">
		<table border="0">
			<tr>
				<td>會員帳號:<font color=red><b>*</b></font></td>
				<td><input type="TEXT" name="memaccount" size="45"
					value="請輸入帳號" /></td>
			</tr>
			<tr>
				<td>密碼:<font color=red><b>*</b></font></td>
				<td><input type="password" name="mempsw" size="45"
					value="請輸入密碼" /></td>
			</tr>
<!-- 			<tr> -->
<!-- 				<td>確認密碼:</td> -->
<!-- 				<td><input type="TEXT" name="checkedpsw" size="45" -->
<!-- 					value=""/></td> -->
<!-- 			</tr> -->

			<tr>
				<td>會員姓名:</td>
				<td><input type="TEXT" name="memname" size="45"
					value="<%= (memVO==null)? "請輸入姓名" : memVO.getMemname()%>" /></td>
			</tr>
			<tr>
				<td>性別:</td>
				<td><input type="radio" name="memgender" value="0" checked />男
					<input type="radio" name="memgender" value="1" />女</td>
			</tr>

			<tr>
				<td>電子信箱:<font color=red><b>*</b></font></td>
				<td><input type="TEXT" name="mememail" size="50"
					value="<%= (memVO==null)? "xxx@gmail.com" : memVO.getMemname()%>" /></td>
			</tr>
			<tr>
				<td>圖片</td>     	
			    <td>
			     	<div><span id="showPic">預覽</span></div> 		
			     	<input type="file" id="img1" name="mempic" accept="image/*"/>
			    </td>
			</tr>
		</table>	
		    <input type="hidden" name="action" value="insert"> 
			 <input type="submit" value="註冊">
	</FORM>
</body>

</html>
