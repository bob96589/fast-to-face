<%@page import="com.acc.model.*"%>
<%@page import="com.pro.model.*"%>
<%@page import="java.util.List"%>
<%@page import="com.cou.model.*"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
	List<CouVO> couVOlist = (List<CouVO>)request.getAttribute("couVOlist");
	
%>


<html>
	<head>
		<title>Insert title here</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
		
	</head>
	<body>
		<%@ include file="/shared/pages/back_header_nav.file" %>		 
		<h2 class="backtitle">課程管理</h2>
		<a href="<%=request.getContextPath()%>/back/course_management/addCou.jsp?" class="btn btn-success" style="margin:10px 0px;padding:4px 10px;font-size:18px;">新增課程</a><br/>   
		<%@ include file="pages/page1.file" %> 
		<table class="table table-bordered">
			<tr class="titleBgcolor">
				<th>課程編號</th>
				<th width="300">課程名稱</th>
				<th>課程圖片</th>
				<th>徽章圖片</th>
				<th>購買點數</th>
				<th>折扣點數</th>
				<th>授課老師</th>
				<th>課程狀態</th>
				<th>所屬學程</th>
				<th>新增時間</th>
				<th>編輯</th>
				<th>刪除</th>
				
			</tr>
			
			<c:forEach var="couVO" items="${couVOlist}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
				<tr ${(couVO.couno==requestScope.couVO.couno) ? 'style="background:rgb(225,237,233)"':''}>
					<td class="number_width">${couVO.couno}</td>
					<td style="text-align:left;"><a href="<%=request.getContextPath()%>/couManagement/cou.do?couno=${couVO.couno}&action=getWholeCouForCourseManagement">${couVO.couname}</a></td>
					<td><img src="<%=request.getContextPath()%>/cou/cou.do?couno=${couVO.couno}&action=getCoupic" height="40"/></td>
					<td><img src="<%=request.getContextPath()%>/cou/cou.do?couno=${couVO.couno}&action=getCoubadge" height="40"/></td>
					<td>${couVO.couprice}</td>
					<td>${couVO.coudiscount}</td>
					<td>
						<c:forEach var="accVO" items="${accVOList}">
							<c:if test="${couVO.accno==accVO.accno}">
								${accVO.accname}
							</c:if>
						</c:forEach>
					</td>
					<td>
						<c:if test="${couVO.coustate==0}">未審核</c:if>
						<c:if test="${couVO.coustate==1}">待審核</c:if>
						<c:if test="${couVO.coustate==2}">已審核</c:if>
						<c:if test="${couVO.coustate==3}">上線中</c:if>
					</td>
					<td style="text-align:left;">
						<c:forEach var="proVO" items="${proVOList}">
							<c:if test="${couVO.prono==proVO.prono}">
								${proVO.proname}
							</c:if>
						</c:forEach>					
					</td>					
					<td><fmt:formatDate value="${couVO.couupdate}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate></td>
					<td class="button_width">
						<form method="post" action="<%=request.getContextPath()%>/couManagement/cou.do">
							<input type="submit" value="編輯"> 
							<input type="hidden" name="couno" value="${couVO.couno}">
							<input type="hidden" name="requestURL"	value="<%= request.getServletPath() %>">
							<input type="hidden" name="whichPage"	value="<%= whichPage %>">
							<input type="hidden" name="action"	value="getOne_For_Update">
						</form>
					</td>
					<td class="button_width">
			 			<form method="post" action="<%=request.getContextPath()%>/couManagement/cou.do">
			    			<input type="submit" value="刪除" onclick="javascript:return confirm('您確定要刪除嗎')"/>
						    <input type="hidden" name="couno" value="${couVO.couno}">
			 			   	<input type="hidden" name="requestURL"	value="<%= request.getServletPath() %>">
			    			<input type="hidden" name="whichPage"	value="<%= whichPage %>">
			    			<input type="hidden" name="action"value="delete">
			    		</form>
					</td>
				</tr>
			</c:forEach>
		
		</table>
		<%@ include file="pages/page2.file" %>
		<%@ include file="/shared/pages/back_footer.file" %>
	</body>
</html>