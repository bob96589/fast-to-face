<%@page import="com.cou.model.CouVO"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="proSvc" scope="page" class="com.pro.model.ProService"/>
<jsp:useBean id="accSvc" scope="page" class="com.acc.model.AccService"/>

<%
	CouVO couVO = (CouVO)request.getAttribute("couVO");

%>

<html>
	<head>
		<title>Insert title here</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
		
		<!-- self -->
		<script src="<%=request.getContextPath()%>/back/course_management/js/addCou.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/back/course_management/css/addCou.css">
		
		
	</head>
	<body>
		<%@ include file="/shared/pages/back_header_nav.file" %>
		<h2 class="backtitle">新增課程</h2>
		<ol class="breadcrumb">
		  	<li><a href="<%=request.getContextPath()%>/couManagement/cou.do?action=getCouList">課程管理</a></li>
		 	<li class="active">新增課程</li>
		</ol>  
		<c:if test="${not empty errorMsgs}">
			<font color='red'>請修正以下錯誤:
				<ul>
					<c:forEach var="message" items="${errorMsgs}">
					<li>${message}</li>
					</c:forEach>
				</ul>
			</font>
		</c:if>
		<form action="<%=request.getContextPath()%>/couManagement/cou.do?" method="post" enctype="multipart/form-data">
			<table class="borderless">
				<tr>
					<td>課程名稱</td>
					<td><input type="text" name="couname" value="<%= (couVO==null)? "": couVO.getCouname() %>" id="couname"/></td>
				</tr>
				<tr>
					<td>課程簡介</td>
					<td><textarea name="couintro" rows="10" cols="50" id="couintro"><%= (couVO==null)? "": couVO.getCouintro() %></textarea></td>
				</tr>
				<tr>
					<td>課程圖片</td>
					<td>
						<div id="previewPic" ></div>
						<input type="file" name="coupic" accept="image/*" id="uploadImage" onchange="previewPic(this)"/>
					</td>
				</tr>
				<tr>
					<td>徽章圖片</td>
					<td>
						<div id="previewBadge" ></div>
						<input type="file" name="coubadge" accept="image/*" id="uploadImage" onchange="previewBadge(this)"/>
					</td>
				</tr>
				<tr>
					<td>購買點數</td>
					<td><input type="text" name="couprice" value="<%= (couVO==null)? "": couVO.getCouprice() %>" id="couprice"/></td>
				</tr>
				<tr>
					<td>折扣點數</td>
					<td><input type="text" name="coudiscount" value="<%= (couVO==null)? "": couVO.getCoudiscount() %>" id="coudiscount"/></td>
				</tr>
				<tr>
					<td>授課老師</td>
					<td>
						<select name="accno">
							<c:forEach var="accVO" items="${accSvc.all}">
								<option value="${accVO.accno}" ${proVO.prono}" ${(accVO.accno==couVO.accno)? 'selected':'' }>${accVO.accname}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td>學程編號</td>
					<td>
						<select name="prono">
							<option value="">未分類
							<c:forEach var="proVO" items="${proSvc.all}">
								<option value="${proVO.prono}" ${(proVO.prono==couVO.prono)? 'selected':'' } >${proVO.prono}-${proVO.proname}</option>
							</c:forEach>						
						</select>
					</td>
				</tr>
				
			</table>
			<input type="submit" value="新增" />
			<button type="button" class="btn btn-warning btn-xs" id="magic_button2"><span class="glyphicon glyphicon-star"></span></button>		
			<input type="hidden" name="action" value="insert"/>
		</form>
		
		<script>
			$(function(){							
				$("#magic_button2").click(function(){									
					$("#couname").val("Android 網路服務與應用");
					$("#couintro").val("手機App採用AsyncTask方式連結Web服務以取得所需資料，並以Android UI元件呈現給使用者。為了達到跨平台需求，資料交換部分採用JSON。");
					$("#couprice").val("200");
					$("#coudiscount").val("180");
				});	
			});
		</script>
		
		<%@ include file="/shared/pages/back_footer.file" %> 
	</body>
</html>