
<%@page import="java.util.Set"%>
<%@page import="com.ass.model.AssVO"%>
<%@page import="com.content.model.*"%>
<%@page import="com.unit.model.*"%>
<%@page import="java.util.List"%>
<%@page import="com.cou.model.*"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
	<head>
		<title>Insert title here</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/back/course_management/css/couDetail.css">
	</head>
	<body>
		<%@ include file="/shared/pages/back_header_nav.file" %>		
		<h2 class="backtitle">${couVO.couname}</h2>
		<ol class="breadcrumb">
		  	<li><a href="<%=request.getContextPath()%>/couManagement/cou.do?action=getCouList">課程管理</a></li>
		 	<li class="active">${couVO.couname}</li>
		</ol>
		 
		<c:forEach var="unitVO" items="${unitVOSet}" varStatus="unitStatus">
		<table class="table table-bordered">
			<tr class="titleBgcolor">				
				<td colspan="5">
				<h2><small>單元${unitStatus.count}</small></h2>
				<h1>${unitVO.unitname}</h1>
				
				</td>
			</tr>
			<tr class="success">
				<td width="150">單元內容編號</td>
				<td width="150">類別</td>
				<td>名稱</td>
				<td width="150">排序</td>
				<td width="150"></td>			
			</tr>			
			<c:forEach var="conVO" items="${conVOSetList[unitStatus.index]}" varStatus="conStatus">
				<tr>
					<td>${conVO.conno}</td>
					<td>${conVO.contype}</td>
					<td style="text-align:left;">${conVO.conname}</td>
					<td>${conVO.conorder}</td>
					<td>
						<c:if test="${conVO.contype=='影片'||conVO.contype=='影片 '}">
							<button type="button" class="btn btn-default btn-sm" style="margin:10px 0px;padding:4px 10px;font-size:18px;" data-toggle="modal" data-target="#${conVO.conno}">查看影片</button>
							<div class="modal fade" id="${conVO.conno}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg">				
									<div class="modal-content" >
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
											<h4 class="modal-title">${conVO.conno}&nbsp-&nbsp${conVO.contype}&nbsp&nbsp${conVO.conname}</h4>
										</div>
										<div class="modal-body">
											<video  width="865" controls >
												<source src="<%=request.getContextPath()%>/shared/video/${conVO.convideo}" type="video/mp4">
											</video>
										</div>
									</div>
								</div>
							</div>
							
			    		</c:if>
			    		<c:if test="${conVO.contype=='評量'||conVO.contype=='評量 '}">
							<button type="button" class="btn btn-default btn-sm" style="margin:10px 0px;padding:4px 10px;font-size:18px;" data-toggle="modal" data-target="#${conVO.conno}">查看評量</button>
							<div class="modal fade" id="${conVO.conno}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg">				
									<div class="modal-content" >
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
											<h4 class="modal-title">${conVO.conno}&nbsp-&nbsp${conVO.contype}&nbsp&nbsp${conVO.conname}</h4>
										</div>
										<div class="modal-body">
							    			<c:forEach var="assVO" items="${assVOSetListList[unitStatus.index][conStatus.index]}" varStatus="assStatus">
							    			<table class="borderless">
							    				<tr>
							    					<th width="90">題目編號</th>
							    					<td>${assVO.assno}</td>
							    				</tr>
							    				<tr>
							    					<th>題目</th>
							    					<td>${assVO.assques}</td>
							    				</tr>
							    				<tr>
							    					<th>答案</th>
							    					<td>${assVO.assans}</td>
							    				</tr>
							    				<tr>
							    					<th>(A)</th>
							    					<td>${assVO.assansa}</td>
							    				</tr>
							    				<tr>
							    					<th>(B)</th>
							    					<td>${assVO.assansb}</td>
							    				</tr>
							    				<tr>
							    					<th>(C)</th>
							    					<td>${assVO.assansc}</td>
							    				</tr>
							    				<tr>
							    					<th>(D)</th>
							    					<td>${assVO.assansd}</td>
							    				</tr>				    			
							    			</table>
							    			<hr/>
							    			</c:forEach>
										</div>
									</div>
								</div>
							</div>
				    		
				    		
			    		</c:if>					
					</td>
				</tr>
			</c:forEach>
		</table>
		</c:forEach>
		
	<%@ include file="/shared/pages/back_footer.file" %>
	</body>
</html>