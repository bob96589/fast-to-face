function previewPic(file) {

    if (file.files && file.files[0]) {
        var reader = new FileReader();
        reader.onload = function (evt) {
            $("#previewPic").html( '<img height="80px" src="' + evt.target.result + '" />');
        }
        $("#previewPic").show();
        reader.readAsDataURL(file.files[0]);
    }
}

function previewBadge(file) {

    if (file.files && file.files[0]) {
        var reader = new FileReader();
        reader.onload = function (evt) {
            $("#previewBadge").html( '<img height="80px" src="' + evt.target.result + '" />');
        }
        $("#previewBadge").show();
        reader.readAsDataURL(file.files[0]);
    }
}