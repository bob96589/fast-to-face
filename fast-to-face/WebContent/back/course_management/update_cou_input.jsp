<%@page import="com.cou.model.CouVO"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% CouVO couVO = (CouVO)request.getAttribute("couVO"); %>
<jsp:useBean id="proSvc" scope="page" class="com.pro.model.ProService"/>
<jsp:useBean id="accSvc" scope="page" class="com.acc.model.AccService"/>

<html>
	<head>
		<title>Insert title here</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
		
		<!-- self -->
		<script src="<%=request.getContextPath()%>/back/course_management/js/update_cou_input.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/back/course_management/css/update_cou_input.css">
		
	</head>
	<body>
		<%@ include file="/shared/pages/back_header_nav.file" %>
		<h2 class="backtitle">編輯課程</h2>
		<ol class="breadcrumb">
		  	<li><a href="<%=request.getContextPath()%>/couManagement/cou.do?action=getCouList">課程管理</a></li>
		 	<li class="active">編輯課程</li>
		</ol>  
		<c:if test="${not empty errorMsgs}">
			<font color='red'>請修正以下錯誤:
				<ul>
					<c:forEach var="message" items="${errorMsgs}">
					<li>${message}</li>
					</c:forEach>
				</ul>
			</font>
		</c:if>
		<form action="<%=request.getContextPath()%>/couManagement/cou.do" method="post" enctype="multipart/form-data">
			<table class="borderless">
				<tr>
					<td>課程編號</td>
					<td><%= couVO.getCouno() %></td>
				</tr>
				<tr>
					<td>課程名稱</td>
					<td><input type="text" name="couname" value="<%= couVO.getCouname() %>"/></td>
				</tr>
				<tr>
					<td>課程簡介</td>
					<td><textarea name="couintro" rows="10" cols="50"><%= couVO.getCouintro() %></textarea></td>
				</tr>
				<tr>
					<td>課程圖片</td>
					<td>
						<div id="previewPic" >
							<img src="<%=request.getContextPath()%>/cou/cou.do?couno=<%= couVO.getCouno() %>&action=getCoupic" height="80"/>
						</div>
						<input type="file" name="coupic" accept="image/*" id="uploadImage" onchange="previewPic(this)"/>
					</td>
				</tr>
				<tr>
					<td>徽章圖片</td>
					<td>
						<div id="previewBadge" >
							<img src="<%=request.getContextPath()%>/cou/cou.do?couno=<%= couVO.getCouno() %>&action=getCoubadge" height="80"/>
						</div>
						<input type="file" name="coubadge" accept="image/*" id="uploadImage" onchange="previewBadge(this)"/>
					</td>
				</tr>
				<tr>
					<td>購買點數</td>
					<td><input type="text" name="couprice" value="<%= couVO.getCouprice() %>"/></td>
				</tr>
				<tr>
					<td>折扣點數</td>
					<td><input type="text" name="coudiscount" value="<%= couVO.getCoudiscount() %>"/></td>
				</tr>
				<tr>
					<td>授課老師</td>
					<td>
						<select name="accno">
							<c:forEach var="accVO" items="${accSvc.all}">
								<option value="${accVO.accno}" ${proVO.prono}" ${(accVO.accno==couVO.accno)? 'selected':'' }>${accVO.accname}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td>課程狀態</td>
					<td>
						<select name="coustate"   >
							<option value="0" <%= (couVO.getCoustate()==0)?"selected":"" %>>未審核</option>
							<option value="1" <%= (couVO.getCoustate()==1)?"selected":"" %>>待審核</option>
							<option value="2" <%= (couVO.getCoustate()==2)?"selected":"" %>>已審核</option>
							<option value="3" <%= (couVO.getCoustate()==3)?"selected":"" %>>上線中</option>
						</select>
					</td>				
				</tr>
				<tr>
					<td>學程編號</td>
					<td>
						<select name="prono">
							<option value="">未分類</option>
							<c:forEach var="proVO" items="${proSvc.all}">
								<option value="${proVO.prono}" ${(proVO.prono==couVO.prono)? 'selected':'' } >${proVO.prono}-${proVO.proname}</option>
							</c:forEach>						
						</select>
					</td>
				</tr>				
			
			</table>
			<input type="submit" value="儲存" />
			<input type="hidden" name="action" value="update"/>
			<input type="hidden" name="couno" value="<%= couVO.getCouno() %>" />					
			<input type="hidden" name="requestURL" value="${requestURL}" />					
			<input type="hidden" name="whichPage" value="${whichPage}" />
		</form>
	<%@ include file="/shared/pages/back_footer.file" %>
	</body>
</html>