<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.top.model.*"%>
<%@ page import="com.fro.model.*"%>
<%@ page import="com.mem.model.*"%>
<%@ page import="com.art.model.*"%>
<%@ page import="com.rep.model.*"%> 
<html>
<head>

<!-- back shared file -->
<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>

<!-- self --> 
<link rel="stylesheet " href="css/back-mainx.css">

<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title>管理主題類別</title>

</head>
<body>
<%@ include file="/shared/pages/back_header_nav.file" %>
<h2 class="backtitle">主題類別管理</h2>  	
  
		
				 
			<%-- 錯誤表列 --%>
			<c:if test="${not empty errorMsgs}">
				<font color='red'>請修正以下錯誤:
					<ul>
						<c:forEach var="message" items="${errorMsgs}">
							<li>${message}</li>
						</c:forEach>
					</ul>
				</font>
			</c:if>
			 
			<table id="mainx-table2" class="table table-bordered">
				<tr class="titleBgcolor">					 
					<th style="text-align:center">主題類別名稱</th>
					<th style="text-align:center">修改</th>					 
					<th style="text-align:center">查詢</th>
				</tr>
				<jsp:useBean id="forSvc" scope="page" class="com.fro.model.ForService" />
				<c:forEach var="forVO" items="${forSvc.all}">
					<tr align='center' valign='middle'>						 
						<td>${forVO.fortype}</td>
						<td class="button_width">
						 <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/back/back-mainx/back.do"  >
			             <input type="submit" value="修改" >
			             <input type="hidden" name=forno value="${forVO.forno}">
			             <input type="hidden" name="action"	value="getOneFor"></FORM>			 						 			  
						</td>						 
						<td class="button_width">
							<FORM METHOD="post"
								ACTION="<%=request.getContextPath()%>/back/back-mainx/back.do">
								<input type="submit" value="查詢"> 
								<input type="hidden" name="forno" value="${forVO.forno}">
							    <input type="hidden" name="action" value="showtop">							
							</FORM>							
						</td>
					</tr>
				</c:forEach>
			</table>
			
	 	</div>
	</div>
    <div  >
    <%  if (request.getAttribute("list") != null) {  %>
		<jsp:include page="back-topic.jsp" />
	<%   }  %>   
    </div>	 
    <div >   
    <%  if (request.getAttribute("forVO") != null && request.getAttribute("topVO") == null && request.getAttribute("memVOlist") == null) {  %>
		<jsp:include page="back-upforum.jsp" />
	<%   }  %>
    </div>   <!-- 修改類別名稱 -->
    
  
<%@ include file="/shared/pages/back_footer.file" %>
</body>
</html>