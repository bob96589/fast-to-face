<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%-- 此頁練習採用 EL 的寫法取值 --%>
<%@ page import="java.util.*"%>
<%@ page import="com.top.model.*"%>
<%@ page import="com.fro.model.*"%>
<%@ page import="com.mem.model.*"%>
<%@ page import="com.art.model.*"%>
<%@ page import="com.rep.model.*"%>
 
<%  
    RepService repSvc = new RepService();
  	List<RepVO> list = repSvc.getAll();
  	pageContext.setAttribute("list", list);
  	
  	ArtService artSvc = new ArtService();
%>     

<%
	MemService memSvc = new MemService();
	List<MemVO> memVOlist = memSvc.getAll();
	pageContext.setAttribute("memVOlist", memVOlist);    
%>
<html>
<head>

<!-- back shared file -->
<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>

<!-- self -->   
<link rel="stylesheet " href="css/back-mainx.css">

<title>管理檢舉清單</title>
</head>
<body bgcolor='white'>
<%@ include file="/shared/pages/back_header_nav.file" %>
<h2 class="backtitle">檢舉文章管理</h2>  
  <%-- 錯誤表列 --%>
	<c:if test="${not empty errorMsgs}">
		<font color='red'>
			<ul>
				<c:forEach var="message" items="${errorMsgs}">
					<li>${message}</li>
				</c:forEach>
			</ul>
		</font>
	</c:if>  
  <div  align="center">      	
	<div align="center" > 
	<%@ include file="pages/page1.file"%> 		    		
	<table class="table table-bordered">
		<tr class="titleBgcolor">
			<th class="number_width">文章編號</th>     	 
			<th class="text_width">檢舉人</th>			
			<th>檢舉原因</th>
			<th>檢舉日期</th>
			<th class="text_width">文章狀態</th>
			<th class="button_width">移除</th>
			<th class="button_width">恢復</th>	
			<th class="button_width">停權</th>		
		</tr>	           
 		      <c:forEach var="repVO1" items="${list}"  begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
			   <tr align='center'   ${(repVO1.artno==param.artno) ? 'style="background:rgb(225,237,233)"':''}><!--將修改的那一筆加入對比色而已-->
			    <td style="vertical-align:middle">
			         <a href="<%=request.getContextPath()%>/back/back-mainx/back.do?whichPage=<%=whichPage%>&artno=${repVO1.artno}&action=getOneArtByRep">${repVO1.artno}</a>							
				</td>	 	
				<td style="vertical-align:middle"><c:forEach var="memVO" items="${memVOlist}">
				    <c:if test="${repVO1.memno==memVO.memno}">${(memVO.memname==null)? memVO.memaccount : memVO.memname}</c:if></c:forEach>
				</td>
				<td id="topic-table2" style="vertical-align:middle">${repVO1.repcon}</td>				
				<td class="time_width"><fmt:formatDate value="${repVO1.reptime}"  pattern="yyyy-MM-dd HH:mm:ss" />
				</td>
				<c:set var="artno" value="${repVO1.artno}" />
				<% 
					Integer artno = (Integer)pageContext.getAttribute("artno");
					ArtVO artVO2 = artSvc.getOneArtByRep(artno);   //根據編號搜尋該筆文章資料
					pageContext.setAttribute("artVO2", artVO2);					
				%>				
				<td style="vertical-align:middle" >
				     <c:if test="${artVO2.artstate==0}">正常</c:if>
				  	 <c:if test="${artVO2.artstate==1}">待處理</c:if>			      
				     <c:if test="${artVO2.artstate==2}">已移除</c:if>
				</td>																								   					
				<td>
				   <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/back/back-mainx/back.do"  >				  
				   <input type="hidden" name="whichPage" value="<%=whichPage%>">
				   <input type="hidden" name="artno" value="${repVO1.artno}">
				   <input type="hidden" name="topno" value="${artVO2.topno}">				   
				   <input type="hidden" name="action" value="deleteRepord">
	               <input type="submit" value="移除" onclick="javascript:return confirm('您確定要移除嗎')"></FORM>					 																					   					
				</td>
				<td>
				   <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/back/back-mainx/back.do"  >				  
				   <input type="hidden" name="whichPage" value="<%=whichPage%>">
				   <input type="hidden" name="artno" value="${repVO1.artno}">
				   <input type="hidden" name="topno" value="${artVO2.topno}">			   
				   <input type="hidden" name="action" value="updaterep">
	               <input type="submit" value="恢復"  ></FORM>					 																					   					
				</td>
				<td>
				  <%@ include file="pages/fancyboxUpdate.file" %> <!-- 鎖帳號燈箱 -->
				      <c:forEach var="memVO2" items="${memVOlist}">
				          <c:if test="${artVO2.memno==memVO2.memno}">	
							<form method="post" action="<%=request.getContextPath()%>/memManagement/mem.do">
								<table class="borderless">
									<tr>
										<td>會員編號</td>
										<td>${memVO2.memno}</td>
									</tr>
									<tr>
										<td>會員名稱</td>
										<td>${memVO2.memname}</td>
									</tr>			
									<tr>
										<td>討論區狀態</td>
										<td>
										<input type="radio" name="memstate" size="45" value="1" required />正常 
										<input type="radio" name="memstate" size="45" value="2" required />討論區停權																				
<!-- 										<select name="memstate"> -->
<!-- 											<option value="0">未驗證</option> -->
<!-- 											<option value="1">正常</option> -->
<!-- 											<option value="2">討論區停權</option> -->
<!-- 											<option value="3">帳號停權</option> -->
<!-- 										</select>									 -->
										</td>
									</tr>									
									<tr>
										<td>會員停權原因</td>
										<td><textarea rows="5" cols="44" name="memreason" placeholder="請輸入詳細停權原因"
											value="${memVO2.memreason}" class="form-control"  ></textarea></td>
									</tr>								
								</table>
								<input type="submit" vlaue="修改">
								<input type="hidden" name="memno" value="${memVO2.memno}">
								<input type="hidden" name="action" value="memState">
								<input type="hidden" name="requestURL" value="<%=request.getAttribute("requestURL")%>" /> 
								<input type="hidden" name="whichPage" value="<%=request.getAttribute("whichPage")%>" />
							    <button type="button" class="btn btn-warning btn-xs" id="rep_button"><span class="glyphicon glyphicon-star"></span></button>
							</form>
					   </c:if>
					</c:forEach>	
				</td>	
			 </tr>	
		   </c:forEach>		 
	</table>
	<script>
	 $(function(){							
     $("#rep_button").click(function(){					
     $(".form-control").val("發文不當!");
       });
            });
	</script>
	<%@ include file="pages/page2.file"%>
	</div>
	 <div>
     <%  if (request.getAttribute("artVO") != null) {  %>      
       <table class="table table-bordered left">  <!-- 檢舉清單中的文章內容 -->
        <tr  class="titleBgcolor">
		  <td  height="80px">
		    <c:forEach var="memVO" items="${memVOlist}">						
			  <c:if test="${artVO.memno==memVO.memno}">
		    <table  >
		      <tr>
		        <td rowspan="3"><img src="<%=request.getContextPath()%>/front/mem/mem.do?memno=${memVO.memno}&action=getMempic" width="100"  /></td> 
			  </tr>
			  <tr>
			    <td>	
				作者:${(memVO.memname==null)? memVO.memaccount : memVO.memname}				
				</td> 
			  </tr>
			  <tr>	
				<td>
				發文時間:<fmt:formatDate value="${artVO.artasktime}"  pattern="yyyy-MM-dd HH:mm:ss" />				 
		        </td>
		     </tr>
		   </table>
		     </c:if>
		   </c:forEach>
		  </td>
		</tr>
		<tr>
		  <td>
		  	<div style="min-height:400px; vertical-align:top;">
				${artVO.artcon}
			</div>
		  </td>
	    </tr>       
	  </table>		  
	<%   }  %>   
    </div>
   </div>
<%@ include file="/shared/pages/back_footer.file" %> 	
</body>
</html>
