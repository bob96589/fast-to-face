<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor" %>

<%--日期格式化標籤 --%>
<%@ page import="java.util.*"%>
<%@ page import="com.top.model.*"%>
<%@ page import="com.fro.model.*"%>
<%@ page import="com.mem.model.*"%>
<%@ page import="com.art.model.*"%>
<%@ page import="com.rep.model.*"%>
<jsp:useBean id="arttime" scope="page" class="com.top.model.TopService"/>
<%-- 此頁練習採用 EL 的寫法取值 --%>
<%
 	List<TopVO> list = (List<TopVO>)request.getAttribute("list"); 			
%>
<%
	MemService memSvc = new MemService();
	List<MemVO> memVOlist = memSvc.getAll();
	pageContext.setAttribute("memVOlist", memVOlist);    
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title>管理主題類別-主題列表</title>      <!--  管理主題類別-點按主題類別查詢到所有主題文章-->
</head>
<body>
	<div id="back-topic-div" align="center">
	<div id="content_section" >
	 <div id="inside_content_2">		
		<table id="topic-table1" class="table table-bordered" >
		    <tr><td colspan="5"><%@ include file="pages/top-page1.file"%></td></tr>
			<tr valign='middle' align="char" class="titleBgcolor">
				<th>主題</th>
				<th>回覆數</th>
				<th>作者</th>
				<th>最新回應</th>				 
				<th class="button_width">功能</th>
			</tr>
			
			<c:forEach var="topVO" items="${list}" begin="<%=pageIndex%>"
				end="<%=pageIndex+rowsPerPage-1%>" varStatus="s">			 			  
				<tr align='center'>
					<td id="topic-table2">
						<a href="<%=request.getContextPath()%>/back/back-mainx/back.do?forvo=${forVO.forno}&topno=${topVO.topno}&action=showart">${topVO.topname}</a>					 
					</td>
					<jsp:useBean id="topService" scope="page" class="com.top.model.TopService"/>
					<td>${topService.getShowfor2(topVO.topno)}</td>
					<td class="time_width">
						<fmt:formatDate value="${topVO.toptime}"  pattern="yyyy-MM-dd HH:mm:ss" /><br>						
						<c:forEach var="memVO" items="${memVOlist}">						
							<c:if test="${topVO.memno==memVO.memno}">
								${(memVO.memname==null)? memVO.memaccount : memVO.memname}
							</c:if>
						</c:forEach>
					</td>
					<td class="time_width">
						<fmt:formatDate value="${arttime.getShowArtTime(topVO.topno).artasktime}"  pattern="yyyy-MM-dd HH:mm:ss" /><br> 							
						<c:forEach var="memVO" items="${memVOlist}">						
							<c:if test="${arttime.getShowArtTime(topVO.topno).memno==memVO.memno}">
							${(memVO.memname==null)? memVO.memaccount : memVO.memname}
							</c:if>
						</c:forEach>																					   					
					</td>
					<td>
					  <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/back/back-mainx/back.do">								
						<input type="submit" value="移除" onclick="javascript:return confirm('您確定要移除嗎')" style="margin-top:10px">
						<input type="hidden" name="whichPage" value="<%=whichPage%>">
						<input type="hidden" name="topno" value="${topVO.topno}">
						<input type="hidden" name="forno" value="${forVO.forno}">
						<input type="hidden" name="action" value="deletetop">									
					  </FORM>	<!-- 移除主題文章 -->
					</td>
				</tr>
		    
		  </c:forEach>
		</table>
		<div id="go1">
			<%@ include file="pages/top-page2.file"%>
		</div>
	</div>
	</div>
    </div>

</body>
</html>