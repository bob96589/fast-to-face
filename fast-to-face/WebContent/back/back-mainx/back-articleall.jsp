<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%-- 此頁練習採用 EL 的寫法取值 --%>
<%@ page import="java.util.*"%>
<%@ page import="com.top.model.*"%>
<%@ page import="com.fro.model.*"%>
<%@ page import="com.mem.model.*"%>
<%@ page import="com.art.model.*"%>
<%@ page import="com.rep.model.*"%>

  
<%  
    TopService topSvc = new TopService();     //查詢所有的主題文章並依照最新發表主題時間排列
  	List<TopVO> list = topSvc.getAllTime();
  	pageContext.setAttribute("list", list);
%>
<%
	MemService memSvc = new MemService();       //查詢所有的會員
	List<MemVO> memVOlist = memSvc.getAll();
	pageContext.setAttribute("memVOlist", memVOlist);    
%>
 
<html>
<head>

<!-- back shared file -->
<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>

<!-- self --> 
<link rel="stylesheet " href="css/back-mainx.css">

<title>管理主題列表</title>
</head>
<body bgcolor='white'>
<%@ include file="/shared/pages/back_header_nav.file" %>
<h2 class="backtitle">主題列表管理</h2>     
    <%-- 錯誤表列 --%>
	<c:if test="${not empty errorMsgs}">
		<font color='red'>請修正以下錯誤:
			<ul>
				<c:forEach var="message" items="${errorMsgs}">
					<li>${message}</li>
				</c:forEach>
			</ul>
		</font>
	</c:if>	
	<div align="center" > 	 
	<%@ include file="pages/page1.file"%> 
	<table class="table table-bordered">
		<tr valign='middle'  class="titleBgcolor" >	     	 
			<th>主題</th>
			<th>回覆數</th>
			<th>作者</th>
			<th>最新回應</th>
		</tr>	   
		<c:forEach var="topVO" items="${list}"  begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
			<tr align='center' >			     
				<td id="topic-table2" style="vertical-align:middle">
					<a href="<%=request.getContextPath()%>/back/back-mainx/back.do?topno=${topVO.topno}&action=showart">${topVO.topname}</a>
				</td>
				<jsp:useBean id="topService" scope="page" class="com.top.model.TopService"/>
				<td class="number_width">${topService.getShowfor2(topVO.topno)}</td>
				<td class="time_width">
					<fmt:formatDate value="${topVO.toptime}"  pattern="yyyy-MM-dd HH:mm:ss" /><br>						
					<c:forEach var="memVO" items="${memVOlist}">						
						<c:if test="${topVO.memno==memVO.memno}">
							${(memVO.memname==null)? memVO.memaccount : memVO.memname}
						</c:if>
					</c:forEach>
				</td>									   					
				<td class="time_width">
					<jsp:useBean id="arttime" scope="page" class="com.top.model.TopService"/>
					<fmt:formatDate value="${arttime.getShowArtTime(topVO.topno).artasktime}"  pattern="yyyy-MM-dd HH:mm:ss" /><br> 							
					<c:forEach var="memVO" items="${memVOlist}">						
						<c:if test="${arttime.getShowArtTime(topVO.topno).memno==memVO.memno}">
							${(memVO.memname==null)? memVO.memaccount : memVO.memname}
						</c:if>
					</c:forEach>																					   					
				</td>
			</tr>
		</c:forEach>	 
	</table>
	<%@ include file="pages/page2.file"%>	
	</div>	 			 
<%@ include file="/shared/pages/back_footer.file" %>
</body>
</html>
