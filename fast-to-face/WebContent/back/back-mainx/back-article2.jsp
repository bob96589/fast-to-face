<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%-- 此頁練習採用 EL 的寫法取值 --%>
<%@ page import="java.util.*"%>
<%@ page import="com.top.model.*"%>
<%@ page import="com.fro.model.*"%>
<%@ page import="com.mem.model.*"%>
<%@ page import="com.art.model.*"%>
<%@ page import="com.rep.model.*"%>

<%--     
<%  

	Integer topno = new Integer(request.getParameter("topno"));
	TopService topSvc = new TopService();
	TopVO topVO = topSvc.getOnetop(topno);
	pageContext.setAttribute("topVO", topVO);
	
%>
<%
	MemService memSvc = new MemService();
	List<MemVO> memVOlist = memSvc.getAll();
	pageContext.setAttribute("memVOlist", memVOlist);
%>

<%
	TopService artSvc = new TopService();
	List<ArtVO> artVOlist = artSvc.getShowfor(topno);
	pageContext.setAttribute("artVOlist", artVOlist);
%>

<%  
      TopService artpage = new TopService(); 			
    List <ArtVO>  artVO = artpage.getShowfor(topno);
    pageContext.setAttribute("artVO", artVO);
%> --%>
<%
 	List<TopVO> artVOlist = (List<TopVO>)request.getAttribute("artVOlist"); 			
%>

<html>
<head>

 
<link rel="stylesheet " href="css/back-mainx.css">
<title>管理主題類別-主題列表-文章列表</title>  <!--未使用XXX 管理主題類別-點按主題名稱查詢到該主題下之文章-->
</head>
<body bgcolor='white'>          	
	<%-- 錯誤表列 
	<c:if test="${not empty errorMsgs}">
		<font color='red'>請修正以下錯誤:
			<ul>
				<c:forEach var="message" items="${errorMsgs}">
					<li>${message}</li>
				</c:forEach>
			</ul>
		</font>
	</c:if>    --%>	 	 	  
    <div  align="center" >
       <div id="back-button">
			<table id="mainx-table1">
				<tr>
					<td><a
						href="<%=request.getContextPath()%>/back/back-mainx/back-mainx.jsp">管理主題類別</a></td>
					<td><a
						href="<%=request.getContextPath()%>/back/back-mainx/back-articleall.jsp">管理主題列表</a></td>
					<td><a
						href="<%=request.getContextPath()%>/back/back-mainx/back-report.jsp">管理檢舉清單</a></td>
				</tr>
			</table>
		</div>
    <table >
	   <tr>
	     <td ><%@ include file="pages/article-page1.file"%>
	     </td>
	     <td></td>
	   </tr>
	</table> 
	<table   border='1'  style="width:800px" class="table table-bordered" >						
		<tr align='center' class="active">
		  	<td id="article-table1" ><主題>${topVO.topname} 
		  	</td>
		</tr>			
		<c:forEach var="artVO" items="${artVOlist}"  begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">																
		<tr class="titleBgcolor">
		   <td>
			 <c:forEach var="memVO" items="${memVOlist}">						
			  <c:if test="${artVO.memno==memVO.memno}">
			   <table>		    
			   <tr>
			     <td rowspan="3">
			       <img src="<%=request.getContextPath()%>/front/mem/mem.do?memno=${memVO.memno}&action=getMempic" width="100"  /> 
				 </td>
			   </tr>
			   <tr>
				 <td>
			    	 作者:${(memVO.memname==null)? memVO.memaccount : memVO.memname} 					
				 </td>
			   </tr>
			   <tr>
				<td>
				發文時間:<fmt:formatDate value="${artVO.artasktime}"  pattern="yyyy-MM-dd HH:mm:ss" />
				<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/back/back-mainx/back.do"  >
				   <input type="hidden" name="topno" value="${topVO.topno}">    									
				   <input type="hidden" name=artno value="${artVO.artno}">
				   <input type="hidden" name="action" value="deleteart">
	               <input type="submit" value="移除" onclick="javascript:return confirm('您確定要移除嗎')"></FORM>
			    </td>
			   </tr>
			  </table>
			 </c:if>
			</c:forEach>
		   </td>
		</tr>
		<tr id="article-table2" >
			<td  >
				${artVO.artcon}
		    </td>
		</tr>    		   									   			
		</c:forEach>	 
	</table>
	<%@ include file="pages/articleall-page2.file"%>	 
   </div>      
</body>
</html>
