<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%-- 此頁練習採用 EL 的寫法取值 --%>
<%@ page import="java.util.*"%>
<%@ page import="com.top.model.*"%>
<%@ page import="com.fro.model.*"%>
<%@ page import="com.mem.model.*"%>
<%@ page import="com.art.model.*"%>
<%@ page import="com.rep.model.*"%>

<%
 	List<ArtVO> artVOlist = (List<ArtVO>)request.getAttribute("artVOlist"); 			
%>   	   

	
<html>
<head>

<!-- back shared file -->
<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>

<!-- self --> 
<link rel="stylesheet " href="css/back-mainx.css">

<title>管理主題列表-文章列表</title>    <!--  管理主題類別-主題列表與管理主題列表-點按主題名稱查詢到該主題下之文章-->
</head>
<body bgcolor='white'>
<%@ include file="/shared/pages/back_header_nav.file" %>           	

    <div  align="center" >
    <table>
	   <tr>
	     <td ><%@ include file="pages/article-page1.file"%>
	     </td>
	     <td></td>
	   </tr>
	</table> 
	<table class="table table-bordered left" >						
		<tr align='center'>
		  	<td id="article-table1" ><主題>${topVO.topname} 
		  	</td>
		</tr>			
		<c:forEach var="artVO" items="${artVOlist}"  begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">																
		<tr align="char" class="titleBgcolor">
			<td>
			  <c:forEach var="memVO" items="${memVOlist}">
			  <c:if test="${artVO.memno==memVO.memno}">
			  <table>			  
		        <tr> 		         	
		         <td rowspan="3">
			       <img src="<%=request.getContextPath()%>/front/mem/mem.do?memno=${memVO.memno}&action=getMempic" width="100"  />
				 </td>
				</tr> 
				<tr>
			 	 <td>			    	 					
			    	  作者:${(memVO.memname==null)? memVO.memaccount : memVO.memname}					 
				 </td>
				</tr>				
				<tr>
				 <td>
			    	 發文時間:<fmt:formatDate value="${artVO.artasktime}"  pattern="yyyy-MM-dd HH:mm:ss" />
				    <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/back/back-mainx/back.do"  >
				    <input type="hidden" name="topno" value="${topVO.topno}">
				    <input type="hidden" name="artno" value="${artVO.artno}">		     
				   <input type="hidden" name="whichPage" value="<%=whichPage%>">
				   <input type="hidden" name="action" value="deleteart">
	               <input type="submit" value="移除" onclick="javascript:return confirm('您確定要移除嗎')"></FORM>
			     </td>
			    </tr>
			   </table>
			  </c:if>
			     	</c:forEach>  
			</td>
		</tr>
		<tr>
			<td >
			  <div style="min-height:400px">
				${artVO.artcon}
			  </div>
		    </td>
		</tr>    		   									   			
		</c:forEach>	 
	</table>
	<%@ include file="pages/article-page2.file"%>	 	
   </div>
<%@ include file="/shared/pages/back_footer.file" %>      
</body>
</html>
