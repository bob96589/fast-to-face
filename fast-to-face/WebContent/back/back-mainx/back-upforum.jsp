<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page import="com.top.model.*"%>
<%@ page import="com.fro.model.*"%>
<%@ page import="java.util.*"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title>管理主題類別-類別修改</title>
</head>
<body>
     <div id="content_section" >
	 <div id="inside_content_2">
	 <div style="border:solid 1px #bbb;">
			<%-- 錯誤表列 --%>
			<c:if test="${not empty errorMsgs}">
				<font color='red'>請修正以下錯誤:
					<ul>
						<c:forEach var="message" items="${errorMsgs}">
							<li>${message}</li>
						</c:forEach>
					</ul>
				</font>
			</c:if>
			<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/back/back-mainx/back.do"  >				
				<table class="borderless">
					<tr>
						<td colspan="2"><h3>主題類別修改：</h3></td>
					</tr>		
					<tr>
						<td>類別編號</td>
						<td>${forVO.forno}<font color=red><b>*</b></font></td>
					    
					</tr>				 					 
					<tr>
						<td>主題名稱</td>
						<td>
						 	<input type="TEXT" name="fortype" size="45" value= "${forVO.fortype}" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="hidden" name="action" value="update">			    				
						    <input type="hidden" name="forno" value="${forVO.forno}">				 
						    <input type="submit" value="送出修改">
					 	</td>
					</tr>					 				 
				 </table>
			 </FORM>
	 </div>
     </div>
     </div>
</body>
</html>