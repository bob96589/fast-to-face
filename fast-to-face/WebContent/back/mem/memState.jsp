<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.mem.model.*" %>
<html>
<head>
<%
	MemService memSvc = new MemService();
	List<MemVO> memList = memSvc.getAll();
	pageContext.setAttribute("memList",memList);
	List<MemVO> memList1 = memSvc.getAll();
	
	List<String> newList = new LinkedList<String>();
	for(MemVO alist : memList1){
		String reason = alist.getMemreason();
		String x = (reason==null)? "尚未被停權":reason;
		newList.add(x);
	}
	
	pageContext.setAttribute("newList",newList);
%>

<meta http-equiv="Content-Type" content="text/html; charset=BIG5">
<title>會員資料管理</title>
<!-- back shared file -->
<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
<!-- self -->

</head>
<body>
<%@ include file="/shared/pages/back_header_nav.file"%>
<h2 class="backtitle">會員資料管理</h2> 

		<%-- 錯誤表列 --%>
		<c:if test="${not empty errorMsgs}">
			<font color='red'>請修正以下錯誤:
				<ul>
					<c:forEach var="message" items="${errorMsgs}">
						<li>${message}</li>
					</c:forEach>
				</ul>
			</font>
		</c:if>
		<table class="table table-bordered">
			<tr class="titleBgcolor">
				<th>會員編號</th>
				<th>會員名稱</th>
				<th>會員停權狀態</th>
				<th>會員停權原因</th>
				<th>會員停權時間</th>
				<th>修改</th>
			</tr>
			<%@ include file="pages/memStatePage1.file"%>
			<% int j=0; %>
				<c:forEach var="memVO" items="${memList}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
					<tr align='center' valign='middle'
						${(memVO.memno==requestScope.memVO.memno) ? 'style="background:rgb(225,237,233)"':''} bgcolor='#ffffff'>
						<td class="number_width">${memVO.memno}</td>
						<td class="text_width">${memVO.memname}</td>
<%-- 						<td><c:choose> --%>
<%-- 							<c:when test="${memVO.memstate==0}">未驗證</c:when> --%>
<%-- 							<c:when test="${memVO.memstate==1}">正常</c:when> --%>
<%-- 							<c:when test="${memVO.memstate==2}">討論區停權</c:when> --%>
<%-- 							<c:otherwise>帳號停權</c:otherwise>	 --%>
<%-- 						</c:choose></td>	 --%>
						<td class="text_width">
							<c:if test="${memVO.memstate==0}">未驗證</c:if>
							<c:if test="${memVO.memstate==1}">正常</c:if>
							<c:if test="${memVO.memstate==2}">討論區停權</c:if>
							<c:if test="${memVO.memstate==3}">帳號停權</c:if>
						</td>
						<td style="text-align: left;"><%= newList.get(j) %></td>
						<td class="time_width">
							<fmt:formatDate value="${memVO.memtime}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate>
						</td>
						<td class="button_width">
							<%@ include file="pages/fancyboxUpdate.file" %>
							<form method="post" action="<%=request.getContextPath()%>/memManagement/mem.do">
								<table class="borderless" style="width:100%;">
									<tr>
										<td>會員編號</td>
										<td>${memVO.memno}
<%-- 										<input type="hidden" name="memno" value="${memVO.memno}"> --%>
										</td>
									</tr>
									<tr>
										<td>會員名稱</td>
										<td>${memVO.memname}</td>
									</tr>			
									<tr>
										<td>會員停權狀態</td>
										<td>
										<input type="radio" name="memstate" size="45" value="1" required />正常&nbsp;&nbsp;&nbsp;
										<input type="radio" name="memstate" size="45" value="2" required />討論區停權&nbsp;&nbsp;&nbsp;
										<input type="radio" name="memstate" size="45" value="3" required />帳號停權
										
<!-- 										<select name="memstate"> -->
<!-- 											<option value="0">未驗證</option> -->
<!-- 											<option value="1">正常</option> -->
<!-- 											<option value="2">討論區停權</option> -->
<!-- 											<option value="3">帳號停權</option> -->
<!-- 										</select>									 -->
										</td>
									</tr>
									
									<tr>
										<td>會員停權原因</td>
										<td><textarea rows="5" name="memreason" placeholder="請輸入詳細停權原因"
											value="${memVO.memreason}" class="form-control" required></textarea></td>
									</tr>
									
								</table>
								<input type="submit" vlaue="修改">
								<input type="hidden" name="memno" value="${memVO.memno}">
								<input type="hidden" name="action" value="memState">
								<input type="hidden" name="requestURL" value="<%=request.getAttribute("requestURL")%>" /> 
								<input type="hidden" name="whichPage" value="<%=request.getAttribute("whichPage")%>" />
							</form>
						</td>					
					</tr>
					<% j++; %>
				</c:forEach>
<%@ include file="pages/memStatePage2.file"%>
<%@ include file="/shared/pages/back_footer.file"%>
</body>
</html>