<%@page import="java.util.Set"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page import="com.pro.model.*"%>
<%@page import="java.util.List"%>
<% 	
	Set<ProVO> proVOSet = (Set<ProVO>)request.getAttribute("proVOSet");
%>



<html>
	<head>
		<title>Insert title here</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
		
		<!-- self -->
		
		
	</head>
	<body>
		<%@ include file="/shared/pages/back_header_nav.file" %>
		<h2 class="backtitle">熱門學程管理</h2>  
		<form method="post" action="<%=request.getContextPath()%>/hotProManagement/pro.do">
			<table class="table table-bordered">
				<tr class="titleBgcolor">
					<th class="number_width">學程編號</th>
					<th width="400">學程名稱</th>
					<th>學程圖片</th>				
					<th>新增時間</th>
					<th>熱門學程展示</th>				
				</tr>
				<%@ include file="pages/page1.file" %> 
				<c:forEach var="proVO" items="${proVOSet}"  begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
					<c:set var="p" value="${proVO}"/>			
					<tr ${(proVO.prono==requestScope.proVO.prono) ? 'bgcolor=#CCCCFF':''}>
						<td>${proVO.prono}</td>
						<td style="text-align:left;">${proVO.proname}</td>
						<td><img src="<%=request.getContextPath()%>/pro/pro.do?prono=${proVO.prono}&action=getPropic" width="150"/></td>
						<td><fmt:formatDate value="${proVO.proupdate}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate></td>					
						<td>
							<input type="radio" name="${proVO.prono}" value="0" ${(proVO.proishot==0) ? 'checked':''}/>未展示
							<input type="radio" name="${proVO.prono}" value="1" ${(proVO.proishot==1) ? 'checked':''}/>展示中							
						</td>										
					</tr>				
				</c:forEach>
			</table>
			<input type="submit" value="儲存"/>
			<input type="hidden" name="action" value="update_hot"/>										
			<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>							
			<input type="hidden" name="whichPage" value="<%= whichPage %>"/>
		</form>
		<%@ include file="pages/page2.file" %>
		<%@ include file="/shared/pages/back_footer.file" %>
	</body>
</html>