<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page trimDirectiveWhitespaces="true"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<body>
<table border='6' bordercolor='#0080c0' width='800' style="border-style: double;" class="table table-hover">
	<tr bgcolor='#e6f0e3' height="50">
		<th class="text-center">廣告編號</th>
		<th class="text-center">廣告名稱</th>
		<th class="text-center">廣告狀態</th>
		<th class="text-center">廣告圖片</th>
		<th class="text-center">廣告描述</th>
<!-- 		<th class="text-center">廣告課程名稱</th> -->
	</tr>
	<tr align='center' valign='middle' bgcolor='#ffffff'>
		<td width="70">${advVO.advno}</td>
		<td width="110">${advVO.advformat}</td>
		<td width="75">${(advVO.advisplay==0)?"停播":"輪播"}</td>
		<td><img height="150" width="300" id="box" class="img-rounded"
			src="<%=request.getContextPath()%>/adv/adv.do?advno=${advVO.advno}&action=getAdvpic"></td>
		<td width="350" align='left'>${advVO.advdescription}</td>
<!-- 		s	 -->
	</tr>
</table>
</body>
</html>