<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.adv.model.*"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%
	response.setBufferSize(64000);
	AdvVO advVO = (AdvVO) request.getAttribute("advVO");

	AdvService advSvc = new AdvService();
	List<AdvVO> allList = advSvc.getAll();
	pageContext.setAttribute("allList", allList);
	List<AdvVO> isPlayList =allList;
	pageContext.setAttribute("isPlayList", isPlayList);
	List<AdvVO> noPlayList = allList;
	pageContext.setAttribute("noPlayList", noPlayList);
	List<AdvVO> showList = allList;
	pageContext.setAttribute("showList", showList);
	List<AdvVO> photoList =allList;
	pageContext.setAttribute("photoList", photoList);
	int count3 = 0;
	int count4 = 0;
%>
<jsp:useBean id="couSvc" scope="page" class="com.cou.model.CouService"/>
<c:set var="allCouList" value="${couSvc.allByCouno}"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>廣告輪播管理</title>
<!-- back shared file -->
<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">

<!-- self -->
<script src="<%=request.getContextPath()%>/back/adv/js/previewPic.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/back/adv/css/jquery-ui.css">
<script src="<%=request.getContextPath()%>/back/adv/js/jquery-1.10.2.js"></script>
<script src="<%=request.getContextPath()%>/back/adv/js/jquery-ui.js"></script>
<script src="<%=request.getContextPath()%>/back/adv/js/tabs.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/back/adv/css/animate.css" />
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/back/adv/css/main.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/back/adv/js/main.js"></script>

</head>
<body>
	<%@ include file="/shared/pages/back_header_nav.file"%>
	<h2 class="backtitle">廣告輪播管理</h2> 

<div id="tabs">
	<ul>
		<li><a href="#tabs-1">所有廣告資料</a></li>
		<li><a href="#tabs-2">輪播廣告</a></li>
		<li><a href="#tabs-3">停播廣告</a></li>
		<li><a href="#tabs-4">新增廣告</a></li>
		<li><a href="#tabs-5">查詢廣告</a></li>
		<li><a href="#tabs-6">廣告預覽</a></li>
	</ul>
	<div id="tabs-1">
		<%@ include file="pages/collapse1.file"%>
					<%-- 錯誤表列 --%>
					<c:if test="${not empty errorMsgs}">
						<font color='red'>請修正以下錯誤:
							<ul>
								<c:forEach var="message" items="${errorMsgs}">
									<li>${message}</li>
								</c:forEach>
							</ul>
						</font>
					</c:if>
					<table  class="table table-hover table-bordered">
						<tr bgcolor='#e6f0e3' height="50">
							<th class="number_width">廣告編號</th>
							<th class="text_width">廣告名稱</th>
							<th class="number_width">廣告狀態</th>
							<th>廣告圖片</th>
							<th>廣告描述</th>
							<th class="time_width">廣告課程名稱</th>
							<th class="button_width">修改</th>
							<th class="button_width">刪除</th>
						</tr>
						<%@ include file="pages/allPage1.file"%>
						<c:forEach var="advVO" items="${allList}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
							<tr align='center' valign='middle'
								${(advVO.advno==requestScope.advVO.advno) ? 'bgcolor=#0080c0':''} bgcolor='#ffffff'>
								<td>${advVO.advno}</td>
								<td>${advVO.advformat}</td>
								<td>${(advVO.advisplay==0)?"停播":"輪播"}</td>
								<td>
									<img height="150" width="300" id="box" class="img-rounded" src="<%=request.getContextPath()%>/adv/adv.do?advno=${advVO.advno}&action=getAdvpic">
								</td>
								<td style="text-align:left;">${advVO.advdescription}</td>
								<td style="text-align:left;">
									<c:forEach var="couVO" items="${allCouList}">
										<c:if test="${advVO.couno==couVO.couno}">
											${couVO.couname}
										</c:if>
									</c:forEach>
								</td>
								<td>
									<%@ include file="pages/fancyboxAllU.file" %>
									<%@ include file="pages/updateAll.file" %>
								</td>
								<td>
									<%@ include file="pages/fancyboxAllD.file" %>
									<%@ include file="pages/deleteAll.file" %>				
								</td>
							</tr>
						</c:forEach>
					</table>
					<%@ include file="pages/allPage2.file"%>
				</div>
			</div>
		</div>
	</div>
	<div id="tabs-2">
		<%@ include file="pages/collapse2.file"%>
		<%-- 錯誤表列 --%>
		<c:if test="${not empty errorMsgs}">
			<font color='red'>請修正以下錯誤:
				<ul>
					<c:forEach var="message" items="${errorMsgs}">
						<li>${message}</li>
					</c:forEach>
				</ul>
			</font>
		</c:if>
		<table class="table table-hover table-bordered">
			<tr bgcolor='#e6f0e3' height="50">
				<th class="number_width">廣告編號</th>
				<th class="text_width">廣告名稱</th>
				<th class="number_width">廣告狀態</th>
				<th>廣告圖片</th>
				<th>廣告描述</th>
				<th class="time_width">廣告課程名稱</th>
				<th class="button_width">修改</th>
				<th class="button_width">刪除</th>
			</tr>
			<%@ include file="pages/isPlayPage.file"%>
			<c:forEach var="advVO" items="${isPlayList}" begin="<%=pageIndex3%>" end="<%=pageIndex+rowsPerPage3-1%>">
				<c:if test="${advVO.advisplay==1 }">
					<tr align='center' valign='middle'
						${(advVO.advno==requestScope.advVO.advno) ? 'bgcolor=#0080c0':''} bgcolor='#FFFFFF'>
						<td>${advVO.advno}</td>
						<td>${advVO.advformat}</td>
						<td>${(advVO.advisplay==0)?"停播":"輪播"}</td>
						<td><img height="150" width="300" id="box"
							class="img-rounded"
							src="<%=request.getContextPath()%>/adv/adv.do?advno=${advVO.advno}&action=getAdvpic"></td>
						<td style="text-align:left;">${advVO.advdescription}</td>
						<td style="text-align:left;">
							<c:forEach var="couVO" items="${allCouList}">
								<c:if test="${advVO.couno==couVO.couno}">
									${couVO.couname}
								</c:if>
							</c:forEach>
						</td>
						<td>
							<%@ include file="pages/fancyboxIsPlayU.file" %>			
							<%@ include file="pages/updateIsPlay.file" %>
						</td>
						<td>
							<%@ include file="pages/fancyboxIsPlayD.file" %>
							<%@ include file="pages/deleteIsPlay.file" %>				
						</td>
					</tr>
					<%
						count3++;
					%>
				</c:if>
			</c:forEach>
			<b>●符 合 查 詢 條 件 如 下 所 示: 共<font color=red><%=count3%></font>筆
			</b>
		</table>
	</div></div></div></div>
	<div id="tabs-3">
		<%@ include file="pages/collapse3.file"%>
		<%-- 錯誤表列 --%>
		<c:if test="${not empty errorMsgs}">
			<font color='red'>請修正以下錯誤:
				<ul>
					<c:forEach var="message" items="${errorMsgs}">
						<li>${message}</li>
					</c:forEach>
				</ul>
			</font>
		</c:if>
		<table class="table table-hover table-bordered">
			<tr align='center' valign='middle' bgcolor='#e6f0e3' height="50">
				<th class="number_width">廣告編號</th>
				<th class="text_width">廣告名稱</th>
				<th class="number_width">廣告狀態</th>
				<th>廣告圖片</th>
				<th>廣告描述</th>
				<th class="time_width">廣告課程名稱</th>
				<th class="button_width">修改</th>
				<th class="button_width">刪除</th>
			</tr>
			<%@ include file="pages/noPlayPage.file"%>
			<c:forEach var="advVO" items="${noPlayList}" begin="<%=pageIndex4%>" end="<%=pageIndex+rowsPerPage4-1%>">
				<c:if test="${advVO.advisplay==0}">
					<tr align='center' valign='middle'
						${(advVO.advno==requestScope.advVO.advno) ? 'bgcolor=#0080c0':''} bgcolor='#FFFFFF'>
						<td>${advVO.advno}</td>
						<td>${advVO.advformat}</td>
						<td>${(advVO.advisplay==0)?"停播":"輪播"}</td>
						<td><img height="150" width="300" id="box"
							class="img-rounded"
							src="<%=request.getContextPath()%>/adv/adv.do?advno=${advVO.advno}&action=getAdvpic"></td>
						<td style="text-align:left;">${advVO.advdescription}</td>
						<td style="text-align:left;">
							<c:forEach var="couVO" items="${allCouList}">
								<c:if test="${advVO.couno==couVO.couno}">
									${couVO.couname}
								</c:if>
							</c:forEach>
						</td>
						<td>
							<%@ include file="pages/fancyboxNoPlayU.file" %>			
							<%@ include file="pages/updateNoPlay.file" %>
						</td>
						<td>
							<%@ include file="pages/fancyboxNoPlayD.file" %>
							<%@ include file="pages/deleteNoPlay.file" %>				
						</td>
					</tr>
					<%
						count4++;
					%>
				</c:if>
			</c:forEach>
			<b>●符 合 查 詢 條 件 如 下 所 示: 共<font color=red><%=count4%></font>筆
			</b>
		</table>
	</div></div></div></div>
	<div id="tabs-4">		
		<%@ include file="pages/collapse4.file"%>
		<%@ include file="pages/insertAdv.file"%>
		</div></div></div>
	</div>
	<div id="tabs-5">
		<%@ include file="pages/collapse5.file"%>
		<%-- 錯誤表列 --%>
		<c:if test="${not empty errorMsgs}">
			<font color='red'>請修正以下錯誤:
				<ul>
					<c:forEach var="message" items="${errorMsgs}">
						<li>${message}</li>
					</c:forEach>
				</ul>
			</font>
		</c:if>
	
		<ul>
			<li>
					<b>選擇廣告編號</b> 
					<select size="1" name="advno" id="select2">
						<c:forEach var="advVO" items="${allList}">
							<option value="${advVO.advno}">${advVO.advno}
						</c:forEach>
					</select> 
					<button type="button" class="btn btn-success"  id="search2">查詢</button>	
					<input type="hidden" name="action" value="getOne_For_Display">
			</li>
			<div id="showResult2"></div>
			<li>
					<b>選擇廣告名稱</b> 
					<select size="1" name="advno" id="select3">
						<c:forEach var="advVO" items="${allList}">
							<option value="${advVO.advno}">${advVO.advformat}
						</c:forEach>
					</select> 
					<button type="button" class="btn btn-success"  id="search3">查詢</button>
					<input type="hidden" name="action" value="getOne_For_Display">
				</FORM>
			</li>
			<div id="showResult3"></div>
			<li>
					<b>選擇廣告課程名稱</b> 
					<select size="1" name="couno" id="select4">
						<c:forEach var="advVO" items="${allList}">						
							<c:forEach var="couVO" items="${allCouList}">
								<c:if test="${advVO.couno==couVO.couno}">
									<option value="${couVO.couno}">${couVO.couname}
								</c:if>
							</c:forEach>						
						</c:forEach>			 
					</select>
					<button type="button" class="btn btn-success" id="search4">查詢</button>	
			</li>
			<div id="showResult4"></div>
		</ul>

		</div></div></div>
	</div>
	<div id="tabs-6">
		<%@ include file="pages/collapse6.file"%>	
		<br>
		<div id="gallery">
		<%@ include file="pages/photoPage.file"%>
		<c:forEach var="advVO" items="${photoList}" begin="<%=pageIndex5%>" end="<%=pageIndex+rowsPerPage5-1%>">
		<c:if test="${advVO.advisplay==1 }">
		<div><a href="<%=request.getContextPath()%>/adv/adv.do?advno=${advVO.advno}&action=getAdvpic"><img height="80" width="160" class="img-rounded"
			src="<%=request.getContextPath()%>/adv/adv.do?advno=${advVO.advno}&action=getAdvpic"></a></div>
		</c:if></c:forEach></div>
		<%@ include file="pages/showAdv.file" %>
		
	</div>
</div>

	<%@ include file="/shared/pages/back_footer.file"%>
</body>
<script type="text/javascript">
$(function(){
	
	$("#search2").click(function(){
		var get_action = $("#search2").next("input[type=hidden]").val();
		var send_advno = $("#select2").val();
		$.post("<%=request.getContextPath()%>/adv/adv.do",
			{action:get_action,advno:send_advno},
			function(abc){
				$("#showResult2").append(abc);
			}		
		);
	});
	
	$("#search3").click(function(){
		var get_action = $("#search3").next("input[type=hidden]").val();
		var send_advno = $("#select3").val();
		$.post("<%=request.getContextPath()%>/adv/adv.do",
			{action:get_action,advno:send_advno},
			function(abc){
				$("#showResult3").append(abc);
			}		
		);
	});
	
	$("#search4").click(function(){
		var get_action = $(this).next("input[type=hidden]").val();
		var send_advno = $("#select4").val();
		$.post("<%=request.getContextPath()%>/searchOne.do",
			{action:get_action,couno:send_advno},
			function(e){
				$("#showResult4").append(e);
			}		
		);
	});
});
</script>
<style>
#gallery {
	float: right;
	width: 70px;
	margin-right: 150px;
	margin-left: 10px;
	border-right: white 1px dotted;	
}
#gallery img {
	display: inline-block;
	margin: 0 0 10px 0;
	border: 1px solid rgb(0,0,0);
}
</style>

</html>