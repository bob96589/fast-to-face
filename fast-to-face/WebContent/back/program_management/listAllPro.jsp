<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page import="com.pro.model.*"%>

<% 	
	List<ProVO> proVOList = (List<ProVO>)request.getAttribute("proVOList");
%>
<html>
	<head>
		<title>Insert title here</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
		
		<!-- self -->
		
	</head>
	<body>
		<%@ include file="/shared/pages/back_header_nav.file" %>
		<h2 class="backtitle">學程管理</h2>
		<a href="<%=request.getContextPath()%>/back/program_management/addPro.jsp" class="btn btn-success" style="margin:10px 0px;padding:4px 10px;font-size:18px;">新增學程</a><br/>  
		<%@ include file="pages/page1.file" %> 
		<table class="table table-bordered">
			<tr class="titleBgcolor">
				<th>學程編號</th>
				<th>學程名稱</th>
				<th>學程圖片</th>
				<th>購買點數</th>
				<th>折扣點數</th>
				<th>學程狀態</th>
				<th>新增時間</th>
				<th>編輯</th>
				<th>刪除</th>
			</tr>
			
			<c:forEach var="proVO" items="${proVOList}"  begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
				<c:set var="p" value="${proVO}"/>			
				<tr ${(proVO.prono==requestScope.proVO.prono) ? 'style="background:rgb(225,237,233)"':''}>
					<td class="number_width">${proVO.prono}</td>
					<td style="text-align:left;">${proVO.proname}</td>
					<td><img src="<%=request.getContextPath()%>/pro/pro.do?prono=${proVO.prono}&action=getPropic" width="150"/></td>
					<td>${proVO.proprice}</td>
					<td>${proVO.prodiscount}</td>
					<td>${(proVO.prostate==0)?"未上線":"上線中"}</td>
					<td><fmt:formatDate value="${proVO.proupdate}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate></td>					
					<td class="button_width">
						<form method="post" action="<%=request.getContextPath()%>/proManagement/pro.do">
							<input type="submit" value="編輯"/>
							<input type="hidden" name="action" value="getOne_For_Update"/>
							<input type="hidden" name="prono" value="${proVO.prono}"/>							
							<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>							
							<input type="hidden" name="whichPage" value="<%= whichPage %>"/>							
						</form>
					</td>
					<td class="button_width">
						<form method="post" action="<%=request.getContextPath()%>/proManagement/pro.do">
							<input type="submit" value="刪除" onclick="javascript:return confirm('您確定要刪除嗎')"/>
							<input type="hidden" name="action" value="delete"/>
							<input type="hidden" name="prono" value="${proVO.prono}"/>							
							<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>							
							<input type="hidden" name="whichPage" value="<%= whichPage %>"/>							
						</form>
					</td>				
				</tr>				
			</c:forEach>
		</table>
		<%@ include file="pages/page2.file" %>
		<%@ include file="/shared/pages/back_footer.file" %>
	</body>
</html>




















