<%@page import="com.pro.model.ProVO"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% ProVO proVO = (ProVO)request.getAttribute("proVO"); %>


<html>
	<head>
		<title>Insert title here</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
		
		<!-- self -->
		<script src="<%=request.getContextPath()%>/back/program_management/js/update_pro_input.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/back/program_management/css/update_pro_input.css">
		
	</head>
	<body>
		<%@ include file="/shared/pages/back_header_nav.file" %>
		<h2 class="backtitle">編輯學程</h2>
		<ol class="breadcrumb">
		  	<li><a href="<%=request.getContextPath()%>/proManagement/pro.do?action=getProList">學程管理</a></li>
		 	<li class="active">編輯學程</li>
		</ol>  
		<c:if test="${not empty errorMsgs}">
			<font color='red'>請修正以下錯誤:
				<ul>
					<c:forEach var="message" items="${errorMsgs}">
					<li>${message}</li>
					</c:forEach>
				</ul>
			</font>
		</c:if>
		<form action="<%=request.getContextPath()%>/proManagement/pro.do" method="post" enctype="multipart/form-data">
			<table class="borderless">
				<tr>
					<td>學程編號</td>
					<td><%= proVO.getProno() %></td>
					<td></td>
				</tr>	
				<tr>
					<td>學程名稱</td>
					<td><input type="text" name="proname" value="<%= proVO.getProname() %>"/></td>
					<td></td>
				</tr>
				<tr>
					<td>學程簡介</td>
					<td><textarea rows="10" cols="45" name="prointro"><%= proVO.getProintro() %></textarea></td>
					<td></td>
				</tr>
				<tr>
					<td>學程圖片</td>
					<td>
						<div id="preview" >
							<img src="<%=request.getContextPath()%>/pro/pro.do?prono=${proVO.prono}&action=getPropic" width="150"/>
						</div>						
						<input type="file" name="propic" accept="image/*" id="uploadImage" onchange="preview(this)"/>
					</td>
					<td></td>
				</tr>
				<tr>
					<td>購買點數</td>
					<td><input type="text" name="proprice" value="<%= proVO.getProprice() %>"/></td>
					<td></td>
				</tr>
				<tr>
					<td>折扣點數</td>
					<td><input type="text" name="prodiscount" value="<%= proVO.getProdiscount() %>"/></td>
					<td></td>
				</tr>
				<tr>
					<td>學程狀態</td>
					<td>
						<select name="prostate"   >
							<option value="0" <%= (proVO.getProstate()==0)?"selected":"" %>>未上線</option>
							<option value="1" <%= (proVO.getProstate()==1)?"selected":"" %>>上線中</option>
						</select>
					</td>
					<td></td>
				</tr>
			</table>
			<input type="submit" value="儲存" />
			<input type="hidden" name="action" value="update"/>
			<input type="hidden" name="prono" value="<%= proVO.getProno() %>" />					
			<input type="hidden" name="requestURL" value="<%= request.getAttribute("requestURL") %>" />					
			<input type="hidden" name="whichPage" value="<%= request.getAttribute("whichPage") %>" />					
		</form>
		<%@ include file="/shared/pages/back_footer.file" %>
	</body>
</html>