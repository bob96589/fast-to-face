function preview(file) {

    if (file.files && file.files[0]) {
        var reader = new FileReader();
        reader.onload = function (evt) {
            $("#preview").html( '<img height="80px" src="' + evt.target.result + '" />');
        }
        reader.readAsDataURL(file.files[0]);
    }
}