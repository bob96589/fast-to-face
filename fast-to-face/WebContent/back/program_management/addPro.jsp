<%@page import="com.pro.model.ProVO"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	ProVO proVO = (ProVO)request.getAttribute("proVO");
%>
<html>
	<head>
		<title>Insert title here</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
		
		<!-- self -->
		<script src="<%=request.getContextPath()%>/back/program_management/js/addPro.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/back/program_management/css/addPro.css">
		
		

	</head>
	<body>
		<%@ include file="/shared/pages/back_header_nav.file" %> 
		<h2 class="backtitle">新增學程</h2>
		<ol class="breadcrumb">
		  	<li><a href="<%=request.getContextPath()%>/proManagement/pro.do?action=getProList">學程管理</a></li>
		 	<li class="active">新增學程</li>
		</ol>
		<c:if test="${not empty errorMsgs}">
			<font color='red'>請修正以下錯誤:
				<ul>
					<c:forEach var="message" items="${errorMsgs}">
					<li>${message}</li>
					</c:forEach>
				</ul>
			</font>
		</c:if>
		<form action="<%=request.getContextPath()%>/proManagement/pro.do" method="post" enctype="multipart/form-data">
			<table class="borderless">	
				<tr>
					<td>學程名稱</td>
					<td><input type="text" name="proname" value="<%= (proVO==null)? "": proVO.getProname() %>" id="proname"/></td>
					<td></td>
				</tr>
				<tr>
					<td>學程簡介</td>
					<td><textarea rows="10" cols="45" name="prointro" id="prointro"><%= (proVO==null)? "": proVO.getProintro() %></textarea></td>
					<td></td>
				</tr>
				<tr>
					<td>學程圖片</td>
					<td><div id="preview" ></div><input type="file" name="propic" accept="image/*" id="uploadImage" onchange="preview(this)"/></td>
					<td></td>
				</tr>
				<tr>
					<td>原價</td>
					<td><input type="text" name="proprice" value="<%= (proVO==null)? "": proVO.getProprice() %>" id="proprice"/></td>
					<td></td>
				</tr>
				<tr>
					<td>特價</td>
					<td><input type="text" name="prodiscount" value="<%= (proVO==null)? "": proVO.getProdiscount() %>" id="prodiscount"/></td>
					<td></td>
				</tr>
			</table>
			<input type="submit" value="新增" />
			<button type="button" class="btn btn-warning btn-xs" id="magic_button"><span class="glyphicon glyphicon-star"></span></button>	
			<input type="hidden" name="action" value="insert"/>
			
			
			<script>
			$(function(){							
				$("#magic_button").click(function(){									
					$("#proname").val("Google Android APP開發");
					$("#prointro").val("Google Android，透過更開放自由的方式，讓硬體廠商可以將其搭載於自行研發的手機或是其他硬體。Google Play市集讓全球開發者可以進行上傳，販售自己撰寫的APP軟體。根據AdMob行動廣告商的調查，有超過七成的iPhone開發者，也計畫投入Android開發的行列，許多知名軟體，也紛紛在iPhone、Android兩大市集佈局。");
					$("#proprice").val("400");
					$("#prodiscount").val("360");
				});				
			});
			</script>
			
			<%@ include file="/shared/pages/back_footer.file" %>					
		</form>
	</body>
</html>