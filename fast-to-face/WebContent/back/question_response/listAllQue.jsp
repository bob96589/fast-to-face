<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.*"%>
<%@ page import="com.que.model.*"%>
<%@page import="java.util.Set"%>
<%@page import="com.cou.model.*"%>
<%@page import="com.acc.model.*"%>
<%-- 此頁練習採用 EL 的寫法取值 --%>

<%	
	AccVO accVO = (AccVO)session.getAttribute("accVO");
	CouVO couVO = (CouVO) request.getAttribute("couVO");	
	AccService accSvc = new AccService();
	Set<CouVO> couVOSet = accSvc.getCousByAccno(accVO.getAccno());
	pageContext.setAttribute("couVOSet", couVOSet);
	
    QueVO queVO = (QueVO) request.getAttribute("queVO");
    QueService queSvc = new QueService();
    List<QueVO> queVOSet = queSvc.getAll();
    pageContext.setAttribute("queVOSet",queVOSet);
    
%>


<html>
<head>
<title>回覆專區</title>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/back/question_response/css/jquery-ui.css">
		<script>$(function(){$(".customized_tab #second").addClass("active");});	</script>
		<script>
			$(function() {
				$("#accordion").accordion();
			});
		</script>
</head>
<body bgcolor='white'>
<%@ include file="/shared/pages/back_header_nav.file" %> 
<h2 class="backtitle">課程回覆專區</h2>



<c:if test="${not empty errorMsgs}">
	<font color='red'>請修正以下錯誤:
	<ul>
		<c:forEach var="message" items="${errorMsgs}">
			<li>${message}</li>
		</c:forEach>
	</ul>
	</font>
</c:if>


	</br>


<div id="accordion" style="text-align:center;">
<c:forEach var="couVO" items="${couVOSet}">
<h3 style="width:100%; background-color:rgb(225,237,233);color:#333; font-family: Microsoft JhengHei;" >${couVO.couname}</h3>
<div style="width:100%;">
<!-- <table border='10' bordercolor='#fff' width="950px" style="border-style:double;color:green;" > -->
<table class="table table-bordered" >
	<tr class="titleBgcolor">
		
		
		<th style="text-align:center;">會員編號</th>
		<th style="text-align:center;">發問日期</th>
		<th style="text-align:center;">問題</th>
		<th style="text-align:center;">回覆日期</th>
		<th style="text-align:center;">回覆</th>
		<th style="text-align:center;">回覆</th>
		<th style="text-align:center;">刪除</th>
	</tr>
	
	<c:forEach var="queVO" items="${queVOSet}" >
	<c:if test="${couVO.couno==queVO.couno}">
		<tr>
			
			<td width="100">${queVO.memno}</td>
			<td width="140"><fmt:formatDate value="${queVO.quesasktime}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate></td>
			<td style="text-align:left;">${queVO.quesaskcon}</td>
			<td width="140"><fmt:formatDate value="${queVO.quesrestime}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate></td>
			<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/que/que.do" >
				<td width="450">
					<textarea id="quesrescon${queVO.quesno}" rows="5" cols="35" name="quesrescon"  >${queVO.quesrescon}</textarea>
				</td>
				<td class="button_width">
					 <button type="button" class="btn btn-warning btn-xs" id="magic_button${queVO.quesno}"><span class="glyphicon glyphicon-star"></span></button>
				     <input type="submit" value="回覆">
				     <input type="hidden" name="quesno" value="${queVO.quesno}">
				     <input type="hidden" name="action"	value="update">
				</td>
				<script>
					$(function(){
						$("#magic_button${queVO.quesno}").click(function(){									
							$("#quesrescon${queVO.quesno}").val("JAVA是簡單的、萬般皆物件、要什麼給什麼。");
						});
					});
				</script>
			</FORM>
			<td class="button_width">
			  	<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/que/que.do" style="margin:0px;">
				    <input type="submit" value="刪除">
				    <input type="hidden" name="quesno" value="${queVO.quesno}">
				    <input type="hidden" name="action"value="delete">
			    </FORM>
			</td>
		</tr>
		</c:if>
	</c:forEach>

</table>
</div>	
</c:forEach>
</div>



<%@ include file="/shared/pages/back_footer.file" %> 
</body>
</html>



