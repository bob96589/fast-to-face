<%@page import="java.util.Set"%>
<%@page import="com.acc.model.*"%>
<%@page import="com.cou.model.*"%>
<%@ page import="com.pro.model.*"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
	Set<CouVO> couVOSet = (Set<CouVO>)request.getAttribute("couVOSet");

%>


<html>
	<head>
		<title>Insert title here</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
		
		<!-- self -->
		
	</head>
	<body>
		<%@ include file="/shared/pages/back_header_nav.file" %>
		<h2 class="backtitle">熱門課程管理</h2>  
		<form method="post" action="<%=request.getContextPath()%>/hotCouManagement/cou.do">
			<table class="table table-bordered">
				<tr class="titleBgcolor">
					<th class="number_width">課程編號</th>
					<th width="400">課程名稱</th>
					<th>課程圖片</th>
					<th>授課老師</th>
					<th>所屬學程</th>
					<th>新增時間</th>
					<th>熱門學程展示</th>				
				</tr>
				<%@ include file="pages/page1.file" %> 
				<c:forEach var="couVO" items="${couVOSet}"  begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
					<tr>
						<td>${couVO.couno}</td>
						<td style="text-align: left;">${couVO.couname}</td>
						<td><img src="<%=request.getContextPath()%>/cou/cou.do?couno=${couVO.couno}&action=getCoupic" height="40"/></td>
						<td>
						<c:forEach var="accVO" items="${accVOList}">
							<c:if test="${couVO.accno==accVO.accno}">
								${accVO.accname}
							</c:if>
						</c:forEach>
						</td>
						<td style="text-align: left;">
							<c:forEach var="proVO" items="${proVOList}">
								<c:if test="${couVO.prono==proVO.prono}">
									${proVO.proname}
								</c:if>
							</c:forEach>					
						</td>
						<td><fmt:formatDate value="${couVO.couupdate}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate></td>				
						<td>
							<input type="radio" name="${couVO.couno}" value="0" ${(couVO.couishot==0) ? 'checked':''}/>未展示
							<input type="radio" name="${couVO.couno}" value="1" ${(couVO.couishot==1) ? 'checked':''}/>展示中							
						</td>										
					</tr>				
				</c:forEach>
			</table>
			<input type="submit" value="儲存"/>
			<input type="hidden" name="action" value="update_hot"/>										
			<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>							
			<input type="hidden" name="whichPage" value="<%= whichPage %>"/>
		</form>
		<%@ include file="pages/page2.file" %>
		<%@ include file="/shared/pages/back_footer.file" %>
	</body>
</html>