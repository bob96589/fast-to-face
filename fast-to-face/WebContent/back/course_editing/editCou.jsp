<%@page import="java.util.List"%>
<%@page import="com.ass.model.*"%>
<%@page import="com.cou.model.*"%>
<%@page import="java.util.Set"%>
<%@page import="com.unit.model.*"%>
<%@page import="com.content.model.*"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
	<head>
		<title>Insert title here</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
		
		<!-- self -->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/back/course_editing/css/editCou.css">
		<script src="<%=request.getContextPath()%>/back/course_editing/js/editCou.js"></script>
		
	</head>
	<body>
<!-- 		<script type="text/javascript"> -->
// 		   $(function(){
// 			   var pageFrom = "${pageFrom}";
// 			   if(pageFrom =="type_1"){
// 				   $("#testBT").click();
// 				   //errMsg
// 			   }			  
// 		   })
<!-- 		</script> -->
		<%@ include file="/shared/pages/back_header_nav.file" %>		
		<h2 class="backtitle">${couVO.couname}<small>，單元：${unitCount}、影片：${videoCount}、評量：${assCount}</small></h2>
		<ol class="breadcrumb">
		  	<li><a href="<%=request.getContextPath()%>/couEdit/cou.do?action=getCouListByAccno">編排課程</a></li>
		 	<li class="active">${couVO.couname}</li>
		</ol>  
		<!--錯誤處理 -->
		<c:if test="${not empty errorMsgs}">
			<font color='red'>				
					動作失敗			
			</font>
		</c:if>
		
		<table class="top_table">
			<tr>
				<td>
					<!--新增單元 -->		
					<%@include file="/back/course_editing/editCou/addUnit.jsp" %>				
				<td>
					<!--提交給管理員審核 -->
					<form method="post" action="<%=request.getContextPath()%>/couEdit/cou.do">
						<button type="submit" class="btn btn-default ${(videoCount<2)?'disabled':''}">${(videoCount<2)?'至少上傳兩部影片，才能提交課程':'提交課程，將無法繼續編輯此課程'}</button>
						<input type="hidden" name="couno" value="${couVO.couno}"/>
						<input type="hidden" name="action"	value="updateCoustateToOne"/>
					</form>				
				</td>				
			</tr>	
		</table>		
		<!--單元迴圈 -->		
		<c:forEach var="unitVO" items="${unitVOSet}" varStatus="unitStatus">
			<table class="table table-bordered" >
				<tr class="active">
					<!--單元排序 -->
					<td>
						<form method="post" action="<%=request.getContextPath()%>/unit/unit.do">
							<button class="btn btn-default ${(unitStatus.first)?'disabled':''}"><span class="glyphicon glyphicon-chevron-up"></span></button>
							<input type="hidden" name="action" value="upward"/>
							<input type="hidden" name="couno" value="${couVO.couno}"/>
							<input type="hidden" name="unitno" value="${unitVO.unitno}"/>
							<input type="hidden" name="unitorder" value="${unitVO.unitorder}"/>
							<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>
						</form>
						<form method="post" action="<%=request.getContextPath()%>/unit/unit.do">
							<button class="btn btn-default ${(unitStatus.last)?'disabled':''}"><span class="glyphicon glyphicon-chevron-down"></span></button>
							<input type="hidden" name="action" value="downward"/>
							<input type="hidden" name="couno" value="${couVO.couno}"/>
							<input type="hidden" name="unitno" value="${unitVO.unitno}"/>
							<input type="hidden" name="unitorder" value="${unitVO.unitorder}"/>
							<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>
						</form>
					</td>
					<td colspan="4" style="text-align: left;">
						<h3 ><small>單元編號:${unitVO.unitno}<small></h3>
						<h3><small>單元${unitStatus.count}:<small></h3>
						<h1>${unitVO.unitname}</h1>
					</td>
					<td>	
						<!--編輯單元 -->
						<%@include file="/back/course_editing/editCou/updateUnit.jsp" %>
						<!--刪除單元 -->
						<form method="post" action="<%=request.getContextPath()%>/unit/unit.do">
							<button type="submit" class="btn btn-default btn-sm" onclick="javascript:return confirm('您確定要刪除嗎')">刪除單元</button>
							<input type="hidden" name="couno" value="${couVO.couno}"/>
							<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>
							<input type="hidden" name="unitno" value="${unitVO.unitno}"/>
							<input type="hidden" name="action"	value="delete"/>
						</form>
						<!--新增影片 -->
						<%@include file="/back/course_editing/editCou/addVideo.jsp" %>
						<!--新增評量 -->
						<%@include file="/back/course_editing/editCou/addAss.jsp" %>

					</td>			
				</tr>
				<tr class="success">
					<td class="button_width">排序</td>
					<td class="text_width">單元內容編號</td>
					<td class="text_width">類別</td>
					<td>名稱</td>					
					<td class="text_width"></td>			
					<td class="text_width"></td>			
				</tr>
				<!--單元內容迴圈 -->				
				<c:if test="${unitList != '[]' && conVOList == '[]'}">無單元內容</c:if>
				<c:forEach var="conVO" items="${conVOSetList[unitStatus.index]}" varStatus="conStatus">
					<tr ${(conVO.conno==conno)?'style="background:rgb(225,237,233)"':''}>
						<!--單元內容排序 -->
						<td>
							<form method="post" action="<%=request.getContextPath()%>/con/con.do">
								<button type="submit" class="btn btn-default btn-xs ${(conStatus.first)?'disabled':''}"><span class="glyphicon glyphicon-chevron-up"></span></button>
								<input type="hidden" name="action" value="upward"/>
								<input type="hidden" name="couno" value="${couVO.couno}"/>
								<input type="hidden" name="unitno" value="${unitVO.unitno}"/>
								<input type="hidden" name="conno" value="${conVO.conno}"/>
								<input type="hidden" name="conorder" value="${conVO.conorder}"/>
								<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>
							</form>
							<form method="post" action="<%=request.getContextPath()%>/con/con.do">
								<button type="submit" class="btn btn-default btn-xs ${(conStatus.last)?'disabled':''}"><span class="glyphicon glyphicon-chevron-down"></span></button>
								<input type="hidden" name="action" value="downward"/>
								<input type="hidden" name="couno" value="${couVO.couno}"/>
								<input type="hidden" name="unitno" value="${unitVO.unitno}"/>
								<input type="hidden" name="conno" value="${conVO.conno}"/>
								<input type="hidden" name="conorder" value="${conVO.conorder}"/>
								<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>
							</form>
						</td>						
						<td>${conVO.conno}</td>
						<td>${conVO.contype}</td>
						<td style="text-align: left;">${conVO.conname}</td>
						
						<!--編輯 -->
						<td>
							<c:if test="${conVO.contype=='影片'||conVO.contype=='影片 '}">
								<%@include file="/back/course_editing/editCou/updateVideo.jsp" %>								
				    		</c:if>
				    		<c:if test="${conVO.contype=='評量'||conVO.contype=='評量 '}">	
								<%@include file="/back/course_editing/editCou/updateAss.jsp" %>
					    					    		
				    		</c:if>					
						</td>
						<!--刪除 -->
						<td>							
							<form method="post" action="<%=request.getContextPath()%>/con/con.do" style="margin:0px;">
								<button type="submit" class="btn btn-warning" onclick="javascript:return confirm('您確定要刪除嗎')" >刪除</button>
								<input type="hidden" name="couno" value="${couVO.couno}"/>
								<input type="hidden" name="unitno" value="${unitVO.unitno}"/>
								<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>
								<input type="hidden" name="conno" value="${conVO.conno}"/>
								<input type="hidden" name="action"	value="delete"/>
							</form>
						</td>
					</tr>
				</c:forEach>			
			</table>
		</c:forEach>
	<%@ include file="/shared/pages/back_footer.file" %>
	</body>
</html>