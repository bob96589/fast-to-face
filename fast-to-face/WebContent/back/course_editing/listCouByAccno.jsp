<%@page import="java.util.Set"%>
<%@page import="com.cou.model.*"%>
<%@page import="com.acc.model.*"%>
<%@page import="com.pro.model.*"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
	Set<CouVO> couVOSet = (Set<CouVO>)request.getAttribute("couVOSet");

%>

<html>
	<head>
		<title>Insert title here</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
		
		
	</head>
	<body>
		<%@ include file="/shared/pages/back_header_nav.file" %>   
		<h2 class="backtitle">編排課程</h2>
		<table class="table table-bordered">
			<tr class="titleBgcolor">
				<th>課程編號</th>
				<th>課程名稱</th>
				<th>課程圖片</th>
				<th>授課老師</th>
				<th>課程狀態</th>
				<th>所屬學程</th>
				<th>編輯課程內容</th>
			</tr>
			<%@ include file="pages/page1.file" %> 
			<c:forEach var="couVO" items="${couVOSet}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
				<tr ${(couVO.couno==param.couno) ? 'style="background:rgb(225,237,233)"':''}>
					<td class="number_width">${couVO.couno}</td>
					<td style="text-align: left;">${couVO.couname}</td>
					<td  class="text_width"><img src="<%=request.getContextPath()%>/cou/cou.do?couno=${couVO.couno}&action=getCoupic" height="40"/></td>
					<td  class="text_width">${accVO.accname}</td>
					<td  class="text_width">
						<c:if test="${couVO.coustate==0}">未審核</c:if>
						<c:if test="${couVO.coustate==1}">待審核</c:if>
						<c:if test="${couVO.coustate==2}">已審核</c:if>
						<c:if test="${couVO.coustate==3}">上線中</c:if>
					</td>
					<td style="text-align: left;" class="time_width">
						<c:forEach var="proVO" items="${proList}">
							<c:if test="${couVO.prono==proVO.prono}">
								${proVO.proname}
							</c:if>
						</c:forEach>					
					</td>
					<td class="text_width">
						<c:if test="${couVO.coustate==0}">
							<form method="post" action="<%=request.getContextPath()%>/couEdit/cou.do">
								<input type="submit" value="編輯課程內容"/>
								<input type="hidden" name="action" value="getWholeCouForCourseEditing"/>
								<input type="hidden" name="couno" value="${couVO.couno}"/>							
								<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>							
								<input type="hidden" name="whichPage" value="<%= whichPage %>"/>							
							</form>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		
		</table>
		<%@ include file="pages/page2.file" %>
		<%@ include file="/shared/pages/back_footer.file" %>
	</body>
</html>