<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
						<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#updateUnit${unitVO.unitno}">編輯單元</button>
						<div class="modal fade" id="updateUnit${unitVO.unitno}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
							    	<div class="modal-header">
							        	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							        	<h4 class="modal-title" id="myModalLabel">編輯單元</h4>
							      	</div>
							  		<div class="modal-body">
							  			<form method="post" action="<%=request.getContextPath()%>/unit/unit.do">
								       		<table class="borderless">
								       			<tr>
								       				<th>單元編號</th>
								       				<td>${unitVO.unitno}</td>			       			
								       			</tr>
								       			<tr>
								       				<th>單元名稱</th>
								       				<td><input type="text" name="unitname" value="${unitVO.unitname}" class="form-control"/></td>			       			
								       			</tr>								       					       		
								       		</table>
							       			<input type="submit" value="儲存"/> 
											<input type="hidden" name="couno" value="${couVO.couno}"/>
											<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>
											<input type="hidden" name="unitno" value="${unitVO.unitno}"/>
											<input type="hidden" name="action"	value="update"/>
										</form>
							      	</div>      
							    </div>
							</div>
						</div>