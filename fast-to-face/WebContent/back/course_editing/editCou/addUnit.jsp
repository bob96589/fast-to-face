<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
		<div>			
			<button type="button" class="btn btn-default btn-success" data-toggle="modal" data-target="#addUnit">新增單元</button>		
			<div class="modal fade" id="addUnit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
				    	<div class="modal-header">
				        	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        	<h4 class="modal-title" id="myModalLabel">新增單元</h4>
				      	</div>
				  		<div class="modal-body">
				  			<form method="post" action="<%=request.getContextPath()%>/unit/unit.do">
					       		<table class="borderless">
					       			<tr>
					       				<th>單元名稱</th>
					       				<td><input type="text" name="unitname" class="form-control" id="unitname"/></td>			       			
					       			</tr>					       						       		
					       		</table>
				       			<input type="submit" value="儲存"/>
				       			<button type="button" class="btn btn-warning btn-xs" id="magic_button"><span class="glyphicon glyphicon-star"></span></button>
				       			<button type="button" class="btn btn-warning btn-xs" id="magic_button2"><span class="glyphicon glyphicon-star"></span></button>
								<input type="hidden" name="couno" value="${couVO.couno}"/>
								<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>
								<input type="hidden" name="action"	value="insert"/>
							</form>
				      	</div>      
				    </div>
				</div>
			</div>
		</div>
		
		<script>
			$(function(){
				$("#magic_button").click(function(){									
					$("#unitname").val("JSON介紹");
				
				});
				$("#magic_button2").click(function(){									
					$("#unitname").val("手機App連結Web服務");
				});	
			});
		</script>