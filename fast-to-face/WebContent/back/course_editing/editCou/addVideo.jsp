<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>



<script>      
//載入本機圖片
function preview${unitVO.unitno}(file) {
    if (file.files && file.files[0]) {
        var reader = new FileReader();
        reader.onload = function (evt) {
            $("#preview${unitVO.unitno}").html( '<video width="640" controls><source src="' + evt.target.result + '" type="video/mp4"></video>');
        }
        $("#preview${unitVO.unitno}").show();
        reader.readAsDataURL(file.files[0]);
    }
}
</script>

						<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#addVideo${unitVO.unitno}">新增影片</button><br/>
						<div class="modal fade" id="addVideo${unitVO.unitno}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
							    	<div class="modal-header">
							        	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							        	<h4 class="modal-title" id="myModalLabel">新增影片</h4>
							      	</div>
							  		<div class="modal-body">
							  			<form method="post" action="<%=request.getContextPath()%>/con/con.do" enctype="multipart/form-data">
								       		<table class="borderless">
								       			<tr>
								       				<th>單元內容名稱</th>
								       				<td><input type="text" name="conname" class="form-control" id="conname${unitVO.unitno}"/></td>			       			
								       			</tr>
								       			<tr>
								       				<th>影片上傳</th>
								       				<td>	
								       					<div id="preview${unitVO.unitno}"></div>								       					
								       					<input type="file" name="convideo" accept="video/mp4" onchange="preview${unitVO.unitno}(this)"/>
								       				</td>			       			
								       			</tr>								       				       		
								       		</table>
							       			<input type="submit" value="新增"/>
							       			<button type="button" class="btn btn-warning btn-xs" id="magic_button${unitVO.unitno}"><span class="glyphicon glyphicon-star"></span></button>
				       						<button type="button" class="btn btn-warning btn-xs" id="magic_button2${unitVO.unitno}"><span class="glyphicon glyphicon-star"></span></button> 
				       						<button type="button" class="btn btn-warning btn-xs" id="magic_button3${unitVO.unitno}"><span class="glyphicon glyphicon-star"></span></button> 
											<input type="hidden" name="couno" value="${couVO.couno}"/>
											<input type="hidden" name="contype" value="影片"/>
											<input type="hidden" name="unitno" value="${unitVO.unitno}"/>
											<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>
											<input type="hidden" name="action"	value="insert_video"/>
										</form>
							      	</div>      
							    </div>
							</div>
						</div>
						
						
						
		<script>
			$(function(){
				$("#magic_button${unitVO.unitno}").click(function(){		
					$("#conname${unitVO.unitno}").val("JSON概論");
				});
				$("#magic_button2${unitVO.unitno}").click(function(){
					$("#conname${unitVO.unitno}").val("API與Gson API功能比較");
				});	
				$("#magic_button3${unitVO.unitno}").click(function(){
					$("#conname${unitVO.unitno}").val("如何執行Java Web程式");
				});
			});
		</script>