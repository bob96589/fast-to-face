<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
						<button type="button" id="testBT" class="btn btn-default btn-sm" data-toggle="modal" data-target="#addTest${unitVO.unitno}">新增評量</button>
						<div class="modal fade" id="addTest${unitVO.unitno}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
							    	<div class="modal-header">
							        	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							        	<h4 class="modal-title" id="myModalLabel">新增評量</h4>
							      	</div>
							  		<div class="modal-body">
							  			<form method="post" action="<%=request.getContextPath()%>/con/con.do">
								       		<table class="borderless">
								       			<tr>
								       				<th>單元內容名稱</th>
								       				<td><input type="text" name="conname" style = "width:300px;" class="form-control" id="title${unitVO.unitno}"/></td>			       			
								       			</tr>								       					       		
								       		</table>
								       		<hr/>
								       		<c:forEach var="data" begin="1" end="2">
									       		<p><b>題目${data}</b></p>
									       		<textarea name="assques${data}" rows="8" cols="68" class="form-control" id="question${unitVO.unitno}${data}"></textarea>
									       		<table class="borderless">
									       			<tr>								       			
									       				<th>答案</th>
									       				<th>選項</th>			       			
									       				<th></th>			       			
									       			</tr>
									       			<tr>
									       				<td><input type="radio" name="assans${data}"  value="A" id="1A${unitVO.unitno}${data}"/></td>
									       				<td>(A)</td>
									       				<td><input  type="text" name="assansa${data}" class="form-control" id="1a${unitVO.unitno}${data}"/></td>			       			
									       			</tr>
									       			<tr>
									       				<td><input type="radio" name="assans${data}"  value="B" id="1B${unitVO.unitno}${data}"/></td>
									       				<td>(B)</td>
									       				<td><input  type="text" name="assansb${data}" class="form-control" id="1b${unitVO.unitno}${data}"/></td>			       			
									       			</tr>
									       			<tr>
									       				<td><input type="radio" name="assans${data}"  value="C" id="1C${unitVO.unitno}${data}"/></td>
									       				<td>(C)</td>
									       				<td><input  type="text" name="assansc${data}" class="form-control" id="1c${unitVO.unitno}${data}"/></td>			       			
									       			</tr>
									       			<tr>
									       				<td><input type="radio" name="assans${data}"  value="D" id="1D${unitVO.unitno}${data}"/></td>
									       				<td>(D)</td>
									       				<td><input  type="text" name="assansd${data}" class="form-control" id="1d${unitVO.unitno}${data}"/></td>			       			
									       			</tr>  					       		
									       		</table>
									       		<hr/>
								       		</c:forEach>
							       			<input type="submit" value="新增"/> 
											<input type="hidden" name="couno" value="${couVO.couno}"/>
											<input type="hidden" name="contype" value="評量"/>
											<input type="hidden" name="unitno" value="${unitVO.unitno}"/>
											<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>
											<input type="hidden" name="action"	value="insert_con_and_ass"/>											
											<button type="button" class="btn btn-warning btn-xs" id="bu${unitVO.unitno}"><span class="glyphicon glyphicon-star"></span></button>	
										</form>
							      	</div>      
							    </div>
							</div>
						</div>
						<script>
							var jq = $.noConflict();
							jq(function(){							
								jq("#bu${unitVO.unitno}").click(function(){									
									jq("#title${unitVO.unitno}").val("JSON應用");
									jq("#question${unitVO.unitno}1").val("在JSON格式中，{\"firstName\": \"John\", \"lastName\": \"Smith\"}，代表什麼?");
									jq("#1A${unitVO.unitno}1").prop("checked",false);
									jq("#1a${unitVO.unitno}1").val("基本資料型態");
									jq("#1B${unitVO.unitno}1").prop("checked",true);
									jq("#1b${unitVO.unitno}1").val("物件");
									jq("#1C${unitVO.unitno}1").prop("checked",false);
									jq("#1c${unitVO.unitno}1").val("陣列");
									jq("#1D${unitVO.unitno}1").prop("checked",false);
									jq("#1d${unitVO.unitno}1").val("執行緒");
									jq("#question${unitVO.unitno}2").val("在JSON格式中，[John, Cindy, Bob, Jack]，代表什麼?");
									jq("#1A${unitVO.unitno}2").prop("checked",false);
									jq("#1a${unitVO.unitno}2").val("基本資料型態");
									jq("#1B${unitVO.unitno}2").prop("checked",false);
									jq("#1b${unitVO.unitno}2").val("物件");
									jq("#1C${unitVO.unitno}2").prop("checked",true);
									jq("#1c${unitVO.unitno}2").val("陣列");
									jq("#1D${unitVO.unitno}2").prop("checked",false);
									jq("#1d${unitVO.unitno}2").val("執行緒");
								});				
							});
						</script>