<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#${conVO.conno}">編輯</button>
								<div class="modal fade" id="${conVO.conno}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
									<div class="modal-dialog">				
										<div class="modal-content" >
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
												<h4 class="modal-title">編輯評量</h4>
											</div>
											<div class="modal-body">
												<form method="post" action="<%=request.getContextPath()%>/con/con.do">
													<table class="borderless">
										       			<tr>
										       				<th>單元內容名稱</th>
										       				<td><input type="text" name="conname" value="${conVO.conname}" class="form-control"/></td>			       			
										       			</tr>								       				       		
										       		</table>
										       		<hr/>										       														
									    			<c:forEach var="assVO" items="${assVOSetListList[unitStatus.index][conStatus.index]}" varStatus="assStatus">
									    			
											       		<p><b>題目${s.count}</b><input type="hidden" name="assno${assStatus.count}" value="${assVO.assno}"/></p>
											       		<textarea name="assques${assStatus.count}" rows="8" class="form-control" >${assVO.assques}</textarea>
												       	<table class="borderless">
												       		<tr>								       			
												       			<th>答案</th>
												       			<th>選項</th>													       					       			
												       			<th></th>													       					       			
												       		</tr>
												       		<tr>
												       			<td><input type="radio" name="assans${assStatus.count}"  value="A" ${(assVO.assans=='A')?'checked':''} /></td>
												       			<td>(A)</td>
												       			<td><input  type="text" name="assansa${assStatus.count}" value="${assVO.assansa}" class="form-control"/></td>			       			
												       		</tr>
												       		<tr>
												       			<td><input type="radio" name="assans${assStatus.count}"  value="B" ${(assVO.assans=='B')?'checked':''}/></td>
												       			<td>(B)</td>
												       			<td><input  type="text" name="assansb${assStatus.count}" value="${assVO.assansb}" class="form-control"/></td>			       			
												       		</tr>
												       		<tr>
												       			<td><input type="radio" name="assans${assStatus.count}"  value="C" ${(assVO.assans=='C')?'checked':''}/></td>
												       			<td>(C)</td>
												       			<td><input  type="text" name="assansc${assStatus.count}" value="${assVO.assansc}" class="form-control"/></td>			       			
												       		</tr>
												       		<tr>
												       			<td><input type="radio" name="assans${assStatus.count}"  value="D" ${(assVO.assans=='D')?'checked':''}/></td>
												       			<td>(D)</td>
												       			<td><input  type="text" name="assansd${assStatus.count}" value="${assVO.assansd}" class="form-control"/></td>			       			
												       		</tr>  					       		
												       	</table>
												       	<hr/>
											       	</c:forEach>
											       	<input type="submit" value="儲存"/> 
													<input type="hidden" name="couno" value="${couVO.couno}"/>
													<input type="hidden" name="conno" value="${conVO.conno}"/>													
													<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>
													<input type="hidden" name="action"	value="updata_con_and_ass"/>
									    			
								    			</form>								    					
											</div>
										</div>
									</div>
								</div>