<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>


  
<script>      
//載入本機圖片
function preview${conVO.conno}(file) {
    if (file.files && file.files[0]) {
        var reader = new FileReader();
        reader.onload = function (evt) {
            $("#preview${conVO.conno}").html( '<video width="640" controls><source src="' + evt.target.result + '" type="video/mp4"></video>');
        }
        $("#preview${conVO.conno}").show();
        reader.readAsDataURL(file.files[0]);
    }
}
</script>







							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#${conVO.conno}">編輯</button>
								<div class="modal fade customized_modal" id="${conVO.conno}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg">				
										<div class="modal-content" >
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
												<h4 class="modal-title">編輯影片</h4>
											</div>
											<div class="modal-body">
											
												<form method="post" action="<%=request.getContextPath()%>/con/con.do" enctype="multipart/form-data">
										       		<table class="borderless">
										       			<tr>
										       				<th>單元內容編號</th>
										       				<td>${conVO.conno}</td>			       			
										       			</tr>
										       			<tr>
										       				<th>單元內容名稱</th>
										       				<td><input type="text" name="conname" value="${conVO.conname}" class="form-control"/></td>			       			
										       			</tr>
										       			<tr>
										       				<th>影片上傳</th>
										       				<td>
										       					<div id="preview${conVO.conno}" >
											       					<video  width="640" controls >
																		<source src="<%=request.getContextPath()%>/shared/video/${conVO.convideo}" type="video/mp4">
																	</video>
										       					</div>									       					
								       							<input type="file" name="convideo" accept="video/mp4" onchange="preview${conVO.conno}(this)"/>
										       				</td>			       			
										       			</tr>										       					       		
										       		</table>										       		
									       			<input type="submit" value="儲存"/> 
													<input type="hidden" name="couno" value="${couVO.couno}"/>
													<input type="hidden" name="conno" value="${conVO.conno}"/>
													<input type="hidden" name="requestURL" value="<%= request.getServletPath() %>"/>
													<input type="hidden" name="action"	value="update_video"/>
												</form>
												
											</div>
										</div>
									</div>
								</div>