<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.acc.model.*"%> 



<html>
	<head>
		<title>後端帳號_新增</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
	</head>

	<script type="text/javascript" src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			
			document.getElementById("imgacc").addEventListener('change',function(){
		
				var file =this.files[0]; 
				
				var reader = new FileReader();
				 reader.readAsDataURL(file);
				 reader.onload = function(e) {
					 
					
					 document.getElementById("showPic").innerHTML="<img src= '"+this.result+"'' width='200px' height='200px'/>";
				 }
				
			},false);
		});

	</script>

	<body>
		<%@ include file="/shared/pages/back_header_nav.file" %>
		<h2 class="backtitle">新增帳號</h2>
		<ol class="breadcrumb">
		  	<li><a href="<%=request.getContextPath()%>/back/backaccount_management/backaccount_management.jsp">帳號管理</a></li>
		 	<li class="active">新增帳號</li>
		</ol>
		<c:if test="${not empty errorMsgs}">
			<font color='red'>請修正以下錯誤:
				<ul>
					<c:forEach var="message" items="${errorMsgs}">
					<li>${message}</li>
					</c:forEach>
				</ul>
			</font>
		</c:if>
		<form method="post" action="<%=request.getContextPath() %>/back/acc/acc.do" enctype="multipart/form-data">
			<table class="borderless">
				<tr>
					<td>帳號</td>
					<td>
						FTF&nbsp;<input type="text" name="acc" size="36" value="${requestScope.accVO.acc}" id="acc">
					</td>
				</tr>
				<tr>
					<td>姓名</td>
					<td>
						<input type="text" name="accname" size="40" value="${requestScope.accVO.accname}" id="accname">
					</td>
				</tr>	
				<tr>
					<td>簡介</td>
					<td>
						<textarea name="accintro" rows="10" cols="60" id="accintro">${requestScope.accVO.accintro}</textarea>
					</td>
				</tr>
				<tr>
					<td>選擇權限</td>
					<td>
						<select size="1" name="accrole">
								<option value=0 ${(requestScope.accVO.accrole==0)?'selected':''}>管理者</option>
								<option value=1 ${(requestScope.accVO.accrole==1)?'selected':''}>老師</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>圖片</td>     	
				    <td>
				     	<div border="1" width="200" height="200"><span id="showPic"><img src="<%=request.getContextPath() %>/shared/image/noPhoto.jpg"></span></div> 		
				     	<input type="file" id="imgacc" name="accpic" accept="image/*"/>
				    </td>
				</tr>
				<tr>
					<td>電子信箱</td>
					<td><input type="text" name="accemail" size="40" value="${requestScope.accVO.accemail}" id="accemail"></td>
				</tr>
			</table>	
			<input type="hidden" name="action" value="insert"> 
			<input type="submit" value="新增帳號">
			<button type="button" class="btn btn-warning btn-xs" id="magic_button"><span class="glyphicon glyphicon-star"></span></button>	
		</form>
		<script>
			$(function(){							
				$("#magic_button").click(function(){									
					$("#acc").val("ron");
					$("#accname").val("黃小彬");
					$("#accintro").val("英國Essex大學Computer Science 碩士畢業，曾擔任HTC智慧型手機專案經理 ，目前任職大專院校及專業訓練機構之講師，具備Java SCJP 、Java SCWCD 原廠講師級證照，擁有豐富的Android APP實務開發經驗與工具書著作。");
					$("#accemail").val("FTFron@gmail.com");
				});				
			});
		</script>	
		<%@ include file="/shared/pages/back_footer.file" %> 
	</body>
</html>