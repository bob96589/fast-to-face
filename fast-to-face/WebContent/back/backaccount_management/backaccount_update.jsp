<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<%@ page import="com.acc.model.*"%>

<%
	AccVO accVO = (AccVO)request.getAttribute("accVO");
%>


<html>
	<head>
		<title>後端帳號資料修改</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
		
		<!-- self -->
		<script type="text/javascript">
		$(document).ready(function(){
			
			document.getElementById("imgacc").addEventListener('change',function(){
		
				var file =this.files[0]; 
				
				var reader = new FileReader();
				 reader.readAsDataURL(file);
				 reader.onload = function(e) {
					 
					
					 document.getElementById("showPic").innerHTML="<img src= '"+this.result+"''  height='150px'/>";
				 }
				
			},false);
		});
		</script>
	</head>
	<body>
		<%@ include file="/shared/pages/back_header_nav.file" %>
		<h2 class="backtitle">修改帳號資料</h2>
		<ol class="breadcrumb">
		  	<li><a href="<%=request.getContextPath()%>/back/backaccount_management/backaccount_management.jsp">帳號管理</a></li>
		 	<li class="active">修改帳號資料</li>
		</ol>
		<c:if test="${not empty errorMsgs}">
			<font color='red'>請修正以下錯誤:
				<ul>
					<c:forEach var="message" items="${errorMsgs}">
					<li>${message}</li>
					</c:forEach>
				</ul>
			</font>
		</c:if>
		<form method="post" action="<%=request.getContextPath()%>/back/acc/acc.do?accno=${accVO.accno}&action=update"  enctype="multipart/form-data">
			<table class="borderless">
				<tr>
					<td>帳號編號<font color=red><b>*</b></font></td>
					<td><%=accVO.getAccno()%></td>
				</tr>
				<tr>
					<td>帳號<font color=red><b>*</b></font></td>
					<td><%=accVO.getAcc()%></td>
				</tr>
				<tr>
					<td>姓名</td>
					<td>
						<input type="TEXT" name="accname" size="45" value="<%=accVO.getAccname()%>" />
					</td>
				</tr>
				<tr>
					<td>簡介</td>
					<td>
						<textarea name="accintro" rows="10" cols="45"><%=accVO.getAccintro() %></textarea>
					</td>
				</tr>
				<tr>
					<td>圖片</td>
					<td>
						<div>
							<span id="showPic">
								<img src="<%=request.getContextPath()%>/back/acc/acc.do?accno=${accVO.accno}&action=getAccpic"  height="150" />
							</span>
						</div>
						<input type="file" id="imgacc" name="accpic" accept="image/*" />
					</td>
				</tr>
				<tr>
					<td>電子信箱</td>
					<td><input type="text" name="accemail" size="45" value="<%=accVO.getAccemail() %>"></td>
				</tr>	
			</table>
			<input type="hidden" name="action" value="update">
			<input type="hidden" name="accno" value="<%=accVO.getAccno()%>">
			<input type="submit" value="送出修改">		
		</form>
		<%@ include file="/shared/pages/back_footer.file" %>
	</body>
</html>