<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.acc.model.*"%>

<%
		AccService accSvc = new AccService();
		List<AccVO> list = accSvc.getAll();
		pageContext.setAttribute("list",list);
%>

<html>
	<head>
		<title>帳號管理</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
	</head>
	<body>
		<%@ include file="/shared/pages/back_header_nav.file" %>		 
		<h2 class="backtitle">帳號管理</h2>
		<a href="<%=request.getContextPath()%>/back/backaccount_management/backaccount_insert.jsp" class="btn btn-success" style="margin:10px 0px;padding:4px 10px;font-size:18px;">新增帳號</a><br/>  
		<%@ include file="pages/back_account1.file"%>
		<table class="table table-bordered">
			<tr class="titleBgcolor">
				<th class="number_width">帳號編號</th>
				<th class="text_width">帳號</th>
				<th class="text_width">姓名</th>
				<th class="text_width">角色</th>
				<th class="text_width">圖片</th>
				<th>簡介</th>
				<th>修改</th>
			</tr>	
			<c:forEach var="accVO" items="${list}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
				<tr align='center' valign='middle'>
					<td>${accVO.accno}</td>
					<td>${accVO.acc}</td>
					<td>${accVO.accname}</td>
					<td>
						<c:choose>
							<c:when test="${accVO.accrole==0}">
								管理者
							</c:when>
							<c:otherwise>
								老師
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						<img src="<%=request.getContextPath()%>/back/acc/acc.do?accno=${accVO.accno}&action=getAccpic" width="100" height="100" />
					</td> 	
					<td style="text-align:left;">${accVO.accintro}</td>
				    <td>
						<form method="post" action="<%=request.getContextPath()%>/back/acc/acc.do">
							<input type="hidden" name="accno"  value="${accVO.accno}">
							<input type="hidden" name="action" value="getOne_For_Update">				
							<input type="submit" value="修改">
						</form>
					</td>
			</c:forEach>
			
		</table>
		<%@ include file="pages/back_account2.file"%>
		<%@ include file="/shared/pages/back_footer.file" %>
	</body>
</html>