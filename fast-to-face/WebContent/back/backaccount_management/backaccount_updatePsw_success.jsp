<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ page import="com.acc.model.*"%>



<html>
	<head>
		<title>後端帳號密碼修改</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
	</head>
	<body>
		<%@ include file="/shared/pages/back_header_nav.file" %>
		<div align="center" style="margin:50px auto;">
			<table  class="borderless">
				<tr>
					<td align="center">
						<h2 class="backtitle" >密碼修改成功</h2>
					</td>
				</tr>
			</table>
		</div>
		<%@ include file="/shared/pages/back_footer.file" %>
	</body>
</html>