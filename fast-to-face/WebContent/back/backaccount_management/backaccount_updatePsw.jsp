<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ page import="com.acc.model.*"%>



<html>
	<head>
		<title>後端帳號密碼修改</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
	</head>
	<body>
		<%@ include file="/shared/pages/back_header_nav.file" %>

		
	
		<div align="center" style="margin:50px auto;">
			<form method="post" action="<%=request.getContextPath() %>/back/acc/acc.do">
				<table  class="borderless">
					<tr>
						<td colspan="2" style="text-align:center;">
							<h2 class="backtitle" >修改帳號密碼</h2>
						</td>
					</tr>
					<%-- 錯誤表列 --%>
					<c:if test="${not empty errorMsgs}">
						<tr>
							<td colspan="2">
								<font color='red'>請修正以下錯誤:
									<ul>
										<c:forEach var="errorMsgs" items="${errorMsgs}">
											<li>${errorMsgs}</li>
										</c:forEach>
									</ul>
								</font>
							</td>
						</tr>
					</c:if>
					<tr>
						<td>帳號</td>
						<td>${accVO.acc}</td>
					</tr>
					<tr>
						<td>更改密碼</td>
						<td><input type="password" name="accpsw" size="45" /></td>
					</tr>
					<tr>
						<td>確認更改密碼</td>
						<td><input type="password" name="accpsw2" size="45" /></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align:center;">
							<input type="hidden" name="action" value="update_accpsw"> 
							<input type="hidden" name="accno" value="${accVO.accno}"> 
							<input type="submit" class="btn btn-success btn-lg" value="確認">
						</td>
					</tr>
				</table>
			</form>
		</div>
		<%@ include file="/shared/pages/back_footer.file" %>
	</body>
</html>