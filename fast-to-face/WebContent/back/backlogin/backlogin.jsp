<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<title>Fast To Face_後端登入</title>
		
		<!-- back shared file -->
<%-- 		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script> --%>
<%-- 		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		 --%>
<%-- 		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css"> --%>
<%-- 		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css"> --%>
<%-- 		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script> --%>
<%-- 		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css"> --%>
<%-- 		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script> --%>
		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/back/backlogin/assets/css/styles.css" />
		<!-- 	ajax 帳密驗證	 -->
		
		<script src="<%=request.getContextPath()%>/back/backlogin/assets/js/jquery-1.7.2.min.js"></script>
		<script src="<%=request.getContextPath()%>/back/backlogin/assets/js/jquery.complexify.js"></script>
		<script src="<%=request.getContextPath()%>/back/backlogin/assets/js/script.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){	
				$("#password1").blur(function(){
					
					var id =$("#email").val();
					var psw =$("#password1").val();
					
					if(id==""||psw==""){
						alert("帳密不可為空值!!");
						return;
					}
					   $.ajax({
							    type:"post",
							    url:"<%=request.getContextPath()%>/back/acc/acc.do",
							    data:{"action":"login_check","acc":id,"accpsw":psw},
							    dataType:"text",
							    success:function(result){
							    	
							    	if(result=="false")
							    		$("#backmsg").html("無此帳號或密碼錯誤");
							    		
									   else
										   $("#backmsg").html("帳號正確");
							    	
							    	}
					            });		   
							   
						   
					   });
					  });
			</script>

	
	
	</head>
	<body>
		<div>		
			<div id="main">
	        	
	        	<h1>FAST TO FACE_後端登入</h1>
	        	
	        	<form action="<%=request.getContextPath()%>/back/acc/acc.do" METHOD="POST">
					<div id="backmsg"></div>
	        		<div class="row email">
		    			<input type="text" id="email" name="acc" placeholder="Account" size="20"/>
<!-- 	        			<div id="backaccmsg"></div> -->
	        		</div>
	        		
	        		<div class="row pass">
	        			<input type="password" id="password1" name="accpsw" placeholder="Password" />
<!-- 	        			<div id="backaccpswmsg"></div> -->
	        		</div>
	        		
	        		
	        		<!-- The rotating arrow -->
	        		<div class="arrowCap"></div>
	        		<div class="arrow"></div>
	        		
	        		<p class="meterText">密碼複雜度</p>
	        		<input type="hidden" name="action" value="login_back"/>
	        		<input type="submit" value="登入" />
	        		
	        	</form>
	       </div>
		</div>
	</body>
</html>