<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ page import="com.acc.model.*"%>



<html>
	<head>
		<title>後端帳號密碼修改</title>
		
		<!-- back shared file -->
		<script src="<%=request.getContextPath()%>/shared/js/jquery-1.11.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/shared/js/jquery-ui.min.js"></script>		
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap.min.css">
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/bootstrap-theme.min.css">
		<script src="<%=request.getContextPath()%>/shared/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<%=request.getContextPath()%>/shared/css/back.css">
		<script src="<%=request.getContextPath()%>/shared/js/back.js"></script>
	</head>
	<body>
		<%@ include file="/shared/pages/back_header_nav_without_slidebar.file" %>
			<div align="center" style="margin:120px;">
				<h2 class="backtitle" align="center">登出成功</h2>
				<div class="center_decs" align="center">本頁面過5秒後，將自動為您轉至首登入頁面。</div>
				<div class="center_decs"  align="center">
					您可以直接前往
					<a href="<%=request.getContextPath()%>/back/backlogin/backlogin.jsp">重新登入</a>。
				</div>
			</div>
			<% response.setHeader("Refresh", "5;URL=" + request.getContextPath() + "/back/backlogin/backlogin.jsp"); %>
		<%@ include file="/shared/pages/back_footer_without_slidebar.file" %>
	</body>
</html>