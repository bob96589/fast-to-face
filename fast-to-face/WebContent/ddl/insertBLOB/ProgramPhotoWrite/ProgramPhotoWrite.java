import java.sql.*;
import java.io.*;

class ProgramPhotoWrite {

        static {
            try {
                 Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
            } catch (Exception e) {
                 e.printStackTrace();
            }
        }

        public static void main(String argv[]) {
              Connection con = null;
              PreparedStatement pstmt = null;
              String url = "jdbc:oracle:thin:@localhost:1521:xe";
              String userid = "user1";
              String passwd = "u111";
              int start = 20001;
              int end = 20003;
              String updateString = "update program set propic = ? where prono = ?";
              
	        
              try {
                con = DriverManager.getConnection(url, userid, passwd);
               
                
             		for(int i = start; i <= end; i++){
		                String picName = i + ".png";
		                File pic = new File(picName);
		                long flen = pic.length();
		                InputStream fin = new FileInputStream(pic);  
		
		                System.out.println("\n\nUpdate the database... ");
		                pstmt = con.prepareStatement(updateString);
		                pstmt.setBinaryStream(1, fin, (int)flen); //void pstmt.setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException
		                pstmt.setInt(2, i);
		                int rowsUpdated = pstmt.executeUpdate();
					
		                System.out.print("Changed " + rowsUpdated);
		
		                if (1 == rowsUpdated)
		                    System.out.println(" row.");
		                else
		                    System.out.println(" rows.");
		
		                fin.close();
		                pstmt.close();
		            }  

              } catch (Exception e) {
                    e.printStackTrace();
              } finally {
                    try {
                      con.close();
                    } catch (SQLException e) {
                    }
             }
      }
}